﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using TMLTH_TOUCHPOINT_API_SERVER.Class;

namespace TMLTH_TOUCHPOINT_CMS.Class
{
    public class ContentTools
    {
        public static string ImageToString(Image image)
        {
            if (image == null)
                return String.Empty;

            var stream = new MemoryStream();
            image.Save(stream, image.RawFormat);
            var bytes = stream.ToArray();

            return Convert.ToBase64String(bytes);
        }
        public static Image StringtoImage(string image)
        {
            if (String.IsNullOrWhiteSpace(image))
            {
                return null;
            }
            var bytes = Convert.FromBase64String(image);
            var stream = new MemoryStream(bytes);
            return Image.FromStream(stream);
        }
        internal static bool saveHtml(string data, string path, string filename)
        {
            if (File.Exists(path + filename + ".htm"))
            {
                File.Delete(path + filename + ".htm");
            }

            File.WriteAllText(path + filename + ".htm", data);

            return true;
        }

        internal static bool updateSelectOption(string data, string filename)
        {
            //File.AppendAllText(DataFactory.CMS_FOLDER_SAVE_CONTENT_FILE_URL + "part\\" + filename + ".cshtml", data);
            //File.AppendAllText(DataFactory.MC_FOLDER_SAVE_CONTENT_FILE_URL + "part\\" + filename + ".cshtml", data);
            //File.AppendAllText(DataFactory.ECOM_FOLDER_SAVE_ADDITION_FILE_URL + "content\\assets\\part\\" + filename + ".cshtml", data);

            return true;
        }

        // save category's icon
        internal static bool saveCategoryIconImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".png"))
            {
                System.IO.File.Delete(path + filename + ".png");
            }

            Image squareImage = ResizeImage(image, new Size(40, 40));
            Bitmap img_cover = new Bitmap(squareImage, new Size(40, 40));// Scale image to 40x40
            img_cover.Save(path + filename + ".png", System.Drawing.Imaging.ImageFormat.Png);

            return true;
        }

        // save category's bg
        internal static bool saveCategoryBgImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".png"))
            {
                System.IO.File.Delete(path + filename + ".png");
            }

            Image squareImage = ResizeImage(image, new Size(144, 107));
            Bitmap img_cover = new Bitmap(squareImage, new Size(144, 107));// Scale image to 116x110
            img_cover.Save(path + filename + ".png", System.Drawing.Imaging.ImageFormat.Png);

            return true;
        }

        // save category's food
        internal static bool saveCategoryMobFoodImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".jpg"))
            {
                System.IO.File.Delete(path + filename + ".jpg");
            }

            Image squareImage = ResizeImage(image, new Size(1200, 600));
            Bitmap img_cover = new Bitmap(squareImage, new Size(1200, 600));// Scale image to 128x128
            img_cover.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        // save category's food
        internal static bool saveCategoryMobItemImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".jpg"))
            {
                System.IO.File.Delete(path + filename + ".jpg");
            }

            Image squareImage = ResizeImage(image, new Size(600, 600));
            Bitmap img_cover = new Bitmap(squareImage, new Size(600, 600));// Scale image to 128x128
            img_cover.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        // save cover's content
        internal static bool saveContentImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".jpg"))
            {
                System.IO.File.Delete(path + filename + ".jpg");
            }

            Image squareImage = ResizeImage(image, new Size(1000, 1000));
            Bitmap img_cover = new Bitmap(squareImage, new Size(1000, 1000));// Scale image to 128x128
            img_cover.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        // save upload's content
        internal static bool saveContentUploadImage(Image image, string path, string filename)
        {
            if (File.Exists(path + filename + ".jpg"))
            {
                File.Delete(path + filename + ".jpg");
            }

            if (image.Width > 1000)
            {
                Size newSize = new Size(image.Width, image.Height);
                newSize.Width = 1000;
                newSize.Height = (int)(1000 * ((double)image.Height / image.Width));

                Image i1000 = ResizeImage(image, newSize);
                Bitmap b1000 = new Bitmap(i1000, newSize); // Scale image to 2000xheight
                b1000.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else
            {
                Bitmap img_content = new Bitmap(image, new Size(image.Width, image.Height));
                img_content.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            return true;
        }

        public static string RemoveLineEndings(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return value;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            return value.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ").Replace(lineSeparator, " ").Replace(paragraphSeparator, " ");
        }

        // Crop in center
        internal static Image ResizeImage(Image imgToResize, Size destinationSize)
        {
            var originalWidth = imgToResize.Width;
            var originalHeight = imgToResize.Height;

            //how many units are there to make the original length
            var hRatio = (float)originalHeight / destinationSize.Height;
            var wRatio = (float)originalWidth / destinationSize.Width;

            //get the shorter side
            var ratio = Math.Min(hRatio, wRatio);

            var hScale = Convert.ToInt32(destinationSize.Height * ratio);
            var wScale = Convert.ToInt32(destinationSize.Width * ratio);

            //start cropping from the center
            var startX = (originalWidth - wScale) / 2;
            var startY = (originalHeight - hScale) / 2;

            //crop the image from the specified location and size
            var sourceRectangle = new Rectangle(startX, startY, wScale, hScale);

            //the future size of the image
            var bitmap = new Bitmap(destinationSize.Width, destinationSize.Height);

            //fi ll-in the whole bitmap
            var destinationRectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            //generate the new image
            using (var g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(imgToResize, destinationRectangle, sourceRectangle, GraphicsUnit.Pixel);
            }

            return bitmap;

        }
        internal static bool saveContentUploadImageCrop(Image image, int maxWidth, int maxHeight, string path, string filename)
        {
            if (File.Exists(path + filename + ".jpg"))
            {
                File.Delete(path + filename + ".jpg");
            }

            int left = 0;
            int top = 0;
            int srcWidth = maxWidth;
            int srcHeight = maxHeight;
            Bitmap bitmap = new Bitmap(maxWidth, maxHeight);
            double croppedHeightToWidth = (double)maxHeight / maxWidth;
            double croppedWidthToHeight = (double)maxWidth / maxHeight;

            if (image.Width > image.Height)
            {
                srcWidth = (int)(Math.Round(image.Height * croppedWidthToHeight));
                if (srcWidth < image.Width)
                {
                    srcHeight = image.Height;
                    left = (image.Width - srcWidth) / 2;
                }
                else
                {
                    srcHeight = (int)Math.Round(image.Height * ((double)image.Width / srcWidth));
                    srcWidth = image.Width;
                    top = (image.Height - srcHeight) / 2;
                }
            }
            else
            {
                srcHeight = (int)(Math.Round(image.Width * croppedHeightToWidth));
                if (srcHeight < image.Height)
                {
                    srcWidth = image.Width;
                    top = (image.Height - srcHeight) / 2;
                }
                else
                {
                    srcWidth = (int)Math.Round(image.Width * ((double)image.Height / srcHeight));
                    srcHeight = image.Height;
                    left = (image.Width - srcWidth) / 2;
                }
            }
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(image, new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                new Rectangle(left, top, srcWidth, srcHeight), GraphicsUnit.Pixel);
            }

            bitmap.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            /*Rectangle rectangleCrop = new Rectangle(0, 0, 256, 256);
            //the future size of the image
            Bitmap bitmap = new Bitmap(256, 256);
            //generate the new image
            using (var g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(image, rectangleCrop, rectangleCrop, GraphicsUnit.Pixel);
            }*/

            bitmap.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            return true;
        }
        internal static bool ErrorControllerLog (string value)
        {
            DateTime now = DateTime.Now;
            string date = now.ToString(DataFactory.DATE_FORMAT_FROM_DATABASE, DataFactory.CULTURE_ENG);
            string folder = DataFactory.FOLDER_TOUCHPOINT_TEXT;
            string namefile = folder + date;
            
            try
            {
                if (Directory.Exists(folder) == false)
                {
                    new FileInfo(folder).Directory.Create();
                }
                int countfile = Directory.GetFiles(folder, "*", SearchOption.AllDirectories).Length;
                string[] allfile = Directory.GetFiles(folder, "*");
                DateTime DateMin = DateTime.Now;
              
                if (countfile > 10)
                {
                    string pathfiledelete = "";
                    foreach (var file in allfile)
                    {
                        string getfilename = Path.GetFileName(file).Split('.')[0].ToString();
                        DateTime checkfilename = DateTime.ParseExact(getfilename, "yyyyMMdd", DataFactory.CULTURE_ENG);

                        if (checkfilename < DateMin)
                        {
                            DateMin = checkfilename;
                            pathfiledelete = file;
                        }
                    }
                    File.Delete(pathfiledelete);
                }
                File.AppendAllText(namefile + ".txt",now.ToString()+"||"+value + Environment.NewLine);
            }
            catch(Exception e)
            {
                ContentTools.ErrorControllerLog("ERROR:" + e.Message);
                return false;
            }
            
            return true;
        }
        public static string getdatetimeleft (DateTime basetime)
        {
            var now = DateTime.Now;


            var date = now - basetime;
            int dd = Convert.ToInt32(date.Days);
            int hh = Convert.ToInt32(date.Hours);
            int mm = Convert.ToInt32(date.Minutes);
            int ss = Convert.ToInt32(date.Seconds);
            string updatedate = "";
            if (dd > 0)
            {
                updatedate = basetime.ToString(DataFactory.DATE_FORMAT_SHORT_YEAR, DataFactory.CULTURE_INFO);
            }
            else if (hh > 0)
            {
                updatedate = hh.ToString() + " hours";
            }
            else if (mm > 0)
            {
                updatedate = mm.ToString() + " mins";
            }
            else if (ss > 0)
            {
                updatedate = " Just now";
            }
            else
            {
                updatedate = basetime.ToString(DataFactory.DATE_FORMAT_SHORT_YEAR, DataFactory.CULTURE_INFO);
            }
            return updatedate;
        }
    }

}