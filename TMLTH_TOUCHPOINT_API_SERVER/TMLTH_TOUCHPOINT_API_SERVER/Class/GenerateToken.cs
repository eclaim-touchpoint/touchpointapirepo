﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    public class GenerateToken
    {
        internal static TokenResult validateSessionToken(string token)
        {
            TokenResult tokenResult = new TokenResult();
            if (!string.IsNullOrWhiteSpace(token))
            {
                try {

                    #region readtoken
                    var readtoken = token;
                    var handler = new JwtSecurityTokenHandler();
                    var jsonToken = handler.ReadToken(readtoken);
                    var tokenS = handler.ReadToken(readtoken) as JwtSecurityToken;
                    var userid = tokenS.Claims.First(Claim => Claim.Type == "AccountID").Value;
                    var username = tokenS.Claims.First(Claim => Claim.Type == "AccountUsername").Value;
                    var name = tokenS.Claims.First(Claim => Claim.Type == "AccountName").Value;
                    var isapp = tokenS.Claims.First(claim => claim.Type == "Isapp").Value;
                    var ip_client = tokenS.Claims.First(Claim => Claim.Type == "IPClient").Value;
                    var Dayofyear = tokenS.Claims.First(Claim => Claim.Type == "DayofYear").Value;
                    var RoleID = tokenS.Claims.First(Claim => Claim.Type == "RoleId").Value;
                    var Session = tokenS.Claims.First(Claim => Claim.Type == "Session").Value;
                    var exp = tokenS.Claims.First(Claim => Claim.Type == "exp").Value;
                    #endregion  
                    
                    #region Check Session
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.UTF8.GetBytes(DataFactory.SECURITY_KEY);
                    DateTime now = DateTime.Now;
                    var Checksession = Convert.ToDateTime(Session);

                    if (Checksession > now)
                    {
                        #region UPDATE SESSTION             
                        var utc = Convert.ToDouble(exp);
                        var newSession = DateTime.Now.AddMinutes(15).ToString();
                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(new Claim[]
                            {
                                    new Claim("AccountID", userid),
                                    new Claim("AccountUsername", username),
                                    new Claim("AccountName", name),
                                    new Claim("IPClient",   ip_client),
                                    new Claim("Isapp",  isapp),
                                    new Claim("DayofYear", Dayofyear),
                                    new Claim("RoleId",RoleID),
                                    new Claim("Session",newSession)
                            }),
                            Expires = DateTime.Now.AddHours(4),
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        };

                        var createtoken = tokenHandler.CreateToken(tokenDescriptor);
                        var newtoken = tokenHandler.WriteToken(createtoken);

                        tokenResult.RESULT = ResultData.CODE_COOKIE_RESULT_ISVALID;
                        tokenResult.TOKEN = newtoken;
                        return tokenResult;
                        #endregion
                    }
                    else
                    {
                        tokenResult.RESULT = ResultData.CODE_COOKIE_RESULT_ERROR;
                        return tokenResult;
                    }
                    #endregion
                }
                catch (Exception e)
                {
                 tokenResult.RESULT = ResultData.CODE_COOKIE_RESULT_ERROR;
                 ContentTools.ErrorControllerLog("ERROR:" + e.Message);
                 return tokenResult;
                }
            }
            tokenResult.RESULT = ResultData.CODE_COOKIE_RESULT_ERROR;
            return tokenResult;
        }
    }
}