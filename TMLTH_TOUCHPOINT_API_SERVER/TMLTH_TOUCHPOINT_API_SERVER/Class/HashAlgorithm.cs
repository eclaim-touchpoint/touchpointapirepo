﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    public class GenerateSHA256
    {
       
        internal static string hashsha256(string value)
        {
          
            var _sha256 = SHA256.Create();
            var inputBytes = Encoding.UTF8.GetBytes(value);
            var hash = _sha256.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
       
    }
   

}