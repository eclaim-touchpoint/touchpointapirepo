﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Globalization;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    public class DataFactory
    {

        // ========= Signature =========
        internal static readonly string SECURITY_KEY = "f6f4b*9BWk3r6N92zU5C";

        //internal static readonly bool DEBUG_MODE = true;
        internal static readonly bool IS_PRODUCTION = ConfigurationManager.AppSettings["IS_PRODUCTION"] == "1" ? true : false;
        internal static readonly float SERVER_API_VERSION = 0.01f;
        internal static readonly string SERVER_API_NAME = "0.01";
        internal static readonly string SERVER_API_DATE = "6 Jan 2019";
        internal static readonly string DATABASE_VERSION = "0.01";

        // ====== ENCRYPTION KEY ======
        internal static readonly string ENC_TOUCHPOINT_KEY = "P$AN^;O4g831pAs";
        internal static readonly string ENC_SIGNIN_KEY = "$OB9(Z&[?gvVU?m";
        internal static readonly string ENC_SECRET_SIGNIN_KEY = "#=jS@K!PC|x6$lp";
        internal static readonly string ENC_PASSWORD_KEY = "Rpc#Ny|{PL{,qN:";

        // ========== OTP KEY (TOKIO MARINE's OTP WEB SERVICE) ============
        internal static readonly string OTP_KEY_REGISTRATION = "0CE6B1B5-F7E3-4319-8099-B8157C4A32D8";
        internal static readonly string OTP_KEY_FORGOT_PASSWORD = "CCE389D1-4CB0-451D-897C-F9416C1A39FF";
        internal static readonly string OTP_KEY_FORGOT_PIN = "9010326B-B80E-4B29-82EF-7848F553CE27	";

        //=========== Date FORMAT ===================
        internal static readonly string DATE_FORMAT_FROM_DATABASE = "yyyyMMdd";
        internal static readonly string DATE_FORMAT = "dd MMM yyyy";
        internal static readonly string DATE_FORMAT_IMAGE = "yyyyMMddHHmmss";
        internal static readonly string DATE_FORMAT_FULL_MONTH = "dd MMMM yyyy";
        internal static readonly string DATE_FORMAT_MONTH_NUMBER = "dd/MM/yyyy";
        internal static readonly string DATE_FORMAT_SHORT_MONTH = "dd MM yyyy"; 
        internal static readonly string DATE_FORMAT_MONTH_TIME24 = "dd MMMM yyyy - HH:mm";
        internal static readonly string DATE_FORMAT_SHORT_YEAR = "dd MMM yy";
        internal static readonly CultureInfo CULTURE_INFO = new CultureInfo("th-TH");
        internal static readonly CultureInfo CULTURE_ENG = new CultureInfo("en-US");

        //======================= FTP SERVER ===========================
        public static readonly string FTP_SEVER_NAME = ConfigurationManager.AppSettings["FTP_SEVER_NAME"];
        public static readonly string FTP_USER = ConfigurationManager.AppSettings["FTP_USER"];
        public static readonly string FTP_PASSWORD = ConfigurationManager.AppSettings["FTP_PASSWORD"];
        public static readonly string FTP_PATH_CER = ConfigurationManager.AppSettings["FTP_PATH_CER"];

        //================= Firebase URL===========================
        public static readonly string FIREBASE_URL = ConfigurationManager.AppSettings["FIREBASE_URL"];
        public static readonly string FIREBASE_MESSAGE = "Message/";
        public static readonly string FIREBASE_MESSAGES_KEY = ConfigurationManager.AppSettings["FIREBASE_MESSAGES_KEY"];
        public static readonly string FIREBASE_SENDER = ConfigurationManager.AppSettings["FIREBASE_SENDER"];

        //===================== PATH =============================
        public static readonly string FOLDER_TOUCHPOINT = ConfigurationManager.AppSettings["TMLTH_TOUCHPOINT_CMS_PATH"];
        public static readonly string URL_TOUCHPOINT_CONTENT = FOLDER_TOUCHPOINT + "content/";
        public static readonly string URL_TOUCHPOINT_CONTENT_UPLOAD = FOLDER_TOUCHPOINT + "upload/";
        public static readonly string FOLDER_TOUCHPOINT_ERROR_LOG = ConfigurationManager.AppSettings["TMLTH_TOUCHPOINT_API_PATH"];


        public static readonly string URL_TOUCHPOINT_EWI = ConfigurationManager.AppSettings["TMLTH_TOUCHPOINT_EWI_URL"];

        public static readonly string FOLDER_TOUCHPOINT_CONTENT_UPLOAD = FOLDER_TOUCHPOINT + "content\\upload\\";
        public static readonly string FOLDER_TOUCHPOINT_TEXT = FOLDER_TOUCHPOINT_ERROR_LOG + "Errorlog\\";
        //=======================DOCTYPE============================
        public static readonly string DOCUMENT_TYPE_PAYMENT_CERTIFICATE = ConfigurationManager.AppSettings["DOCUMENT_TYPE_PAYMENT_CERTIFICATE"];
        public static readonly string DOCUMENT_TYPE_PAYMENT_RECEIPT_DOCUMENT_TYPE = ConfigurationManager.AppSettings["DOCUMENT_TYPE_PAYMENT_RECEIPT_DOCUMENT_TYPE"];
        public static readonly string DOCUMENT_TYPE_PAYMENT_PERIOD = ConfigurationManager.AppSettings["DOCUMENT_TYPE_PAYMENT_PERIOD"];
        
        //=================CHECK TEST================================
        public static readonly string IS_TOKEN_DEBUG = ConfigurationManager.AppSettings["IS_TOKEN_DEBUG"];

        internal const int REF_CONTENT_TYPE_NEWS = 1;
        internal const int REF_CONTENT_TYPE_HEALTHTIPS = 2;
        internal const int REF_CONTENT_TYPE_DOCUMENT = 3;
        internal const int REF_CONTENT_TYPE_TUTORIAL = 4;

        #region CONTENT_TYPE
        public const int CONTENT_TYPE_NEWS = 1;
        public const int CONTENT_TYPE_HEALTHTIPS = 2;
        public const int CONTENT_TYPE_DOCUMENT = 3;
        public const int CONTENT_TYPE_TUTORIAL = 4;
        #endregion

        public const string LOAN_TYPE_AUTO = "A";
        public const string LOAN_TYPE_PAYMENT = "P";

        public const string PAYMENT_OBJECTIVE_TYPE_NORMAL = "PRM"; //ชำระเบี้ย
        public const string PAYMENT_OBJECTIVE_TYPE_APL_FULL = "APL_FULL"; //เงินกู้อัตโนมัติ เต็ม
        public const string PAYMENT_OBJECTIVE_TYPE_APL_PAR = "APL_PAR"; //เงินกู้อัตโนมัติ เลือกจ่าย
        public const string PAYMENT_OBJECTIVE_TYPE_RPL_FULL = "RPL_FULL"; //เงินกู้กรมธรรม์ เต็ม
        public const string PAYMENT_OBJECTIVE_TYPE_RPL_PAR = "RPL_PAR"; //เงินกู้กรมธรรม์ เลือกจ่าย

        public static readonly string LOAN_ACCESS_KEY = "c768a396-dd8f-4dc0-9fa8-6da1dc787aa7";
        public static readonly string LOAN_PROFILE_ID = "TML00004";

        internal static DateTime FromUnixTime(double unixTimeStamp)
        {
            DateTime datetime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            datetime = datetime.AddSeconds(unixTimeStamp).ToLocalTime();
            return datetime;
        }

        internal static long ToUnixTime(DateTime date)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        
    }
}