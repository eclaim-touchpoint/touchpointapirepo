﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    internal static class EncryptDecryptAlgorithm
    {
        internal static string EncryptData(string sdata, string key)
        {
            StringBuilder sb = new StringBuilder();
            byte[] bdata = Encoding.UTF8.GetBytes(sdata);
            int nLen = bdata.Length;
            int nKeyLen = key.Length;

            for (int i = 0; i < nLen; i++)
            {
                sb.Append(ToHex( ((char)(bdata[i] ^ key[i % nKeyLen])).ToString()  ));
            }
            return sb.ToString();
        }

        private static string ToHex(string data)
        {
            StringBuilder sb = new StringBuilder();
            int nCh = (int)(data[0]); 
            sb.Append((char)(((nCh >> 4) & 15) + 0x41));
            sb.Append((char)((nCh & 15) + 0x41));
            return sb.ToString();
        }

	    internal static string DecryptData(string data, string key) 
        {
            try
            {
                int nLen = data.Length;
                int nKeyLen = key.Length;
                byte[] bdata = new byte[nLen / 2];
                int nCount = 0;
                for (int i = 0; i < nLen; i += 2)
                {
                    byte byteData = ToByte("" + data[i] + data[i + 1]);
                    bdata[nCount] = (byte)(byteData ^ key[nCount % nKeyLen]);
                    nCount++;
                }
                return Encoding.UTF8.GetString(bdata);
            }
            catch (Exception )
            {
                return null;
            }
        }

        private static byte ToByte(String data)
        {
            return (byte)((((int)data[0] - 0x41) << 4) | ((int)data[1] - 0x41));
        }
    }
}