﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    internal class TokenResult
    {
        public int RESULT { get; set; }
        public string TOKEN { get; set; }
    }
    internal class CookieResult
    {
        public int IS_BLANK { get; set; } // when mobile load, some part does not have cookie
        public int RESULT { get; set; }
        public int ID { get; set; }
        public int ROLE_ID { get; set; }
        public string USERNAME { get; set; }
        public string NAME { get; set; }
        public string NEW_TOKEN { get; set; }
        public string IP { get; set; }
        public int IS_APP { get; set; }
        public int IS_ACTIVE { get; set; }
    }

    internal class ResultData
    {
        //*** cookie token
        internal const int CODE_COOKIE_RESULT_ERROR = 0;
        internal const int CODE_COOKIE_RESULT_TOKEN_DENIED = -1;
        internal const int CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR = -500;
        internal const int CODE_COOKIE_RESULT_IP_NOTSAME_ERROR = -600;

        internal const int CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR = -999;
        internal const int CODE_COOKIE_RESULT_ISVALID = 1;

        //*** key attributes
        internal static readonly string CODE_SUCCESS = "SUCCESS";
        internal static readonly string CODE_COMPLETE = "COMPLETE";
        internal static readonly string CODE_BLOCKED = "BLOCK:";
        internal static readonly string CODE_CANT_PAYMENT = "CANT_PAYMENT:";
        internal static readonly string CODE_USER_INFO = "USERINFO;";
        internal static readonly string CODE_ERROR = "ERROR:";
        internal static readonly string CODE_RESULTID = "RESULTID:";
        internal static readonly string CODE_OTPCOUNT = "COUNT:";
        internal static readonly string CODE_DUPLICATE = "DUPLICATE:";
        internal static readonly string CODE_LOGOUT = "LOGOUT:";
        internal static readonly string CODE_BANNED = "BANNED:";
        internal static readonly string CODE_REJECT = "REJECT:";
        internal static readonly string CODE_RETRY = "RETRY:";
        internal static readonly string CODE_IS_REQUIRE = "ISREQUIRE:";
        internal static readonly string CODE_REQUIRE_EMAIL = "REQUIRE EMAIL";
        internal static readonly string CODE_NOTFOUND = "NOTFOUND:";
        internal static readonly string CODE_MODIFYDATE = "MODIFYDATE:";
        internal static readonly string CODE_PERMISSION = "PERMISSION:";
        internal static readonly string CODE_DENIED = "DENIED";
        internal static readonly string CODE_SESSION_TIMEOUT = "SESSION_TIMEOUT:";
        internal static readonly string CODE_TOKEN_DENIED = "TOKEN_DENIED:";
        internal static readonly string CODE_TOKEN_EXPIRE = "TOKEN_EXPIRE:";
        internal static readonly string CODE_IP_CHANGE = "IP_CHANGE:";
        internal static readonly string CODE_MAX_FILE_LENGTH = "MAX_FILE_LENGTH:";

        //*** error pre-code
        internal static readonly string CODE_ERROR_TOUCHPOINT = "1-100-00";
        internal static readonly string CODE_ERROR_SIGNIN = "1-000-00";
        internal static readonly string CODE_ERROR_REGISTER = "1-000-01";
        internal static readonly string CODE_ERROR_ACCOUNT = "1-099-00";
        internal static readonly string CODE_ERROR_HASHPASSWORD = "1-000-99";
        internal static readonly string CODE_ERROR_OTP = "1-200-01";
        internal static readonly string CODE_ERROR_MESSAGE = "1-300-01";

        //*** error post-code
        internal static readonly string CODE_ERROR_COOKIE_CRASH = "201";
        internal static readonly string CODE_ERROR_FORMAT_MISSING = "401";
        internal static readonly string CODE_ERROR_GET_METHOD = "402";
        internal static readonly string CODE_ERROR_DECRYPT_DATA = "403";
        internal static readonly string CODE_ERROR_ACCOUNT_DATE = "404";
        internal static readonly string CODE_ERROR_ACCOUNT_SUSPEND = "405";
        internal static readonly string CODE_ERROR_PROJECT_SUSPEND = "406";
        internal static readonly string CODE_ERROR_DATA_NOT_FOUND = "407";
        internal static readonly string CODE_ERROR_NO_PERMISSION = "408";
        internal static readonly string CODE_ERROR_CANT_CREATE_FOLDER = "4081";
        internal static readonly string CODE_ERROR_CANT_ADD_CERTIFICATE = "4082";
        internal static readonly string CODE_ERROR_CANT_DOWLOAD = "4083";
        internal static readonly string CODE_ERROR_CANT_CREATE_FILE_FROM_BYTE = "4084";
        internal static readonly string CODE_ERROR_CANT_CONVERT_FILE_TO_BASE64 = "4085";
        internal static readonly string CODE_ERROR_CONNECT_FTP_FAIL = "4086";
        internal static readonly string CODE_ERROR_NO_INSERT = "409";
        internal static readonly string CODE_ERROR_DATA_INCOMPATIBLE = "496";
        internal static readonly string CODE_ERROR_JSON_WRONG_FORMAT = "497";
        internal static readonly string CODE_ERROR_DATA_WRONG = "498";
        internal static readonly string CODE_ERROR_SEEM_TO_HACK = "499";
        internal static readonly string CODE_ERROR_SQL_REJECT = "701";
        internal static readonly string CODE_ERROR_SQL_FORMAT = "702";
        internal static readonly string CODE_ERROR_SQL_CORRUPT = "703";
        internal static readonly string CODE_ERROR_SQL_NORESULT = "704";
        internal static readonly string CODE_ERROR_SQL_DUPLICATERESULT = "705";
        internal static readonly string CODE_ERROR_SQL_INSERT = "706";
        internal static readonly string CODE_ERROR_FIREBASE_PUSH_MESSAGE = "801";

        internal static readonly string CODE_ERROR_OTHER = "901";
    }
}