﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebrequestForgotPassword
    {
        public string idcard { get; set; }
        public string email { get; set; }
        public string mobile_no { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }

    public class WebResponseForgotPassword
    {
        public string idcard { get; set; }
        public string clntnum { get; set; }
        public string chdrnum { get; set; }
        public string userid { get; set; }
        public string name_ins { get; set; }
        public string lastname_ins { get; set; }
        public string mobile_no { get; set; }
        public string email { get; set; }
        public string agreement { get; set; }
        public DateTime upddate { get; set; }
        public DateTime register_dt { get; set; }
        public string phone { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }
}