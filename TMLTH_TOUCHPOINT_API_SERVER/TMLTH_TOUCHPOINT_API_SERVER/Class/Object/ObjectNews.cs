﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestNews
    {
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseNews
    {   
        public decimal id { get; set; }
        public string title { get; set; }
        public string detail { get; set; }
        public int? display_order { get; set; }
        public string application { get; set; }
        public string picture_link { get; set; }
        public string cover_picture_link { get; set; }
    }
   
}