﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestUpdateInfoOTP
    {
        public string idcard { get; set; }
        public string userid { get; set; }
        public string clntnum { get; set; }
        public int forgot_type { get; set; }
        public string mobile { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebSubmitOTPValidate
    {
        public string idcard { get; set; }
        public string mobile { get; set; }
        public string userid { get; set; }
        public string clntnum { get; set; }
        public int forgot_type { get; set; }
        public string otp { get; set; }
        public string refotp { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebSubmitChangePassword
    {
        public string idcard { get; set; }
        public string clntnum { get; set; }
        public string userid { get; set; }
        public string password { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebSubmitChangeEmail
    {
        public string idcard { get; set; }
        public string userid { get; set; }
        public string clntnum { get; set; }
        public string email { get; set; }
        public string admin { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseUpdateInfoOTP
    {
        public string strResult { get; set; }
    }


}