﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestAppVersion
    {
        //public int id;
        public string ip_client { get; set; }
        //public DateTime start_date { get; set; }
        //public DateTime end_date { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebSubmitAppVersion
    {
        public int id { get; set; }
        public string android_name { get; set; }
        public int is_edit { get; set; }
        public int android_code { get; set; }
        public string ios_name { get; set; }
        public int ios_code { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
}