﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestFamily
    {
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseFamily
    {
      
        public int id { get; set; }
        public string name_th { get; set; }
        public string name_en { get; set; }
        public string name_jp { get; set; }
        public int? name_order { get; set; }
        public bool? name_stat { get; set; }
    }
}