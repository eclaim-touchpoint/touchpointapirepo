﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebSubmitAgreement
    {
        public int id { get; set; }
        public string detail { get; set; }
        public int active { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestAgreement
    {
        public int id { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebSubmitAgreementLog
    {
        public int id { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestAgreementDataTable
    {
        public int id { get; set; }
        public int no { get; set; }
        public int active { get; set; }
        public decimal version { get; set; }
        internal DateTime? start_date_raw { get; set; }
        internal DateTime? end_date_raw { get; set; }
        public string start_date { get { return start_date_raw != null ? start_date_raw.Value.ToString(DataFactory.DATE_FORMAT) : "-"; } }
        public string end_date { get { return end_date_raw != null ? end_date_raw.Value.ToString(DataFactory.DATE_FORMAT) : "-"; } }
    }
}