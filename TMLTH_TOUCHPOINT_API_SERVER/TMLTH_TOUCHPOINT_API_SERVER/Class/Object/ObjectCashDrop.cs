﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestCashDrop
    {
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseCashDrop
    {
        public string policy_no { get; set; }
        internal decimal? due_dateRaw { get; set; }
        public string due_date { get { return due_dateRaw == null ? "" : DateTime.ParseExact(due_dateRaw.Value.ToString(), DataFactory.DATE_FORMAT_FROM_DATABASE, CultureInfo.InvariantCulture).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); }  }
        internal decimal? pay_dateRaw { get; set; }
        public string pay_date { get { return pay_dateRaw == null ? "" : DateTime.ParseExact(pay_dateRaw.Value.ToString(), DataFactory.DATE_FORMAT_FROM_DATABASE, CultureInfo.InvariantCulture).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public decimal? amount { get; set; }
        public decimal? loan_amount { get; set; }
        public decimal? paid_amount { get; set; }
        public decimal? total_amount { get; set; }
        public string pay_method { get; set; }
        public string cheq_no { get; set; }
        public string bank_name { get; set; }
        public string bank_acckey { get; set; }
    }
    public class WebResponseCashDropNext
    {
        public string policy_no { get; set; }
        public decimal? due_dateRaw { get; set; }
        public string due_date { get { return due_dateRaw == null ? "" : DateTime.ParseExact(due_dateRaw.Value.ToString(), DataFactory.DATE_FORMAT_FROM_DATABASE, CultureInfo.InvariantCulture).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public string pay_type { get; set; }
        public string bank_name { get; set; }
        public string bank_acckey { get; set; }
    }
}