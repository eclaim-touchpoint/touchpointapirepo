﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
	
    public class WebRequestSearchContent
    {
        public string admin { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestContent
    {
        public int id { get; set; }
        public int category { get; set; }
        public int ischeckcookie { get; set; }
        public string admin { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestContentNotification
    {
        public int is_premium { get; set; }   
        public int is_check_cookie { get; set; }
        public string admin { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get;  set; }
    }
    public class WebResponseContentNotification
    {
        public int id { get; set; }
        public int content_type_id { get; set; }
        public string title { get; set; }
        public string image_url { get; set; }
        internal DateTime start_dateRaw { get; set; }
        public string start_date { get { return start_dateRaw.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_ENG); } }
        internal DateTime? end_dateRaw { get; set; }
        public string end_date { get { return end_dateRaw == null ? "" : end_dateRaw.Value.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_ENG); } }
        public int has_url_link { get; set; }
        public string url_link { get; set; }
        public int is_premium_user { get; set; }
        public int is_external_notification { get; set; }
    }

    public class WebResponseContent
    {
        public int id { get; set; }
        public int? no { get; set; }
        public int content_type_id { get; set; }
        public string title { get; set; }
        public string image_url { get; set; }
        internal DateTime start_dateRaw { get; set; }
        public string start_date { get { return start_dateRaw.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_ENG); } }
        internal DateTime? end_dateRaw { get; set; }
        public string end_date { get { return end_dateRaw == null ? "" : end_dateRaw.Value.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_ENG); }} 
        public int has_url_link { get; set; }
        public string url_link { get; set; }
        public int is_application_popup { get; set; }
        public int is_premium_user { get; set; }
        public int is_external_notification { get; set; }
    }

    public class WebResponseContentCategory
    {
        public int id { get; set; }
        public int no { get; set; }
        public int content_type_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class WebResponseContentSearchDataTable
    {
        public int no { get; set; }
        public int id { get; set; }
        public string content_type_name { get; set; }
        public string title { get; set; }
        public string image_url { get; set; }
        internal  DateTime start_date_raw { get; set; }
        public string start_date { get { return start_date_raw == null ? "-" : start_date_raw.ToString(DataFactory.DATE_FORMAT); } } 
        public string end_date { get { return end_date_raw == null ? "-" : end_date_raw.Value.ToString(DataFactory.DATE_FORMAT); } }
        internal DateTime? end_date_raw { get; set; }
        public int has_url_link { get; set; }
        public string url_link { get; set; }
        public int is_application_popup { get; set; }
        public int is_premium_user { get; set; }
        public int is_external_notification { get; set; }
    }

    public class WebResponseContentCategorySearchDataTable
    {
        public int no { get; set; }
        public int id { get; set; }
        public string content_type_name { get; set; }
        public string name { get; set; }
    }

    public class WebSubmitContent
    {
        public int id { get; set; }
        public int content_type_id { get; set; }
        public int category { get; set; }
        public string title { get; set; }
        public string image_url { get; set; }
        public DateTime start_date { get; set; }
        public DateTime? end_date { get; set; }             
                
        public int has_url_link { get; set; }
        public string url_link { get; set; }
        public int is_application_popup { get; set; }
        public int is_premium_user { get; set; }
        public int is_external_notification { get; set; }
        public int active { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebSubmitContentCategory
    {
        public int id { get; set; }
        public int content_type_id { get; set; }
        public List<ListContent> list_content { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int active { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebSubmitContentCategorySorting
    {
        public List<ListContent> list_category { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class ListContent
    {
        public int id { get; set; }
        public int no { get; set; }
    }
}