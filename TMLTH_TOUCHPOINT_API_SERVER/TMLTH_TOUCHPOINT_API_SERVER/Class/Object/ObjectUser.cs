﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
   
    public class WebRequestUserDetailIDCard
    {
        public string Card_ID { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestCheckOldPassword
    {
        public string clntnum { get; set; }
        public string userid { get; set; }
        public string password { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebResponeUserDetail
    {
     
        public string idcard { get; set; }
        public string clntnum { get; set; }
        public string chdrnum { get; set; }
        public string userid { get; set; }
        public string name_ins { get; set; }
        public string lastname_ins { get; set; }
        public string mobile_no { get; set; }
        public string email { get; set; }
    }

    public class WebResponseAppVersion
    {
        public string appid { get; set; }
        public string appnm { get; set; }
        public string appversion { get; set; }
        public string appflag { get; set; }
        public string appplatform { get; set;}
        public string applatest { get; set;}
        public string strappTH { get; set; }
        public string strappEN { get; set; }
    }

    public class WebResponseDataDate
    {
        public string appnm { get; set; }
        public string applatest { get; set; }
        public string strappTH { get; set; }
        public string strappEN { get; set; }
    }


}