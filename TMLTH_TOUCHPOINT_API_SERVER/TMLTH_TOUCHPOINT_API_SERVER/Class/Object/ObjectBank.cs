﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestBank
    {
        public int id { get; set; }
        public string code { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }

    

    public class WebResponseBank
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name_th { get; set; }
        public string name_eng { get; set; }
        public string name_jp { get; set; }
        public string swift_code { get; set; }
        public string path { get; set; }
        public bool? direct_credit { get; set; }
        public bool? stat_direct_credit { get; set; }
        public bool? direct_debit { get; set; }
        public bool? stat_direct_dedit { get; set; }
        public int? order { get; set; }        
    }

    public class WebResponseBankPaymentDetail
    {
        public int id { get; set; }
        public string code { get; set; }
        public string path { get; set; }
        public int? order { get; set; }
    }

}