﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class SubmitDevice
    {
        public int? application_id { get; set; }
        public string application_code { get; set; }
        public string mobile_manufacturer { get; set; }
        public string mobile_model { get; set; }
        public string mobile_os_name { get; set; }
        public string mobile_os_version { get; set; }
    }
}