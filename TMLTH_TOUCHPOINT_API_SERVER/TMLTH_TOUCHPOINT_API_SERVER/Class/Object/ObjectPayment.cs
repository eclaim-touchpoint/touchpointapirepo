﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{   

    public class WebRequstPaymentCertificateDocument
    {
    
        public string doctype { get; set; }
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }
    public class WebRequestPaymentInformation
    {
        public string input_insurede { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebSubmitPostPayment
    {
        public string chdrnuminstment { get; set; }
        public string chdrnum { get; set; }
        public string userid { get; set; }
        public string accesskey { get; set; }
        public string profileid { get; set; }
        public string signedfieldname { get; set; }
        public string signdatetime { get; set; }
        public string paymenttype { get; set; }
        public string bankcode { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }
        public string ref4 { get; set; }
        public string ref5 { get; set; }
        public string refsend { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string language { get; set; }
        public string billtoforename { get; set; }
        public string billtosurname { get; set; }
        public string billtoaddressline1 { get; set; }
        public string billtoaddressline2 { get; set; }
        public string billtoaddressline3 { get; set; }
        public string billtoaddressline4 { get; set; }
        public string billtoaddresscity { get; set; }
        public string billtoaddresscountry { get; set; }
        public string billtoaddresspostalcode { get; set; }
        public string billtoemail { get; set; }
        public string billtophone { get; set; }
        public string billtomobile { get; set; }
        public string remark { get; set; }
        public string signature { get; set; }
        public string posturl { get; set; }
        public string paymentcode { get; set; }
        public string transactionid { get; set; }
        public int famid { get; set; }
        public string billcode { get; set; }
        public string productcode { get; set; }
        public decimal fee { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    //public class WebResponpaymentCertificateDocument
    //{
    //    public string chdrnum { get; set; }
    //    public string NewsHeadLine { get; set; }
    //    public string NewsContent { get; set; }
    //    public string NewsOrder { get; set; }
    //    public string NewsIsShow { get; set; }
    //    public string billfreq { get; set; }
    //    public string thaidesc { get; set; }
    //    public string docname { get; set; }
    //    public string ettername { get; set; }
    //    public string doctype { get; set; }
    //    public string docfullname { get; set; }
    //    internal DateTime? update_dtRaw { get; set; }
    //    public string update_dt { get { return update_dtRaw != null ? update_dtRaw.Value.ToString() : ""; } }
    //    internal string dateraw { get; set; }
    //    internal DateTime dateprtdate { get  { return DateTime.ParseExact(dateraw, "yyyyMMdd", new CultureInfo("en-US"));} }
    //    internal DateTime now { get  { return DateTime.Now; } }
    //    internal int daysDiff { get { return ((TimeSpan)(dateprtdate - now)).Days; } }
    //    internal int backdate { get; set; }
    //    public string allow_download { get
    //    {
    //            var YesOrNo = "";
    //            if(daysDiff < backdate)
    //            {
    //                YesOrNo = "Y";
    //                return YesOrNo;
    //            }
    //            YesOrNo = "N";
    //            return YesOrNo;

    //    }}
    //    internal string prtdate { get; set;  }
    //    public string prtdate_sort { get
    //        {

    //            var result = prtdate; 
    //            int length = prtdate.Length;
    //            var text = "";
    //            if ( length == 6)
    //            {

    //                text = "20" + prtdate;                                    
    //                bool isNumeric = Information.IsNumeric(text);
    //                int lengthtext = text.Length;
    //                var substring = Int32.Parse(text.Substring(0, 3));
    //                if (isNumeric == false){
    //                    result = "";
    //                }else if (text == "0" || text == "99999999"){
    //                    result = "";
    //                }else if (text == null){
    //                    result = "";
    //                }                    
    //                else if (lengthtext == 5){
    //                    result = "0" + text;
    //                }

    //                else if (substring < 1900){
    //                    result = "";
    //                }
    //                else{
    //                    result = text;
    //                }

    //            }
    //            return result ; 
    //        }
    //    } 

    public class WebResponsePaymentCertificateDocument
     
    {     
        public string chdrnum { get; set; }
        public string agntnum_name { get; set; }
        public string insured { get; set; }
        public string agntnum { get; set; }
        public string crtable { get; set; }
        public string billfreq { get; set; }
        public string thaidesc { get; set; }
        public string docname { get; set; }
        public string lettername { get; set; }
        public string doctype { get; set; }
        public string docfullname { get; set; }
        public DateTime? update_dt { get; set; }
        public string prtdate { get; set; }
        public string allow_download { get; set; }
        public DateTime? prtdate_sort { get; set; }
    }

    public class isErr777
    {
        public objErr777 err777 { get; set;}
    }
    public class objErr777
    {
        public string chdrnum { get; set; }
        public string resp_code { get; set; }     
    }
    public class WebRequestGetPremiumResult
    {
        public string chdrnum { get; set; }
        public int isloan { get; set; }
        public string loantype { get; set; }
        public int loanpaytype { get; set; }
        public decimal loanpaytotal { get; set; }
        public string PaymentType { get; set; }
        public string BankCode { get; set; }        
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestPaymentPeriod
    {
        public string chdrnum { get; set; }
        public string insured_no { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestPaymentRequestTemplate
    {
        //send to function 
        public string strChdrnum { get; set; }
        public string strPaymentType { get; set; }
        public string strBankCode { get; set; }
        //public string strClntnum { get; set; }
        //public string strIDCard { get; set; }
        public string strPayerName { get; set; }
        public string strFamID { get; set; }
        public string strOsName { get; set; }
        public string strOsVer { get; set; }
        public string strBrwsName { get; set; }
        public string strBrwsVer { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        //จำนวนเงินกู้ที่ชำระ
        public int isloan { get; set; } //ชำระเงินกู้
        public string loantype { get; set; }
        public int loanpaytype { get; set; }
        public decimal? loanpaytotal { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestPaymentTemplate
    {
        //SP_TP_GET_ACCESS_KEY
        public string app_nme { get; set; }
        //SP_TP_GET_LIST_PAYMENT_PERIOD_DETAIL
        public string clntmun { get; set; }
        public string chdrnum { get; set; }
        public string idcard { get; set; }
        public string paymenttype { get; set; }
        public string bankcode { get; set; }
        public string payername { get; set; }
        public string famid { get; set; }
        public string osname { get; set; }
        public string osver { get; set; }
        public string brwsname { get; set; }
        public string brwsver { get; set; }
        //send to function 
        public string strChdrnum { get; set; }
        public string strPaymentType { get; set; }
        public string strBankCode { get; set; }
        public string strClntnum { get; set; }
        public string strIDCard { get; set; }
        public string strPayerName { get; set; }
        public string strFamID { get; set; }
        public string strOsName { get; set; }
        public string strOsVer { get; set; }
        public string strBrwsName { get; set; }
        public string strBrwsVer { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }
    public class Listpayment
    {
        public string chdrnum { get; set; }
        public string paymenttype { get; set; }
        public string bankcode { get; set; }

    }

    public class WebResponsePaymentPeriod
    {
        public string insured { get; set; }
        //internal string payment_dateRaw { get; set; }
        //public string payment_date { get { return DateTime.Parse(payment_dateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public string payment_date { get; set; }
        public int? payment_period { get; set; }
        public decimal period_year { get; set; }
        public decimal period_time { get; set; }
        public decimal payment_premium { get; set; }
        public string stat { get; set; }
        public string msg { get; set; }
        public string resp_code { get; set; }
        public string billing_code { get; set; }
        public string product_code { get; set; }
        public string channel { get; set; }
        public string unpaid { get; set; }
        public string chdrnum { get; set; }
        public string chdrnum_instment { get; set; }
        public string remark { get; set; }
        public string remark2 { get; set; }


    }

    public class WebRequestPaymentPeriodDetail
    {
        public int isloan { get; set; }
        public string loantype { get; set; }
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestPaymentLoanDetail
    {
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponsePaymentPeriodDetail
    {
        public string insured { get; set; }
        public string name_insured { get; set; }
        public string payment_date { get; set; }
        public string payment_date_thai { get; set; }
        public int? payment_period { get; set; }
        public int? period_year { get; set; }
        public int? period_time { get; set; }
        public decimal payment_premium { get; set; }
        public string addr1 { get; set; }
        public string addr2 { get; set; }
        public string addr3 { get; set; }
        public string addr4 { get; set; }
        public string addr5 { get; set; }
        public string zipcode { get; set; }
        public string stat { get; set; }
        public string msg { get; set; }
        public string today { get; set; }
        public string com_code { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }
        public string ref4 { get; set; }
        public string ref5 { get; set; }
        public string ref_send { get; set; }
        public string currency_code { get; set; }
        public string language { get; set; }
        public string billing_code { get; set; }
        public string product_code { get; set; }
        public string agen_code { get; set; }
        public string agen_name { get; set; }
        public string insured_phone { get; set; }
        public decimal sum_insured { get; set; }
        public string coverage_plan { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string payment_option { get; set; }
        public string payment_period_dtm { get; set; }
        public string source_business { get; set; }
        public decimal payment_amount { get; set; }
        public string chdrnum_instment { get; set; }
    }

    public class WebResponsePaymentRequestTemplate
    {
        public string insured { get; set; }
        public string name_insured { get; set; }
        public string payment_date { get; set; }
        public string payment_date_thai { get; set; }
        public int? payment_period { get; set; }
        public int? period_year { get; set; }
        public int? period_time { get; set; }
        public decimal payment_premium { get; set; }
        public string addr1 { get; set; }
        public string addr2 { get; set; }
        public string addr3 { get; set; }
        public string addr4 { get; set; }
        public string addr5 { get; set; }
        public string zipcode { get; set; }
        public string stat { get; set; }
        public string msg { get; set; }
        public string today { get; set; }
        public string com_code { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }
        public string ref4 { get; set; }
        public string ref5 { get; set; }
        public string ref_send { get; set; }
        public string currency_code { get; set; }
        public string language { get; set; }
        public string billing_code { get; set; }
        public string product_code { get; set; }
        public string agen_code { get; set; }
        public string agen_name { get; set; }
        public string insured_phone { get; set; }
        public decimal? sum_insured { get; set; }
        public string coverage_plan { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string payment_option { get; set; }
        public string payment_period_dtm { get; set; }
        public string source_business { get; set; }
        public decimal payment_amount { get; set; }
        public string chdrnum_instment { get; set; }
    }

    public class WebRequestPaymentDTLByChdrnumInstment
    {
        public string chdrnuminstment { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }

    public class WebResponseLoanPaymentDTLByChdrnumInstment
    {
        public string chdrnum_instment { get; set; }
        public string chdrnum { get; set; }
        public string userid { get; set; }
        public string paymenttype { get; set; }
        public string bankcode { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public int? idv_family_id { get; set; }
        public string idv_paytype_detail { get; set; }

        public string remark { get; set; }
        public string resp_code { get; set; }
        public string resp_mess { get; set; }

        public bool? payment_status { get; set; }
        public string payment_detail { get; set; }
        public string payment_code { get; set; }
        public string payment_status_short_msg { get; set; }
        public string payment_status_msg { get; set; }
        public string bankName { get; set; }
        public string itemtype { get; set; }
    }


    public class WebResponsePaymentDTLByChdrnumInstment
    {
        public int paymentid { get; set; }
        public string chdrnum_instment { get; set; }
        public string chdrnum { get; set; }
        public string userid { get; set; }
        public string accesskey { get; set; }
        public string profileid { get; set; }
        public string signed_field_names { get; set; }
        public string signed_datetime { get; set; }
        public string paymenttype { get; set; }
        public string bankcode { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }
        public string ref4 { get; set; }
        public string ref5 { get; set; }
        public string ref_send { get; set; }
        public decimal? amount { get; set; }
        public string currency { get; set; }
        public string language { get; set; }
        public string bill_to_forename { get; set; }
        public string bill_to_surname { get; set; }
        public string bill_to_address_line1 { get; set; }
        public string bill_to_address_line2 { get; set; }
        public string bill_to_address_line3 { get; set; }
        public string bill_to_address_line4 { get; set; }
        public string bill_to_address_city { get; set; }
        public string bill_to_address_country{ get; set; }
        public string bill_to_address_postal_code { get; set; }
        public string bill_to_address_email { get; set; }
        public string bill_to_address_phone { get; set; }
        public string bill_to_address_mobile { get; set; }
        public string remark { get; set; }
        public string signature { get; set; }
        public string resp_code { get; set; }
        public string resp_mess { get; set; }
        public bool? payment_status { get; set; }
        public string payment_detail { get; set; }
        public string payment_code { get; set; }
        public string post_url { get; set; }
        public DateTime? post_date { get; set; }
        public string post_back_by { get; set; }
        public DateTime? post_back_date { get; set; }
        public string transaction_id { get; set; }
        public int? idv_family_id { get; set; }
        public string idv_paytype_detail { get; set; }

        public string payment_status_short_msg { get; set; }
        public string payment_status_msg { get; set; }
        public string bankName { get; set; }
        public string itemtype { get; set; }
    }

    public class WebRequestPaymentHistory
    {

        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }

    public class WebResponsePaymentHistory
    {
        public string policy_no { get; set; }
        public string tran_no { get; set; }
        public string bill_no { get; set; }
        public string compyear { get; set; }
        public string compprd { get; set; }
        public string pay_date { get; set; }
        public decimal? total_prm { get; set; }
        public string due_date { get; set; }
        public string pay_type { get; set; }
        public string ppaytype { get; set; }
        public string docfullname { get; set; }
        public string credit_card_no { get; set; }

    }


    public class WebRequestGetPaymentResult
    {
        public string chdrnum { get; set; }
        public string chdrnum_instment { get; set; }
        public int isloan { get; set; }
        public string loantype { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class updatedata
    {
        public string RESP_CODE { get; set; }
        public string RESP_MESS { get; set; }
        public bool PAYMENT_STATUS { get; set; }
        public string PAYMENT_CODE { get; set; }
        public string REQUEST_ACCESS_KEY { get; set; }
        public string REQUEST_PROFILE_ID { get; set; }
       
    }
    public class WebResponseGetPaymentResult
    {
        public List<updatedata> isdata { get; set; }
        public string insured { get; set; }
        public string name_insured { get; set; }
        public string payment_date { get; set; }
        public string payment_date_thai { get; set; }
        public int? payment_period { get; set; }
        public int? preiod_year { get; set; }
        public int? preiod_time { get; set; }
        public decimal payment_premium { get; set; }
        public string addr1 { get; set; }
        public string addr2 { get; set; }
        public string addr3 { get; set; }
        public string addr4 { get; set; }
        public string addr5 { get; set; }
        public string stat { get; set; }
        public string msg { get; set; }
        public string today { get; set; }
        public string com_code { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }
        public string ref4 { get; set; }
        public string ref5 { get; set; }
        public string ref_send { get; set; }
        public string current_code { get; set; }
        public string language { get; set; }
        public string billing_code { get; set; }
        public string product_code { get; set; }
        public string agen_code { get; set; }
        public string agen_name { get; set; }
        public string insured_phone { get; set; }
        public decimal sum_insured { get; set; }
        public string coverage_plan { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string payment_option { get; set; }
        public string payment_period_dtm { get; set; }
        public string source_business { get; set; }
    }
    public class clsPaymentOnline
    {
        public int paymentid { get; set; }
        public string chdrnum_instment { get; set; }
        public string chdrnum { get; set; }
        public string user_id { get; set; }
        public string accesskey { get; set; }
        public string profileid { get; set; }
        public string signed_field_names { get; set; }
        public string signed_datetime { get; set; }
        public string payment_type { get; set; }
        public string bank_code { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }
        public string ref4 { get; set; }
        public string ref5 { get; set; }
        public string ref_send { get; set; }
        public string bill_to_forename { get; set; }
        public string bill_to_surname { get; set; }
        public double payment_amount { get; set; }
        public string currency_code { get; set; }
        public string language { get; set; }
        public string insured_name { get; set; }
        public string insured_surname { get; set; }
        public string bill_to_address_line1 { get; set; }
        public string bill_to_address_line2 { get; set; }
        public string bill_to_address_line3 { get; set; }
        public string bill_to_address_line4 { get; set; }       
        public string bill_to_address_city { get; set; }
        public string bill_to_address_country { get; set; }
        public string bill_to_address_postal_code { get; set; }
        public string bill_to_address_email { get; set; }
        public string bill_to_address_phone { get; set; }
        public string bill_to_address_mobile { get; set; }
        public string remark { get; set; }
        public string signature { get; set; }
        public string resp_code { get; set; }
        public string resp_mess { get; set; }
        public bool? payment_status { get; set; }
        public string payment_detail { get; set; }
        public string payment_code { get; set; }
        public string post_url { get; set; }
        public DateTime? post_date { get; set; }
        public string post_back_by { get; set; }
        public DateTime? post_back_date { get; set; }    
        public string transaction_id { get; set; }
        public string fam_id { get; set; }
        public string billing_code { get; set; }
        public string product_code { get; set; }
        public double fees { get; set; }
        public decimal? amount { get; set; }
        public string currency { get; set; }
        public int? idv_family_id { get; set; }
        public string idv_paytype_detail { get; set; }
        public string objective_type_code { get; set; }
        public string[] url_tracking_result { get; set; }

    }
    public class clsPremiumFee
    {
        public double fee { get; set; }
        public double amount { get; set; }
        public double total_amount { get; set; }
    }
}