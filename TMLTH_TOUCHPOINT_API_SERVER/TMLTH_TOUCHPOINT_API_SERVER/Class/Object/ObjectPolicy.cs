﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestPolicyDetail
    {
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
  
    public class WebRequestPolicy
    {
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestPolicyUser
    {
        public string appid { get; set; }
        //public string policyno { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestPolicyOverview
    {
        public string clntnum_ins { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestPolicyInvestment
    {
        //public string clntnum_ins { get; set; }
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebResponsePolicyDetail
    {
        public string policyno { get; set; }
        public string billfreq { get; set; }
        public string occdate { get; set; }
        public string riskcessdate { get; set; }
        public string ptdate { get; set; }
        public string basicinstprem { get; set; }
        public string crtable { get; set; }
        public string longdesc { get; set; }
        public string description { get; set; }
        public string covsumins { get; set; }
        public string covinstprem { get; set; }
        public string covstatcode { get; set; }
        public string covstatcodedesc { get; set; }
        public string life { get; set; }
        public string coverage { get; set; }
        public string rider { get; set; }
        public string termpolicy { get; set; }
        public string paypolicy { get; set; }
    }

    public class WebResponsePolicyExclusion
    {
        public string chdrnum { get; set; }
        public decimal fupno { get; set; }
        public string fupcode { get; set; }
        public string fupremk { get; set; }
        public decimal fupredt { get; set; }
        public string exclusion { get; set; }
        
    }
    public class WebResponsePolicyBrochure
    {
        public string ImageGraph { get; set; }
    }
    public class WebResponsePolicy
    {
        public string policyno { get; set; }
        public string crtable { get; set; }
        public string billfreq { get; set; }
        //internal string occdateRaw { get; set; }
        //public string occdate { get { return DateTime.Parse(occdateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public string occdate { get; set; }
        public string riskcessdate { get; set; }
        public string ptdate { get; set; }
        //public string riskcessdateRaw { get; set; }
        //public string riskcessdate { get { return DateTime.Parse(riskcessdateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        //public string ptdateRaw { get; set; }
        //public string ptdate { get { return DateTime.Parse(riskcessdateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public decimal? basicinstprem { get; set; }
        public decimal? covinstprem { get; set; }
        public string agentname { get; set; }
        public string agenttel { get; set; }
        public string polyear { get; set; }
        public string polprd { get; set; }
        public decimal? riskassessment { get; set; }
        internal decimal? riskassessmentdateRaw { get; set; }
        public string riskassessmentdate { get { return string.IsNullOrWhiteSpace(riskassessmentdateRaw.ToString()) ? "-" : DateTime.ParseExact(riskassessmentdateRaw.Value.ToString(), "yyyyMMdd", DataFactory.CULTURE_ENG).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); }  }
        public decimal? resultloan01 { get; set; }
        public decimal? resultloan02 { get; set; }
        public decimal? resultloan03 { get; set; }
        public decimal? resultloan04 { get; set; }
        public int isunitlink { get; set; }
        public int hasfund { get
            {
                var checkcrtable = crtable.Substring(0, 3);
                if (checkcrtable.Equals("URP"))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string productname { get; set; }
        public string graphimage { get; set; }
    }

    public class WebResponsePolicylist
    {
        public string clntnum { get; set; }
        public string chdrnum { get; set; }
    }
    
    public class WebResponsePolicyOverview
    {
        public string clntnum_ins { get; set; }
        public string basic_sa { get; set;}
        public string accident { get; set; }
        public string hs { get; set; }
        public string hshc { get; set; }
        public string hb { get; set; }
        public string cir { get; set; }
        public string me { get; set;}
        public string fund { get; set; }
    }
    public class WebResponsePolicyUser
    {
        public string chdrnum { get; set; }
    }
    public class WebResponsePolicyLoan
    {
       public string policyno { get; set; }
       public string loantype { get; set; }
       public string loanstartdate { get; set; }
       //public string loanstartdate { get { return DateTime.Parse(loanstartdateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
       public decimal? loanoriginalamount { get; set; }
       public decimal? loancurramount { get; set; }
       public decimal? interest { get; set; }
       public decimal? total { get; set; }
    }

    public class WebResponsePolicyLoanListClntnum
    {
        public string policyno { get; set; }
        public decimal? total_apl { get; set; }
        public decimal? total_rpl { get; set; }
        public decimal? total { get; set; }
    }

    public class WebResponsePolicyInvestment
    {
        public string policyno { get; set; }
        public string crtable { get; set; }
        public string billfreq { get; set; }
        //internal string occdateRaw { get; set; }
        //public string occdate { get { return DateTime.Parse(occdateRaw).ToString(DataFactory.DATE_FORMAT_FULL_MONTH, DataFactory.CULTURE_INFO); } }
        //public string occdate { get; set; }
        //public string riskcessdate { get; set; }
        public string occdate { get;set;}
        public string riskcessdate { get;set;}
        public string ptdate { get;set;}
        //internal string riskcessdateRaw { get; set; }
       // public string riskcessdate { get { return DateTime.Parse(riskcessdateRaw).ToString(DataFactory.DATE_FORMAT_FULL_MONTH, DataFactory.CULTURE_INFO); } }

        //internal string ptdateRaw { get; set; }
        //public string ptdate { get { return DateTime.Parse(ptdateRaw).ToString(DataFactory.DATE_FORMAT_FULL_MONTH, DataFactory.CULTURE_INFO); } }
        //public string ptdate { get; set; }
        public decimal? basicinstprem { get; set; }
        public decimal? covinstprem { get; set; }
        public string agentname { get; set; }
        public string agenttel { get; set; }
        public string polyear { get; set; }
        public string polprd { get; set; }
        public decimal? riskassessment { get; set; }
        public string riskassessmentdate { get; set; }
        public decimal? resultloan01 { get; set; }
        public decimal? resultloan02 { get; set; }
        public decimal? resultloan03 { get; set; }
        public decimal? resultloan04 { get; set; }
    }
    public class WebResponsePolicyCashDrop
    {
        public string policyno { get; set; }
        public string billfreq { get; set; }
        public string occdate { get; set; }
        public string riskcessdate { get; set; }
        public string ptdate { get; set; }
        public string basicinstprem { get; set; }
        public string crtable { get; set; }
        public string longdesc { get; set; }
        public string description { get; set; }
        public string covsumins { get; set; }
        public string covstacode { get; set; }
        public string covstatcodedesc { get; set; }
        public string life { get; set; }
        public string coverage { get; set; }
        public string rider { get; set; }
        public string termpolicy { get; set; }
        public string paypolicy { get; set; }
    }

}

