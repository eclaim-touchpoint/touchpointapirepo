﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
   
        public class WebRequestDirectDebit
        {
  
            public string ip_client { get; set; }
            public string admin { get; set; }
        }

        public class WebResponseDirectDebit
        {
            public int id { get; set; }
            public string code { get; set; }
            public string nameth { get; set; }
            public string nameen { get; set; }
            public string namejp { get; set; }
            public string swiftcode { get; set; }
            public string path { get; set; }
            public bool? direct_credit { get; set; }
            public bool? stat_direct_credit { get; set; }
            public bool? direct_debit { get; set; }
            public bool? stat_direct_debit { get; set; }
            public int? order { get; set; }
        }
        public class WebRequestDirectCredit
        {
            //public string chdrnum { get; set; }
            public string ip_client { get; set; }
            public string admin { get; set; }
        }

        public class WebResponseDirectCredit
        {
            public int id { get; set; }
            public string code { get; set; }
            public string nameth { get; set; }
            public string nameen { get; set; }
            public string namejp { get; set; }
            public string swiftcode { get; set; }
            public string path { get; set; }
            public bool? direct_credit { get; set; }
            public bool? stat_direct_credit { get; set; }
            public bool? direct_debit { get; set; }
            public bool? stat_direct_debit { get; set; }
            public int? order { get; set; }


        }
    
}