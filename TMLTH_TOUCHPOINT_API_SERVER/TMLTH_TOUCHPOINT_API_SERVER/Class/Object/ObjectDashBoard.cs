﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestDashBoard
    {
        public int id { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseUpdateContent
    {
        public int id { get; set; }
        public int refid { get; set; }
        internal string titleraw { get; set; }
        public string title
        {
            get
            {
                int count = titleraw.Count();
                if (count >= 150)
                {
                    return titleraw.Substring(0, 150) + "...";
                }
                return titleraw;
            }
        }

        internal DateTime updateraw { get; set; }
        public string update { get { return ContentTools.getdatetimeleft(updateraw); } }

    }
    public class WebResponseUpdateFAQ
    {
        public int id { get; set; }
        internal string questionraw { get; set; }
        public string question
        {
            get
            {
                int count = questionraw.Count();
                if (count >= 150)
                {
                    return questionraw.Substring(0, 150) + "...";
                }
                return questionraw;
            }
        }

        internal DateTime updateraw { get; set; }
        public string update { get { return ContentTools.getdatetimeleft(updateraw); } }


    }
    public class WebResponseUpdateApptemimage
    {
        
        public int id { get; set; }
        public int refid { get; set; }
        public string url { get; set; }
        internal DateTime updateraw { get; set; }
        public string update { get { return ContentTools.getdatetimeleft(updateraw); }}
    }

    public class WebResponseUpdateApptemMenu
    {

        public int id { get; set; }
        public int refid { get; set; }
        public string name { get; set; }
        public string des { get; set; }
        internal DateTime updateraw { get; set; }
        public string update { get {return ContentTools.getdatetimeleft(updateraw); } }
        public int active { get; set; }
    }


}
    