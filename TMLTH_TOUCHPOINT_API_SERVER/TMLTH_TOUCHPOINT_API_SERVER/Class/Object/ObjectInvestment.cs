﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestInvestment
    {
        public string chdrnum { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseInvestment
    {
        public string coverage { get; set; }
        public string fund_code { get; set; }
        public string effect_date { get; set; }
        public decimal? unit { get; set; }
        public decimal? nowprice { get; set; }
        public decimal? estimate_fund_value { get; set; }
        public string fund_shot_name { get; set; }
        public string fund_full_name { get; set; }
        public string product_shot_name { get; set; }
        public string product_long_name { get; set; }
        public string product_code { get; set; }
        public string fundhousemanagement { get; set; }
        public string asset_shot_name { get; set; }
        public string asset_long_name { get; set; }
        public decimal? unit_alloc_perc_amt { get; set; }
    }

    public class WebResponseInvestmentSummary
    {
        public string policy_no { get; set; }
        public decimal? fund { get; set; }
    }

    public class WebRequestInvestmentTran
    {
        public string chdrnum { get; set; }
        public string datefrom { get; set; }
        public string dateto { get; set; }
        public string coverage { get; set; }
        public string ip_client { get; set; }
        public string fund_code { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }


    public class WebResposeInvestmentTran
    {
        public string fundcode { get; set; }
        public string batchnumber { get; set; }
        public string companycode { get; set; }
        public string policynumber { get; set; }
        public decimal? batchdate  { get; set; }
        public string coveragenumber { get; set; }
        public decimal? fundprice { get; set; }
        public string effectdate { get; set; }
        public decimal? unit { get; set; }
        public decimal? price { get; set; }
        public decimal? pricedate { get; set; }
        public string assetnameeng { get; set; }
        public string assetnameengabv { get; set; }
        public string assetnameth { get; set; }
        public string fundnameth { get; set; }
        public string transecdetail { get; set; }
        public decimal? transecnumber { get; set; }
        public decimal? unitbalance { get; set; }
        public decimal? navperunit { get; set; }
        public string coveragecode { get; set; }
        public string productcode { get; set; }

    }
}