﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    
        public class WebRequestClaims
        {
        
            public string chdrnum { get; set; }
            public string ip_client { get; set; }
            public string admin { get; set; }
            public SubmitDevice device { get; set; }
        }

        public class WebResponseClaims
        {
            public string policy_on { get; set; }
            public decimal? incident_dc { get; set; }
            public decimal? claim_recv_dc { get; set; }
            public string incident_dt { get; set; }
            public string claim_recv_dt { get; set; }
            public string hospital { get; set; }
            public string longdesc { get; set; }
            public string decision_status { get; set; }
            public string paym_amt { get; set; }
            public string reason { get; set; }
            public decimal rgpynum { get; set; }
            public string aprvdate { get; set; }
            public string claimevd { get; set; }
            public string decision_code { get; set; }
            public string claimevd_code { get; set; }
            public string fclamsts { get; set; }

        }

        public class WebRequestClaimsDetail
        {

            public string chdrnum { get; set; }
            public string incident_dt { get; set; }
            public int rgpnum { get; set; }
            public int pagesize { get; set; }
            public string ip_client { get; set; }
            public string admin { get; set; }
            public SubmitDevice device { get; set; }
        }

        public class WebResponseClaimsDetail
        {
            public int? rowcnt { get; set; }
            public string incident_dt { get; set; }
            public string claim_recv_dt { get; set; }
            public string hospital { get; set; }
            public string crtable_desc { get; set; }
            public string claimevd { get; set; }
            public string decision_status { get; set; }
            public string aprvdate { get; set; }
            public string totalamt { get; set; }
            public string reason { get; set; }
            public string longdesc { get; set; }
            public string start_dt { get; set; }
            public string end_dt { get; set; }
            public string paym_amt { get; set; }
            public string claim_amt { get; set; }
            public string first_paydate { get { return first_paydateraw == null ? "" : DateTime.ParseExact(first_paydateraw.Value.ToString(), DataFactory.DATE_FORMAT_FROM_DATABASE, CultureInfo.InvariantCulture).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
            internal decimal? first_paydateraw { get; set; }
            public string pay_type { get; set; }
            public string AccountNo { get; set; }
            public string Bank { get; set; }

        }
    
}