﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebResponseRegisterDOPA
    {
        public string code { get; set; }
        public string des { get; set; }
    }
        public class WebResponseRegister
    {
        public string insured_secuityno { get; set; }
        public string clntnum_ins { get; set; }
        public string lgivname_ins { get; set; }
        public string lsurname_ins { get; set; }
        public string cltphone01_ins { get; set; }
        public string cltphone02_ins { get; set; }
        public string secuityno_own { get; set; }
        public string secuityno_ins { get; set; }
        public string clntnum_own { get; set; }
        public string lgivname_own { get; set; }
        public string lsurname_own { get; set; }
        public string chdrnum { get; set; }
        public string pstatcode { get; set; }
        public string id_type { get; set; }
        public string is_math_id { get; set; }
        public string is_math_mobile { get; set; }
        public string chdrnum_system_existing { get; set; }
        public string chdrnum_system_isvalid { get; set; }
        public string chdrnum_input_existing { get; set; }
        public string chdrnum_input_isvalid { get; set; }
        public string is_math_birth { get; set; }
        public string massage { get; set; }
        public string ip_client { get; set; }
        //public string admin { get; set; }
    }
    public class WebRequestRegister
    {
        public string idcard { get; set; }
        public string lgivname_ins { get; set; }
        public string lsurname_ins { get; set; }
        public string userid { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string agreement { get; set; }
        public string mobileno { get; set; }
        public string ip_client { get; set; }
        //public string admin { get; set; }
    }

    public class WebSubmitRegister
    {
        public string idcard { get; set; }
        public string lgivname_ins { get; set; }
        public string lsurname_ins { get; set; }
        public string lasercode { get; set; }
        public string userid { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string agreement { get; set; }
        public string mobileno { get; set; }
        public DateTime birthdate { get; set; }
        public decimal birthdateDecimal { get { return decimal.Parse(birthdate.ToString(DataFactory.DATE_FORMAT_FROM_DATABASE, DataFactory.CULTURE_ENG)); } }
        public int type { get; set; } //NOTE: type == 1 is request, type == 2 is submit
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }
}