﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestInsuranceCard
    {
        public string chdrnum { get; set; }
        public string lng { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebResponseInsuranceCard
    {
        public int? cardtype { get; set; }
        public string cardtypedesc { get; set; }
        public string title { get; set; }
        public string chdrnum { get; set; }
        public string insuredname { get; set; }
        public string crrdatepafrom { get; set; }
        public string crrdatepato { get; set; }
        public string summassurepa { get; set; }
        public string hpropdtebase { get; set; }
        public string crrdatehsn { get; set; }
        public string roomhsn { get; set; }
        public string acc24hsn { get; set; }
        public string crrdatehs2n { get; set; }
        public string roomhs2n { get; set; }
        public string acc24hs2n { get; set; }
        public string crrdatehshc { get; set; }
        public string roomhshc { get; set; }
        public string acc24hshc { get; set; }
        public string opdsumm { get; set; }
        public string opdtype { get; set; }
        public string remark1 { get; set; }
        public string remark2 { get; set; }
        public string ispa { get; set; }
        public string existhsn { get; set; }
        public string exisths2n { get; set; }
        public string existhshc { get; set; }
        public string existopd { get; set; }
        public string crtable { get; set; }
        public string prddesc { get; set; }
        public string sumbasic { get; set; }
        public string existhcp1 { get; set; }
        public string existhcp2 { get; set; }
        public string existhcp3 { get; set; }
        public string opdplustext { get; set; }
        public string opdplustitle { get; set; }
        public string hcp1text { get; set; }
        public string hcp2title { get; set; }
        public string hcp2text { get; set; }
        public string hcp3title { get; set; }
        public string hcp3text { get; set; }
    }
}