﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestInvestmentOverview
    {
        public string clntnum_ins { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestPolicyOverviewAccident
    {
        public string clntnum_ins { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestPolicyOverviewHealth
    {
        public string clntnum_ins { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestPolicyOverviewLife
    {
        public string clntnum_ins { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseInvestmentOverviewList
    {
        public IEnumerable<WebResponseInvestmentOverview> investment { get; set; }
    }

    public class WebResponseInvestmentOverview
    {
        public string unit_virtual_fund { get; set; }
        public string long_desc { get; set; }
        public string fund_house { get; set; }
        public string policy_no { get; set; }
        public string eff_date { get; set; }
        //public string eff_date { get { return DateTime.Parse(eff_dateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public decimal? tunavpr { get; set; }
        public decimal? current_unit_bal { get; set; }
        public decimal? fund_value { get; set; }
        public string statusdescth { get; set; }
    }
    public class WebResponseInvestmentPolicy
    {
        public string unit_virtual_fund { get; set; }
        public string long_desc { get; set; }
        public string fund_house { get; set; }
        public string policy_no { get; set; }
        //public string eff_date { get; set; }
        internal string eff_dateRaw { get; set; }
        public string eff_date { get { return DateTime.Parse(eff_dateRaw).ToString(DataFactory.DATE_FORMAT_MONTH_NUMBER, DataFactory.CULTURE_INFO); } }
        public decimal? tunavpr { get; set; }
        public decimal? current_unit_bal { get; set; }
        public decimal? fund_value { get; set; }
    }
    public class WebResponseInvestmentLoan
    {
        public string policy_no { get; set; }
        public string loan_type { get; set; }
        public string loan_start_date { get; set; }
        public decimal loan_original_amount { get; set; }
        public decimal loan_curr_amount { get; set; }
        public decimal? interest { get; set; }
        public decimal total { get; set; }
    }

    public class WebResponsePolicyLoanSummary
    {
        public string policy_no { get; set; }
        public decimal? total_apl { get; set; }
        public decimal? total_rpl { get; set; }
        public decimal? total { get; set; }
        public string billing_code { get; set; }
        public string product_code { get; set;}
        public string source_business { get; set; }
    }

    public class WebResponsePolicyLoanPayFlag
    {
        public string chdrnum { get; set; }
        public string loan_payment_flag { get; set; }
        public string reason { get; set; }
        public string payment_status { get; set; }
        public decimal? occ_date { get; set; }
        public decimal? pt_date { get; set; }
        public decimal? capitalize_date { get; set; }
    }


    public class WebResponsePolicyOverviewAccident
    {
        public string plan_name { get; set; }
        public string policy_no { get; set; }
        public string crrcd { get; set; }
        //public string crrcd { get { return DateTime.Parse(crrcdRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public decimal? sum_ins { get; set; }
        public decimal? premium { get; set; }
        public string billreq_desc { get; set; }
        public string status { get; set; }
        public string statusdescth { get; set; }
    }
    public class WebResponsePolicyOverviewHealth
    {
        public string plan_name { get; set; }
        public string policy_no { get; set; }
        public string crrcd { get; set; }
        //public string crrcd { get { return DateTime.Parse(crrcdRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public string sum_ins { get; set; }
        public decimal? premium { get; set; }
        public string billreq_desc { get; set; }
        public string status { get; set; }
        public string statusdescth { get; set; }
    }
    public class WebResponsePolicyOverviewLife
    {
        public string plan_name { get; set; }
        public string policy_no { get; set; }
        public string occ_date { get; set; }
        //public string occ_date { get { return DateTime.Parse(occ_dateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public decimal? sum_ins { get; set; }
        public decimal? premium { get; set; }
        public string billreq_desc { get; set; }
        public string status { get; set; }
        public string statusdescth { get; set; }

    }
}