﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestBranch
    {
        public int page { get; set; }
        public string province { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebReponseBranch
    {
        public string code { get; set; }
        public string thdesc { get; set; }
        public string addline1 { get; set; }
        public string addline2 { get; set; }
        public string addlist { get; set; }
        public string addprov { get; set; }
        public string addpost { get; set; }
        public string addtel { get; set; }
        public string addfax { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string status { get; set; }	  

    }

    public class WebRequestBranchProvince
    {
      
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }

    }

    public class WebReponseBranchProvince
    {
        public string thaddprov { get; set; }
        public string enaddprov { get; set; }
    

    }
} 