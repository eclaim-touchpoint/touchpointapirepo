﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebSubmitTemplateAppImage
    {
        public int id { get; set; }
        public int templateapppageid { get; set; }
        public string imageurl { get; set; }

        public int active { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestTemplateApp
    {
        public int id { get; set; }
        public int templateapppageid { get; set; }
        public int ischeckcookie { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestSelectTemplateApp
    {
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseTemplateAppImage
    {
        public int id { get; set; }
        public string image_url { get; set; }
        
    }
    public class WebResponseTemplateApp
    {
        public int id { get; set; }
        public int template_app_page_id { get; set; }
        public string name { get; set; }
        public int position_no { get; set; }
        public int size_total { get; set; }
        public string description { get; set; }
        public string create_by_name { get; set; }
        internal DateTime create_date_raw { get; set; }
        public string create_date { get { return create_date_raw == null ? "-" : create_date_raw.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public string modify_by_name { get; set; }
        internal DateTime? modify_date_raw { get; set; }
        public string modify_date { get { return modify_date_raw == (DateTime?)null ? "-" : modify_date_raw.Value.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
        public int active { get; set; }
    }

    public class WebResponseSelectAppTemplate
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

    }
    public class WebResponseAppTemplateDataTable
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
     
        public string create_by_name { get; set; }
        public string image_url { get; set; }

        internal DateTime create_date_raw { get; set; }
        public string create_date { get { return create_date_raw == null ? "-" : create_date_raw.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
      
        public string modify_by_name { get; set; }
        internal DateTime? modify_date_raw { get; set; }
        public string modify_date { get { return modify_date_raw == (DateTime?)null ? "-" : modify_date_raw.Value.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }

    }

    public class WebSubmitAppTemplate
    {
        public int id { get; set; }
        public List<ListTemplate> list_app_template { get; set;}
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }

    }
    public class ListTemplate
    {
        public int id { get; set; }
        public int position_no { get; set; }
        public int size_total { get; set; }     
        public int active { get; set; }
        public string description { get; set; }
         public string name { get; set; }

    }
    public class WebSubmitAppTemplateMenuActive
    {
        public int id { get; set; }
        public int active { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }

    }
}