﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestHospitalProvince
    {
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
      
    }

    public class WebResponseHospitalProvince
    {
        public string province_th { get; set; }
        public string province_en { get; set; }
    }

    public class WebRequestHospital
    {
        public string lng { get; set; }
        public string province { get; set; }
        public int page { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebRequestSearchHospital
    {
        public int typesearch { get; set; }
        public string lng { get; set; }
        public string province { get; set; }
        public string name { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get;set;}
        //public string type { get; set; }
        public int page { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseSearchHospital
    {
        public Guid id { get; set; }
        public string thname { get; set; }
        public string engname { get; set; }
        public string thaddress { get; set; }
        public string engaddress { get; set; }
        public string thtumbon { get; set; }
        public string engtumbon { get; set; }
        public string thcity { get; set; }
        public string engcity { get; set; }
        public string thlocation { get; set; }
        public string englocation { get; set; }
        public string postcode { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
        public string type { get; set; }
        internal string _latRaw { get; set; }
        public double? _lat { get { return Double.Parse(_latRaw); } }
        internal string _longRaw { get; set; }
        public double? _long { get { return Double.Parse(_longRaw); } }

        //internal string occdateRaw { get; set; }
        //public string occdate { get { return DateTime.Parse(occdateRaw).ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO); } }
    }
    public class WebResponseHospital
    {
        public string id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string tumbon { get; set; }
        public string city { get; set; }
        public string location { get; set; }
        public string postcode { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
        public string type { get; set; }
        public bool? ipdgroup { get; set; }
        public bool? opdgroup { get; set; }
        public bool? dentalgroup { get; set; }
        public bool? ipdindividual { get; set; }
        public bool? opdindividual { get; set; }
        public bool? opdpaindividual { get; set; }
        public bool? medicalcenter { get; set; }
        public string lat { get; set; }
        public string _long { get; set; }
    }
}