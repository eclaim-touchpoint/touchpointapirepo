﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{

    public class SubmitProfileChangePasswordData
    {
        public int id { get; set; }
        public string new_ppw { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }
    public class WebRequestProfile
    {
      
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRequestSearchProfile
    {
        public string ip_client { get; set; }
        public string admin { get; set; }
    }

    public class WebResponseProfile
    {
        public int id { get; set; }
        public string una { get; set; } // USERNAME
        public string pass { get; set; } // PASSWORD
        public int system_account_role_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string employee_no { get; set; }
        public string name_en { get; set; }
        public string name_na { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public int has_image_url { get; set; }
        public int wrong_count { get; set; }
        public DateTime wrong_date { get; set; }
        public string otp { get; set; }
        public int otp_count { get; set; }
        public DateTime otp_date { get; set; }
        public int active { get; set; }
        public int create_by { get; set; }
        public DateTime create_date { get; set; }
        public int modify_by { get; set; }
        public DateTime modify_date { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
    }
}