﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class EncryptTableData
    {
        public int sEcho { get; set; }
        public int start { get; set; }
        public int len { get; set; }
        public int sort { get; set; }
        public long id { get; set; }
        public string search { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public int mode { get; set; }//web or APP   0 == web 1 == app
        public SubmitDevice device { get; set; }
    }
}