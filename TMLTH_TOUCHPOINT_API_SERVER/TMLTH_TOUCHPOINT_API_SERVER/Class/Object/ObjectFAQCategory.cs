﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestSearchFAQCategory
    {
        public int id { get; set; }
        public string admin { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }
}