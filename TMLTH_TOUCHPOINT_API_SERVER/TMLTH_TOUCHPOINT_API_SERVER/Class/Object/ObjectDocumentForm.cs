﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestDocumentForm
    {
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseDocumentForm
    {
        public int id { get; set; }
        public int docgrpid { get; set; }
        public string docgrpnm { get; set; }
        public string docnm{ get; set; }
        public string docfn { get; set; }
       
    }
}