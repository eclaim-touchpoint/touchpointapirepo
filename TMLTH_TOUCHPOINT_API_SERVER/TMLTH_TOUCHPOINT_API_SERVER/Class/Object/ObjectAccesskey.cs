﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
  
        public class WebRequestAccesskey
        {
            public string App_NME { get; set; }
            public string App_Type { get; set; }
            public string Service_NME { get; set; }
            public string ip_client { get; set; }
            public string admin { get; set; }
            public SubmitDevice device { get; set; }
        }

        public class WebReponseAccesskey
        {
            public int id { get; set; }
            public string access_key { get; set; }
            public string profile_id { get; set; }
            public string app_nme { get; set; }
            public string app_type { get; set; }
            public string service_nme { get; set; }
        }
    
}