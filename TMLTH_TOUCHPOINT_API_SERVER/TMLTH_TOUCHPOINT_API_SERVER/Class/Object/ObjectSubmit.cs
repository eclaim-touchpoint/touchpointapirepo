﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestSigninAuthenication
    {
        public string u { get; set; }
        public string p { get; set; }
        public int type { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebResponseSigninAuthenication
    {
        public string idcard { get; set; }
        public string clntnum { get; set; }
        public string chdrnum { get; set; }
        public string password { get; set; }
        public string userid { get; set; }
        public string nameins { get; set; }
        public string lastnameins { get; set; }
        public string mobileno { get; set; }
        public string email { get; set; }
        public string agreement { get; set; }
        public DateTime? upddate { get; set; }
        public DateTime? registerdt { get; set; }
        public string phone { get; set; }
        public DateTime? phonedt { get; set; }
        public string prevphone { get; set; }
        public string registerfrom { get; set; }
        public string updatefrom { get; set; }
        public string sessionid { get; set; }
        public int? has_image_url { get; set; }
    }
    public class WebResponseSigninAuthenicationCMS
    {
        public int id { get; set; }
        public int system_account_role_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string employee_no { get; set; }
        public  string name_en { get; set; }
        public  string name_na { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public int has_image_url { get; set; }
        public  int wrong_count { get; set; }
        public DateTime wrong_date { get; set; }
        public string otp { get; set; }
        public int otp_count { get; set; }
        public DateTime otp_date { get; set; }
        public int active { get; set; }
        public int create_by { get; set; }
        public DateTime create_date { get; set; }
        public int modify_by { get; set; }
        public DateTime modify_date { get; set; }
    }

    public class WebResponseGetUser
    {
        public string idcard { get; set; }
        public string clntnum { get; set; }
        public string chdrnum { get; set; }
        public string password { get; set; }
        public string userid { get; set; }
        public string name_ins { get; set; }
        public string lastname_ins { get; set; }
        public string mobile_no { get; set; }
        public string email { get; set; }
        public string agreement { get; set; }
        public string upddate { get; set; }
        public string register_dt { get; set; }
        public string phone { get; set; }
        public string phone_dt { get; set; }
        public string prev_phone { get; set; }
        public string register_from { get; set; }
        public string update_from { get; set; }
        public string SESSTION_ID { get; set; }
    }
    public class WebRequestforgetpassword
    {
        public string idcard { get; set; }
        public string birthdate { get; set; }
        public string email { get; set; }
        public string mobileno { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebResponseforgetpassword
    {
        public string idcard { get; set; }
        public string clntnum { get; set; }
        public string chdrnum { get; set; }
        public string userid { get; set; }
        public string nameins { get; set; }
        public string lastnameins { get; set; }
        public string mobileno { get; set; }
        public string email { get; set; }
        public string agreement { get; set; }
        public DateTime? upddate { get; set; }
        public DateTime? registerdt { get; set; }
        public string birthDate { get; set; }
        public string phone { get; set; }
    }

}