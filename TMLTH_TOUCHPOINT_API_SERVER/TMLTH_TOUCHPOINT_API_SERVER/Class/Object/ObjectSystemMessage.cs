﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestSystemMessage
    {
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebRensponSystemMessage
    {                                                
        public Guid id { get; set; }
        public string admin { get; set; }
        public string clntnum { get; set; }
        public string chdrnum { get; set; }
        public int type_id { get; set; }
        public string header { get; set; }
        public string message { get; set; }
        public int has_img_url { get; set; }
        public int active { get; set; }
        public int is_read { get; set; }
        public DateTime? read_firstdate_raw { get; set; }
        public DateTime? read_lastdate_raw { get; set; }
        public string read_firstdate { get { return read_firstdate_raw == null ? "" : ((DateTime)read_firstdate_raw).ToString(DataFactory.DATE_FORMAT_MONTH_TIME24, DataFactory.CULTURE_INFO); } }
        public string read_lastdate { get { return read_lastdate_raw == null ? "" : ((DateTime)read_lastdate_raw).ToString(DataFactory.DATE_FORMAT_MONTH_TIME24, DataFactory.CULTURE_INFO); } }
        internal DateTime createdateraw { get; set; }
        internal DateTime? modify_dateraw { get; set; }
        public string createdate { get { return createdateraw == null ? "" : createdateraw.ToString(DataFactory.DATE_FORMAT_MONTH_TIME24, DataFactory.CULTURE_INFO); } }
        public string modify_date { get { return modify_dateraw == null ? "" : ((DateTime)modify_dateraw).ToString(DataFactory.DATE_FORMAT_MONTH_TIME24, DataFactory.CULTURE_INFO); } }
    }
    public class WebSubmitSystemMessage
    {
        public Guid id { get; set; }
        public int active { get; set; }     
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
}