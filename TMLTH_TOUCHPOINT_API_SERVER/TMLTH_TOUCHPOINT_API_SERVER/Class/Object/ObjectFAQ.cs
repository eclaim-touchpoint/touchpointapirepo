﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class.Object
{
    public class WebRequestFAQ
    {
        public int id { get; set; }
        public string admin { get; set; }
        public string ip_client { get; set; }
        public SubmitDevice device { get; set; }
    }

    public class WebResponseFAQ
    {
        public int id { get; set; }
        public int faq_category_id { get; set; }
        public int faq_no { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
    }

    public class WebResponseFAQAll
    {

        public int id { get; set; }
        public int faq_category_id { get; set; }
        public int faq_no { get; set; }
        internal string questionRaw { get; set; }
        public string question { get { return !string.IsNullOrWhiteSpace(questionRaw) ? string.Join("", (from x in questionRaw select x).Skip(0).Take(150).Select(x => x)) : ""; } }
    }

    public class WebResponseFAQSearchDataTable
    {
       
        public int id { get; set; }       
        public int no { get; set; }
        public int category_no { get; set; }
        public string name{ get; set; }
        public string description { get; set; }



    }
    public class WebSubmitCatagoryFAQ
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int no { get; set; }
        public int active { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebSubmitFAQ
    {
        public int id { get; set; }
        public int category_id {get; set;}
        public string question { get; set; }
        public string answer { get; set; }
        public int faq_no { get; set; }
        public int active { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class WebSubmitFAQSorting
    {       
        public List<ListFAQ> list_question { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string nameth { get; set; }
        public int active { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class ListFAQ
    {
        public int id { get; set; }   
        public int faq_no { get; set; }
    }
    public class WebSubmitCatagoryFAQSorting
    {
        public List<ListCatagoryFAQ> list_category { get; set; }
        public string ip_client { get; set; }
        public string admin { get; set; }
        public SubmitDevice device { get; set; }
    }
    public class ListCatagoryFAQ
    {
        public int id { get; set; }
        public int no { get; set; }
    }
}