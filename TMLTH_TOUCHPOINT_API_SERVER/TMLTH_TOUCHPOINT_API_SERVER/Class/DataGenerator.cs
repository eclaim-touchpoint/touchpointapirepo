﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using TMLTH_TOUCHPOINT_API_SERVER.Class;


namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    public class DataGenerator
    {
        internal static HttpResponseMessage generateTextResult(string data)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(data,Encoding.UTF8, "text/plain");
            return response;
        }

        internal static HttpResponseMessage generateJsonResult(object data)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(JsonConvert.SerializeObject(data),Encoding.UTF8, "text/plain");
            return response;
        }

        internal static string generateSystemAccountCookies(string token)/*string account_id, int account_department,*//* string username,string name,int hasimguser,string ipaddress, int day_of_year)*/
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(token);

            return EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_SIGNIN_KEY);
        }
    }
}