﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using TMLTH_TOUCHPOINT_API_SERVER.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    public class DataValidation
    {
        /**
         * return 1 = success
         * return 0 = no cookie
         * return -1 = cookie expire
         * return -2 = error
         */
        internal static CookieResult validateSystemAccountCookie(string token, string ip_client)
        {
            CookieResult cookieResult = new CookieResult();
            DateTime now = DateTime.Now;
            if (!string.IsNullOrWhiteSpace(token))
            {
                #region ReadToken and Set Variable
                // string DecryptToken = EncryptDecryptAlgorithm.DecryptData(token, DataFactory.ENC_SIGNIN_KEY);
                var handler = new JwtSecurityTokenHandler();
                var jwtToken = handler.ReadToken(token) as JwtSecurityToken;
                var userid = jwtToken.Claims.First(Claim => Claim.Type == "AccountID").Value;
                var username = jwtToken.Claims.First(Claim => Claim.Type == "AccountUsername").Value;
                var name = jwtToken.Claims.First(Claim => Claim.Type == "AccountName").Value;
                var isapp = jwtToken.Claims.First(claim => claim.Type == "Isapp").Value;
                var HasImageUrl = jwtToken.Claims.First(claim => claim.Type == "HasImageUrl").Value;
                var t_ip_client = jwtToken.Claims.First(Claim => Claim.Type == "IPClient").Value;
                var Dayofyear = jwtToken.Claims.First(Claim => Claim.Type == "DayofYear").Value;
                var RoleID = jwtToken.Claims.First(Claim => Claim.Type == "RoleId").Value;
                var Session = jwtToken.Claims.First(Claim => Claim.Type == "Session").Value;
                var Exp = long.Parse(jwtToken.Claims.First(Claim => Claim.Type == "exp").Value);
                var key = Encoding.UTF8.GetBytes(DataFactory.SECURITY_KEY);
                
                #endregion

                #region Set system_account_data
                string[] system_account_data = new string[8];
                system_account_data[0] = userid;
                system_account_data[1] = username;
                system_account_data[2] = name;
                system_account_data[3] = t_ip_client;
                system_account_data[4] = isapp;
                system_account_data[5] = Dayofyear;
                system_account_data[6] = RoleID;
                system_account_data[7] = HasImageUrl;
                #endregion
                if (!DataValidation.isSystemAccountIDValid(system_account_data, ip_client))
                {
                    //if (ip_client != t_ip_client)
                    //{
                    //    cookieResult.RESULT = ResultData.CODE_COOKIE_RESULT_IP_NOTSAME_ERROR;
                    //    return cookieResult;
                    //}

                    cookieResult.RESULT = -600;
                    return cookieResult;
                }

                cookieResult.IS_APP = int.Parse(isapp);
                //*** validate account
                if (isapp == "1")
                {
                    DateTime Expiration = DataFactory.FromUnixTime(Exp);
                    //======================CHECK TEST====================
                    var newSession = "";
                    DateTime exp = DateTime.Now;
                    if (DataFactory.IS_TOKEN_DEBUG.Equals("1"))
                    {
                        newSession = DateTime.Now.AddMinutes(15).ToString();
                        exp = exp.AddHours(4);
                    }
                    else
                    {
                        newSession = DateTime.Now.AddMinutes(15).ToString();
                        exp = exp.AddHours(4);
                    }
                    //=====================================================

                    //GENERATE NEW TOKEN
                    SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim("AccountID", userid),
                            new Claim("AccountUsername", username),
                            new Claim("AccountName", name),
                            new Claim("IPClient",   ip_client),
                            new Claim("Isapp",  isapp),
                            new Claim("DayofYear", Dayofyear),
                            new Claim("RoleId",RoleID),
                            new Claim("Session", newSession),
                            new Claim("HasImageUrl", HasImageUrl)
                        }),
                        Expires = exp,
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var createtoken = handler.CreateToken(tokenDescriptor);
                    var newtoken = handler.WriteToken(createtoken);

                    if (Expiration.Ticks < now.Ticks)
                    {
                        //ERROR TOKEN TIMEOUT
                        cookieResult.RESULT = ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR;
                        cookieResult.ID = Convert.ToInt32(userid);
                        cookieResult.USERNAME = username;
                        cookieResult.NAME = name;
                        cookieResult.ROLE_ID = Convert.ToInt32(RoleID);
                        cookieResult.IP = system_account_data[4];
                        cookieResult.IS_BLANK = 0;
                        cookieResult.NEW_TOKEN = newtoken;
                        return cookieResult;
                    }
                    else
                    {
                        cookieResult.NEW_TOKEN = newtoken;
                        var Checksession = Convert.ToDateTime(Session);
                        if (Checksession.Ticks < now.Ticks)
                        {
                            //ERROR SESSTION TIMEOUT
                            cookieResult.RESULT = ResultData.CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR;
                            cookieResult.ID = Convert.ToInt32(userid);
                            cookieResult.USERNAME = username;
                            cookieResult.NAME = name;
                            cookieResult.ROLE_ID = Convert.ToInt32(RoleID);
                            cookieResult.IP = system_account_data[4];
                            cookieResult.IS_BLANK = 0;
                            return cookieResult;
                        }
                            
                    }
                }

                //*** insert value obj
                cookieResult.RESULT = ResultData.CODE_COOKIE_RESULT_ISVALID;
                cookieResult.ID = Convert.ToInt32(userid);
                cookieResult.USERNAME = username;
                cookieResult.NAME = name;
                cookieResult.ROLE_ID = Convert.ToInt32(RoleID);
                cookieResult.IP = system_account_data[4];
                cookieResult.IS_BLANK = 0;
                return cookieResult;

                /*var issuerKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(DataFactory.SECURITY_KEY));
                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = issuerKey,
                };
                
                try
                {
                    // ****** Check Token     
                    SecurityToken securityToken = null;
                    ClaimsPrincipal Principal = handler.ValidateToken(token, validationParameters, out securityToken);
                    
                    //*** insert value obj
                    cookieResult.RESULT = ResultData.CODE_COOKIE_RESULT_ISVALID;
                    cookieResult.ID = Convert.ToInt32(userid);
                    cookieResult.USERNAME = username;
                    cookieResult.NAME = name;
                    cookieResult.ROLE_ID = Convert.ToInt32(RoleID);
                    cookieResult.IP = system_account_data[4];

                    return cookieResult;
                }
                catch (Exception e)
                {
                    cookieResult.RESULT = ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED;
                    return cookieResult;
                }*/
            }
            else
            {
                cookieResult.RESULT = 0;
                cookieResult.IS_BLANK = 1;
                return cookieResult;
            }
        }
        internal static string GetCookieErrorMesseage (int result)
        {
            if (result == ResultData.CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR)
            {
                return ResultData.CODE_SESSION_TIMEOUT + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR;
            }
            else if (result == ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR)
            {
                return ResultData.CODE_TOKEN_EXPIRE + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR;
            }
            else if (result == ResultData.CODE_COOKIE_RESULT_IP_NOTSAME_ERROR)
            {
                return ResultData.CODE_IP_CHANGE + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_IP_NOTSAME_ERROR;
            }
            else
            {
                return ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED;
            }
        }

        internal static bool isSystemAccountIDValid(string[] system_account_data,string ip_client)
        {
            if (system_account_data.Length != 8)
            {
                return false;
            }

            //*** validate account user id
            if (!Regex.IsMatch(system_account_data[0], "^(0*[1-9][0-9]*(\".[0-9]+)?|0+\".[0-9]*[1-9][0-9]*)$"))
            {
                return false;
            }
            //*** validate role id
            //if (!Regex.IsMatch(system_account_data[1], "^(0*[1-9][0-9]*(\".[0-9]+)?|0+\".[0-9]*[1-9][0-9]*)$"))
            //{
            //    return false;
            //}
            //*** Comment Username
            //*** validate ip
            if (system_account_data[4] != "1")
            {
                if (!IsIPv4(system_account_data[3]))
                {
                    return false;
                }
                if (!ip_client.Equals(system_account_data[3]))
                {
                    return false;
                }
            }
            //*** validate day
            if (!Regex.IsMatch(system_account_data[5], "[0-9]{3}"))
            {
                return false;
            }
            int day = int.Parse(system_account_data[5]);
            if (!DataValidation.isDateValid(day))
            {
                return false;
            }
            return true;
        }

        internal static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        internal static string generateRandomSring(int len)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, len).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        internal static bool isDateValid(int date)
        {
            if (date + 30 >= DateTime.Now.DayOfYear)
            {
                return true;
            }
            return false;
        }

        internal static bool IsIPv4(string value)
        {
            var quads = value.Split('.');

            //*** if we do not have 4 quads, return false
            if (!(quads.Length == 4)) return false;

            //*** for each quad
            foreach (var quad in quads)
            {
                int q;
                if (!Int32.TryParse(quad, out q)
                    || !q.ToString().Length.Equals(quad.Length)
                    || q < 0
                    || q > 255) { return false; }
            }
            return true;
        }
    }
}