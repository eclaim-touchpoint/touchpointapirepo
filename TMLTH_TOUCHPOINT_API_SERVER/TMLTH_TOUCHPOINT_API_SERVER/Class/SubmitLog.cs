﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Class
{
    public class SubmitLog
    {

        internal static bool ACTION_LOG(webapp_mobileEntities tmlth, 
            string clntnum, int? system_account_id, string feature,string controller_name, string detail, SubmitDevice device)
        {
            try
            {
                var submit_log = tmlth.SP_TP_SYSTEM_LOG_ACTION(clntnum, system_account_id, feature,controller_name,detail,
                    device.application_id,device.application_code,
                    device.mobile_manufacturer,device.mobile_model,device.mobile_os_name,device.mobile_os_version);
                return true;
            }
            catch (Exception e)
            {
                ContentTools.ErrorControllerLog("ERROR_SUBMITLOG:" + feature + ":" + e.Message);
                return false;
            }
        }
        public class DeviceInfo
        {
            public string application_id { get { return ""; } }
            public string application_code { get { return ""; } }
            public string mobile_manufacturer { get { return "BLUEWIND SOLUTION"; } }
            public string mobile_model { get { return "TOUCHPOINT_API_WEBSITE"; } }
            public string mobile_os_name { get { return "WEBSITE"; } }
            public string mobile_os_version { get { return ""; } }
        }
    }

}