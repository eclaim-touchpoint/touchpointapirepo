﻿using FluentFTP;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;
using WinSCP;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class WebRequestFileFTPServerController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestFileFTPServer";
            bool production = DataFactory.IS_PRODUCTION;
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestFileFTPServer request_data = JsonConvert.DeserializeObject<WebRequestFileFTPServer>(decryptData);
                StringBuilder sb = new StringBuilder();

                try
                {
                    #region validateCookie
                    CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "FTP",
                    this.GetType().Name,
                    (
                        "DOCFULLNAME" + "=" + request_data.docfullname
                    )
                    ,
                    request_data.device);
                    var docnameArray = request_data.docfullname.Split('\\');
                    string docname = docnameArray[docnameArray.Length - 1];
                    string path = "";
                    #region FTP_CONNECT                  
                    try
                    {
                        sb.Append("{");
                        string url = "";
                        if (production)
                        {
                            url = DataFactory.FTP_SEVER_NAME + "/" + request_data.docfullname.Replace("\\", "/");
                        }
                        else
                        {
                            url = DataFactory.FTP_SEVER_NAME + "\\LETTER\\POS014\\TEST_FTP.pdf".Replace("\\", "/");
                        }
                        FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create(url);
                        ftpRequest.Credentials = new NetworkCredential(DataFactory.FTP_USER, DataFactory.FTP_PASSWORD);
                        ftpRequest.UseBinary = true;
                        ftpRequest.UsePassive = true;
                        ftpRequest.KeepAlive = true;
                        ftpRequest.EnableSsl = true;
                        try {
                            System.Security.Cryptography.X509Certificates.X509Certificate x509 = new System.Security.Cryptography.X509Certificates.X509Certificate(DataFactory.FTP_PATH_CER);
                            ftpRequest.ClientCertificates.Add(x509);
                        } catch (Exception e) {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||1:" + url + "||" + e);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_CANT_ADD_CERTIFICATE);
                        }

                        FtpWebResponse ftpResponse = null;

                        try
                        {
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                            ServicePointManager.ServerCertificateValidationCallback = ServerCertificateValidationCallback;
                            ServicePointManager.Expect100Continue = true;

                            ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                            ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                        }
                        catch(Exception e)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||2:" + url + "||" + e);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_CANT_DOWLOAD);
                        }

                        int bufferSize = 2048;
                        byte[] byteBuffer = new byte[bufferSize];
                        string localPath = DataFactory.FOLDER_TOUCHPOINT_CONTENT_UPLOAD + "\\client\\" + cookieData.ID + "\\";
                        try
                        {
                            if (Directory.Exists(localPath) == false)
                            {
                                new FileInfo(localPath).Directory.Create();
                            }
                        } 
                        catch (Exception e)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||3:" + url + "||" + e);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_CANT_CREATE_FOLDER);
                        }
                        string localFile = localPath + docname;
                        path = "client/" + cookieData.ID + "/" + docname;
                        try
                        {
                            System.IO.Stream ftpStream = ftpResponse.GetResponseStream();
                            // Open a File Stream to Write the Downloaded File 
                            System.IO.FileStream localFileStream = new System.IO.FileStream(localFile, System.IO.FileMode.Create);
                            // Buffer for the Downloaded Data 
                            //byte[] x = StreamToByteArray(ftpStream); NOTE: Don't Remove this line this is line to parse file in stream to byte array
                            int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                            // Download the File by Writing the Buffered Data Until the Transfer is Complete 
                            
                                while (bytesRead > 0) {
                                    localFileStream.Write(byteBuffer, 0, bytesRead);
                                    bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                                }
                          
                            localFileStream.Close();
                        } catch (Exception e) {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||4:" + docname + "||" + e);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_CANT_CREATE_FILE_FROM_BYTE);
                        }

                        try
                        {
                            if (System.IO.File.Exists(localFile))
                            {
                                byte[] fileBytes = System.IO.File.ReadAllBytes(localFile);
                                string base64String = Convert.ToBase64String(fileBytes);
                                sb.Append("\"path\":");
                                sb.Append(JsonConvert.SerializeObject(path));
                                sb.Append(",\"base64\":");
                                sb.Append(JsonConvert.SerializeObject(base64String));
                            } else
                            {
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_NOTFOUND);
                            }
                        }
                        catch (Exception e)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||5:" + docname + "||" + e);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_CANT_CONVERT_FILE_TO_BASE64);
                        }

                    }
                    catch (Exception e)
                    {
                        var errorlog = ContentTools.ErrorControllerLog(page + "||6:" + docname + "||" + e);
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_CONNECT_FTP_FAIL);
                    }
                    
                    #endregion
                 
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||7:||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }

        }

        public static byte[] StreamToByteArray(System.IO.Stream stream)
        {
            if (stream is System.IO.MemoryStream)
            {
                return ((System.IO.MemoryStream)stream).ToArray();
            }
            else
            {
                // Jon Skeet's accepted answer 
                return ReadFully(stream);
            }
        }

        public static byte[] ReadFully(System.IO.Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        void OnValidateCertificate(FtpClient control, FtpSslValidationEventArgs e)
        {
            // add logic to test if certificate is valid here
            e.Accept = true;
        }
        private bool ServerCertificateValidationCallback(object sender,
                                                X509Certificate certificate,
                                                X509Chain chain,
                                                SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}
