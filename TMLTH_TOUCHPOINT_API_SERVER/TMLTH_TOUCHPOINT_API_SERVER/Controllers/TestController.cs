﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class TestController : ApiController
    {

        public string Get()
        {
            ContentTools.ErrorControllerLog("===EWI_START===");
            try
            {
                var client = new HttpClient();
                var response = client.PostAsJsonAsync(DataFactory.URL_TOUCHPOINT_EWI,
                    EncryptDecryptAlgorithm.EncryptData(JsonConvert.SerializeObject(new Object[1]{
                    new {
                        chdrnum = "11073217"
                    }}), DataFactory.ENC_TOUCHPOINT_KEY)).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var errorlog = ContentTools.ErrorControllerLog("EWI_RESULT:" + result);
                    if (result.StartsWith(ResultData.CODE_ERROR))
                    {
                        string[] error = result.Split(':');
                        return error[1];
                    }
                    else if (result.StartsWith(ResultData.CODE_NOTFOUND) ||
                             result.StartsWith(ResultData.CODE_BLOCKED) ||
                             result.StartsWith(ResultData.CODE_SESSION_TIMEOUT) ||
                             result.StartsWith(ResultData.CODE_TOKEN_DENIED) ||
                             result.StartsWith(ResultData.CODE_TOKEN_EXPIRE))
                    {
                        return ResultData.CODE_ERROR + result;
                    }
                    else
                    {
                        result = EncryptDecryptAlgorithm.DecryptData(response.Content.ReadAsStringAsync().Result, DataFactory.ENC_TOUCHPOINT_KEY);
                        return result;
                    }
                }
                else
                {
                    var errorlog = ContentTools.ErrorControllerLog("EWI_RESULT:" + "ERROR");
                    return "ERROR";
                }
            } catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog("EWI_RESULT_ERROR_EXCEPTION:" + e);
                return "ERROR EXCEPTION: " + e;
            }
        }

    }
}
