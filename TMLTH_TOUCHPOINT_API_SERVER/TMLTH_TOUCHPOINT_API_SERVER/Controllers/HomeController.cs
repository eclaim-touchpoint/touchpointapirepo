﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using System.Text;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class HomeController : Controller
    {
        public string Index()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html>");
            sb.Append("<body style='font-family: sans-serif; ' ><div style='font-size:18px; font-weight: 400; padding:10px 5px; border: 1px solid; '>");
            sb.Append("<b style='color:#000000;'>TMLTH TOUCHPOINT API: API Server</b><br/>");
            sb.Append("Server Version: " + DataFactory.SERVER_API_NAME + "<br/>");
            sb.Append("Server Status: <b style='color:#008800;'>OK</b><br/>");
            sb.Append("Database Version: " + DataFactory.DATABASE_VERSION + "<br/>");
            sb.Append("Server Time: " + DateTime.Now);
            sb.Append("<span style='float:right; font-size:14px; font-weight: 700; color:#888;'>Last Updated: " + DataFactory.SERVER_API_DATE + "</span><br/>");
            sb.Append("</div>");
            sb.Append("<div style='background: #333; color:#fff; font-size:12px; padding:10px 5px; text-align: right;'>");
            sb.Append("Developed by <b>Bluewind Solution Co., Ltd.</b><br/>");
            sb.Append("If server failed or has any problem, please contact us<br/>");
            sb.Append("telephone: (+66)2-108-4693, email: support@bluewindsolution.com<br/>");
            sb.Append("website: <a style='color:#fff;' href='http://www.bluewindsolution.com/'>http://www.bluewindsolution.com/</a>");
            sb.Append("</div></body></html>");

            return sb.ToString();
        }
    }
}
