﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Hospital
{
    public class WebRequestHospitalProvinceController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestHospitalProvince";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestHospitalProvince request_data = JsonConvert.DeserializeObject<WebRequestHospitalProvince>(decryptData);
                StringBuilder sb = new StringBuilder();


                try
                {
                    // This Feature not require Login
                    //CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    //if (cookieData.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_SEEM_TO_HACK, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    //TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    //if (tokendata.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    SubmitLog.ACTION_LOG(tmlth,
                        null,
                        null,
                        "Hospital",
                        this.GetType().Name,
                        "",
                        request_data.device);

                    #region start query(fetch) data from db
                    List <WebResponseHospitalProvince> hospitalp = tmlth.SP_TP_GET_HOSPITAL_PROVINCE().Select( x=> new WebResponseHospitalProvince {
                        province_th = x.PROVINCE_TH,
                        province_en = x.PROVINCE_EN,
                    }).ToList<WebResponseHospitalProvince>();
                    if (!hospitalp.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }

                    sb.Append("{");
                    //sb.Append("\"token\":");
                    //sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append("\"hospitalprovince\":");
                    sb.Append(JsonConvert.SerializeObject(hospitalp));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));

                    #endregion

                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
