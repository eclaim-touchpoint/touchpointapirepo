﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Hospital
{
    public class WebRequestSearchHospitalController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestSearchHospital";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            StringBuilder sb = new StringBuilder();
            WebRequestSearchHospital request_data = JsonConvert.DeserializeObject<WebRequestSearchHospital>(decryptData);

            try
            {
                //CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                //if (cookieData.RESULT != 1)
                //{
                //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                //        ResultData.CODE_ERROR_SEEM_TO_HACK, DataFactory.ENC_TOUCHPOINT_KEY));
                //}
                //TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                //if (tokendata.RESULT != 1)
                //{
                //    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                //}
                int skip = request_data.page * 10;
                string province = request_data.province.ToLower();
                string name = request_data.name.ToLower();
                sb.Append("{");

                SubmitLog.ACTION_LOG(tmlth,
                    null,
                    null,
                    "Hospital",
                    this.GetType().Name,
                    (
                        "TYPESEARCH" + "=" + request_data.typesearch + "," +
                        "LNG" + "=" + request_data.lng + "," +
                        "PROVINCE" + "=" + request_data.province + "," +
                        "NAME" + "=" + request_data.name + "," +
                        "LATITUDE" + "=" + request_data.latitude + "," +
                        "LONGITUDE" + "=" + request_data.longitude + "," +
                        "PAGE" + "=" + request_data.page
                    ),
                    request_data.device);
                //sb.Append("\"token\":");
                //sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                if (request_data.typesearch == 1)//Find nearby
                {
                    var hospitaldata = (from _hos in tmlth.tHospital
                                        where (_hos.ipdindividual.Value || _hos.opdpaindividual.Value) && _hos.type == "Hospital"
                                        select new WebResponseSearchHospital
                                        {
                                            id = _hos.id,
                                            thname = _hos.thname,
                                            engname = _hos.engname,
                                            thaddress = _hos.thaddress,
                                            engaddress = _hos.engaddress,
                                            thtumbon = _hos.thtumbon,
                                            engtumbon = _hos.engtumbon,
                                            thcity = _hos.thcity,
                                            engcity = _hos.engcity,
                                            thlocation = _hos.thlocation,
                                            englocation = _hos.englocation,
                                            postcode = _hos.postcode,
                                            tel = _hos.tel,
                                            fax = _hos.fax,
                                            type = _hos.type,
                                            _latRaw = _hos.lat,
                                            _longRaw = _hos.@long
                                        }).AsEnumerable();
                    var coord = new  GeoCoordinate(request_data.latitude ?? 0, request_data.longitude ?? 0);
                    var nearbyResult = (from _h in hospitaldata
                                        let geo = new GeoCoordinate { Latitude = Convert.ToDouble(_h._latRaw), Longitude = Convert.ToDouble(_h._longRaw) }
                                        orderby geo.GetDistanceTo(coord)
                                        let distance = geo.GetDistanceTo(coord)
                                        where distance <= 5000
                                        select _h).AsQueryable();

                    sb.Append("\"searchresult\":");
                    sb.Append(JsonConvert.SerializeObject(nearbyResult.OrderBy(x => x.id).Skip(skip).Take(10)));
                    sb.Append(",\"totalResult\":");
                    sb.Append(JsonConvert.SerializeObject(nearbyResult.Count()));
                }
                else if (request_data.typesearch == 2)//Find just hospital in province
                {
                    List<WebResponseSearchHospital> hospital = tmlth.SP_TP_GET_HOSPITAL(request_data.province, request_data.lng).Select(x => new WebResponseSearchHospital
                    {
                        id = x.ID,
                        thname = x.NAME,
                        thaddress = x.ADDRESS,
                        thtumbon = x.TUMBON,
                        thcity = x.CITY,
                        thlocation = x.LOCATION,
                        englocation = x.LOCATION,
                        postcode = x.POSTCODE,
                        tel = x.TEL,
                        fax = x.FAX,
                        type = x.TYPE,
                        _latRaw = x.LAT,
                        _longRaw = x.LONG,
                    }).ToList<WebResponseSearchHospital>();

                    sb.Append("\"searchresult\":");
                    sb.Append(JsonConvert.SerializeObject(hospital.OrderBy(x => x.id).Skip(skip).Take(10)));
                    sb.Append(",\"totalResult\":");
                    sb.Append(JsonConvert.SerializeObject(hospital.Count()));
                }
                else if(request_data.typesearch == 3)// Find hospital same name
                {
                    var hospitalname = (from _hos in tmlth.tHospital
                                        where (_hos.ipdindividual.Value || _hos.opdpaindividual.Value) && _hos.type == "Hospital" && (_hos.thname.Contains(request_data.name) || _hos.engname.ToLower().Contains(name))
                                        select new WebResponseSearchHospital
                                        {
                                            id = _hos.id,
                                            thname = _hos.thname,
                                            engname = _hos.engname,
                                            thaddress = _hos.thaddress,
                                            engaddress = _hos.engaddress,
                                            thtumbon = _hos.thtumbon,
                                            engtumbon = _hos.engtumbon,
                                            thcity = _hos.thcity,
                                            engcity = _hos.engcity,
                                            thlocation = _hos.thlocation,
                                            englocation = _hos.englocation,
                                            postcode = _hos.postcode,
                                            tel = _hos.tel,
                                            fax = _hos.fax,
                                            type = _hos.type,
                                            _latRaw = _hos.lat,
                                            _longRaw = _hos.@long
                                        });
                    sb.Append("\"searchresult\":");
                    sb.Append(JsonConvert.SerializeObject(hospitalname.OrderBy(x => x.id).Skip(skip).Take(10)));
                    sb.Append(",\"totalResult\":");
                    sb.Append(JsonConvert.SerializeObject(hospitalname.Count()));
                }
                else if (request_data.typesearch == 4)// Find clinic same name
                {
                    var clinicname = (from _hos in tmlth.tHospital
                                       let _eng_name_low = (_hos.engname+"").Trim().ToLower()
                                       let _th_name = (_hos.thname+"").Trim()
                                      where (_hos.ipdindividual.Value || _hos.opdindividual.Value) &&
                                        (_hos.type.Equals("Clinic")) && 
                                        (_th_name.Contains(request_data.name) || _eng_name_low.Contains(name))
                                        select new WebResponseSearchHospital
                                        {
                                            id = _hos.id,
                                            thname = _hos.thname,
                                            engname = _hos.engname,
                                            thaddress = _hos.thaddress,
                                            engaddress = _hos.engaddress,
                                            thtumbon = _hos.thtumbon,
                                            engtumbon = _hos.engtumbon,
                                            thcity = _hos.thcity,
                                            engcity = _hos.engcity,
                                            thlocation = _hos.thlocation,
                                            englocation = _hos.englocation,
                                            postcode = _hos.postcode,
                                            tel = _hos.tel,
                                            fax = _hos.fax,
                                            type = _hos.type,
                                            _latRaw = _hos.lat,
                                            _longRaw = _hos.@long
                                        });
                    sb.Append("\"searchresult\":");
                    sb.Append(JsonConvert.SerializeObject(clinicname.OrderBy(x => x.id).Skip(skip).OrderBy(x => x.id).Skip(skip).Take(10)));
                    sb.Append(",\"totalResult\":");
                    sb.Append(JsonConvert.SerializeObject(clinicname.Count()));
                }
                else
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_DATA_WRONG);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_DATA_WRONG);
                }
                sb.Append("}");
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT + ":" + e.Message);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }
}
