﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Hospital
{
    public class WebRequestHospitalController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestHospital";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                StringBuilder sb = new StringBuilder();
                WebRequestHospital request_data = JsonConvert.DeserializeObject<WebRequestHospital>(decryptData);

                try
                {
                    /*CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_SESSION_TIMEOUT + ResultData.CODE_ERROR_TOUCHPOINT);
                        }
                        else if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR + ResultData.CODE_ERROR_TOUCHPOINT);
                        }
                        else
                        {
                            var errorlog = ContentTools.ErrorControllerLog("WebRequestHospital" + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_SEEM_TO_HACK);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_SEEM_TO_HACK);
                        }
                        #endregion
                    }
                    #endregion*/

                    //TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    //if (tokendata.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    SubmitLog.ACTION_LOG(tmlth,
                        null,
                        null,
                        "Hospital",
                        this.GetType().Name,
                        (
                        "LNG" + "=" + request_data.lng + "," +
                        "PROVINCE" + "=" + request_data.province + "," +
                        "PAGE" + "=" + request_data.page
                        ),
                        request_data.device);

                    int skip = request_data.page * 10;
                    List<WebResponseHospital> hospital = tmlth.SP_TP_GET_HOSPITAL(request_data.province, request_data.lng).Select(x => new WebResponseHospital
                    {
                        id = x.ID.ToString(),
                        name = x.NAME,
                        address = x.ADDRESS,
                        tumbon = x.TUMBON,
                        city = x.CITY,
                        location =x.LOCATION,
                        postcode = x.POSTCODE,
                        tel = x.TEL,
                        fax = x.FAX,
                        type = x.TYPE,
                        ipdgroup = x.IPDGROUP,
                        opdgroup = x.OPDGROUP,
                        dentalgroup = x.DENTALGROUP,
                        ipdindividual = x.IPDINDIVIDUAL,
                        opdindividual = x.OPDINDIVIDUAL,
                        opdpaindividual = x.OPDPAINDIVIDUAL,
                        medicalcenter = x.MEDICALCENTER,
                        lat = x.LAT,
                        _long = x.LONG,
                    }).Skip(skip).Take(10).ToList<WebResponseHospital>();
                   
                  
                    if (!hospital.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    sb.Append("{");
                    //sb.Append("\"token\":");
                    //sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"hospital\":");                    
                    sb.Append(JsonConvert.SerializeObject(hospital));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||"  + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
         
        }
    }
}

