﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.Agreement
{
    public class WebSubmitAgreementLogController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitAgreementLog";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebSubmitAgreementLog submit_data = JsonConvert.DeserializeObject<WebSubmitAgreementLog[]>(decryptData)[0];
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);
                StringBuilder sb = new StringBuilder();
                DateTime now = DateTime.Now;
                try
                {
                    #region validateCookie
                    //if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    //{
                    //    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                    //}
                    #endregion
                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                        "Agreement",
                        this.GetType().Name,
                        (
                        "AGREEMENT_ID" + "=" + submit_data.id + "," +
                        "CLNTNUM" + "=" + cookieData.ID
                        ),
                        submit_data.device);
                    if (submit_data.id > 0)
                    {
                        var agreement = (from _ver in tmlth.TP_AGREEMENT
                                         where _ver.ID == submit_data.id
                                         select _ver);
                        if (agreement == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        TP_AGEEMENT_LOG newagreementlog = new TP_AGEEMENT_LOG()
                        {
                            AGREEMENT_ID = submit_data.id,
                            CLNTNUM = cookieData.ID.ToString(),
                            CONFIRM_DATE = now,
                            APPLICATION_ID = submit_data.device.application_id,
                            APPLICATION_CODE = submit_data.device.application_code,
                            MOBILE_MANUFACTURER = submit_data.device.mobile_manufacturer,
                            MOBILE_MODEL = submit_data.device.mobile_model,
                            MOBILE_OS_NAME = submit_data.device.mobile_os_name,
                            MOBILE_OS_VERSION = submit_data.device.mobile_os_version,
                        };
                        tmlth.TP_AGEEMENT_LOG.AddObject(newagreementlog);
                        tmlth.SaveChanges();
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                    }
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }



        }
    }
}
