﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Accesskey
{
    public class WebRequestAccessKeyController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestAccessKey";
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page+ "||"  + ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestAccesskey request_data = JsonConvert.DeserializeObject<WebRequestAccesskey>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        var errorlog = ContentTools.ErrorControllerLog("WebRequestAccessKey" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        #endregion
                    }
                    #endregion
                    /*TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    if (tokendata.RESULT != 1)
                    {
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    }*/
                    #region start query(fetch) data from db
                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1? cookieData.ID.ToString() : null ,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Accesskey", 
                        this.GetType().Name,
                        (
                        "APP_NME" + "=" + request_data.App_NME + "," +
                        "APP_TYPE" + "=" + request_data.App_Type + "," +
                        "SERVICE_NME" + "=" + request_data.Service_NME
                        ),
                        request_data.device);

                    WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY(request_data.App_NME, "", "").Select(x => new WebReponseAccesskey
                    {
                        id = x.REQUEST_ID,
                        access_key = x.REQUEST_ACCESS_KEY,
                        profile_id = x.REQUEST_PROFILE_ID,
                        app_nme = x.REQUEST_APP_NME,
                        app_type = x.REQUEST_APP_TYPE,
                        service_nme = x.REQUEST_SERVICE_NME,
                    }).FirstOrDefault();
                 
                    if (Accesskey == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    //WebReponseAccesskey q = JsonConvert.DeserializeObject<WebReponseAccesskey>(Accesskey.ToString());
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"accesskey\":");
                    sb.Append(JsonConvert.SerializeObject(Accesskey));
                    sb.Append("}");
                  //  WebResponsBankDetail request_data = JsonConvert.DeserializeObject<WebRequestBankDetail>(decryptData); 
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion

                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog( page + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog( page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}




            

