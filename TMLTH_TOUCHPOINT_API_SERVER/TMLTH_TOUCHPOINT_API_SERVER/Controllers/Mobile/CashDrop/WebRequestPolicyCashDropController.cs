﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.CashDrop
{
    public class WebRequestPolicyCashDropController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPolicyCashDrop";
            if (ModelState.IsValid)
            {
               
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestCashDrop request_data = JsonConvert.DeserializeObject<WebRequestCashDrop>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                       
                        var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "CashDrop",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum
                        ),
                        request_data.device);

                    #region start query(fetch) data from db
                    //*********DATA TEST*************
                    List<WebResponseCashDrop> policycashdrop = tmlth.SP_TP_GET_POLICY_CASH_DROP(request_data.chdrnum).Select(x => new WebResponseCashDrop
                    {
                        policy_no = x.POLICY_NO,
                        due_dateRaw = x.DUE_DATE,
                        pay_dateRaw = x.PAY_DATE,
                        amount = x.PREM_AMOUNT,
                        loan_amount = x.LOAN_AMOUNT,
                        paid_amount = x.PAID_AMOUNT,
                        total_amount = x.TOTAL_AMOUNT,
                        pay_method = x.PAY_METHOD,
                        cheq_no = x.CHEQ_NO,
                        bank_name = x.BANK_NAME,
                        bank_acckey = x.BANK_ACCKEY,
                    }).ToList<WebResponseCashDrop>();
                    
                    WebResponseCashDropNext policycashdropnext = tmlth.SP_TP_GET_POLICY_CASH_DROP_NEXT(request_data.chdrnum).Select(x => new WebResponseCashDropNext
                    {
                        policy_no = x.POLICY_NO,
                        due_dateRaw = x.DUE_DATE,
                        pay_type = x.PAY_TYPE,
                        bank_name = x.BANK_NAME,
                        bank_acckey = x.BANK_ACCKEY
                    }).FirstOrDefault();

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"policycashdrop\":");
                    sb.Append(JsonConvert.SerializeObject(policycashdrop));
                    sb.Append(",\"policycashdropnext\":");
                    sb.Append(JsonConvert.SerializeObject(policycashdropnext));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog( page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
