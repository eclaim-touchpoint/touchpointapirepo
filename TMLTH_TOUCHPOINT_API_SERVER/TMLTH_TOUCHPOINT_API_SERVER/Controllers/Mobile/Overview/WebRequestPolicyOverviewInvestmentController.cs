﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using Newtonsoft.Json.Linq;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Claims
{
    public class WebRequestPolicyOverviewInvestmentController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPolicyOverviewInvestment";
            if (ModelState.IsValid)
            {
               // var decryptData = data;
               string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestInvestmentOverview request_data = JsonConvert.DeserializeObject<WebRequestInvestmentOverview>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        
                            var errorlog = ContentTools.ErrorControllerLog("WebRequestPolicyOverviewInvestment" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Overview",
                        this.GetType().Name,
                        (
                        "CLNTNUM_INS" + "=" + request_data.clntnum_ins
                        ),
                        request_data.device);
                    TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    if (tokendata.RESULT != 1)
                    {
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    }

                    List<WebResponseInvestmentOverview> investmentOverviews = tmlth.SP_TP_GET_INVESTMENT_OVERVIEW(request_data.clntnum_ins).Select(x => new WebResponseInvestmentOverview
                    {
                        unit_virtual_fund = x.UNIT_VIRTUAL_FUND,
                        long_desc = x.LONG_DESC,
                        fund_house = x.FUND_HOUSE,
                        policy_no = x.POLICY_NO,
                        eff_date = x.EFF_DATE,
                        tunavpr = x.TUNAVPR,
                        current_unit_bal = x.CURRENT_UNIT_BAL,
                        fund_value = x.FUND_VALUE,
                        statusdescth = x.STATUS_DESC_TH

                    }).ToList<WebResponseInvestmentOverview>();

                    var investment_unit = investmentOverviews.GroupBy(x => x.unit_virtual_fund);
                    var suminvestment = investmentOverviews.Sum(x => x.fund_value);
                  
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"investmentoverviews\":");
                    sb.Append(JsonConvert.SerializeObject(investment_unit));
                    sb.Append(",\"suminvestment\":");
                    sb.Append(JsonConvert.SerializeObject(suminvestment));
                    sb.Append("}");

                    //return DataGenerator.generateTextResult(sb.ToString());
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
    }
}

