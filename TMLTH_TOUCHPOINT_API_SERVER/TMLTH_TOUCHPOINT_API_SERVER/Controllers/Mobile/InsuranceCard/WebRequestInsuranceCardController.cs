﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Insurance_card
{
    public class WebRequestInsuranceCardController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestInsuranceCard";
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestInsuranceCard request_data = JsonConvert.DeserializeObject<WebRequestInsuranceCard>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        var errorlog = ContentTools.ErrorControllerLog("WebRequestInsuranceCard" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "InsuranceCard",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum + "," +
                        "LNG" + "=" + request_data.lng
                        ),
                        request_data.device);

                    //TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    //if (tokendata.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    #region start query(fetch) data from db
                    string clntnum = cookieData.ID.ToString();
                    
                    WebResponseInsuranceCard insurancecard = tmlth.SP_TP_GET_INSURANCE_CARD(request_data.chdrnum, request_data.lng).Select(x=> new WebResponseInsuranceCard
                    {
                        cardtype = x.card_type,
                        cardtypedesc = x.card_type_desc,
                        title = x.title,
                        chdrnum = x.chdrnum,
                        insuredname = x.insured_name,
                        crrdatepafrom = x.crrdate_pa_FROM,
                        crrdatepato = x.crrdate_pa_to,
                        summassurepa = x.summassure_pa,
                        hpropdtebase = x.hpropdte_base,
                        crrdatehsn = x.crrdate_hsn,
                        roomhsn = x.room_hsn,
                        acc24hsn = x.acc24_hsn,
                        crrdatehs2n = x.crrdate_hs2n,
                        roomhs2n = x.room_hs2n,
                        acc24hs2n = x.acc24_hs2n,
                        crrdatehshc = x.crrdate_hshc,
                        roomhshc = x.room_hshc,
                        acc24hshc = x.acc24_hshc,
                        opdsumm = x.opd_summ,
                        opdtype = x.opd_type,
                        remark1 = x.remark1,
                        remark2 = x.remark2 ,
                        ispa = x.IsPA,
                        existhsn = x.existHSN,
                        exisths2n = x.existHS2N,
                        existhshc = x.existHSHC,
                        existopd = x.existOPD,
                        crtable = x.crtable,
                        prddesc = x.prddesc,
                        sumbasic = x.sumbasic,
                        existhcp1 = x.existHCP1,
                        existhcp2 = x.existHCP2,
                        existhcp3 = x.existHCP3,
                        opdplustext = x.opdplus_text,
                        opdplustitle = x.opdplus_title,
                        hcp1text = x.hcp1_text,
                        hcp2title = x.hcp2_title,
                        hcp2text = x.hcp2_text,
                        hcp3title = x.hcp3_title,
                        hcp3text = x.hcp3_text
                    }).FirstOrDefault();
                    //WebResponePolicy getdata = JsonConvert.DeserializeObject<WebResponePolicy>(JsonConvert.SerializeObject(policy));
                    //if (insurancecard == null)
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    //}
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"insurancecard\":");
                    sb.Append(JsonConvert.SerializeObject(insurancecard));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
