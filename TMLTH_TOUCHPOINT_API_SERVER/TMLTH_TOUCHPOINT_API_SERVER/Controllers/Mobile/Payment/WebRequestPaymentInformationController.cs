﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Payment
{
    public class WebRequestPaymentInformationController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPaymentInformation";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);

                WebRequestPaymentInformation request_data = JsonConvert.DeserializeObject<WebRequestPaymentInformation>(data);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                       
                            var errorlog = ContentTools.ErrorControllerLog("WebRequestPaymentInformation" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Payment",
                        this.GetType().Name,
                        (
                        "INPUT_INSUREDE" + "=" + request_data.input_insurede
                        )
                        ,
                        request_data.device);


                    List<WebResponseDirectCredit> directcredit = tmlth.SP_TP_GET_DIREC_CREDIT().Select(x => new WebResponseDirectCredit
                    {
                        id = x.BNK_ID,
                        code = x.BNK_CODE,
                        nameth = x.BNK_NAME_TH,
                        nameen = x.BNK_NAME_ENG,
                        namejp = x.BNK_NAME_JP,
                        swiftcode = x.BNK_SWIFT_CODE,
                        path = x.BNK_PATH,
                        direct_credit = x.BNK_DIRECT_CREDIT,
                        stat_direct_credit = x.BNK_STAT_DIRECT_CREDIT,
                        direct_debit = x.BNK_DIRECT_DEBIT,
                        stat_direct_debit = x.BNK_STAT_DIRECT_DEBIT,
                        order = x.BNK_ORDER,
                    }).ToList<WebResponseDirectCredit>();

                    if (!directcredit.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    List<WebResponseDirectDebit> directdebit = tmlth.SP_TP_GET_DIREC_DEBIT().Select(x => new WebResponseDirectDebit
                    {
                        id = x.BNK_ID,
                        code = x.BNK_CODE,
                        nameth = x.BNK_NAME_TH,
                        nameen = x.BNK_NAME_ENG,
                        namejp = x.BNK_NAME_JP,
                        swiftcode = x.BNK_SWIFT_CODE,
                        path = x.BNK_PATH,
                        direct_credit = x.BNK_DIRECT_CREDIT,
                        stat_direct_credit = x.BNK_STAT_DIRECT_CREDIT,
                        direct_debit = x.BNK_DIRECT_DEBIT,
                        stat_direct_debit = x.BNK_STAT_DIRECT_DEBIT,
                        order = x.BNK_ORDER,
                    }).ToList<WebResponseDirectDebit>();
                    if (!directdebit.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    List<WebResponseFamily> family = tmlth.SP_TP_GET_FAM().Select(x => new WebResponseFamily
                    {
                        id = x.IDV_FAMILY_ID,
                        name_th = x.IDV_FAMILY_NAME_TH,
                        name_en = x.IDV_FAMILY_NAME_ENG,
                        name_jp = x.IDV_FAMILY_NAME_JP,
                        name_order = x.IDV_FAMILY_NAME_ORDER,
                        name_stat = x.IDV_FAMILY_NAME_STAT,

                    }).ToList<WebResponseFamily>();

                    if (!family.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }

                    WebResponsePaymentPeriodDetail paymentperioddetail = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD_DETAIL(request_data.input_insurede).Select(x => new WebResponsePaymentPeriodDetail
                    {
                        insured = x.INSURED,
                        name_insured = x.NAME_INSURE,
                        payment_date = x.PAYMENT_DATE,
                        payment_date_thai = x.PAYMENT_DATE_THAI,
                        payment_period = x.PAYMENT_PERIOD,
                        period_year = x.PRERIOD_YEAR,
                        period_time = x.PRERIOD_TIME,
                        payment_premium = x.PAYMENT_PREMIUM,
                        addr1 = x.ADDR1,
                        addr2 = x.ADDR2,
                        addr3 = x.ADDR3,
                        addr4 = x.ADDR4,
                        addr5 = x.ADDR5,
                        stat = x.STAT,
                        msg = x.MSG,
                        today = x.TODAY,
                        com_code = x.COM_CODE,
                        ref1 = x.REF1,
                        ref2 = x.REF2,
                        ref3 = x.REF3,
                        ref4 = x.REF4,
                        ref5 = x.REF5,
                        ref_send = x.REF_SEND,
                        currency_code = x.CURREN_CODE,
                        billing_code = x.BILLING_CODE,
                        product_code = x.PRODUCT_CODE,
                        agen_code = x.AGEN_CODE,
                        agen_name = x.AGEN_NAME,
                        insured_phone = x.INSURED_PHONE,
                        sum_insured = x.SUM_INSURED,
                        coverage_plan = x.COVERAGE_PLAN,
                        user_id = x.USER_ID,
                        user_name = x.USER_NAME,
                        payment_option = x.PAYMENT_OPTION,
                        payment_period_dtm = x.PAYMENT_PERIOD_DTM,
                        source_business = x.SOURCE_BUSINESS,
                    }).FirstOrDefault();

                    if (paymentperioddetail == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"directcredit\":");
                    sb.Append(JsonConvert.SerializeObject(directcredit));
                    sb.Append(",\"directdebit\":");
                    sb.Append(JsonConvert.SerializeObject(directdebit));
                    sb.Append(",\"family\":");
                    sb.Append(JsonConvert.SerializeObject(family));
                    sb.Append(",\"paymentperioddetail\":");
                    sb.Append(JsonConvert.SerializeObject(paymentperioddetail));
                    sb.Append("}");

                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }
}
