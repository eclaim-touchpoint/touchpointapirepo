﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.Payment
{
    public class WebRequestPaymentRequestTemplateController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
        }
        string page = "WebRequestPaymentRequestTemplate";
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
           
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestPaymentRequestTemplate request_data = JsonConvert.DeserializeObject<WebRequestPaymentRequestTemplate>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        
                         var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Payment",
                        this.GetType().Name,
                        (
                        "STRCHDRNUM=" + request_data.strChdrnum + "," +
                        "STRPAYMENTTYPE=" + request_data.strPaymentType + "," +
                        "STRBANKCODE=" + request_data.strBankCode + "," +
                        "STRPAYERNAME=" + request_data.strPayerName + "," +
                        "STRFAMID=" + request_data.strFamID + "," +
                        "STROSNAME=" + request_data.strOsName + "," +
                        "STROSVER=" + request_data.strOsVer + "," +
                        "STRBRWSNAME=" + request_data.strBrwsName + "," +
                        "STRBRWSVER=" + request_data.strBrwsVer + "," +
                        "ISLOAN=" + request_data.isloan + "," +
                        "LOANTYPE=" + request_data.loantype + "," +
                        "LOANPAYTYPE=" + request_data.loanpaytype + "," +
                        "LOANPAYTOTAL=" + request_data.loanpaytotal
                        ),
                        request_data.device);

                    ContentTools.ErrorControllerLog(page + "||" + "STRCHDRNUM=" + request_data.strChdrnum + "," +
                        "STRPAYMENTTYPE=" + request_data.strPaymentType + "," +
                        "STRBANKCODE=" + request_data.strBankCode + "," +
                        "STRPAYERNAME=" + request_data.strPayerName + "," +
                        "STRFAMID=" + request_data.strFamID + "," +
                        "STROSNAME=" + request_data.strOsName + "," +
                        "STROSVER=" + request_data.strOsVer + "," +
                        "STRBRWSNAME=" + request_data.strBrwsName + "," +
                        "STRBRWSVER=" + request_data.strBrwsVer + "," +
                        "ISLOAN=" + request_data.isloan + "," +
                        "LOANTYPE=" + request_data.loantype + "," +
                        "LOANPAYTYPE=" + request_data.loanpaytype + "," +
                        "LOANPAYTOTAL=" + request_data.loanpaytotal);
                    try
                    {
                        var client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                        var arrayPayerName = request_data.strPayerName.ToString().Trim().Split(new string[]{ " " }, StringSplitOptions.None);
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                        System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                        {
                            return true; // **** Always accept
                        };

                        //TMLTH_TOUCHPOINT_API_SERVER.TmlthOnlinePayment.PaymentSupRequestTemplateInfoDto request_data3 = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.TmlthOnlinePayment.PaymentSupRequestTemplateInfoDto>(decryptData);
                        //TMLTH_TOUCHPOINT_API_SERVER.TmlthOnlinePayment.ResultInfoOfPaymentSupRequestInfoDtoaKPUf2By result = client.GetPaymentRequestTemplate(request_data3);//

                        string clntnum = cookieData.ID.ToString();
                        string idcard = (from x in tmlth.tidpassoccc
                                       where x.clntnum == clntnum
                                         select x.idcard).FirstOrDefault();

                        WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", request_data.isloan == 1 ? "PAYMENT_LOAN" : "PAYMENT").Select(x => new WebReponseAccesskey
                        {
                            id = x.REQUEST_ID,
                            access_key = x.REQUEST_ACCESS_KEY,
                            profile_id = x.REQUEST_PROFILE_ID,
                            app_nme = x.REQUEST_APP_NME,
                            app_type = x.REQUEST_APP_TYPE,
                            service_nme = x.REQUEST_SERVICE_NME,
                        }).FirstOrDefault();

                        WebResponeUserDetail getdetailidcard = tmlth.SP_TP_GET_USER_DETAIL_ID_CARD(idcard).Select(x => new WebResponeUserDetail
                        {
                            idcard = x.idcard,
                            clntnum = x.clntnum,
                            chdrnum = x.chdrnum,
                            userid = x.userid,
                            name_ins = x.name_ins,
                            lastname_ins = x.lastname_ins,
                            mobile_no = x.mobile_no,
                            email = x.email
                        }).FirstOrDefault();

                        if (getdetailidcard == null)
                        {
                            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND, DataFactory.ENC_TOUCHPOINT_KEY));
                        }

                        WebResponsePaymentRequestTemplate request_template_detail = new WebResponsePaymentRequestTemplate();
                        string objective_type = DataFactory.PAYMENT_OBJECTIVE_TYPE_NORMAL;
                        string citemtype = "";

                        DateTime now = DateTime.Now;
                        CultureInfo TH_CULTURE = new CultureInfo("th-TH");

                        if (request_data.isloan == 1)
                        {
                            //NOTE: API Know is loan by access key and profile id
                            //Accesskey.profile_id = DataFactory.LOAN_PROFILE_ID; Cancelled used by store
                            //Accesskey.access_key = DataFactory.LOAN_ACCESS_KEY;
                            if (request_data.loantype == DataFactory.LOAN_TYPE_AUTO)
                            {
                                if (request_data.loanpaytype == 1)
                                {
                                    objective_type = DataFactory.PAYMENT_OBJECTIVE_TYPE_APL_FULL;
                                }
                                else if (request_data.loanpaytype == 2)
                                {
                                    objective_type = DataFactory.PAYMENT_OBJECTIVE_TYPE_APL_PAR;
                                }
                                else
                                {
                                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                               ResultData.CODE_ERROR_DATA_NOT_FOUND, DataFactory.ENC_TOUCHPOINT_KEY));
                                }
                            }
                            else if (request_data.loantype == DataFactory.LOAN_TYPE_PAYMENT)
                            {
                                if (request_data.loanpaytype == 1)
                                {
                                    objective_type = DataFactory.PAYMENT_OBJECTIVE_TYPE_APL_FULL;
                                }
                                else if (request_data.loanpaytype == 2)
                                {
                                    objective_type = DataFactory.PAYMENT_OBJECTIVE_TYPE_RPL_PAR;
                                }
                                else
                                {
                                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                               ResultData.CODE_ERROR_DATA_NOT_FOUND, DataFactory.ENC_TOUCHPOINT_KEY));
                                }
                            }
                            else
                            {
                                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                               ResultData.CODE_ERROR_DATA_NOT_FOUND, DataFactory.ENC_TOUCHPOINT_KEY));
                            }
                            
                            WebResponsePolicyLoanSummary loanSummary = tmlth.SP_TP_GET_POLICY_LOAN_SUMMARY(request_data.strChdrnum).Select(x => new WebResponsePolicyLoanSummary
                            {
                                policy_no = x.POLICY_NO,
                                total_apl = x.TOTAL_APL,
                                total_rpl = x.TOTAL_RPL,
                                total = x.TOTAL,
                                billing_code = x.BILLING_CODE,
                                product_code = x.PRODUCT_CODE,
                                source_business = x.SOURCE_BUSINESS
                            }).FirstOrDefault();

                            request_template_detail = tmlth.SP_TP_GET_POLICY_LOAN_PAYMENT(request_data.strChdrnum).Select(x => new WebResponsePaymentRequestTemplate {
                                insured = x.INSURED,
                                name_insured = x.NAME_INSURE,
                                //payment_date = x.PAYMENT_DATE,
                                //payment_date_thai = x.PAYMENT_DATE_THAI,
                                payment_period = x.PAYMENT_PERIOD,
                                //period_year = x.PRERIOD_YEAR,
                                //period_time = x.PRERIOD_TIME,
                                //payment_premium = x.PAYMENT_PREMIUM,
                                addr1 = x.ADDR1,
                                addr2 = x.ADDR2,
                                addr3 = x.ADDR3,
                                addr4 = x.ADDR4,
                                addr5 = x.ADDR5,
                                //stat = x.STAT,
                                //msg = x.MSG,
                                //today = x.TODAY,
                                //com_code = x.COM_CODE,
                                ref1 = "4511" + request_data.strChdrnum,
                                ref2 = now.ToString("ddMMyyyy", TH_CULTURE),
                                //ref3 = x.REF3,
                                //ref4 = x.REF4,
                                //ref5 = x.REF5,
                                //zipcode = x.Zipcode,
                                ref_send = "4511" + request_data.strChdrnum,
                                currency_code = "THB", // HARDCODE
                                billing_code = x.BILLING_CODE,
                                product_code = x.PRODUCT_CODE,
                                agen_code = x.AGEN_CODE,
                                agen_name = x.AGEN_NAME,
                                insured_phone = x.INSURED_PHONE,
                                sum_insured = x.SUM_INSURED,
                                coverage_plan = x.COVERAGE_PLAN,
                                user_id = x.USER_ID,
                                user_name = x.USER_NAME,
                                //payment_option = x.PAYMENT_OPTION,  NOTE Q3 == เงินกู้อัตโนมัติ, Q4 == เงินกู้กรมธรรม์
                                payment_option = request_data.loantype == DataFactory.LOAN_TYPE_AUTO ? "Q3" : request_data.loantype == DataFactory.LOAN_TYPE_PAYMENT ? "Q4" : "",
                                //payment_period_dtm = x.PAYMENT_PERIOD_DTM,
                                source_business = loanSummary.source_business,
                                language = "TH" // HARDCODE
                            }).FirstOrDefault();

                            citemtype = "ชำระเงินกู้" + (request_data.loantype == DataFactory.LOAN_TYPE_AUTO ? "อัตโนมัติ" : request_data.loantype == DataFactory.LOAN_TYPE_PAYMENT ? "กรมธรรม์" : "");
                            ///////////////////////////////////////////////////////////////////////////////////////
                            request_data.strBankCode = (request_data.strPaymentType == "001" || request_data.strPaymentType == "025") ? request_data.strBankCode : request_data.strBankCode;
                            request_template_detail.chdrnum_instment = "4511" + request_data.strChdrnum + "|" + now.ToString("ddMMyyyy", TH_CULTURE);
                        }
                        else
                        {
                            request_template_detail = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD_DETAIL(request_data.strChdrnum).Select(x => new WebResponsePaymentRequestTemplate
                            {
                                insured = x.INSURED,
                                name_insured = x.NAME_INSURE,
                                payment_date = x.PAYMENT_DATE,
                                payment_date_thai = x.PAYMENT_DATE_THAI,
                                payment_period = x.PAYMENT_PERIOD,
                                period_year = x.PRERIOD_YEAR,
                                period_time = x.PRERIOD_TIME,
                                payment_premium = x.PAYMENT_PREMIUM,
                                addr1 = x.ADDR1,
                                addr2 = x.ADDR2,
                                addr3 = x.ADDR3,
                                addr4 = x.ADDR4,
                                addr5 = x.ADDR5,
                                stat = x.STAT,
                                msg = x.MSG,
                                today = x.TODAY,
                                com_code = x.COM_CODE,
                                ref1 = x.REF1,
                                ref2 = x.REF2,
                                ref3 = x.REF3,
                                ref4 = x.REF4,
                                ref5 = x.REF5,
                                zipcode = x.Zipcode,
                                ref_send = x.REF_SEND,
                                currency_code = x.CURREN_CODE,
                                billing_code = x.BILLING_CODE,
                                product_code = x.PRODUCT_CODE,
                                agen_code = x.AGEN_CODE,
                                agen_name = x.AGEN_NAME,
                                insured_phone = x.INSURED_PHONE,
                                sum_insured = x.SUM_INSURED,
                                coverage_plan = x.COVERAGE_PLAN,
                                user_id = x.USER_ID,
                                user_name = x.USER_NAME,
                                payment_option = x.PAYMENT_OPTION,
                                payment_period_dtm = x.PAYMENT_PERIOD_DTM,
                                source_business = x.SOURCE_BUSINESS,
                                language = x.LANGUAGE_,
                                payment_amount = x.PAYMENT_PREMIUM
                            }).FirstOrDefault();

                            /*if(request_data.strPaymentType == "001")
                            {
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + "ขณะนี้ บริษัทอยู่ในระหว่างปรับปรุงช่องทางชำระเบี้ยประกันภัยผ่านบัตรเครดิต \nจึงขอระงับการบริการในช่องทางดังกล่าว \nทั้งนี้ท่านยังคงสามารถชำระเบี้ยประกันภัยผ่านวิธีหักบัญชีธนาคารได้ตามปกติ\nจึงขอเรียนขออภัยในความไม่สะดวกมา ณ ที่นี้");
                            }*/

                            citemtype = "ชำระเบี้ยประกันภัย";
                            request_data.strBankCode = (request_data.strPaymentType == "001" || request_data.strPaymentType == "025") ? request_data.strBankCode : request_data.strBankCode;
                            request_template_detail.chdrnum_instment = request_template_detail.insured + "|" + request_template_detail.period_year + "|" + request_template_detail.period_time;

                        }

                        // ค่าธรรมเนียม บัตรเครดิต
                        List<clsPremiumFee> IstCalculatePremiumFee = new List<clsPremiumFee>();
                        IstCalculatePremiumFee = GetPremiumfee(request_data.isloan, request_data.loantype, request_data.loanpaytype, request_data.loanpaytotal, request_data.strChdrnum, request_data.strPaymentType, request_data.strBankCode, request_template_detail);
                        // BINDING DATA TO SERVICE "PaymentRequestTemplate"
                        ContentTools.ErrorControllerLog(page + "|IstCalculatePremiumFee:" + JsonConvert.SerializeObject(IstCalculatePremiumFee));
                        TmlthOnlinePayment.PaymentSupRequestTemplateInfoDto Para = new TmlthOnlinePayment.PaymentSupRequestTemplateInfoDto();
                        Para.ACCESS_KEY = Accesskey.access_key;
                        Para.PROFILE_ID = Accesskey.profile_id;
                        Para.PAYMENT_TYPE = request_data.strPaymentType;
                        Para.BANK_CODE = request_data.strBankCode;
                        Para.REF1 = request_template_detail.ref_send;
                        Para.REF2 = request_template_detail.ref2;
                        Para.REF3 = request_template_detail.ref3;
                        Para.REF4 = request_template_detail.ref4;
                        Para.REF5 = request_template_detail.ref5;

                        Para.AMOUNT = IstCalculatePremiumFee.FirstOrDefault().total_amount;

                        Para.BILL_TO_ADDRESS_CITY = request_template_detail.addr4;
                        Para.BILL_TO_EMAIL = getdetailidcard.email;
                        Para.LANGUAGE = request_template_detail.language;

                        Para.BILL_TO_ADDRESS_COUNTRY = "TH";
                        Para.BILL_TO_ADDRESS_LINE1 = request_template_detail.addr1;
                        Para.BILL_TO_ADDRESS_LINE2 = request_template_detail.addr2;
                        Para.BILL_TO_ADDRESS_LINE3 = request_template_detail.addr3;
                        Para.BILL_TO_ADDRESS_LINE4 = "";
                        Para.BILL_TO_ADDRESS_POSTAL_CODE = request_template_detail.zipcode;
                        
                        Para.BILL_TO_FORENAME = arrayPayerName[0].ToString().Trim();
                        Para.BILL_TO_SURNAME = arrayPayerName.Length > 1 ? arrayPayerName[1].ToString().Trim() : arrayPayerName[0].ToString().Trim();
                        Para.BILL_TO_MOBILE = getdetailidcard.mobile_no;
                        Para.BILL_TO_PHONE = getdetailidcard.mobile_no;
                        Para.CURRENCY = request_template_detail.currency_code;
                        Para.REMARK = "";
                        Para.OS_NAME = request_data.strOsName;
                        Para.OS_VERSION = request_data.strOsVer;
                        Para.BROWSER_NAME = request_data.strBrwsName;
                        Para.BROWSER_VERSION = request_data.strBrwsVer;
                        Para.MOBILE_FLAG = true;
                        Para.INFO_VALUE01 = request_template_detail.name_insured;
                        Para.INFO_VALUE02 = request_template_detail.insured;
                        Para.INFO_VALUE03 = citemtype;
                        Para.INFO_VALUE04 = request_template_detail.period_year + "/" + request_template_detail.period_time;
                        Para.BILLING_CODE = request_template_detail.billing_code;
                        Para.PRODUCT_CODE = request_template_detail.product_code;
                        Para.CLIENT_NUMBER = cookieData.ID.ToString();
                        Para.CONTRACT_NUMBER = request_data.strChdrnum;
                        Para.FEE = IstCalculatePremiumFee.FirstOrDefault().fee;
                        Para.AGENCY_CODE = request_template_detail.agen_code;
                        Para.AGENCY_NAME = request_template_detail.agen_name;
                        Para.INSURED_MOBILE = request_template_detail.insured_phone;
                        Para.SUM_INSURED = Convert.ToDouble(request_template_detail.sum_insured <= 0 ? 0 : request_template_detail.sum_insured);
                        Para.COVERAGE_PLAN = request_template_detail.coverage_plan;
                        Para.USER_ID = request_template_detail.user_id;
                        Para.USER_NAME = request_template_detail.user_name;
                        Para.PAYMENT_OPTION = request_template_detail.payment_option;
                        Para.OBJECTIVE_TYPE_CODE = objective_type;

                        //NOTE DATA TEST พี่นี
                        //if (!DataFactory.IS_PRODUCTION)
                        //{
                        //    Para.AMOUNT = 100;
                        //    Para.BILL_TO_EMAIL = "sansanee.soi@tokiomarinelife.co.th";
                        //    Para.LANGUAGE = "TH";
                        //    Para.BILL_TO_ADDRESS_COUNTRY = "TH";
                        //    Para.BILL_TO_ADDRESS_LINE1 = "เลขที่ 705 ม.10";
                        //    Para.BILL_TO_ADDRESS_LINE2 = "ซ.เศรษฐกิจ 31/11";
                        //    Para.BILL_TO_ADDRESS_LINE3 = "ต.อ่าวน้อย";
                        //    Para.BILL_TO_ADDRESS_LINE4 = "อ.เมืองฯ";
                        //    Para.BILL_TO_ADDRESS_CITY = "ประจวบคีรีขันธ์";
                        //    Para.BILL_TO_ADDRESS_POSTAL_CODE = "77210";
                        //    Para.BILL_TO_MOBILE = "0918864241";
                        //    Para.BILL_TO_PHONE = "0918864241";
                        //}

                        ///////////////////////////////////////////////////////////////////////////////////////
                        string format = "dd/MM/yyyy";
                        if (request_data.isloan == 1) {
                            Para.PAYMENT_PERIOD_DTM = now;
                        } else
                        {
                            DateTime expenddt = DateTime.ParseExact(request_template_detail.payment_period_dtm, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                            Para.PAYMENT_PERIOD_DTM = expenddt;
                        }
                        Para.SOURCE_BUSINESS = request_template_detail.source_business;
                        TmlthOnlinePayment.ResultInfoOfPaymentSupRequestInfoDtoaKPUf2By pararesult = client.GetPaymentRequestTemplate(Para);
                        clsPaymentOnline RESULT_PAYMENT_RESPONSE = new clsPaymentOnline();
                        RESULT_PAYMENT_RESPONSE.chdrnum_instment = request_template_detail.chdrnum_instment;
                        RESULT_PAYMENT_RESPONSE.chdrnum = request_template_detail.insured;
                        RESULT_PAYMENT_RESPONSE.user_id = request_template_detail.user_id;
                        RESULT_PAYMENT_RESPONSE.accesskey = Accesskey.access_key;
                        RESULT_PAYMENT_RESPONSE.profileid = Accesskey.profile_id;
                        RESULT_PAYMENT_RESPONSE.signed_field_names = pararesult.Data.ListFormCollection["SIGNED_FIELD_NAMES"];
                        RESULT_PAYMENT_RESPONSE.signed_datetime = pararesult.Data.ListFormCollection["SIGNED_DATE_TIME"];
                        RESULT_PAYMENT_RESPONSE.payment_type = pararesult.Data.ListFormCollection["PAYMENT_TYPE"];
                        RESULT_PAYMENT_RESPONSE.bank_code = pararesult.Data.ListFormCollection["BANK_CODE"];
                        RESULT_PAYMENT_RESPONSE.ref1 = pararesult.Data.ListFormCollection["REF1"];
                        RESULT_PAYMENT_RESPONSE.ref2 = pararesult.Data.ListFormCollection["REF2"];
                        RESULT_PAYMENT_RESPONSE.ref3 = pararesult.Data.ListFormCollection["REF3"];
                        RESULT_PAYMENT_RESPONSE.ref4 = pararesult.Data.ListFormCollection["REF4"];
                        RESULT_PAYMENT_RESPONSE.ref5 = pararesult.Data.ListFormCollection["REF5"];
                        RESULT_PAYMENT_RESPONSE.ref_send = pararesult.Data.ListFormCollection["REF1"];
                        RESULT_PAYMENT_RESPONSE.payment_amount = string.IsNullOrWhiteSpace(pararesult.Data.ListFormCollection["AMOUNT"]) ? 0 : double.Parse(pararesult.Data.ListFormCollection["AMOUNT"]);
                        RESULT_PAYMENT_RESPONSE.currency_code = pararesult.Data.ListFormCollection["CURRENCY"];
                        RESULT_PAYMENT_RESPONSE.language = pararesult.Data.ListFormCollection["LANGUAGE"];
                        RESULT_PAYMENT_RESPONSE.insured_name = pararesult.Data.ListFormCollection["BILL_TO_FORENAME"];
                        RESULT_PAYMENT_RESPONSE.insured_surname = pararesult.Data.ListFormCollection["BILL_TO_SURNAME"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_line1 = pararesult.Data.ListFormCollection["BILL_TO_ADDRESS_LINE1"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_line2 = pararesult.Data.ListFormCollection["BILL_TO_ADDRESS_LINE2"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_line3 = pararesult.Data.ListFormCollection["BILL_TO_ADDRESS_LINE3"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_line4 = pararesult.Data.ListFormCollection["BILL_TO_ADDRESS_LINE4"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_city = pararesult.Data.ListFormCollection["BILL_TO_ADDRESS_CITY"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_country = pararesult.Data.ListFormCollection["BILL_TO_ADDRESS_COUNTRY"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_postal_code = pararesult.Data.ListFormCollection["BILL_TO_ADDRESS_POSTAL_CODE"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_email = pararesult.Data.ListFormCollection["BILL_TO_EMAIL"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_phone = pararesult.Data.ListFormCollection["BILL_TO_PHONE"];
                        RESULT_PAYMENT_RESPONSE.bill_to_address_mobile = pararesult.Data.ListFormCollection["BILL_TO_MOBILE"];
                        RESULT_PAYMENT_RESPONSE.remark = pararesult.Data.ListFormCollection["REMARK"];
                        RESULT_PAYMENT_RESPONSE.signature = pararesult.Data.ListFormCollection["SIGNATURE"];
                        RESULT_PAYMENT_RESPONSE.post_url = pararesult.Data.POST_URL;
                        RESULT_PAYMENT_RESPONSE.payment_code = ""; //pararesult.Data.ListFormCollection["PAYMENT_CODE"]; //result.Data.PAYMENT_OPTION;
                        RESULT_PAYMENT_RESPONSE.transaction_id = pararesult.Data.ListFormCollection["TRANSACTION_ID"];
                        RESULT_PAYMENT_RESPONSE.fam_id = request_data.strFamID;
                        RESULT_PAYMENT_RESPONSE.billing_code = pararesult.Data.ListFormCollection["BILLING_CODE"];
                        RESULT_PAYMENT_RESPONSE.product_code = pararesult.Data.ListFormCollection["PRODUCT_CODE"];
                        RESULT_PAYMENT_RESPONSE.fees = string.IsNullOrWhiteSpace(pararesult.Data.ListFormCollection["FEE"]) ? 0 : double.Parse(pararesult.Data.ListFormCollection["FEE"]);
                        RESULT_PAYMENT_RESPONSE.objective_type_code = pararesult.Data.ListFormCollection["OBJECTIVE_TYPE_CODE"];
                        RESULT_PAYMENT_RESPONSE.url_tracking_result = pararesult.Data.URL_TRACKING_RESULT;

                        try
                        {
                            var resultInsert = tmlth.SP_TP_SET_POST_PAYMENT_DTL(RESULT_PAYMENT_RESPONSE.chdrnum_instment,
                            RESULT_PAYMENT_RESPONSE.chdrnum,
                            RESULT_PAYMENT_RESPONSE.user_id,
                            RESULT_PAYMENT_RESPONSE.accesskey,
                            RESULT_PAYMENT_RESPONSE.profileid,
                            RESULT_PAYMENT_RESPONSE.signed_field_names,
                            RESULT_PAYMENT_RESPONSE.signed_datetime,
                            RESULT_PAYMENT_RESPONSE.payment_type,
                            RESULT_PAYMENT_RESPONSE.bank_code,
                            RESULT_PAYMENT_RESPONSE.ref1,
                            RESULT_PAYMENT_RESPONSE.ref2,
                            RESULT_PAYMENT_RESPONSE.ref3,
                            RESULT_PAYMENT_RESPONSE.ref4,
                            RESULT_PAYMENT_RESPONSE.ref5,
                            RESULT_PAYMENT_RESPONSE.ref_send,
                            (decimal)RESULT_PAYMENT_RESPONSE.payment_amount,
                            RESULT_PAYMENT_RESPONSE.currency_code,
                            RESULT_PAYMENT_RESPONSE.language,
                            RESULT_PAYMENT_RESPONSE.bill_to_forename,
                            RESULT_PAYMENT_RESPONSE.bill_to_surname,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_line1,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_line2,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_line3,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_line4,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_city,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_country,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_postal_code,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_email,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_phone,
                            RESULT_PAYMENT_RESPONSE.bill_to_address_mobile,
                            RESULT_PAYMENT_RESPONSE.remark,
                            RESULT_PAYMENT_RESPONSE.signature,
                            RESULT_PAYMENT_RESPONSE.post_url,
                            RESULT_PAYMENT_RESPONSE.payment_code,
                            RESULT_PAYMENT_RESPONSE.transaction_id,
                            (RESULT_PAYMENT_RESPONSE.fam_id) == null ? (int?)null : int.Parse(RESULT_PAYMENT_RESPONSE.fam_id),
                            RESULT_PAYMENT_RESPONSE.billing_code,
                            RESULT_PAYMENT_RESPONSE.product_code,
                            (decimal?)(RESULT_PAYMENT_RESPONSE.fees),
                            RESULT_PAYMENT_RESPONSE.objective_type_code,
                            (decimal?)RESULT_PAYMENT_RESPONSE.payment_amount
                            );
                            //if (resultInsert == 1)
                            //{
                            StringBuilder html = new StringBuilder();
                            html.Append("<html><head><meta charset='UTF-8'></head><body>");
                            html.Append("<form action ='" + pararesult.Data.POST_URL.Trim() + "' id='payment_form' method='post'>");

                            foreach (var item in pararesult.Data.ListFormCollection)
                            {
                                html.Append("<input type='hidden' id='" + item.Key + "' name='" + item.Key + "' value='" + item.Value + "'/>");
                            }

                            html.Append("</form>");
                            html.Append("<script type='text/javascript'>document.getElementById('payment_form').submit();</script>");
                            html.Append("</body></html>");

                            byte[] bForm = Encoding.UTF8.GetBytes(html.ToString());
                            string Result = Convert.ToBase64String(bForm);

                            if (!DataFactory.IS_PRODUCTION)
                            {
                                ContentTools.ErrorControllerLog(page + "||" + JsonConvert.SerializeObject(request_template_detail));
                                ContentTools.ErrorControllerLog(page + "||FORM HTML||" + html.ToString());
                            }

                            sb.Append("{");
                            sb.Append("\"resulthtml\":");
                            sb.Append(JsonConvert.SerializeObject(Result));
                            sb.Append(",\"paymentperioddetail\":");
                            sb.Append(JsonConvert.SerializeObject(request_template_detail));
                            sb.Append(",\"urltracking\":");
                            sb.Append(JsonConvert.SerializeObject(RESULT_PAYMENT_RESPONSE.url_tracking_result));
                            sb.Append("}");
                            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                        }
                        catch (Exception e)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                        }
                    }
                    catch (Exception e)
                    {
                        var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                    }
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT,
                DataFactory.ENC_TOUCHPOINT_KEY));
            }
        }

        public List<clsPremiumFee> GetPremiumfee(int isloan, string loantype, int loanpaytype, decimal? loanpaytotal, string chdrnum, string paymenttype, string bankcode, WebResponsePaymentRequestTemplate request_template_detail)
        {
            var objResult = new clsPremiumFee();
            List<clsPremiumFee> IstResult = new List<clsPremiumFee>();
            //var objBLPa = new clsBLPaymentOnline();
            webapp_mobileEntities tmlth = new webapp_mobileEntities();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                
                //CALCULATE PREMIUM FEE
                if (paymenttype == "001")
                {
                    if (isloan == 1)
                    {
                        var client = new HttpClient();
                        var response = client.PostAsJsonAsync(DataFactory.URL_TOUCHPOINT_EWI,
                            EncryptDecryptAlgorithm.EncryptData(JsonConvert.SerializeObject(new Object[1]{
                            new {
                                chdrnum = chdrnum
                            }}), DataFactory.ENC_TOUCHPOINT_KEY)).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            if (result.StartsWith(ResultData.CODE_ERROR))
                            {
                                string[] error = result.Split(':');
                                DataGenerator.generateTextResult(ResultData.CODE_ERROR + error[1]);
                                return null;
                            }
                            else if (result.StartsWith(ResultData.CODE_NOTFOUND) ||
                                     result.StartsWith(ResultData.CODE_BLOCKED) ||
                                     result.StartsWith(ResultData.CODE_SESSION_TIMEOUT) ||
                                     result.StartsWith(ResultData.CODE_TOKEN_DENIED) ||
                                     result.StartsWith(ResultData.CODE_TOKEN_EXPIRE))
                            {


                                DataGenerator.generateTextResult(ResultData.CODE_ERROR + result);
                                return null;
                            }
                            else
                            {
                                #region LOAN_SUMMARY_PRODUCTION
                                if (DataFactory.IS_PRODUCTION)
                                {
                                    result = EncryptDecryptAlgorithm.DecryptData(response.Content.ReadAsStringAsync().Result, DataFactory.ENC_TOUCHPOINT_KEY);
                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseType life_policyloansenquiryResponseType = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseType>(result);
                                    string Returncode = life_policyloansenquiryResponseType.Returncode;
                                    string Runmessage = life_policyloansenquiryResponseType.Runmessage;

                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseContentType life_policyloansenquiryResponseContentType = life_policyloansenquiryResponseType.content;
                                    decimal totalInterest = life_policyloansenquiryResponseContentType.loanDetailsCollection.Where(x => x.loanType == loantype).Sum(x => decimal.Parse(x.totalInterest));

                                    WebResponsePolicyLoanSummary loanSummary = tmlth.SP_TP_GET_POLICY_LOAN_SUMMARY(chdrnum).Select(x => new WebResponsePolicyLoanSummary
                                    {
                                        policy_no = x.POLICY_NO,
                                        total_apl = x.TOTAL_APL,
                                        total_rpl = x.TOTAL_RPL,
                                        total = x.TOTAL,
                                        billing_code = x.BILLING_CODE,
                                        product_code = x.PRODUCT_CODE,
                                        source_business = x.SOURCE_BUSINESS
                                    }).FirstOrDefault();

                                    WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT_LOAN").Select(x => new WebReponseAccesskey
                                    {
                                        id = x.REQUEST_ID,
                                        access_key = x.REQUEST_ACCESS_KEY,
                                        profile_id = x.REQUEST_PROFILE_ID,
                                        app_nme = x.REQUEST_APP_NME,
                                        app_type = x.REQUEST_APP_TYPE,
                                        service_nme = x.REQUEST_SERVICE_NME,
                                    }).FirstOrDefault();

                                    if (Accesskey == null)
                                    {
                                        var errorlog = ContentTools.ErrorControllerLog(page + "||METHOID GET_PREMIUM_FEE||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                        return null;
                                    }

                                    string access_key = Accesskey.access_key,
                                       profile_id = Accesskey.profile_id,
                                       payment_type = paymenttype,
                                       bank_code = bankcode,
                                       billing_code = loanSummary.billing_code,
                                       product_code = loanSummary.product_code,
                                       source_business = loanSummary.source_business,
                                       currency = "THB";

                                    double amount = (double)totalInterest;
                                    if ((loantype == DataFactory.LOAN_TYPE_AUTO || loantype == DataFactory.LOAN_TYPE_PAYMENT) && loanpaytype == 2)
                                    {
                                        amount = Convert.ToDouble(loanpaytotal);
                                    }

                                    var tmlthService = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                    var result_fee = tmlthService.GetCalculatePremiumFee(access_key, profile_id, payment_type, bank_code, billing_code, product_code, amount, currency, source_business);

                                    double dblfee = result_fee.Data,
                                            dblamount = Convert.ToDouble(amount),
                                            dbltotalAmount = dblamount + dblfee;

                                    objResult.fee = result_fee.Data;
                                    objResult.amount = Convert.ToDouble(amount);
                                    objResult.total_amount = dbltotalAmount;
                                    #endregion
                                }
                                else
                                {
                                    #region LOAN_SUMMARY_TEST
                                    result = EncryptDecryptAlgorithm.DecryptData(response.Content.ReadAsStringAsync().Result, DataFactory.ENC_TOUCHPOINT_KEY);
                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseType life_policyloansenquiryResponseType = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseType>(result);
                                    string Returncode = life_policyloansenquiryResponseType.Returncode;
                                    string Runmessage = life_policyloansenquiryResponseType.Runmessage;

                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseContentType life_policyloansenquiryResponseContentType = life_policyloansenquiryResponseType.content;
                                    decimal totalInterest = life_policyloansenquiryResponseContentType.loanDetailsCollection.Where(x => x.loanType == loantype).Sum(x => decimal.Parse(x.totalInterest));

                                    WebResponsePolicyLoanSummary loanSummary = tmlth.SP_TP_GET_POLICY_LOAN_SUMMARY(chdrnum).Select(x => new WebResponsePolicyLoanSummary
                                    {
                                        policy_no = x.POLICY_NO,
                                        total_apl = x.TOTAL_APL,
                                        total_rpl = x.TOTAL_RPL,
                                        total = x.TOTAL,
                                        billing_code = x.BILLING_CODE,
                                        product_code = x.PRODUCT_CODE,
                                        source_business = x.SOURCE_BUSINESS
                                    }).FirstOrDefault();

                                    string access_key = DataFactory.LOAN_ACCESS_KEY,
                                       profile_id = DataFactory.LOAN_PROFILE_ID,
                                       payment_type = paymenttype,
                                       bank_code = bankcode,
                                       billing_code = loanSummary.billing_code,
                                       product_code = loanSummary.product_code,
                                       source_business = loanSummary.source_business,
                                       currency = "THB";

                                    double amount = (double)totalInterest;
                                    if ((loantype == DataFactory.LOAN_TYPE_AUTO || loantype == DataFactory.LOAN_TYPE_PAYMENT) && loanpaytype == 2)
                                    {
                                        amount = Convert.ToDouble(loanpaytotal);
                                    }

                                    var tmlthService = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                    var result_fee = tmlthService.GetCalculatePremiumFee(access_key, profile_id, payment_type, bank_code, billing_code, product_code, amount, currency, source_business);

                                    double dblfee = result_fee.Data,
                                            dblamount = Convert.ToDouble(amount),
                                            dbltotalAmount = dblamount + dblfee;

                                    objResult.fee = result_fee.Data;
                                    objResult.amount = Convert.ToDouble(amount);
                                    objResult.total_amount = dbltotalAmount;
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            var errorlog = ContentTools.ErrorControllerLog("WebRequestPaymentRequestTemplate" + "||" + "REQUEST EWI FEE FAILED");
                            return null;
                        }
                    }
                    else
                    {
                        #region getAccessKey
                        WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT").Select(x => new WebReponseAccesskey
                        {
                            id = x.REQUEST_ID,
                            access_key = x.REQUEST_ACCESS_KEY,
                            profile_id = x.REQUEST_PROFILE_ID,
                            app_nme = x.REQUEST_APP_NME,
                            app_type = x.REQUEST_APP_TYPE,
                            service_nme = x.REQUEST_SERVICE_NME,
                        }).FirstOrDefault();
                        if (Accesskey == null)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        #endregion

                        var db_payment_amount = request_template_detail.payment_amount;
                        // double db_payment_amount = 0;
                        TmlthOnlinePayment.OnlinePaymentSupportServicesClient client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();

                        string access_key = Accesskey.access_key;
                        string profile_id = Accesskey.profile_id;
                        string payment_type = paymenttype;
                        string bank_code = bankcode;
                        string billing_code = request_template_detail.billing_code;
                        string product_code = request_template_detail.product_code;
                        double amount = Convert.ToDouble(db_payment_amount);
                        string currency = request_template_detail.currency_code;
                        string source_business = request_template_detail.source_business;

                        TmlthOnlinePayment.ResultInfoOfdouble result_fee = client.GetCalculatePremiumFee(access_key, profile_id, payment_type, bank_code, billing_code, product_code, amount, currency, source_business); // without amount
                        double dblfee = result_fee.Data;
                        double dblAmout = Convert.ToDouble(db_payment_amount);
                        double dblTotalAmount = dblAmout + dblfee;

                        objResult.fee = result_fee.Data;
                        objResult.amount = Convert.ToDouble(db_payment_amount);
                        objResult.total_amount = dblTotalAmount;
                    }
                }
                else
                {
                    if (isloan == 1) {
                        objResult.fee = 0.00;
                        objResult.amount = Convert.ToDouble(loanpaytotal);
                        objResult.total_amount = Convert.ToDouble(loanpaytotal);
                    } else
                    {
                        objResult.fee = 0.00;
                        objResult.amount = Convert.ToDouble(request_template_detail.payment_amount);
                        objResult.total_amount = Convert.ToDouble(request_template_detail.payment_amount);
                    }
                    
                }

                IstResult.Add(objResult);

                return IstResult;
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog("WebRequestPaymentRequestTemplate" + "||"  + e);
                return null;
            }
        }
    }
}
