﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Payment
{
    public class WebRequestPaymentPeriodController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPaymentPeriod";
            if (ModelState.IsValid)
            {
                //var decryptData = data;
               string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);

                WebRequestPaymentPeriod request_data = JsonConvert.DeserializeObject<WebRequestPaymentPeriod>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        
                            var errorlog = ContentTools.ErrorControllerLog("WebRequestPaymentPeriod" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Payment",
                        this.GetType().Name,
                        (
                            "CHDRNUM" + "=" + request_data.chdrnum
                        ),
                        request_data.device);

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    #region get listpaymentPeriod
                    List<WebResponsePaymentPeriod> paymentperiod = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD(cookieData.ID.ToString(), request_data.chdrnum).Select(x => new WebResponsePaymentPeriod
                    {
                        insured = x.INSURED,
                        payment_date = x.PAYMENT_DATE,
                        payment_period = x.PAYMENT_PERIOD,
                        period_year = x.PERIOD_YEAR,
                        period_time = x.PERIOD_TIME,
                        payment_premium = x.PAYMENT_PREMIUM,
                        stat = x.STAT,
                        msg = x.MSG,
                        billing_code = x.BILLING_CODE,
                        product_code = x.PRODUCT_CODE,
                        channel = x.CHANNEL,                        
                        unpaid = x.UNPAID
                    }).ToList<WebResponsePaymentPeriod>();
                    var iserror777 = new List<clsPaymentOnline>();
                    var checkstat = paymentperiod.Where(x => x.stat == "N");
                    var objBL = new clsPaymentOnline();
                    if (!checkstat.Any())
                    {
                        foreach (var paymentdata in paymentperiod)
                        {
                            var chdrnum_instment = request_data.chdrnum + "|" + paymentdata.period_year + "|" + paymentdata.period_time;
                            //  var listgetchdrnuminstment = new clsPaymentOnline();
                            clsPaymentOnline listgetchdrnuminstment = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(chdrnum_instment.ToString().Trim()).Select(x => new clsPaymentOnline
                            {
                                paymentid = x.PAYMENT_ID,
                                chdrnum_instment = x.CHDRNUM_INSTMENT,
                                chdrnum = x.CHDRNUM,
                                user_id = x.USER_ID,
                                accesskey = x.ACCESS_KEY,
                                profileid = x.PROFILE_ID,
                                signed_field_names = x.SIGNED_FIELD_NAMES,
                                signed_datetime = x.SIGNED_DATE_TIME,
                                payment_type = x.PAYMENT_TYPE,
                                bank_code = x.BANK_CODE,
                                ref1 = x.REF1,
                                ref2 = x.REF2,
                                ref3 = x.REF3,
                                ref4 = x.REF4,
                                ref5 = x.REF5,
                                ref_send = x.REF_SEND,
                                amount = x.AMOUNT,
                                currency = x.CURRENCY,
                                language = x.LANGUAGE,
                                bill_to_forename = x.BILL_TO_FORENAME,
                                bill_to_surname = x.BILL_TO_SURNAME,
                                bill_to_address_line1 = x.BILL_TO_ADDRESS_LINE1,
                                bill_to_address_line2 = x.BILL_TO_ADDRESS_LINE2,
                                bill_to_address_line3 = x.BILL_TO_ADDRESS_LINE3,
                                bill_to_address_line4 = x.BILL_TO_ADDRESS_LINE4,
                                bill_to_address_city = x.BILL_TO_ADDRESS_CITY,
                                bill_to_address_country = x.BILL_TO_ADDRESS_COUNTRY,
                                bill_to_address_postal_code = x.BILL_TO_ADDRESS_POSTAL_CODE,
                                bill_to_address_email = x.BILL_TO_EMAIL,
                                bill_to_address_phone = x.BILL_TO_PHONE,
                                bill_to_address_mobile = x.BILL_TO_MOBILE,
                                remark = x.REMARK,
                                signature = x.SIGNATURE,
                                resp_code = x.RESP_CODE,
                                resp_mess = x.RESP_MESS,
                                payment_status = x.PAYMENT_STATUS,
                                payment_detail = x.PAYMENT_DETAIL,
                                payment_code = x.PAYMENT_CODE,
                                post_url = x.POST_URL,
                                post_date = x.POST_DATE,
                                post_back_by = x.POST_BACK_BY,
                                post_back_date = x.POST_BACK_DATE,
                                transaction_id = x.TRANSACTION_ID,
                                idv_family_id = x.IDV_FAMILY_ID,
                                idv_paytype_detail = x.IDV_PAYTYPE_DETAIL,
                                billing_code = paymentdata.billing_code,
                                product_code = paymentdata.product_code,
                                fees = 0

                            }).FirstOrDefault();                            
                        
                            if (listgetchdrnuminstment != null)
                            {
                                if ((listgetchdrnuminstment.payment_status != true) && !string.IsNullOrWhiteSpace(listgetchdrnuminstment.transaction_id))
                                {
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                                    ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
                                    {
                                        return true; // **** Always accept
                                    };
                                    WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT").Select(x => new WebReponseAccesskey
                                    {
                                        id = x.REQUEST_ID,
                                        access_key = x.REQUEST_ACCESS_KEY,
                                        profile_id = x.REQUEST_PROFILE_ID,
                                        app_nme = x.REQUEST_APP_NME,
                                        app_type = x.REQUEST_APP_TYPE,
                                        service_nme = x.REQUEST_SERVICE_NME,
                                    }).FirstOrDefault();
                                    if (Accesskey == null)
                                    {
                                        var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                    }
                                    var client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                    TmlthOnlinePayment.ResultInfoOfPaymentSupResponseInfoDtoaKPUf2By check = client.CheckResponseDataWithTransactionID(listgetchdrnuminstment.transaction_id, false);
                                    if (check.Data != null)
                                    {
                                        if (!string.IsNullOrWhiteSpace(check.Data.RESP_CODE))
                                        {
                                            bool checkConnectionFail = CheckCode777(check.Data.RESP_CODE);
                                            if (checkConnectionFail)
                                            {
                                                var objecterror777 = new clsPaymentOnline
                                                {
                                                    chdrnum = listgetchdrnuminstment.chdrnum,
                                                    resp_code = check.Data.RESP_CODE
                                                };
                                                iserror777.Add(objecterror777);

                                            }
                                        }
                                    }

                                    listgetchdrnuminstment.resp_code = check.Data.RESP_CODE;
                                    listgetchdrnuminstment.resp_mess = check.Data.RESP_MESS;
                                    listgetchdrnuminstment.payment_status = check.Data.PAYMENT_STATUS;
                                    listgetchdrnuminstment.payment_code = check.Data.PAYMENT_CODE;
                                    listgetchdrnuminstment.accesskey = Accesskey.access_key;
                                    listgetchdrnuminstment.profileid = Accesskey.profile_id;
                                   
                                    tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(
                                        check.Data.RESP_CODE,
                                        check.Data.RESP_MESS,
                                        check.Data.PAYMENT_STATUS,
                                        "",
                                        check.Data.PAYMENT_CODE,
                                        check.Data.TRANSACTION_ID,
                                        listgetchdrnuminstment.accesskey,
                                        listgetchdrnuminstment.profileid
                                    );

                                }
                            }
                        } //end foreach


                        //NOTE: MUST NEW SELECT BECAUSE DATA IS UPDATED BY SP_TP_UPDATE_POST_PAYMENT_DTL CHECKED PAYMENT SERVICE
                        paymentperiod = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD(cookieData.ID.ToString(), request_data.chdrnum).Select(x => new WebResponsePaymentPeriod
                        {
                            insured = x.INSURED,
                            payment_date = x.PAYMENT_DATE,
                            payment_period = x.PAYMENT_PERIOD,
                            period_year = x.PERIOD_YEAR,
                            period_time = x.PERIOD_TIME,
                            payment_premium = x.PAYMENT_PREMIUM,
                            stat = x.STAT,
                            msg = x.MSG,
                            billing_code = x.BILLING_CODE,
                            product_code = x.PRODUCT_CODE,
                            channel = x.CHANNEL,
                            unpaid = x.UNPAID
                        }).ToList<WebResponsePaymentPeriod>();

                        if (!string.IsNullOrWhiteSpace(request_data.chdrnum))
                        {
                            paymentperiod = paymentperiod.Where(x => x.insured.Equals(request_data.chdrnum)).ToList<WebResponsePaymentPeriod>();
                        }

                        if (iserror777.Count() > 0)
                        {
                            foreach(var obj777 in iserror777)
                            { 
                                foreach (var payment in paymentperiod)
                                {
                                    if (payment.insured == obj777.chdrnum)
                                    {
                                        payment.resp_code = obj777.resp_code;
                                    }
                                    payment.remark = "**หมายเหตุ N/A รอการตรวจสอบจากธนาคาร (กรุณาทำรายการใหม่อีกครั้ง)";
                                }
                            }
                        }
                        var checkunpaid = paymentperiod.Where(x => x.unpaid.Equals("Y"));
                        if (!checkunpaid.Any())
                        {
                            foreach (var a in checkunpaid) 
                            {
                                a.remark2 = "**หมายเหตุ กรมธรรม์เกินกำหนดการชำระ กรุณาติดต่อเจ้าหน้าที่ Call Center 02 - 650 - 1400";
                            }
                        }
                    }
                   
                    sb.Append(",\"paymentperiod\":");
                    sb.Append(JsonConvert.SerializeObject(paymentperiod));

                    //if (!paymentperiod.Any())
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_DATA_NOT_FOUND, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}
                    #endregion
                    #region get paymentHistory
                    List<WebResponsePaymentHistory> paymenthistory = tmlth.SP_TP_GET_PAYMENT_HISTORY(request_data.chdrnum).Select(x => new WebResponsePaymentHistory
                    {
                        policy_no = x.POLICY_NO,
                        tran_no = x.TRAN_NO,
                        bill_no = x.BILL_NO,
                        compyear = x.COMPYEAR,
                        compprd = x.COMPPRD,
                        pay_date = x.PAY_DATE,
                        total_prm = x.TOTAL_PRM,
                        due_date = x.DUE_DATE,
                        pay_type = x.PAY_TYPE,
                        ppaytype = x.PPAYTYPE,
                        docfullname = x.DOCFULLNAME,
                        credit_card_no = x.CREDIT_CARD_NO,
                    }).ToList<WebResponsePaymentHistory>();
                    //var paymenthistoryorderdesc = paymenthistory.OrderByDescending(x => x.due_date);
                    //if (!paymenthistory.Any())
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_DATA_NOT_FOUND, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}
                    #endregion
                    #region get paymentCertificateDOCUMENT
                    WebResponsePaymentCertificateDocument paymentcertificate = tmlth.SP_TP_GET_PAYMENT_CERTIFICATE_DOCUMENT(request_data.chdrnum, DataFactory.DOCUMENT_TYPE_PAYMENT_CERTIFICATE).Select(x => new WebResponsePaymentCertificateDocument
                    {
                        chdrnum = x.POLICY_NO,
                        agntnum_name = x.AGNTNUM_NAME,
                        insured = x.INSURED,
                        agntnum = x.AGNTNUM,
                        crtable = x.CRTABLE,
                        billfreq = x.BILLFREQ,
                        thaidesc = x.THAIDESC,
                        docname = x.DOCNAME,
                        lettername = x.LETTERNAME,
                        doctype = x.DOCTYPE,
                        docfullname = x.DOCFULLNAME,
                        update_dt = x.UPDATE_DT,
                        prtdate = x.PRTDATE,
                        allow_download = x.ALLOW_DOWNLOAD,
                        prtdate_sort = x.PRTDATE_SORT,
                    }).FirstOrDefault();

                    #endregion
                    sb.Append(",\"paymenthistory\":");
                    sb.Append(JsonConvert.SerializeObject(paymenthistory));
                    sb.Append(",\"paymentcertificate\":");
                    sb.Append(JsonConvert.SerializeObject(paymentcertificate));
                    sb.Append("}");

                    //return DataGenerator.generateTextResult(sb.ToString());
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
        public bool CheckCode777(string val)
        {
            var blRes = new bool();
            if (val.Length > 0)
            {
                blRes = val.Substring(val.Length - 3) == "777" ? true : false;
            }
            return blRes;
        }
    }
}
