﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;
using TMLTH_TOUCHPOINT_API_SERVER.TmlthOnlinePayment;
using System.Globalization;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Payment
{
    public class WebRequestListPaymentPeriodDetailController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestListPaymentPeriodDetail";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestPaymentPeriodDetail request_data = JsonConvert.DeserializeObject<WebRequestPaymentPeriodDetail>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                DateTime now = DateTime.Now;
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Payment",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum
                        ),
                        request_data.device);


                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    
                    List<WebResponseDirectCredit> directcredit = tmlth.SP_TP_GET_DIREC_CREDIT().Select(x => new WebResponseDirectCredit
                    {
                        id = x.BNK_ID,
                        code = x.BNK_CODE,
                        nameth = x.BNK_NAME_TH,
                        nameen = x.BNK_NAME_ENG,
                        namejp = x.BNK_NAME_JP,
                        swiftcode = x.BNK_SWIFT_CODE,
                        path = x.BNK_PATH,
                        direct_credit = x.BNK_DIRECT_CREDIT,
                        stat_direct_credit = x.BNK_STAT_DIRECT_CREDIT,
                        direct_debit = x.BNK_DIRECT_DEBIT,
                        stat_direct_debit = x.BNK_STAT_DIRECT_DEBIT,
                        order = x.BNK_ORDER,
                    }).ToList<WebResponseDirectCredit>();

                    sb.Append(",\"directcredit\":");
                    sb.Append(JsonConvert.SerializeObject(directcredit));
                   
                    List<WebResponseDirectDebit> directdebit = tmlth.SP_TP_GET_DIREC_DEBIT().Select(x => new WebResponseDirectDebit
                    {
                        id = x.BNK_ID,
                        code = x.BNK_CODE,
                        nameth = x.BNK_NAME_TH,
                        nameen = x.BNK_NAME_ENG,
                        namejp = x.BNK_NAME_JP,
                        swiftcode = x.BNK_SWIFT_CODE,
                        path = x.BNK_PATH,
                        direct_credit = x.BNK_DIRECT_CREDIT,
                        stat_direct_credit = x.BNK_STAT_DIRECT_CREDIT,
                        direct_debit = x.BNK_DIRECT_DEBIT,
                        stat_direct_debit = x.BNK_STAT_DIRECT_DEBIT,
                        order = x.BNK_ORDER,
                    }).ToList<WebResponseDirectDebit>();

                    sb.Append(",\"directdebit\":");
                    sb.Append(JsonConvert.SerializeObject(directdebit));
                    
                    List<WebResponseFamily> family = tmlth.SP_TP_GET_FAM().Select(x => new WebResponseFamily
                    {
                        id = x.IDV_FAMILY_ID,
                        name_th = x.IDV_FAMILY_NAME_TH,
                        name_en = x.IDV_FAMILY_NAME_ENG,
                        name_jp = x.IDV_FAMILY_NAME_JP,
                        name_order = x.IDV_FAMILY_NAME_ORDER,
                        name_stat = x.IDV_FAMILY_NAME_STAT,

                    }).ToList<WebResponseFamily>();

                    sb.Append(",\"family\":");
                    sb.Append(JsonConvert.SerializeObject(family));

                    if (request_data.isloan == 1)
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                        ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                        {
                            return true; // **** Always accept
                        };
                        var client = new HttpClient();
                        var response = client.PostAsJsonAsync(DataFactory.URL_TOUCHPOINT_EWI,
                            EncryptDecryptAlgorithm.EncryptData(JsonConvert.SerializeObject(new Object[1]{
                            new {
                                chdrnum = request_data.chdrnum
                            }}), DataFactory.ENC_TOUCHPOINT_KEY)).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            if (result.StartsWith(ResultData.CODE_ERROR))
                            {
                                string[] error = result.Split(':');
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + error[1]);
                            }
                            else if (result.StartsWith(ResultData.CODE_NOTFOUND) ||
                                     result.StartsWith(ResultData.CODE_BLOCKED) ||
                                     result.StartsWith(ResultData.CODE_SESSION_TIMEOUT) ||
                                     result.StartsWith(ResultData.CODE_TOKEN_DENIED) ||
                                     result.StartsWith(ResultData.CODE_TOKEN_EXPIRE))
                            {
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + result); 
                            }
                            else
                            {
                                if (DataFactory.IS_PRODUCTION)
                                {
                                    #region LOAN_SUMMARY_PRODUCTION
                                    result = EncryptDecryptAlgorithm.DecryptData(response.Content.ReadAsStringAsync().Result, DataFactory.ENC_TOUCHPOINT_KEY);
                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseType life_policyloansenquiryResponseType = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseType>(result);
                                    string Returncode = life_policyloansenquiryResponseType.Returncode;
                                    string Runmessage = life_policyloansenquiryResponseType.Runmessage;

                                    string ref1 = "4511" + request_data.chdrnum;
                                    sb.Append(",\"ref1\":");
                                    sb.Append(JsonConvert.SerializeObject(ref1));

                                    CultureInfo cultureinfo = new CultureInfo("th-TH");
                                    string ref2 = now.ToString("ddMMyyyy", cultureinfo);
                                    sb.Append(",\"ref2\":");
                                    sb.Append(JsonConvert.SerializeObject(ref2));

                                    string policy_name = (from x in tmlth.tpolicy
                                                          where x.chdrnum == request_data.chdrnum
                                                          select x.insured).FirstOrDefault();

                                    sb.Append(",\"policy_name\":");
                                    sb.Append(JsonConvert.SerializeObject(policy_name));

                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseContentType life_policyloansenquiryResponseContentType = life_policyloansenquiryResponseType.content;
                                    decimal totalInterest = life_policyloansenquiryResponseContentType.loanDetailsCollection.Where(x => x.loanType == request_data.loantype).Sum(x => decimal.Parse(x.totalInterest));
                                    sb.Append(",\"loantotal\":");
                                    sb.Append(JsonConvert.SerializeObject(totalInterest));

                                    int is_success = Returncode == "EWI-0000I" ? 1 : 0;
                                    sb.Append(",\"is_success\":");
                                    sb.Append(JsonConvert.SerializeObject(is_success));

                                    sb.Append(",\"loanmessage\":");
                                    sb.Append(JsonConvert.SerializeObject(Runmessage));

                                    #region CODE777
                                    string chdrnum_instment = ref1 + "|" + ref2;
                                    WebResponsePaymentDTLByChdrnumInstment checkData = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(chdrnum_instment).Select(x => new WebResponsePaymentDTLByChdrnumInstment
                                    {
                                        paymentid = x.PAYMENT_ID,
                                        chdrnum_instment = x.CHDRNUM_INSTMENT,
                                        chdrnum = x.CHDRNUM,
                                        userid = x.USER_ID,
                                        accesskey = x.ACCESS_KEY,
                                        profileid = x.PROFILE_ID,
                                        signed_field_names = x.SIGNED_FIELD_NAMES,
                                        signed_datetime = x.SIGNED_DATE_TIME,
                                        paymenttype = x.PAYMENT_TYPE,
                                        bankcode = x.BANK_CODE,
                                        ref1 = x.REF1,
                                        ref2 = x.REF2,
                                        ref3 = x.REF3,
                                        ref4 = x.REF4,
                                        ref5 = x.REF5,
                                        ref_send = x.REF_SEND,
                                        amount = x.AMOUNT,
                                        currency = x.CURRENCY,
                                        language = x.LANGUAGE,
                                        bill_to_forename = x.BILL_TO_FORENAME,
                                        bill_to_surname = x.BILL_TO_SURNAME,
                                        bill_to_address_line1 = x.BILL_TO_ADDRESS_LINE1,
                                        bill_to_address_line2 = x.BILL_TO_ADDRESS_LINE2,
                                        bill_to_address_line3 = x.BILL_TO_ADDRESS_LINE3,
                                        bill_to_address_line4 = x.BILL_TO_ADDRESS_LINE4,
                                        bill_to_address_city = x.BILL_TO_ADDRESS_CITY,
                                        bill_to_address_country = x.BILL_TO_ADDRESS_COUNTRY,
                                        bill_to_address_postal_code = x.BILL_TO_ADDRESS_POSTAL_CODE,
                                        bill_to_address_email = x.BILL_TO_EMAIL,
                                        bill_to_address_phone = x.BILL_TO_PHONE,
                                        bill_to_address_mobile = x.BILL_TO_MOBILE,
                                        remark = x.REMARK,
                                        signature = x.SIGNATURE,
                                        resp_code = x.RESP_CODE,
                                        resp_mess = x.RESP_MESS,
                                        payment_status = x.PAYMENT_STATUS,
                                        payment_detail = x.PAYMENT_DETAIL,
                                        payment_code = x.PAYMENT_CODE,
                                        post_url = x.POST_URL,
                                        post_date = x.POST_DATE,
                                        post_back_by = x.POST_BACK_BY,
                                        post_back_date = x.POST_BACK_DATE,
                                        transaction_id = x.TRANSACTION_ID,
                                        idv_family_id = x.IDV_FAMILY_ID,
                                        idv_paytype_detail = x.IDV_PAYTYPE_DETAIL
                                    }).FirstOrDefault();

                                    if (checkData != null)
                                    {
                                        try
                                        {
                                            var payment_client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                                            ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                            System.Security.Cryptography.X509Certificates.X509Chain chain,
                                            System.Net.Security.SslPolicyErrors sslPolicyErrors)
                                            {
                                                return true; // **** Always accept
                                            };

                                            TmlthOnlinePayment.ResultInfoOfPaymentSupResponseInfoDtoaKPUf2By check = payment_client.CheckResponseDataWithTransactionID(checkData.transaction_id, false);
                                            WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT_LOAN").Select(x => new WebReponseAccesskey
                                            {
                                                id = x.REQUEST_ID,
                                                access_key = x.REQUEST_ACCESS_KEY,
                                                profile_id = x.REQUEST_PROFILE_ID,
                                                app_nme = x.REQUEST_APP_NME,
                                                app_type = x.REQUEST_APP_TYPE,
                                                service_nme = x.REQUEST_SERVICE_NME,
                                            }).FirstOrDefault();

                                            if (check.Data != null)
                                            {
                                                //Update ข้อมูลผลชำระเงิน
                                                checkData.resp_code = check.Data.RESP_CODE;
                                                checkData.resp_mess = check.Data.RESP_MESS;
                                                checkData.payment_status = check.Data.PAYMENT_STATUS;
                                                checkData.payment_code = check.Data.PAYMENT_CODE;
                                                checkData.accesskey = Accesskey.access_key;
                                                checkData.profileid = Accesskey.profile_id;
                                                var postpayment = tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(checkData.resp_code,
                                                   checkData.resp_mess, checkData.payment_status, checkData.payment_detail,
                                                   checkData.payment_code, checkData.transaction_id, checkData.accesskey, checkData.profileid);
                                                var checkConnectionFail = false;

                                                if (!string.IsNullOrWhiteSpace(check.Data.RESP_CODE))
                                                {
                                                    checkConnectionFail = CheckCode777(check.Data.RESP_CODE);
                                                }

                                                if (checkConnectionFail)
                                                {
                                                    return DataGenerator.generateTextResult(ResultData.CODE_CANT_PAYMENT + request_data.isloan);
                                                }
                                            }


                                        }
                                        catch (Exception e)
                                        {
                                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                                        }
                                    }
                                    else
                                    {
                                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                    }
                                    #endregion

                                    #endregion
                                }
                                else
                                {

                                    #region LOAN_SUMMARY_TEST
                                    result = EncryptDecryptAlgorithm.DecryptData(response.Content.ReadAsStringAsync().Result, DataFactory.ENC_TOUCHPOINT_KEY);
                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseType life_policyloansenquiryResponseType = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseType>(result);
                                    string Returncode = life_policyloansenquiryResponseType.Returncode;
                                    string Runmessage = life_policyloansenquiryResponseType.Runmessage;

                                    string ref1 = "4511" + request_data.chdrnum;
                                    sb.Append(",\"ref1\":");
                                    sb.Append(JsonConvert.SerializeObject(ref1));

                                    CultureInfo cultureinfo = new CultureInfo("th-TH");
                                    string ref2 = now.ToString("ddMMyyyy", cultureinfo);
                                    sb.Append(",\"ref2\":");
                                    sb.Append(JsonConvert.SerializeObject(ref2));

                                    string policy_name = (from x in tmlth.tpolicy
                                                          where x.chdrnum == request_data.chdrnum
                                                          select x.insured).FirstOrDefault();

                                    sb.Append(",\"policy_name\":");
                                    sb.Append(JsonConvert.SerializeObject(policy_name));

                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseContentType life_policyloansenquiryResponseContentType = life_policyloansenquiryResponseType.content;
                                    decimal totalInterest = life_policyloansenquiryResponseContentType.loanDetailsCollection.Where(x => x.loanType == request_data.loantype).Sum(x => decimal.Parse(x.totalInterest));
                                    sb.Append(",\"loantotal\":");
                                    sb.Append(JsonConvert.SerializeObject(totalInterest));

                                    int is_success = Returncode == "EWI-0000I" ? 1 : 0;
                                    sb.Append(",\"is_success\":");
                                    sb.Append(JsonConvert.SerializeObject(is_success));

                                    sb.Append(",\"loanmessage\":");
                                    sb.Append(JsonConvert.SerializeObject(Runmessage));

                                    #region CODE777
                                    string chdrnum_instment = ref1 + "|" + ref2;
                                    WebResponsePaymentDTLByChdrnumInstment checkData = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(chdrnum_instment).Select(x => new WebResponsePaymentDTLByChdrnumInstment
                                    {
                                        paymentid = x.PAYMENT_ID,
                                        chdrnum_instment = x.CHDRNUM_INSTMENT,
                                        chdrnum = x.CHDRNUM,
                                        userid = x.USER_ID,
                                        accesskey = x.ACCESS_KEY,
                                        profileid = x.PROFILE_ID,
                                        signed_field_names = x.SIGNED_FIELD_NAMES,
                                        signed_datetime = x.SIGNED_DATE_TIME,
                                        paymenttype = x.PAYMENT_TYPE,
                                        bankcode = x.BANK_CODE,
                                        ref1 = x.REF1,
                                        ref2 = x.REF2,
                                        ref3 = x.REF3,
                                        ref4 = x.REF4,
                                        ref5 = x.REF5,
                                        ref_send = x.REF_SEND,
                                        amount = x.AMOUNT,
                                        currency = x.CURRENCY,
                                        language = x.LANGUAGE,
                                        bill_to_forename = x.BILL_TO_FORENAME,
                                        bill_to_surname = x.BILL_TO_SURNAME,
                                        bill_to_address_line1 = x.BILL_TO_ADDRESS_LINE1,
                                        bill_to_address_line2 = x.BILL_TO_ADDRESS_LINE2,
                                        bill_to_address_line3 = x.BILL_TO_ADDRESS_LINE3,
                                        bill_to_address_line4 = x.BILL_TO_ADDRESS_LINE4,
                                        bill_to_address_city = x.BILL_TO_ADDRESS_CITY,
                                        bill_to_address_country = x.BILL_TO_ADDRESS_COUNTRY,
                                        bill_to_address_postal_code = x.BILL_TO_ADDRESS_POSTAL_CODE,
                                        bill_to_address_email = x.BILL_TO_EMAIL,
                                        bill_to_address_phone = x.BILL_TO_PHONE,
                                        bill_to_address_mobile = x.BILL_TO_MOBILE,
                                        remark = x.REMARK,
                                        signature = x.SIGNATURE,
                                        resp_code = x.RESP_CODE,
                                        resp_mess = x.RESP_MESS,
                                        payment_status = x.PAYMENT_STATUS,
                                        payment_detail = x.PAYMENT_DETAIL,
                                        payment_code = x.PAYMENT_CODE,
                                        post_url = x.POST_URL,
                                        post_date = x.POST_DATE,
                                        post_back_by = x.POST_BACK_BY,
                                        post_back_date = x.POST_BACK_DATE,
                                        transaction_id = x.TRANSACTION_ID,
                                        idv_family_id = x.IDV_FAMILY_ID,
                                        idv_paytype_detail = x.IDV_PAYTYPE_DETAIL
                                    }).FirstOrDefault();

                                    if (checkData != null)
                                    {
                                        try
                                        {
                                            var payment_client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                                            ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                            System.Security.Cryptography.X509Certificates.X509Chain chain,
                                            System.Net.Security.SslPolicyErrors sslPolicyErrors)
                                            {
                                                return true; // **** Always accept
                                            };

                                            TmlthOnlinePayment.ResultInfoOfPaymentSupResponseInfoDtoaKPUf2By check = payment_client.CheckResponseDataWithTransactionID(checkData.transaction_id, false);
                                            WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT_LOAN").Select(x => new WebReponseAccesskey
                                            {
                                                id = x.REQUEST_ID,
                                                access_key = x.REQUEST_ACCESS_KEY,
                                                profile_id = x.REQUEST_PROFILE_ID,
                                                app_nme = x.REQUEST_APP_NME,
                                                app_type = x.REQUEST_APP_TYPE,
                                                service_nme = x.REQUEST_SERVICE_NME,
                                            }).FirstOrDefault();

                                            if (check.Data != null)
                                            {
                                                //Update ข้อมูลผลชำระเงิน
                                                checkData.resp_code = check.Data.RESP_CODE;
                                                checkData.resp_mess = check.Data.RESP_MESS;
                                                checkData.payment_status = check.Data.PAYMENT_STATUS;
                                                checkData.payment_code = check.Data.PAYMENT_CODE;
                                                checkData.accesskey = Accesskey.access_key;
                                                checkData.profileid = Accesskey.profile_id;
                                                var postpayment = tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(checkData.resp_code,
                                                   checkData.resp_mess, checkData.payment_status, checkData.payment_detail,
                                                   checkData.payment_code, checkData.transaction_id, checkData.accesskey, checkData.profileid);
                                                var checkConnectionFail = false;

                                                if (!string.IsNullOrWhiteSpace(check.Data.RESP_CODE))
                                                {
                                                    checkConnectionFail = CheckCode777(check.Data.RESP_CODE);
                                                }

                                                if (checkConnectionFail)
                                                {
                                                    return DataGenerator.generateTextResult(ResultData.CODE_CANT_PAYMENT + request_data.isloan);
                                                }
                                            }


                                        }
                                        catch (Exception e)
                                        {
                                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                                        }
                                    }
                                    else
                                    {
                                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                    }
                                    #endregion

                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            var errorlog = ContentTools.ErrorControllerLog("ERROR:" + request_data.chdrnum + "|EWI CAN'T REQUEST");
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR+ "EWI CAN'T REQUEST");
                        }
                    }
                    else
                    {

                        var ListPaymentPeriodDetail = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD_DETAIL(request_data.chdrnum).FirstOrDefault();
                       

                        WebResponsePaymentPeriodDetail paymentperioddetail = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD_DETAIL(request_data.chdrnum).Select(x => new WebResponsePaymentPeriodDetail
                        {
                            insured = x.INSURED,
                            name_insured = x.NAME_INSURE,
                            payment_date = x.PAYMENT_DATE,
                            payment_date_thai = x.PAYMENT_DATE_THAI,
                            payment_period = x.PAYMENT_PERIOD,
                            period_year = x.PRERIOD_YEAR,
                            period_time = x.PRERIOD_TIME,
                            payment_premium = x.PAYMENT_PREMIUM,
                            addr1 = x.ADDR1,
                            addr2 = x.ADDR2,
                            addr3 = x.ADDR3,
                            addr4 = x.ADDR4,
                            addr5 = x.ADDR5,
                            stat = x.STAT,
                            msg = x.MSG,
                            today = x.TODAY,
                            com_code = x.COM_CODE,
                            ref1 = x.REF1,
                            ref2 = x.REF2,
                            ref3 = x.REF3,
                            ref4 = x.REF4,
                            ref5 = x.REF5,
                            zipcode = x.Zipcode,
                            ref_send = x.REF_SEND,
                            currency_code = x.CURREN_CODE,
                            billing_code = x.BILLING_CODE,
                            product_code = x.PRODUCT_CODE,
                            agen_code = x.AGEN_CODE,
                            agen_name = x.AGEN_NAME,
                            insured_phone = x.INSURED_PHONE,
                            sum_insured = x.SUM_INSURED,
                            coverage_plan = x.COVERAGE_PLAN,
                            user_id = x.USER_ID,
                            user_name = x.USER_NAME,
                            payment_option = x.PAYMENT_OPTION,
                            payment_period_dtm = x.PAYMENT_PERIOD_DTM,
                            source_business = x.SOURCE_BUSINESS,
                        }).FirstOrDefault();

                        if (paymentperioddetail == null)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||PAYMENT_PERIOD_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }

                        string chdrnum_instment = paymentperioddetail.insured + "|" + paymentperioddetail.period_year + "|" + paymentperioddetail.period_time;
                        if (paymentperioddetail.stat == "Y" && paymentperioddetail.msg == "")
                        {
                            WebResponsePaymentDTLByChdrnumInstment listCheckData = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(chdrnum_instment).Select(x => new WebResponsePaymentDTLByChdrnumInstment
                            {
                                paymentid = x.PAYMENT_ID,
                                chdrnum_instment = x.CHDRNUM_INSTMENT,
                                chdrnum = x.CHDRNUM,
                                userid = x.USER_ID,
                                accesskey = x.ACCESS_KEY,
                                profileid = x.PROFILE_ID,
                                signed_field_names = x.SIGNED_FIELD_NAMES,
                                signed_datetime = x.SIGNED_DATE_TIME,
                                paymenttype = x.PAYMENT_TYPE,
                                bankcode = x.BANK_CODE,
                                ref1 = x.REF1,
                                ref2 = x.REF2,
                                ref3 = x.REF3,
                                ref4 = x.REF4,
                                ref5 = x.REF5,
                                ref_send = x.REF_SEND,
                                amount = x.AMOUNT,
                                currency = x.CURRENCY,
                                language = x.LANGUAGE,
                                bill_to_forename = x.BILL_TO_FORENAME,
                                bill_to_surname = x.BILL_TO_SURNAME,
                                bill_to_address_line1 = x.BILL_TO_ADDRESS_LINE1,
                                bill_to_address_line2 = x.BILL_TO_ADDRESS_LINE2,
                                bill_to_address_line3 = x.BILL_TO_ADDRESS_LINE3,
                                bill_to_address_line4 = x.BILL_TO_ADDRESS_LINE4,
                                bill_to_address_city = x.BILL_TO_ADDRESS_CITY,
                                bill_to_address_country = x.BILL_TO_ADDRESS_COUNTRY,
                                bill_to_address_postal_code = x.BILL_TO_ADDRESS_POSTAL_CODE,
                                bill_to_address_email = x.BILL_TO_EMAIL,
                                bill_to_address_phone = x.BILL_TO_PHONE,
                                bill_to_address_mobile = x.BILL_TO_MOBILE,
                                remark = x.REMARK,
                                signature = x.SIGNATURE,
                                resp_code = x.RESP_CODE,
                                resp_mess = x.RESP_MESS,
                                payment_status = x.PAYMENT_STATUS,
                                payment_detail = x.PAYMENT_DETAIL,
                                payment_code = x.PAYMENT_CODE,
                                post_url = x.POST_URL,
                                post_date = x.POST_DATE,
                                post_back_by = x.POST_BACK_BY,
                                post_back_date = x.POST_BACK_DATE,
                                transaction_id = x.TRANSACTION_ID,
                                idv_family_id = x.IDV_FAMILY_ID,
                                idv_paytype_detail = x.IDV_PAYTYPE_DETAIL,
                            }).FirstOrDefault();

                            //Get AccessKey
                            WebReponseAccesskey Accesskey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", request_data.isloan == 1 ? "PAYMENT_LOAN" : "PAYMENT").Select(x => new WebReponseAccesskey
                            {
                                id = x.REQUEST_ID,
                                access_key = x.REQUEST_ACCESS_KEY,
                                profile_id = x.REQUEST_PROFILE_ID,
                                app_nme = x.REQUEST_APP_NME,
                                app_type = x.REQUEST_APP_TYPE,
                                service_nme = x.REQUEST_SERVICE_NME,
                            }).FirstOrDefault();

                            if (Accesskey == null)
                            {
                                var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                    ResultData.CODE_ERROR_DATA_NOT_FOUND);
                            }

                            if (listCheckData != null)
                            {
                                if ((listCheckData.payment_status != true) && !string.IsNullOrWhiteSpace(listCheckData.transaction_id))
                                {

                                    try
                                    {
                                        var client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                        //ENABLE TLS
                                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                                        //IGNORE CERT
                                        ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                                        {
                                            return true; // **** Always accept
                                        };

                                        TmlthOnlinePayment.ResultInfoOfPaymentSupResponseInfoDtoaKPUf2By check = client.CheckResponseDataWithTransactionID(listCheckData.transaction_id, false);
                                        if (check.Data != null)
                                        {
                                            //Update ข้อมูลผลชำระเงิน
                                            listCheckData.resp_code = check.Data.RESP_CODE;
                                            listCheckData.resp_mess = check.Data.RESP_MESS;
                                            listCheckData.payment_status = check.Data.PAYMENT_STATUS;
                                            listCheckData.payment_code = check.Data.PAYMENT_CODE;
                                            listCheckData.accesskey = Accesskey.access_key;
                                            listCheckData.profileid = Accesskey.profile_id;
                                            var postpayment = tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(listCheckData.resp_code,
                                                listCheckData.resp_mess, listCheckData.payment_status, listCheckData.payment_detail,
                                                listCheckData.payment_code, listCheckData.transaction_id, listCheckData.accesskey, listCheckData.profileid);

                                            var checkConnectionFail = false;
                                            if (!string.IsNullOrWhiteSpace(check.Data.RESP_CODE))
                                            {
                                                checkConnectionFail = CheckCode777(check.Data.RESP_CODE);
                                            }
                                            if (checkConnectionFail)
                                            {
                                                paymentperioddetail.stat = "N";
                                                paymentperioddetail.msg = check.Data.RESP_MESS;
                                            }
                                            else
                                            {

                                                paymentperioddetail = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD_DETAIL(request_data.chdrnum).Select(x => new WebResponsePaymentPeriodDetail
                                                {
                                                    insured = x.INSURED,
                                                    name_insured = x.NAME_INSURE,
                                                    payment_date = x.PAYMENT_DATE,
                                                    payment_date_thai = x.PAYMENT_DATE_THAI,
                                                    payment_period = x.PAYMENT_PERIOD,
                                                    period_year = x.PRERIOD_YEAR,
                                                    period_time = x.PRERIOD_TIME,
                                                    payment_premium = x.PAYMENT_PREMIUM,
                                                    addr1 = x.ADDR1,
                                                    addr2 = x.ADDR2,
                                                    addr3 = x.ADDR3,
                                                    addr4 = x.ADDR4,
                                                    addr5 = x.ADDR5,
                                                    stat = x.STAT,
                                                    msg = x.MSG,
                                                    today = x.TODAY,
                                                    com_code = x.COM_CODE,
                                                    ref1 = x.REF1,
                                                    ref2 = x.REF2,
                                                    ref3 = x.REF3,
                                                    ref4 = x.REF4,
                                                    ref5 = x.REF5,
                                                    zipcode = x.Zipcode,
                                                    ref_send = x.REF_SEND,
                                                    currency_code = x.CURREN_CODE,
                                                    billing_code = x.BILLING_CODE,
                                                    product_code = x.PRODUCT_CODE,
                                                    agen_code = x.AGEN_CODE,
                                                    agen_name = x.AGEN_NAME,
                                                    insured_phone = x.INSURED_PHONE,
                                                    sum_insured = x.SUM_INSURED,
                                                    coverage_plan = x.COVERAGE_PLAN,
                                                    user_id = x.USER_ID,
                                                    user_name = x.USER_NAME,
                                                    payment_option = x.PAYMENT_OPTION,
                                                    payment_period_dtm = x.PAYMENT_PERIOD_DTM,
                                                    source_business = x.SOURCE_BUSINESS,
                                                }).FirstOrDefault();

                                                paymentperioddetail.ref1 = paymentperioddetail.com_code + paymentperioddetail.ref1;

                                            }
                                        }
                                        //PaymentSupRequestTemplateInfoDto request_data3 = JsonConvert.DeserializeObject<PaymentSupRequestTemplateInfoDto>(decryptData);
                                        //TMLTH_TOUCHPOINT_API_SERVER.TmlthOnlinePayment.PaymentSupRequestTemplateInfoDto request_data3 = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.TmlthOnlinePayment.PaymentSupRequestTemplateInfoDto>(decryptData);
                                        //TmlthPayment.ResultInfoOfPaymentSupRequestInfoDtoaKPUf2By ParaResult = client.GetPaymentRequestTemplate(request_data3);
                                        //TMLTH_TOUCHPOINT_API_SERVER.TmlthOnlinePayment.ResultInfoOfPaymentSupRequestInfoDtoaKPUf2By result = client.GetPaymentRequestTemplate(request_data3);

                                    }
                                    catch (Exception e)
                                    {
                                        var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                                    }

                                }
                            }

                        }

                        sb.Append(",\"paymentperioddetail\":");
                        sb.Append(JsonConvert.SerializeObject(paymentperioddetail));
                    }
                   
                    sb.Append("}");
                   
                  return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }

        public bool CheckCode777(string val)
        {
            var blRes = new bool();
            if (val.Length > 0)
            {
                blRes = val.Substring(val.Length - 3) == "777" ? true : false;
            }
            return blRes;
        }
    }

}
