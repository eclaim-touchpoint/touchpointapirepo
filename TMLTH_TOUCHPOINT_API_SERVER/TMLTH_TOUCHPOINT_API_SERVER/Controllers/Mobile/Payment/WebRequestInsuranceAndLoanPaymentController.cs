﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Payment
{
    public class WebRequestInsuranceAndLoanPaymentController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestInsuranceAndLoanPayment";
            //string clntnum = "60054082";
            //string chdrnum = "13434234";
            ////https://staging.tokiomarinelife.co.th/OnlinePaymentWS_SupportServices/OnlinePaymentSupportServices.svc
            //var x = new TmlthPayment.OnlinePaymentSupportServices();
            //var transaction_id = "46e7d2c3-51dd-4428-a28f-7c9d2945d057";
            //try
            //{
            //    var a = x.CheckResponseDataWithTransactionID(transaction_id);
            //}
            //catch (Exception e)
            //{
            //    return DataGenerator.generateTextResult(e.Message);
            //}
            //tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD(clntnum, chdrnum);

            //return DataGenerator.generateTextResult("HELLO TMLTH");

            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestPaymentPeriod request_data = JsonConvert.DeserializeObject<WebRequestPaymentPeriod>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        
                            var errorlog = ContentTools.ErrorControllerLog("WebRequestInsuranceAndLoanPayment" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Payment",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum + "," +
                        "INSURED_NO" + "=" + request_data.insured_no
                        ),
                        request_data.device);


                    List<WebResponsePaymentPeriod> paymentperiod = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD(request_data.insured_no, request_data.chdrnum).Select(x => new WebResponsePaymentPeriod
                    {
                        insured = x.INSURED,
                        payment_date = x.PAYMENT_DATE,
                        payment_period = x.PAYMENT_PERIOD,
                        period_year = x.PERIOD_YEAR,
                        period_time = x.PERIOD_TIME,
                        payment_premium = x.PAYMENT_PREMIUM,
                        stat = x.STAT,
                        msg = x.MSG,
                        billing_code = x.BILLING_CODE,
                        product_code = x.PRODUCT_CODE,
                        channel = x.CHANNEL,
                        unpaid = x.UNPAID,
                    }).ToList<WebResponsePaymentPeriod>();

                    List<objErr777> lstError777 = new List<objErr777>();

                    if(paymentperiod.First().stat == "Y")
                    {

                        foreach(var payment in paymentperiod)
                        {
                            payment.chdrnum_instment =  payment.insured + "|" + payment.period_year + "|" + payment.period_time;
                            var payment_dtl = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(payment.chdrnum_instment.ToString().Trim()).FirstOrDefault();
                            if(payment_dtl != null)
                            {
                                if ((payment_dtl.PAYMENT_STATUS  != true) && !(string.IsNullOrWhiteSpace(payment_dtl.TRANSACTION_ID)))
                                {
                                    //Enable TLS
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                                    //Ignore Cert
                                    ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
                                    {
                                        return true; // **** Always accept
                                    };
                                    
                                    // Get AccessKey
                                    WebReponseAccesskey lstDataAccessKey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT").Select(x => new WebReponseAccesskey
                                    {
                                        id = x.REQUEST_ID,
                                        access_key = x.REQUEST_ACCESS_KEY,
                                        profile_id = x.REQUEST_PROFILE_ID,
                                        app_nme = x.REQUEST_APP_NME,
                                        app_type = x.REQUEST_APP_TYPE,
                                        service_nme = x.REQUEST_SERVICE_NME
                                    }).FirstOrDefault();

                                    if (lstDataAccessKey == null)
                                    {
                                        var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                    }

                                    var client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                    var check = client.CheckResponseDataWithTransactionID(payment_dtl.TRANSACTION_ID, false);
                                    if(check != null)
                                    {
                                        if (!string.IsNullOrWhiteSpace(check.Data.RESP_CODE))
                                        {
                                            Check d = new Check();
                                            var checkConnectionFail = d.CheckCode777(check.Data.RESP_CODE);
                                            if (checkConnectionFail)
                                            {
                                                objErr777 objError777 = new objErr777();
                                                objError777.chdrnum = payment.insured;
                                                objError777.resp_code = check.Data.RESP_CODE;
                                                lstError777.Add(objError777);
                                            }
                                        } 
                                    }

                                    tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(
                                        check.Data.RESP_CODE, 
                                        check.Data.RESP_MESS, 
                                        check.Data.PAYMENT_STATUS, 
                                        "", 
                                        check.Data.PAYMENT_CODE, 
                                        check.Data.TRANSACTION_ID, 
                                        lstDataAccessKey.access_key,
                                        lstDataAccessKey.profile_id
                                    );

                                }

                            }
                        }

                        paymentperiod = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD(request_data.insured_no, request_data.chdrnum).Select(x => new WebResponsePaymentPeriod
                        {
                            insured = x.INSURED,
                            payment_date = x.PAYMENT_DATE,
                            payment_period = x.PAYMENT_PERIOD,
                            period_year = x.PERIOD_YEAR,
                            period_time = x.PERIOD_TIME,
                            payment_premium = x.PAYMENT_PREMIUM,
                            stat = x.STAT,
                            msg = x.MSG,
                            billing_code = x.BILLING_CODE,
                            product_code = x.PRODUCT_CODE,
                            channel = x.CHANNEL,
                            unpaid = x.UNPAID,
                        }).ToList<WebResponsePaymentPeriod>();

                        if (lstError777.Count > 0) {
                            foreach (var obj777 in lstError777) {
                                foreach (var objData in paymentperiod) {
                                    if(objData.insured == obj777.chdrnum)
                                    {
                                        objData.resp_code = obj777.resp_code;
                                    }
                                    objData.remark = "**หมายเหตุ N/A รอการตรวจสอบจากธนาคาร (กรุณาทำรายการใหม่อีกครั้ง)";
                                }
                            }
                        }

                        if(paymentperiod.Where(x => x.unpaid == "Y").Count() > 0){
                            foreach(var payment in paymentperiod){
                                payment.remark2 = "**หมายเหตุ กรมธรรม์เกินกำหนดการชำระ กรุณาติดต่อเจ้าหน้าที่ Call Center 02-650-1400";
                            }
                        }
                    }
                    
                    List<WebResponsePolicyLoanListClntnum> policyloan = tmlth.SP_TP_GET_POLICY_LOAN_BY_CLNTNUM(cookieData.ID.ToString()).Select(x => new WebResponsePolicyLoanListClntnum
                    {
                        policyno = x.POLICY_NO,
                        total_apl = x.TOTAL_APL,
                        total_rpl = x.TOTAL_RPL,
                        total = x.TOTAL
                    }).ToList<WebResponsePolicyLoanListClntnum>();

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"paymentperiod\":");
                    sb.Append(JsonConvert.SerializeObject(paymentperiod));
                    sb.Append(",\"policyloan\":");
                    sb.Append(JsonConvert.SerializeObject(policyloan));
                    sb.Append("}");
                   return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }

    class Check {
        public bool CheckCode777(string val) {
            var blRes = new bool();
            if (val.Length > 0) {
                blRes = val.Substring(val.Length - 3) == "777" ? true : false;
            }
            return blRes;
        }
    }
}
