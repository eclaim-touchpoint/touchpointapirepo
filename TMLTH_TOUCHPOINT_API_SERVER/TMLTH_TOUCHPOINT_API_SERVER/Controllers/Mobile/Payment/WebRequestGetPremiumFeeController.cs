﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.Payment
{
    public class WebRequestGetPremiumFeeController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestGetPremiumFee";
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestGetPremiumResult request_data = JsonConvert.DeserializeObject<WebRequestGetPremiumResult>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken

                        var errorlog = ContentTools.ErrorControllerLog("WebRequestGetPremiumFee" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Payment",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum + "," +
                        "PAYMENTTYPE" + "=" + request_data.PaymentType + "," +
                        "BANKCODE" + "=" + request_data.BankCode
                        ),
                        request_data.device);

                    //GET CalculatePremiumFee
                    var objResult = new clsPremiumFee();
                    var istResult = new List<clsPremiumFee>();
                    var objBLPa = new clsPaymentOnline();

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                    ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true; // **** Always accept
                    };
                   
                    if (request_data.isloan == 1)
                    {
                        var client = new HttpClient();
                        var response = client.PostAsJsonAsync(DataFactory.URL_TOUCHPOINT_EWI,
                            EncryptDecryptAlgorithm.EncryptData(JsonConvert.SerializeObject(new Object[1]{
                            new {
                                chdrnum = request_data.chdrnum
                            }}), DataFactory.ENC_TOUCHPOINT_KEY)).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            if (result.StartsWith(ResultData.CODE_ERROR))
                            {
                                string[] error = result.Split(':');
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + error[1]);
                            }
                            else if (result.StartsWith(ResultData.CODE_NOTFOUND) ||
                                     result.StartsWith(ResultData.CODE_BLOCKED) ||
                                     result.StartsWith(ResultData.CODE_SESSION_TIMEOUT) ||
                                     result.StartsWith(ResultData.CODE_TOKEN_DENIED) ||
                                     result.StartsWith(ResultData.CODE_TOKEN_EXPIRE))
                            {
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + result);
                            }
                            else
                            {
                                if (DataFactory.IS_PRODUCTION)
                                {
                                    #region LOAN_SUMMARY_PRODUCTION
                                    result = EncryptDecryptAlgorithm.DecryptData(response.Content.ReadAsStringAsync().Result, DataFactory.ENC_TOUCHPOINT_KEY);
                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseType life_policyloansenquiryResponseType = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseType>(result);
                                    string Returncode = life_policyloansenquiryResponseType.Returncode;
                                    string Runmessage = life_policyloansenquiryResponseType.Runmessage;

                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_PRODUCTION.life_policyloansenquiryResponseContentType life_policyloansenquiryResponseContentType = life_policyloansenquiryResponseType.content;
                                    decimal totalInterest = life_policyloansenquiryResponseContentType.loanDetailsCollection.Where(x => x.loanType == request_data.loantype).Sum(x => decimal.Parse(x.totalInterest));
                                    ContentTools.ErrorControllerLog(page + "||EWI_TOTAL_INTEREST:" + JsonConvert.SerializeObject(totalInterest));
                                    WebResponsePolicyLoanSummary loanSummary = tmlth.SP_TP_GET_POLICY_LOAN_SUMMARY(request_data.chdrnum).Select(x => new WebResponsePolicyLoanSummary
                                    {
                                        policy_no = x.POLICY_NO,
                                        total_apl = x.TOTAL_APL,
                                        total_rpl = x.TOTAL_RPL,
                                        total = x.TOTAL,
                                        billing_code = x.BILLING_CODE,
                                        product_code = x.PRODUCT_CODE,
                                        source_business = x.SOURCE_BUSINESS
                                    }).FirstOrDefault();

                                    WebReponseAccesskey IsDataAcessKey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT_LOAN").Select(x => new WebReponseAccesskey
                                    {
                                        id = x.REQUEST_ID,
                                        access_key = x.REQUEST_ACCESS_KEY,
                                        profile_id = x.REQUEST_PROFILE_ID,
                                        app_nme = x.REQUEST_APP_NME,
                                        app_type = x.REQUEST_APP_TYPE,
                                        service_nme = x.REQUEST_SERVICE_NME
                                    }).FirstOrDefault();

                                    if (IsDataAcessKey == null)
                                    {
                                        var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                    }

                                    string access_key = IsDataAcessKey.access_key,
                                       profile_id = IsDataAcessKey.profile_id,
                                       payment_type = request_data.PaymentType,
                                       bank_code = request_data.BankCode,
                                       billing_code = loanSummary.billing_code,
                                       product_code = loanSummary.product_code,
                                       source_business = loanSummary.source_business,
                                       currency = "THB";

                                    double amount = (double)totalInterest;
                                    if ((request_data.loantype == DataFactory.LOAN_TYPE_AUTO || request_data.loantype == DataFactory.LOAN_TYPE_PAYMENT) && request_data.loanpaytype == 2)
                                    {
                                        amount = Convert.ToDouble(request_data.loanpaytotal);
                                    }

                                    ContentTools.ErrorControllerLog(page + "||RESULT_FEE_AMOUNT:" + amount);

                                    if (request_data.PaymentType == "001")
                                    {
                                        var tmlthService = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                        var result_fee = tmlthService.GetCalculatePremiumFee(access_key, profile_id, payment_type, bank_code, billing_code, product_code, amount, currency, source_business);
                                        double dblfee = result_fee.Data,
                                               dblamount = Convert.ToDouble(amount),
                                               dbltotalAmount = dblamount + dblfee;

                                        objResult.fee = result_fee.Data;
                                        objResult.amount = Convert.ToDouble(amount);
                                        objResult.total_amount = dbltotalAmount;
                                        ContentTools.ErrorControllerLog(page + " LOAN CASE 001 result_fee:" + result_fee.Data + ", amount:" + amount + ", total:" + dbltotalAmount);
                                    }
                                    else
                                    {
                                        objResult.fee = 0.00;
                                        objResult.amount = Convert.ToDouble(amount);
                                        objResult.total_amount = Convert.ToDouble(amount);
                                        ContentTools.ErrorControllerLog(page + " LOAN CASE OTHER result_fee:" + objResult.fee + ", amount:" + objResult.amount + ", total:" + objResult.total_amount);
                                    }

                                    sb.Append("{");
                                    sb.Append("\"token\":");
                                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                                    sb.Append(",\"premiumfee\":");
                                    sb.Append(JsonConvert.SerializeObject(objResult));
                                    sb.Append("}");
                                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                                    #endregion
                                }
                                else
                                {
                                    #region LOAN_SUMMARY_TEST
                                    result = EncryptDecryptAlgorithm.DecryptData(response.Content.ReadAsStringAsync().Result, DataFactory.ENC_TOUCHPOINT_KEY);
                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseType life_policyloansenquiryResponseType = JsonConvert.DeserializeObject<TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseType>(result);
                                    string Returncode = life_policyloansenquiryResponseType.Returncode;
                                    string Runmessage = life_policyloansenquiryResponseType.Runmessage;

                                    TMLTH_TOUCHPOINT_API_SERVER.EWI_SERVICE_TEST.life_policyloansenquiryResponseContentType life_policyloansenquiryResponseContentType = life_policyloansenquiryResponseType.content;
                                    decimal totalInterest = life_policyloansenquiryResponseContentType.loanDetailsCollection.Where(x => x.loanType == request_data.loantype).Sum(x => decimal.Parse(x.totalInterest));
                                    ContentTools.ErrorControllerLog(page + "||EWI_TOTAL_INTEREST:" + JsonConvert.SerializeObject(totalInterest));
                                    WebResponsePolicyLoanSummary loanSummary = tmlth.SP_TP_GET_POLICY_LOAN_SUMMARY(request_data.chdrnum).Select(x => new WebResponsePolicyLoanSummary
                                    {
                                        policy_no = x.POLICY_NO,
                                        total_apl = x.TOTAL_APL,
                                        total_rpl = x.TOTAL_RPL,
                                        total = x.TOTAL,
                                        billing_code = x.BILLING_CODE,
                                        product_code = x.PRODUCT_CODE,
                                        source_business = x.SOURCE_BUSINESS
                                    }).FirstOrDefault();
                                    
                                    WebReponseAccesskey IsDataAcessKey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT_LOAN").Select(x => new WebReponseAccesskey
                                    {
                                        id = x.REQUEST_ID,
                                        access_key = x.REQUEST_ACCESS_KEY,
                                        profile_id = x.REQUEST_PROFILE_ID,
                                        app_nme = x.REQUEST_APP_NME,
                                        app_type = x.REQUEST_APP_TYPE,
                                        service_nme = x.REQUEST_SERVICE_NME
                                    }).FirstOrDefault();

                                    if (IsDataAcessKey == null)
                                    {
                                        var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                    }

                                    string access_key = IsDataAcessKey.access_key,
                                       profile_id = IsDataAcessKey.profile_id,
                                       payment_type = request_data.PaymentType,
                                       bank_code = request_data.BankCode,
                                       billing_code = loanSummary.billing_code,
                                       product_code = loanSummary.product_code,
                                       source_business = loanSummary.source_business,
                                       currency = "THB";

                                    double amount = (double)totalInterest;
                                    if ((request_data.loantype == DataFactory.LOAN_TYPE_AUTO || request_data.loantype == DataFactory.LOAN_TYPE_PAYMENT) && request_data.loanpaytype == 2)
                                    {
                                        amount = Convert.ToDouble(request_data.loanpaytotal);
                                    }

                                    ContentTools.ErrorControllerLog(page + "||RESULT_FEE_AMOUNT:" + amount);

                                    if (request_data.PaymentType == "001")
                                    {
                                        var tmlthService = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                                        var result_fee = tmlthService.GetCalculatePremiumFee(access_key, profile_id, payment_type, bank_code, billing_code, product_code, amount, currency, source_business);
                                        double dblfee = result_fee.Data,
                                               dblamount = Convert.ToDouble(amount),
                                               dbltotalAmount = dblamount + dblfee;

                                        objResult.fee = result_fee.Data;
                                        objResult.amount = Convert.ToDouble(amount);
                                        objResult.total_amount = dbltotalAmount;
                                        ContentTools.ErrorControllerLog(page + " LOAN CASE 001 result_fee:" + result_fee.Data + ", amount:" + amount + ", total:" + dbltotalAmount);
                                    }
                                    else
                                    {
                                        objResult.fee = 0.00;
                                        objResult.amount = Convert.ToDouble(amount);
                                        objResult.total_amount = Convert.ToDouble(amount);
                                        ContentTools.ErrorControllerLog(page + " LOAN CASE OTHER result_fee:" + objResult.fee + ", amount:" + objResult.amount + ", total:" + objResult.total_amount);
                                    }

                                    sb.Append("{");
                                    sb.Append("\"token\":");
                                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                                    sb.Append(",\"premiumfee\":");
                                    sb.Append(JsonConvert.SerializeObject(objResult));
                                    sb.Append("}");
                                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            var errorlog = ContentTools.ErrorControllerLog("ERROR:" + request_data.chdrnum + "|EWI CAN'T REQUEST");
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + "EWI CAN'T REQUEST");
                        }
                    }
                    else
                    {
                        WebReponseAccesskey IsDataAcessKey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", "PAYMENT").Select(x => new WebReponseAccesskey
                        {
                            id = x.REQUEST_ID,
                            access_key = x.REQUEST_ACCESS_KEY,
                            profile_id = x.REQUEST_PROFILE_ID,
                            app_nme = x.REQUEST_APP_NME,
                            app_type = x.REQUEST_APP_TYPE,
                            service_nme = x.REQUEST_SERVICE_NME
                        }).FirstOrDefault();

                        if (IsDataAcessKey == null)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }

                        #region PAYMENT_PERIOD
                        WebResponsePaymentPeriodDetail isdata = tmlth.SP_TP_GET_LIST_PAYMENT_PERIOD_DETAIL(request_data.chdrnum).Select(x => new WebResponsePaymentPeriodDetail
                        {
                            insured = x.INSURED,
                            name_insured = x.NAME_INSURE,
                            payment_date = x.PAYMENT_DATE,
                            payment_date_thai = x.PAYMENT_DATE_THAI,
                            payment_period = x.PAYMENT_PERIOD,
                            period_year = x.PRERIOD_YEAR,
                            period_time = x.PRERIOD_TIME,
                            payment_premium = x.PAYMENT_PREMIUM,
                            addr1 = x.ADDR1,
                            addr2 = x.ADDR2,
                            addr3 = x.ADDR3,
                            addr4 = x.ADDR4,
                            addr5 = x.ADDR5,
                            stat = x.STAT,
                            msg = x.MSG,
                            today = x.TODAY,
                            com_code = x.COM_CODE,
                            ref1 = x.REF1,
                            ref2 = x.REF2,
                            ref3 = x.REF3,
                            ref4 = x.REF4,
                            ref5 = x.REF5,
                            zipcode = x.Zipcode,
                            ref_send = x.REF_SEND,
                            currency_code = x.CURREN_CODE,
                            billing_code = x.BILLING_CODE,
                            product_code = x.PRODUCT_CODE,
                            agen_code = x.AGEN_CODE,
                            agen_name = x.AGEN_NAME,
                            insured_phone = x.INSURED_PHONE,
                            sum_insured = x.SUM_INSURED,
                            coverage_plan = x.COVERAGE_PLAN,
                            user_id = x.USER_ID,
                            user_name = x.USER_NAME,
                            payment_option = x.PAYMENT_OPTION,
                            payment_period_dtm = x.PAYMENT_PERIOD_DTM,
                            source_business = x.SOURCE_BUSINESS,
                            payment_amount = x.PAYMENT_PREMIUM
                        }).FirstOrDefault();

                        if (request_data.PaymentType == "001")
                        {
                            var db_payment_amount = isdata.payment_amount;
                            var client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                            string access_key = IsDataAcessKey.access_key,
                                profile_id = IsDataAcessKey.profile_id,
                                payment_type = request_data.PaymentType,
                                bank_code = request_data.BankCode,
                                billing_code = isdata.billing_code,
                                product_code = isdata.product_code,
                                currency = isdata.currency_code,
                                source_business = isdata.source_business;

                            double amount = Convert.ToDouble(db_payment_amount);

                            var result_fee = client.GetCalculatePremiumFee(access_key, profile_id, payment_type, bank_code, billing_code, product_code, amount, currency, source_business);
                            double dblfee = result_fee.Data,
                                   dblamount = Convert.ToDouble(db_payment_amount),
                                   dbltotalAmount = dblamount + dblfee;

                            objResult.fee = result_fee.Data;
                            objResult.amount = Convert.ToDouble(db_payment_amount);
                            objResult.total_amount = dbltotalAmount;
                        }
                        else
                        {
                            objResult.fee = 0.00;
                            objResult.amount = Convert.ToDouble(isdata.payment_amount);
                            objResult.total_amount = Convert.ToDouble(isdata.payment_amount);
                        }
                        #endregion

                        sb.Append("{");
                        sb.Append("\"token\":");
                        sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                        sb.Append(",\"premiumfee\":");
                        sb.Append(JsonConvert.SerializeObject(objResult));
                        sb.Append("}");
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    }
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
    }
}
