﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.Payment
{
    public class WebRequestGetPaymentResultController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
        }
      
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            var page = "WebRequestGetPaymentResult";
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestGetPaymentResult request_data = JsonConvert.DeserializeObject<WebRequestGetPaymentResult>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Payment",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum + "," +
                        "CHDRNUM_INSTMENT" + "=" + request_data.chdrnum_instment
                        ),
                        request_data.device);

                    string chdrinstment = request_data.chdrnum_instment;
                    DateTime now = DateTime.Now;
                    CultureInfo TH_CULTURE = new CultureInfo("th-TH");
                    if (request_data.isloan == 1)
                    {
                        chdrinstment = "4511" + request_data.chdrnum + "|" + now.ToString("ddMMyyyy", TH_CULTURE);
                    }
                    
                    List<WebResponsePaymentDTLByChdrnumInstment> getpayment_dtl = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(chdrinstment).Select(x => new WebResponsePaymentDTLByChdrnumInstment
                    {
                        paymentid = x.PAYMENT_ID,
                        chdrnum_instment = x.CHDRNUM_INSTMENT,
                        chdrnum = x.CHDRNUM,
                        userid = x.USER_ID,
                        accesskey = x.ACCESS_KEY,
                        profileid = x.PROFILE_ID,
                        signed_field_names = x.SIGNED_FIELD_NAMES,
                        signed_datetime = x.SIGNED_DATE_TIME,
                        paymenttype = x.PAYMENT_TYPE,
                        bankcode = x.BANK_CODE,
                        ref1 = x.REF1,
                        ref2 = x.REF2,
                        ref3 = x.REF3,
                        ref4 = x.REF4,
                        ref5 = x.REF5,
                        ref_send = x.REF_SEND,
                        amount = x.AMOUNT,
                        currency = x.CURRENCY,
                        language = x.LANGUAGE,
                        bill_to_forename = x.BILL_TO_FORENAME,
                        bill_to_surname = x.BILL_TO_SURNAME,
                        bill_to_address_line1 = x.BILL_TO_ADDRESS_LINE1,
                        bill_to_address_line2 = x.BILL_TO_ADDRESS_LINE2,
                        bill_to_address_line3 = x.BILL_TO_ADDRESS_LINE3,
                        bill_to_address_line4 = x.BILL_TO_ADDRESS_LINE4,
                        bill_to_address_city = x.BILL_TO_ADDRESS_CITY,
                        bill_to_address_country = x.BILL_TO_ADDRESS_COUNTRY,
                        bill_to_address_postal_code = x.BILL_TO_ADDRESS_POSTAL_CODE,
                        bill_to_address_email = x.BILL_TO_EMAIL,
                        bill_to_address_phone = x.BILL_TO_PHONE,
                        bill_to_address_mobile = x.BILL_TO_MOBILE,
                        remark = x.REMARK,
                        signature =x.SIGNATURE,
                        resp_code = x.RESP_CODE,
                        resp_mess = x.RESP_MESS,
                        payment_status = x.PAYMENT_STATUS,
                        payment_detail = x.PAYMENT_DETAIL,
                        payment_code = x.PAYMENT_CODE,
                        post_url = x.POST_URL,
                        post_date = x.POST_DATE,
                        post_back_by = x.POST_BACK_BY,
                        post_back_date = x.POST_BACK_DATE,
                        transaction_id = x.TRANSACTION_ID,
                        idv_family_id = x.IDV_FAMILY_ID,
                        idv_paytype_detail = x.IDV_PAYTYPE_DETAIL,
                    }).ToList<WebResponsePaymentDTLByChdrnumInstment>();

                    if (getpayment_dtl.Any())
                    {
                        var payment_dtl = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(chdrinstment).FirstOrDefault();

                        WebResponseBank bankdetail = tmlth.SP_TP_GET_BANK_DETAIL(payment_dtl.BANK_CODE).Select(x => new WebResponseBank
                        {
                            id = x.BNK_ID,
                            code = x.BNK_CODE,
                            name_th = x.BNK_NAME_TH,
                            name_eng = x.BNK_NAME_ENG,
                            name_jp = x.BNK_NAME_JP,
                            swift_code = x.BNK_SWIFT_CODE,
                            path = x.BNK_SWIFT_CODE,
                            direct_credit = x.BNK_DIRECT_CREDIT,
                            stat_direct_credit = x.BNK_STAT_DIRECT_CREDIT,
                            direct_debit = x.BNK_DIRECT_DEBIT,
                            stat_direct_dedit = x.BNK_STAT_DIRECT_DEBIT,
                            order = x.BNK_ORDER,
                        }).FirstOrDefault();

                        if (payment_dtl != null)
                        {
                            //Enable TLS
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                            // Ignore Cert
                            ServicePointManager.ServerCertificateValidationCallback = ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors) 
                        {
                            return true; // **** Always accept
                        };

                            // Get AccessKey
                            WebReponseAccesskey lstDataAccessKey = tmlth.SP_TP_GET_ACCESS_KEY("TOUCHPOINT", "MOBILE", request_data.isloan == 1 ? "PAYMENT_LOAN" : "PAYMENT").Select(x => new WebReponseAccesskey
                            {
                                id = x.REQUEST_ID,
                                access_key = x.REQUEST_ACCESS_KEY,
                                profile_id = x.REQUEST_PROFILE_ID,
                                app_nme = x.REQUEST_APP_NME,
                                app_type = x.REQUEST_APP_TYPE,
                                service_nme = x.REQUEST_SERVICE_NME
                            }).FirstOrDefault();

                            if (lstDataAccessKey == null)
                            {
                                var errorlog = ContentTools.ErrorControllerLog(page + "||ACCESS_KEY_NOTFOUND:" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                    ResultData.CODE_ERROR_DATA_NOT_FOUND);
                            }

                            var client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                            TmlthOnlinePayment.ResultInfoOfPaymentSupResponseInfoDtoaKPUf2By check = client.CheckResponseDataWithTransactionID(payment_dtl.TRANSACTION_ID, false);
                            if (check != null)
                            {
                                payment_dtl.RESP_CODE = check.Data.RESP_CODE;
                                payment_dtl.RESP_MESS = check.Data.RESP_MESS;
                                payment_dtl.PAYMENT_STATUS = check.Data.PAYMENT_STATUS;
                                payment_dtl.PAYMENT_CODE = check.Data.PAYMENT_CODE;
                                payment_dtl.ACCESS_KEY = lstDataAccessKey.access_key;
                                payment_dtl.PROFILE_ID = lstDataAccessKey.profile_id;


                                var updatepost = tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(
                                    check.Data.RESP_CODE,
                                    check.Data.RESP_MESS,
                                    check.Data.PAYMENT_STATUS,
                                    "",
                                    check.Data.PAYMENT_CODE,
                                    check.Data.TRANSACTION_ID,
                                    lstDataAccessKey.access_key,
                                    lstDataAccessKey.profile_id);

                                WebResponsePaymentDTLByChdrnumInstment paymentperiodAgain = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT(request_data.chdrnum_instment).Select(x => new WebResponsePaymentDTLByChdrnumInstment
                                {
                                    paymentid = x.PAYMENT_ID,
                                    chdrnum_instment = x.CHDRNUM_INSTMENT,
                                    chdrnum = x.CHDRNUM,
                                    userid = x.USER_ID,
                                    accesskey = x.ACCESS_KEY,
                                    profileid = x.PROFILE_ID,
                                    signed_field_names = x.SIGNED_FIELD_NAMES,
                                    signed_datetime = x.SIGNED_DATE_TIME,
                                    paymenttype = x.PAYMENT_TYPE,
                                    bankcode = x.BANK_CODE,
                                    ref1 = x.REF1,
                                    ref2 = x.REF2,
                                    ref3 = x.REF3,
                                    ref4 = x.REF4,
                                    ref5 = x.REF5,
                                    ref_send = x.REF_SEND,
                                    amount = x.AMOUNT,
                                    currency = x.CURRENCY,
                                    language = x.LANGUAGE,
                                    bill_to_forename = x.BILL_TO_FORENAME,
                                    bill_to_surname = x.BILL_TO_SURNAME,
                                    bill_to_address_line1 = x.BILL_TO_ADDRESS_LINE1,
                                    bill_to_address_line2 = x.BILL_TO_ADDRESS_LINE2,
                                    bill_to_address_line3 = x.BILL_TO_ADDRESS_LINE3,
                                    bill_to_address_line4 = x.BILL_TO_ADDRESS_LINE4,
                                    bill_to_address_city = x.BILL_TO_ADDRESS_CITY,
                                    bill_to_address_country = x.BILL_TO_ADDRESS_COUNTRY,
                                    bill_to_address_postal_code = x.BILL_TO_ADDRESS_POSTAL_CODE,
                                    bill_to_address_email = x.BILL_TO_EMAIL,
                                    bill_to_address_phone = x.BILL_TO_PHONE,
                                    bill_to_address_mobile = x.BILL_TO_MOBILE,
                                    remark = x.REMARK,
                                    signature = x.SIGNATURE,
                                    resp_code = x.RESP_CODE,
                                    resp_mess = x.RESP_MESS,
                                    payment_status = x.PAYMENT_STATUS,
                                    payment_detail = x.PAYMENT_DETAIL,
                                    payment_code = x.PAYMENT_CODE,
                                    post_url = x.POST_URL,
                                    post_date = x.POST_DATE,
                                    post_back_by = x.POST_BACK_BY,
                                    post_back_date = x.POST_BACK_DATE,
                                    transaction_id = x.TRANSACTION_ID,
                                    idv_family_id = x.IDV_FAMILY_ID,
                                    idv_paytype_detail = x.IDV_PAYTYPE_DETAIL,
                                }).FirstOrDefault();
                                paymentperiodAgain.bankName = bankdetail.name_th;

                                if (request_data.isloan == 1)
                                {
                                    if (paymentperiodAgain.payment_status == true)
                                    {
                                        string loanString = (request_data.loantype == DataFactory.LOAN_TYPE_AUTO ? "อัตโนมัติ" : request_data.loantype == DataFactory.LOAN_TYPE_PAYMENT ? "กรมธรรม์" : "");
                                        string citemtype = "ชำระเงินกู้" + loanString;
                                        //paymentperiodAgain.payment_status = "Y";
                                        paymentperiodAgain.payment_status_short_msg = "ชำระเงินกู้" + loanString + "เรียบร้อย";
                                        paymentperiodAgain.payment_status_msg = "บริษัท ขอขอบคุณสำหรับการชำระเงินกู้" + loanString + "จำนวน " + (paymentperiodAgain.amount == null ? "-" : paymentperiodAgain.amount.Value.ToString("N2")) + " บาท และจะดำเนินการจัดส่งใบเสร็จรับเงินกู้" + loanString + "ให้ท่านต่อไป";
                                        paymentperiodAgain.itemtype = citemtype;

                                    }
                                    else
                                    {
                                        paymentperiodAgain.payment_status_short_msg = "ชำระเงินกู้ไม่สำเร็จ";
                                        Check d = new Check();
                                        var checkConnectionFail = d.CheckCode777(check.Data.RESP_CODE);
                                        if (checkConnectionFail)
                                        {
                                            paymentperiodAgain.payment_status_msg = check.Data.RESP_MESS;
                                        }
                                        else
                                        {
                                            paymentperiodAgain.payment_status_msg = "ขออภัยท่านทำรายการไม่สำเร็จ กรุณาตรวจสอบรายการกับธนาคารเจ้าของบัญชีของท่าน";
                                        }
                                        var errorlog = ContentTools.ErrorControllerLog(page + "|| PAYMENTFAIL:" + paymentperiodAgain.payment_status_short_msg + "||" + paymentperiodAgain.payment_status_msg);
                                    }
                                }
                                else
                                {
                                    if (paymentperiodAgain.payment_status == true)
                                    {
                                        string citemtype = "ชำระเบี้ยประกันภัย";
                                        //paymentperiodAgain.payment_status = "Y";
                                        paymentperiodAgain.payment_status_short_msg = "ชำระเบี้ยประกันภัยเรียบร้อย";
                                        paymentperiodAgain.payment_status_msg = "บริษัท ขอขอบคุณสำหรับการชำระเบี้ยประกันภัยจำนวน " + (paymentperiodAgain.amount == null ? "-" : paymentperiodAgain.amount.Value.ToString("N2")) + " บาท และจะดำเนินการจัดส่งใบเสร็จรับเงินเบี้ยประกันภัยให้ท่านต่อไป";
                                        paymentperiodAgain.itemtype = citemtype;

                                        //var lstBankDetail = tmlth.SP_TP_GET_BANK_DETAIL(paymentperiodAgain.bankcode);
                                        //paymentperiodAgain.;
                                    }
                                    else
                                    {
                                        paymentperiodAgain.payment_status_short_msg = "ชำระเบี้ยประกันภัยไม่สำเร็จ";
                                        Check d = new Check();
                                        var checkConnectionFail = d.CheckCode777(check.Data.RESP_CODE);
                                        if (checkConnectionFail)
                                        {
                                            paymentperiodAgain.payment_status_msg = check.Data.RESP_MESS;
                                        }
                                        else
                                        {
                                            paymentperiodAgain.payment_status_msg = "ขออภัยท่านทำรายการไม่สำเร็จ กรุณาตรวจสอบรายการกับธนาคารเจ้าของบัญชีของท่าน";
                                        }
                                        var errorlog = ContentTools.ErrorControllerLog(page + "|| PAYMENTFAIL:" + paymentperiodAgain.payment_status_short_msg + "||" + paymentperiodAgain.payment_status_msg);
                                    }
                                }
                                sb.Append("{");
                                sb.Append("\"token\":");
                                sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                                sb.Append(",\"paymentperiod\":");
                                sb.Append(JsonConvert.SerializeObject(paymentperiodAgain));
                                sb.Append("}");
                            }
                        }

                    }
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT,
                    DataFactory.ENC_TOUCHPOINT_KEY));
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT,
                DataFactory.ENC_TOUCHPOINT_KEY));
            }
        }
        class Check
        {
            public bool CheckCode777(string val)
            {
                var blRes = new bool();
                if (val.Length > 0)
                {
                    blRes = val.Substring(val.Length - 3) == "777" ? true : false;
                }
                return blRes;
            }
        }
    }
}
