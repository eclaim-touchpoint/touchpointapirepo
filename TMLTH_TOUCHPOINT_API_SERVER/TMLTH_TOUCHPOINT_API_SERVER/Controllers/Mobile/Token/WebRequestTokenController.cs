﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.Token
{
    public class WebRequestTokenController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestToken";
            if (ModelState.IsValid)
            {
                //string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                //WebRequestToken request_data = JsonConvert.DeserializeObject<WebRequestToken>(decryptData);
                StringBuilder sb = new StringBuilder();
                //CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    //#region validateCookie
                    //if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    //{
                    //    #region validateToken

                    //    var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                    //    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                    //    #endregion
                    //}
                    //#endregion

                    //SubmitLog.ACTION_LOG(tmlth,
                    //    cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                    //    cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                    //    "-",
                    //    this.GetType().Name,
                    //    (
                    //    "CHDRNUM" + "=" + request_data.chdrnum
                    //    ),
                    //    request_data.device);



                    var client = new HttpClient();
                    string json = JsonConvert.SerializeObject(new
                    {
                        PROFILE_UID = "E1A9B752-D366-421D-A605-CA61A2FAA2D8"
                    });
                    var result = new StringContent(json,Encoding.UTF8, "application/json");

                    var response = client.PostAsJsonAsync("http://staging.tokiomarinelife.co.th/ndidcallback_UAT/api/v1/Token/", result).Result;
                    string test = response.Content.ReadAsStringAsync().Result;
                    if (response.IsSuccessStatusCode)
                    {
                       
                        if (response.Content.ReadAsStringAsync().Result.StartsWith("SUCCESS"))
                        {
                            return DataGenerator.generateTextResult(sb.ToString());
                        }
                    }


                    return DataGenerator.generateTextResult(sb.ToString());
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||"  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }

    }
}
