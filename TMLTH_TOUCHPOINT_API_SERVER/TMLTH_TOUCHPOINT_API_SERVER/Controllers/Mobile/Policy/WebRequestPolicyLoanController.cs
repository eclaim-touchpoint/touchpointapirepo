﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Controllers.Payment;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Policy
{
    public class WebRequestPolicyLoanController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                //var decryptData = data;
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestPolicyInvestment request_data = JsonConvert.DeserializeObject<WebRequestPolicyInvestment>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                       
                         var errorlog = ContentTools.ErrorControllerLog("WebRequestPolicyInvestment" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Policy",
                        this.GetType().Name,
                        (
                            "CHDRNUM" + "=" + request_data.chdrnum
                        ),
                        request_data.device);

                    List<WebResponseInvestmentLoan> policyLoan = tmlth.SP_TP_GET_POLICY_LOAN(request_data.chdrnum).Select(x => new WebResponseInvestmentLoan
                    {
                        policy_no = x.POLICY_NO,
                        loan_type = x.LOAN_TYPE,
                        loan_start_date = x.LOAN_START_DATE,
                        loan_original_amount = x.LOAN_ORIGINAL_AMOUNT,
                        loan_curr_amount = x.LOAN_CURR_AMOUNT,
                        interest = x.INTEREST,
                        total = x.TOTAL
                    }).ToList<WebResponseInvestmentLoan>();

                    policyLoan = policyLoan.Where(x => x.policy_no == request_data.chdrnum).ToList<WebResponseInvestmentLoan>();

                    WebResponsePolicyLoanSummary policyLoanSummary = tmlth.SP_TP_GET_POLICY_LOAN_SUMMARY(request_data.chdrnum).Select(x => new WebResponsePolicyLoanSummary
                    {
                       policy_no = x.POLICY_NO,
                       total_apl = x.TOTAL_APL,
                       total_rpl = x.TOTAL_RPL,
                       total  = x.TOTAL,
                       billing_code = x.BILLING_CODE,
                       product_code = x.PRODUCT_CODE,
                       source_business = x.SOURCE_BUSINESS
                    }).FirstOrDefault();

                    WebResponsePolicyLoanPayFlag policyLoanPayFlag = tmlth.SP_TP_GET_POLICY_LOAN_PAYFLAG(request_data.chdrnum).Select(x => new WebResponsePolicyLoanPayFlag
                    {
                       chdrnum = x.CHDRNUM,
                       capitalize_date = x.CAPITALIZE_DATE,
                       loan_payment_flag = x.LOAN_PAYMENT_FLAG,
                       occ_date = x.OCCDATE,
                       pt_date = x.PTDATE,
                       reason = x.REASON,
                       payment_status = x.PAYMENT_STATUS
                    }).FirstOrDefault();

                    CultureInfo TH_CULTURE = new CultureInfo("th-TH");
                    var now = DateTime.Now;

                    //WebResponsePaymentDTLByChdrnumInstment paymentDtl = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT("4511" + request_data.chdrnum + "|" + now.ToString("ddMMyyyy", TH_CULTURE)).Select(x => new WebResponsePaymentDTLByChdrnumInstment
                    //{
                    //    paymentid = x.PAYMENT_ID,
                    //    chdrnum_instment = x.CHDRNUM_INSTMENT,
                    //    chdrnum = x.CHDRNUM,
                    //    userid = x.USER_ID,
                    //    accesskey = x.ACCESS_KEY,
                    //    profileid = x.PROFILE_ID,
                    //    signed_field_names = x.SIGNED_FIELD_NAMES,
                    //    signed_datetime = x.SIGNED_DATE_TIME,
                    //    paymenttype = x.PAYMENT_TYPE,
                    //    bankcode = x.BANK_CODE,
                    //    ref1 = x.REF1,
                    //    ref2 = x.REF2,
                    //    ref3 = x.REF3,
                    //    ref4 = x.REF4,
                    //    ref5 = x.REF5,
                    //    ref_send = x.REF_SEND,
                    //    amount = x.AMOUNT,
                    //    currency = x.CURRENCY,
                    //    language = x.LANGUAGE,
                    //    bill_to_forename = x.BILL_TO_FORENAME,
                    //    bill_to_surname = x.BILL_TO_SURNAME,
                    //    bill_to_address_line1 = x.BILL_TO_ADDRESS_LINE1,
                    //    bill_to_address_line2 = x.BILL_TO_ADDRESS_LINE2,
                    //    bill_to_address_line3 = x.BILL_TO_ADDRESS_LINE3,
                    //    bill_to_address_line4 = x.BILL_TO_ADDRESS_LINE4,
                    //    bill_to_address_city = x.BILL_TO_ADDRESS_CITY,
                    //    bill_to_address_country = x.BILL_TO_ADDRESS_COUNTRY,
                    //    bill_to_address_postal_code = x.BILL_TO_ADDRESS_POSTAL_CODE,
                    //    bill_to_address_email = x.BILL_TO_EMAIL,
                    //    bill_to_address_phone = x.BILL_TO_PHONE,
                    //    bill_to_address_mobile = x.BILL_TO_MOBILE,
                    //    remark = x.REMARK,
                    //    signature = x.SIGNATURE,
                    //    resp_code = x.RESP_CODE,
                    //    resp_mess = x.RESP_MESS,
                    //    payment_status = x.PAYMENT_STATUS,
                    //    payment_detail = x.PAYMENT_DETAIL,
                    //    payment_code = x.PAYMENT_CODE,
                    //    post_url = x.POST_URL,
                    //    post_date = x.POST_DATE,
                    //    post_back_by = x.POST_BACK_BY,
                    //    post_back_date = x.POST_BACK_DATE,
                    //    transaction_id = x.TRANSACTION_ID,
                    //    idv_family_id = x.IDV_FAMILY_ID,
                    //    idv_paytype_detail = x.IDV_PAYTYPE_DETAIL
                    //}).FirstOrDefault<WebResponsePaymentDTLByChdrnumInstment>();

                    //List<objErr777> lstError777 = new List<objErr777>();

                    //if (paymentDtl != null)
                    //{
                    //    if (paymentDtl.payment_status != true && !string.IsNullOrWhiteSpace(paymentDtl.transaction_id))
                    //    {

                    //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                    //        System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate (object sender2, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                    //        System.Security.Cryptography.X509Certificates.X509Chain chain,
                    //        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    //        {
                    //            return true; // **** Always accept
                    //        };


                    //        var client = new TmlthOnlinePayment.OnlinePaymentSupportServicesClient();
                    //        TmlthOnlinePayment.ResultInfoOfPaymentSupResponseInfoDtoaKPUf2By check = client.CheckResponseDataWithTransactionID(paymentDtl.transaction_id, false);

                    //        //UPDATE CHECK DATA
                    //        tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(
                    //            check.Data.RESP_CODE,
                    //            check.Data.RESP_MESS,
                    //            check.Data.PAYMENT_STATUS,
                    //            "",
                    //            check.Data.PAYMENT_CODE,
                    //            check.Data.TRANSACTION_ID,
                    //            "c768a396-dd8f-4dc0-9fa8-6da1dc787aa7",
                    //            "TML00004"
                    //        );
                            
                    //        //if(stat==1) คุณไม่สามารถชำระเงินได้เนื่องจากจ่ายเงินไปแล้ว
                    //        //if stat==0 && resp_code != 777
                    //        if (check.Data != null)
                    //        {
                    //            Check d = new Check();
                    //            var checkConnectionFail = d.CheckCode777(check.Data.RESP_CODE);
                    //            if (checkConnectionFail)
                    //            {
                    //                objErr777 objError777 = new objErr777();
                    //                objError777.chdrnum = paymentDtl.chdrnum;
                    //                objError777.resp_code = check.Data.RESP_CODE;
                    //                lstError777.Add(objError777);
                    //            }

                    //            tmlth.SP_TP_UPDATE_POST_PAYMENT_DTL(
                    //               check.Data.RESP_CODE,
                    //               check.Data.RESP_MESS,
                    //               check.Data.PAYMENT_STATUS,
                    //               "",
                    //               check.Data.PAYMENT_CODE,
                    //               check.Data.TRANSACTION_ID,
                    //               "c768a396-dd8f-4dc0-9fa8-6da1dc787aa7",
                    //               "TML00004"
                    //           );
                    //        }

                    //        WebResponseLoanPaymentDTLByChdrnumInstment resultpaymentDtl = tmlth.SP_TP_GET_PAYMENT_DTL_BY_CHDRNUM_INSTMENT("4511" + request_data.chdrnum + "|" + now.ToString("ddMMyyyy", TH_CULTURE)).Select(x => new WebResponseLoanPaymentDTLByChdrnumInstment
                    //        {
                    //            chdrnum_instment = x.CHDRNUM_INSTMENT,
                    //            chdrnum = x.CHDRNUM,
                    //            userid = x.USER_ID,
                    //            paymenttype = x.PAYMENT_TYPE,
                    //            bankcode = x.BANK_CODE,
                    //            ref1 = x.REF1,
                    //            ref2 = x.REF2,
                    //            remark = x.REMARK,
                    //            resp_code = x.RESP_CODE,
                    //            resp_mess = x.RESP_MESS,
                    //            payment_status = x.PAYMENT_STATUS,
                    //            payment_detail = x.PAYMENT_DETAIL,
                    //            payment_code = x.PAYMENT_CODE,
                    //            idv_family_id = x.IDV_FAMILY_ID,
                    //            idv_paytype_detail = x.IDV_PAYTYPE_DETAIL
                    //        }).FirstOrDefault<WebResponseLoanPaymentDTLByChdrnumInstment>();

                    //        if (resultpaymentDtl.payment_status == false && !resultpaymentDtl.resp_code.EndsWith("777"))
                    //        {
                    //            //true ชำระได้
                    //        }
                    //        else if (resultpaymentDtl.payment_status == true)
                    //        {
                    //            paymentDtl.remark = "มีการชำระสำเร็จแล้ว ภายในวันนั้น ไม่อนุญาติให้ชำระ มากกว่า 1 รายการภายในวันเดียวกัน";
                    //        } else if (resultpaymentDtl.payment_status == false && resultpaymentDtl.resp_code.EndsWith("777"))
                    //        {
                    //            paymentDtl.remark = "**หมายเหตุ N/A รอการตรวจสอบจากธนาคาร (กรุณาทำรายการใหม่อีกครั้ง)";
                    //        }
                    //    }
                        
                    //}
                  
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"investmentOverviews\":");
                    sb.Append(JsonConvert.SerializeObject(policyLoan));
                    sb.Append(",\"loanSummary\":");
                    sb.Append(JsonConvert.SerializeObject(policyLoanSummary));
                    sb.Append(",\"loanPayflag\":");
                    sb.Append(JsonConvert.SerializeObject(policyLoanPayFlag));
                    //sb.Append(",\"payment_dtl\":");
                    //sb.Append(JsonConvert.SerializeObject(paymentDtl));
                    sb.Append("}");

                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog("WebRequestPolicyInvestment" + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog("WebRequestPolicyInvestment" + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
    }

    class Check
    {
        public bool CheckCode777(string val)
        {
            var blRes = new bool();
            if (val.Length > 0)
            {
                blRes = val.Substring(val.Length - 3) == "777" ? true : false;
            }
            return blRes;
        }
    }
}
