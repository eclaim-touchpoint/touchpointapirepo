﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.Policy
{
    public class WebSubmitPolicyController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestPolicy request_data = JsonConvert.DeserializeObject<WebRequestPolicy>(decryptData);
                StringBuilder sb = new StringBuilder();
                try
                {
                    CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_SESSION_TIMEOUT + ResultData.CODE_ERROR_TOUCHPOINT);
                        }
                        else if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR + ResultData.CODE_ERROR_TOUCHPOINT);
                        }
                        else
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_SEEM_TO_HACK);
                        }
                        #endregion
                    }
                    #endregion

                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
