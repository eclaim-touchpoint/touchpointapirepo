﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Investment
{
    public class WebRequestPolicyInvestmentTRANController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                //var decryptData = data;
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestInvestmentTran request_data = JsonConvert.DeserializeObject<WebRequestInvestmentTran>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                       
                            var errorlog = ContentTools.ErrorControllerLog("WebRequestInvestmentTRAN" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                             return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Policy",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum + "," +
                        "DATEFROM" + "=" + request_data.datefrom + "," +
                        "DATETO" + "=" + request_data.dateto + "," +
                        "COVERAGE" + "=" + request_data.coverage 
                        ),
                        request_data.device);

                    //TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    //if (tokendata.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    List<WebResposeInvestmentTran> investmenttran = tmlth.SP_TP_GET_INVESTMENT_TRAN(request_data.chdrnum,request_data.datefrom,request_data.dateto,request_data.coverage).Select(x => new WebResposeInvestmentTran
                    {
                        fundcode = x.FUND_CODE,
                        batchnumber = x.BATCH_NUMBER,
                        companycode = x.COMPANY_CODE,
                        policynumber = x.POLICY_NUMBER,
                        batchdate = x.BATCH_DATE,
                        coveragenumber = x.COVERAGE_NUMBER,
                        fundprice = x.FUND_PRICE,
                        effectdate = x.EFFECT_DATE,
                        unit = x.UNIT,
                        price = x.PRICE,
                        pricedate = x.PRICE_DATE,
                        assetnameeng = x.ASSET_NAME_ENG,
                        assetnameengabv = x.ASSET_NAME_ENG_ABV,
                        assetnameth = x.ASSET_NAME_TH,
                        fundnameth = x.FUND_NAME_TH,
                        transecdetail= x.TRANSEC_DETAIL,
                        transecnumber = x.TRANSEC_NUMBER,
                        unitbalance = x.UNIT_BALANCE,
                        navperunit = x.NAV_PER_UNIT,
                        coveragecode = x.COVERAGE_CODE,
                        productcode = x.PRODUCT_CODE
                    }).ToList<WebResposeInvestmentTran>();

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"investmenttran\":");
                    sb.Append(JsonConvert.SerializeObject(investmenttran.Where(x => x.fundcode == request_data.fund_code)));
                    sb.Append("}");
                    //return DataGenerator.generateTextResult(sb.ToString());
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog("WebRequestInvestmentTRAN" + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog("WebRequestInvestmentTRAN" + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
    }
}
