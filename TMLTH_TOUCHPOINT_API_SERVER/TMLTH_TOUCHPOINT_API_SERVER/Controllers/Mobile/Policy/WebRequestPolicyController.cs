﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Policy
{
    public class WebRequestPolicyController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPolicy";
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY); 
                WebRequestPolicy request_data = JsonConvert.DeserializeObject<WebRequestPolicy>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                       
                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Policy",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum
                        ),
                        request_data.device);


                    #region start query(fetch) data from db
                    //*********DATA TEST*************

                    //var Forgotpass = tmlth.SP_TP_GET_USER_FORGOT_PASSWORD(request_data.idcard,request_data.email,request_data.mobileno);
                    WebResponsePolicy policy = tmlth.SP_TP_GET_POLICY(request_data.chdrnum).Select(x => new WebResponsePolicy
                    {
                        policyno = x.POLICY_NO,
                        crtable = x.CRTABLE,
                        billfreq = x.BILL_FREQ,
                        occdate = x.OCC_DATE,
                        riskcessdate = x.RISK_CESS_DATE,
                        ptdate = x.PT_DATE,
                        basicinstprem = x.BASIC_INST_PREM,
                        covinstprem = x.COV_INST_PREM,
                        agentname = x.AGENT_NAME,
                        agenttel = x.AGENT_TEL,
                        polyear = x.POL_YEAR,
                        polprd = x.POL_PRD,
                        riskassessment = x.RISK_ASSESSMENT,
                        riskassessmentdateRaw = x.RISK_ASSESSMENT_DATE,
                        resultloan01 = x.RESULT_LOAN_01,
                        resultloan02 = x.RESULT_LOAN_02,
                        resultloan03 = x.RESULT_LOAN_03,
                        resultloan04 = x.RESULT_LOAN_04,
                        productname = x.PRODUCT_NAME_TH,
                        graphimage =  x.GRAPH_IMAGE,
                        isunitlink = x.IS_UNIT_LINK == "TRUE" ? 1 : 0
                    }).FirstOrDefault();

                    if (policy == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                 
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"policy\":");
                    sb.Append(JsonConvert.SerializeObject(policy));
                   
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
