﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Policy
{
    public class WebRequestPolicyListController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPolicyUser";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestPolicyUser request_data = JsonConvert.DeserializeObject<WebRequestPolicyUser>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                       
                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Policy",
                        this.GetType().Name,
                        (
                        "APPID" + "=" + request_data.appid
                        ),
                        request_data.device);

                    #region start query(fetch) data from db
                    //*********DATA TEST*************
                    string clntnum = cookieData.ID.ToString();
                    string trimClntnum = cookieData.ID.ToString().Trim();
                    string userid = cookieData.USERNAME;
                    DateTime month = DateTime.Now.AddMonths(-3);
                    var notification_data = (from _noti in tmlth.TP_SYSTEM_MESSAGE
                                        where _noti.REF_CLNTNUM.Equals(trimClntnum)
                                        && _noti.CLIENT_READ_FIRST_DATE == null
                                        && _noti.ACTIVE == 1
                                        && _noti.IS_FINISH == 1
                                        && _noti.CREATE_DATE >= month
                                        select _noti);
                    int notification = notification_data.Count();
                    var SP_TP_GET_LIST_POLICY = tmlth.SP_TP_GET_LIST_POLICY(clntnum, userid).AsQueryable();

                    string[] policylist = (from a in SP_TP_GET_LIST_POLICY
                                           select a.CHDRNUM).ToArray();
                                     

                    var check_has_fund = tmlth.SP_TP_GET_INVESTMENT_OVERVIEW(clntnum).ToList();
                    int has_investmentoverview_fund = 0;
                    if (check_has_fund.Any()) {
                        has_investmentoverview_fund = 1;
                    }
                  
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"policylist\":");
                    sb.Append(JsonConvert.SerializeObject(policylist));
                    sb.Append(",\"has_investmentoverview_fund\":");
                    sb.Append(JsonConvert.SerializeObject(has_investmentoverview_fund));
                    sb.Append(",\"notification\":");
                    sb.Append(JsonConvert.SerializeObject(notification));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
