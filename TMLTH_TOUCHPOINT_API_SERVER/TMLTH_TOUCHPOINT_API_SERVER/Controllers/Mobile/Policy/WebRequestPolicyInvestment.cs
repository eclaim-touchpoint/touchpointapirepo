﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Claims
{
    public class WebRequestPolicyInvestmentController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
        
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestInvestment request_data = JsonConvert.DeserializeObject<WebRequestInvestment>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        var errorlog = ContentTools.ErrorControllerLog("WebRequestInvestment" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Policy",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum
                        ),
                        request_data.device);

                    //TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    //if (tokendata.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    List<WebResponseInvestment> investment = tmlth.SP_TP_GET_INVESTMENT(request_data.chdrnum).Select(x => new WebResponseInvestment
                    {
                        coverage = x.COVERAGE,
                        fund_code = x.FUND_CODE,
                        effect_date = x.EFFECT_DATE,
                        unit = x.UNIT,
                        nowprice = x.NOWPRICE,
                        estimate_fund_value = x.ESTIMATE_FUND_VALUE,
                        fund_shot_name = x.FUND_SHOT_NAME,
                        fund_full_name = x.FUND_FULL_NAME,
                        product_shot_name = x.PRODUCT_SHOT_NAME,
                        product_long_name = x.PRODUCT_LONG_NAME,
                        product_code = x.PRODUCT_CODE,
                        fundhousemanagement = x.FundHouseManagement,
                        asset_shot_name = x.ASSET_SHOT_NAME,
                        asset_long_name = x.ASSET_LONG_NAME,
                        unit_alloc_perc_amt = x.UNIT_ALLOC_PERC_AMT,
                    }).ToList<WebResponseInvestment>();

                    WebResponseInvestmentSummary investment_summary = tmlth.SP_TP_GET_INVESTMENT_SUMMARY_BY_POLICY(request_data.chdrnum).Select(x => new WebResponseInvestmentSummary
                    {
                        policy_no = x.POLICY_NO,
                        fund = x.FUND
                    }).FirstOrDefault();

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"investment_RPP\":");
                    sb.Append(JsonConvert.SerializeObject(investment.Where(x => x.coverage == "01")));
                    sb.Append(",\"investment_RSP\":");
                    sb.Append(JsonConvert.SerializeObject(investment.Where(x => x.coverage == "02")));
                    sb.Append(",\"investment_TopUp\":");
                    sb.Append(JsonConvert.SerializeObject(investment.Where(x => x.coverage == "03")));
                    sb.Append(",\"investment_summary\":");
                    sb.Append(JsonConvert.SerializeObject(investment_summary));
                    sb.Append("}");
                   // return DataGenerator.generateTextResult(sb.ToString());
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog("WebRequestInvestment" + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog("WebRequestInvestment" + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
    }
}

