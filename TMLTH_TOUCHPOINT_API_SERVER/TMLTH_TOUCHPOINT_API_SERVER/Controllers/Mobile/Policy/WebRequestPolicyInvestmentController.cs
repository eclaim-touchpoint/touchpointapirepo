﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Policy
{
    public class WebRequestPolicyInvestmentController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPolicyInvestment";
            if (ModelState.IsValid)
            {
                //var decryptData = data;
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestPolicyInvestment request_data = JsonConvert.DeserializeObject<WebRequestPolicyInvestment>(decryptData);
                StringBuilder sb = new StringBuilder();

                try
                {
                    CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_SESSION_TIMEOUT + ResultData.CODE_ERROR_TOUCHPOINT);
                        }
                        else if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR + ResultData.CODE_ERROR_TOUCHPOINT);
                        }
                        else
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_SEEM_TO_HACK);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_SEEM_TO_HACK);
                        }
                        #endregion
                    }
                    #endregion

                    //WebResponsePolicyInvestment policy = tmlth.SP_TP_GET_POLICY(request_data.policy_no).Select(x => new WebResponsePolicyInvestment
                    //{
                    //    policyno = x.POLICY_NO,
                    //    crtable = x.CRTABLE,
                    //    billfreq = x.BILL_FREQ,
                    //    occdate = x.OCC_DATE,
                    //    riskcessdate = x.RISK_CESS_DATE,
                    //    ptdate = x.PT_DATE,
                    //    basicinstprem = x.BASIC_INST_PREM,
                    //    covinstprem = x.COV_INST_PREM,
                    //    agentname = x.AGENT_NAME,
                    //    agenttel = x.AGENT_TEL,
                    //    polyear = x.POL_YEAR,
                    //    polprd = x.POL_PRD,
                    //    riskassessment = x.RISK_ASSESSMENT,
                    //    riskassessmentdate = x.RISK_ASSESSMENT_DATE,
                    //    resultloan01 = x.RESULT_LOAN_01,
                    //    resultloan02 = x.RESULT_LOAN_02,
                    //    resultloan03 = x.RESULT_LOAN_03,
                    //    resultloan04 = x.RESULT_LOAN_04
                    //}).FirstOrDefault();
                    //if (policy == null)
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    //}
                    List<WebResponseInvestmentLoan> investmentOverviews = tmlth.SP_TP_GET_POLICY_LOAN(request_data.policy_no).Select(x => new WebResponseInvestmentLoan
                    {
                        policy_no = x.POLICY_NO,
                        loan_type = x.LOAN_TYPE,
                        loan_start_date = x.LOAN_START_DATE,
                        loan_original_amount = x.LOAN_ORIGINAL_AMOUNT,
                        loan_curr_amount = x.LOAN_CURR_AMOUNT,
                        interest = x.INTEREST,
                        total = x.TOTAL

                    }).ToList<WebResponseInvestmentLoan>();

                    //var investment_unit = investmentOverviews.OrderBy(x => x.loan_type).Select(x => x.loan_type).ToList();
                    //JArray refine_investmentOverviews = new JArray();
                    //var unit = "";
                    //foreach (var investment in investment_unit)
                    //{
                    //    if (unit != investment)
                    //    {
                    //        //string _investment = JsonConvert.SerializeObject(investmentOverviews.Where(x => x.unit_virtual_fund.Equals(investment)));
                    //        //refine_investmentOverviews.Add(JsonConvert.DeserializeObject(_investment));
                    //        refine_investmentOverviews.Add(JToken.FromObject(investmentOverviews.Where(x => x.loan_type.Equals(investment))));
                    //    }
                    //    unit = investment;
                    //}
                    //if (!investmentOverviews.Any())
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    //}

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    //sb.Append(",\"policy\":");
                    //sb.Append(JsonConvert.SerializeObject(policy));
                    sb.Append(",\"investmentOverviews\":");
                    sb.Append(JsonConvert.SerializeObject(investmentOverviews));
                    sb.Append("}");

                    //return DataGenerator.generateTextResult(sb.ToString());
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
    }
}
