﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Policy
{
    public class WebRequestPolicyDetailController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestPolicyDetail";

            if (ModelState.IsValid)
            {
               // var decryptData = data;
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestPolicyDetail request_data = JsonConvert.DeserializeObject<WebRequestPolicyDetail>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                       
                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Policy",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum
                        ),
                        request_data.device);

                    #region get policyDetail
                    List<WebResponsePolicyDetail> policydetail = tmlth.SP_TP_GET_POLICY_DETAIL(request_data.chdrnum).Select(x => new WebResponsePolicyDetail
                    {
                        policyno = x.POLICY_NO,
                        billfreq = x.BILL_FREQ,
                        occdate= x.OCC_DATE,
                        riskcessdate=x.RISK_CESS_DATE,
                        ptdate= x.PT_DATE,
                        basicinstprem= x.BASIC_INSTPREM,
                        crtable= x.CRTABLE,
                        longdesc= x.LONGDESC,
                        description= x.DESCRIPTION,
                        covsumins= x.COV_SUMINS,
                        covinstprem = x.COV_INSTPREM,
                        covstatcode= x.COV_STATCODE,
                        covstatcodedesc=  x.COV_STATCODE_DESC,
                        
                        life= x.LIFE,
                        coverage= x.COVERAGE,
                        rider= x.RIDER,
                        termpolicy= x.TERM_POLICY ,
                        paypolicy= x.PAY_POLICY 
                    }).ToList<WebResponsePolicyDetail>();

                    #endregion

                    #region get policyexclusion
                    List<WebResponsePolicyExclusion> policyexclusion = tmlth.SP_TP_GET_POLICY_EXCLUSION(request_data.chdrnum).Select(x => new WebResponsePolicyExclusion
                    {
                        chdrnum = x.chdrnum,
                        fupno = x.fupno,
                        fupcode = x.fupcode,
                        fupremk = x.fupremk,
                        fupredt = x.fupremdt,
                        exclusion = x.Exclusion
                      
                    }).ToList<WebResponsePolicyExclusion>();
                    #endregion

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"policydetail\":");
                    sb.Append(JsonConvert.SerializeObject(policydetail));
                    sb.Append(",\"policyexclusion\":");
                    sb.Append(JsonConvert.SerializeObject(policyexclusion));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
               
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
