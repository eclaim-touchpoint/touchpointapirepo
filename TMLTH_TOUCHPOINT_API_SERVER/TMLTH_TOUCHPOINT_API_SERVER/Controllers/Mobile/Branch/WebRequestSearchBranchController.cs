﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.branch
{
    public class WebRequestSeachBranchController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            var page = "WebRequestSeachBranch";
            if (ModelState.IsValid)
            {
                //string decryptData = data;
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestBranch request_data = JsonConvert.DeserializeObject<WebRequestBranch>(decryptData);
                StringBuilder sb = new StringBuilder();
                int skip = request_data.page * 10;
                try
                {
                    // This Feature not require Login
                    //CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    //if (cookieData.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_SEEM_TO_HACK, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    //TokenResult tokendata = GenerateToken.validateSessionToken(request_data.admin);
                    //if (tokendata.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    //*** set account detail
                    //int employee_id = cookieData.ID;
                    //int employee_role_id = cookieData.ROLE_ID;
                    SubmitLog.ACTION_LOG(tmlth,
                    null,
                    null,
                    "Branch",
                    this.GetType().Name,
                    (
                        "PAGE" + "=" + request_data.page + "," +
                        "PROVINCE" + "=" + request_data.province
                    ),
                    request_data.device);

                    List<WebReponseBranch> branchResult = tmlth.SP_TP_GET_BRANCH("TH").Select(x => new WebReponseBranch
                    {
                        code = x.code,
                        thdesc = x.descript,
                        addline1 = x.addline1 ?? "",
                        addline2 = x.addline2 ?? "",
                        addlist = x.adddist,
                        addprov = x.addprov,
                        addpost = x.addpost,
                        addtel = x.addtel,
                        addfax = x.addfax,
                        latitude = x.lat,
                        longitude = x.@long,
                        status = x.status,
                    }).ToList<WebReponseBranch>();

                    if (!string.IsNullOrWhiteSpace(request_data.province))
                    {
                        branchResult = branchResult.Where(y => y.addprov == request_data.province).ToList<WebReponseBranch>();
                    }

                    int totalResult = branchResult.Count();
                    // 20191113
                    // deprecate
                    // p'toon: request to show all branch 
                    // branchResult = branchResult.Skip(skip).Take(10).ToList<WebReponseBranch>();

                    sb.Append("{");
                    sb.Append("\"branchresult\":");
                    sb.Append(JsonConvert.SerializeObject(branchResult));
                    sb.Append(",\"total\":");
                    sb.Append(totalResult);
                    sb.Append("}");
                   // return DataGenerator.generateJsonResult(sb.ToString());
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);

                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
                
            //return DataGenerator.generateTextResult((sb.ToString()));
        }
    }
}
