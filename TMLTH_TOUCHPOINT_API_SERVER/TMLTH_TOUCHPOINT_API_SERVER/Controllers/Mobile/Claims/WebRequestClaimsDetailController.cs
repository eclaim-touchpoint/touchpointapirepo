﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Claims
{
    public class WebRequestClaimsDetailController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestClaimsDetail";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
              
                WebRequestClaimsDetail request_data = JsonConvert.DeserializeObject<WebRequestClaimsDetail>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        
                        var errorlog = ContentTools.ErrorControllerLog("WebRequestClaimsDetail" + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        #endregion
                    }
                    #endregion
                        SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Claims",
                        this.GetType().Name,
                        (
                        "CHDRNUM" + "=" + request_data.chdrnum + "," +
                        "INCIDENT_DT" + "=" + request_data.incident_dt + "," +
                        "RGPNUM" + "=" + request_data.rgpnum + "," +
                        "PAGESIZE" + "=" + request_data.pagesize

                        ),
                        request_data.device);

                    // datatest policyno = 12321441 incident_dt = 20171113 rgpnum = 1 pagesize = 1

                    List<WebResponseClaimsDetail> claimsdetail = tmlth.SP_TP_GET_CLAIM_DETAIL(request_data.chdrnum,request_data.incident_dt,request_data.rgpnum,request_data.pagesize).Select(x => new WebResponseClaimsDetail
                    {
                       rowcnt = x.rowcnt,
                       incident_dt = x.incident_dt,
                       claim_recv_dt = x.claim_recv_dt,
                       hospital = x.hospital,
                       crtable_desc = x.crtable_desc,
                       claimevd = x.claimevd,
                       decision_status = x.decision_status,
                       aprvdate = x.aprvdate,
                       totalamt = x.totalamt,
                       reason = x.reason,
                       longdesc = x.longdesc,
                       start_dt = x.start_dt,
                       end_dt = x.end_dt,
                       paym_amt = x.paym_amt ?? "-",
                       claim_amt =x.claim_amt,
                       first_paydateraw = x.first_paydate,
                       pay_type = x.pay_type,
                       AccountNo = x.AccountNo ?? "-",
                       Bank = x.Bank ?? "-"
                    }).ToList<WebResponseClaimsDetail>();

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append(",\"claimsdetail\":");
                    sb.Append(JsonConvert.SerializeObject(claimsdetail));
                    sb.Append("}");

                    
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }

        }
    }
}

