﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.Test
{
    public class WebSubmitUserProfileController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebSubmitProfile request_data = JsonConvert.DeserializeObject<WebSubmitProfile>(decryptData);
                StringBuilder sb = new StringBuilder();
                //Image image1 = Image.FromFile("D:\\TEST.png");
                //var stringimage = ContentTools.ImageToString(image1);
                //var encode = ContentTools.Base64Encode(img);
                //string test = "iVBORw0KGgoAAAANSUhEUgAABIAAAAKICAIAAACHSRZaAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAB4ySURBVHhe7d0xTGTbecDxrSyaWEixlJHSUCVIbkiKiGz1JDeUW0WUFDFCSkPhgiq7HUWKXbnBSoOLSDSWnixZUJKOEsuKtAUFJSWNJbTVy4F73j52uAMzw53vnjP392vevTPDzLCL7 / n + vsPdNz8AAAAQQoABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYAABAEAEGAAAQRIABAAAEEWAAAABBBBgAAEAQAQYAABBEgAEAAAQRYAAAAEEEGAAAQBABBgAAEESAAQAABBFgAAAAQQQYDMubN / 5XDwDQG6MYDI4GAwDoizkMhkiDAQD0whAGA6XBAADimcBguDQYAEAw4xcMlwADAAhm / IJB02AAAJHMXjBoAgwAIJLZC4ZOgwEAhDF4ARoMACCIqQu4p8EAAAIYuYB7AgwAIICRC8g0GADAopm3gJ9oMACAhTJsAd / QYAAAi2PSAsZpMACABTFmAeMEGADAghizgBYaDABgEcxYQDsNBgDQOQMWMJEGAwDolukKeI4GAwDokNEKeIEGAwDoirkKeJkGAwDohKEKmIoGAwB4PRMVMC0NBgDwSsYpYAYaDADgNcxSwGw0GADA3AxSwMw0GADAfExRwDw0GFU7Oztbe3B8fJxvAoAQRihgThqMeqX0Sj / AycrKSr4JAEKYn4D5aTAqdXh42ARYkm8CgBAWHuBVjLBUqvnRTfI + AISw8AAdMMVSnaa + krwPACEsPEA3DLJU5P379019JfkmAAhh4QE6Y5alCkdHR016Jdvb2 / lWAAhhWgK6lCbavAWlGo1GTX1tbW3d3d3lWwEghFEJ6JgGo2Tn5 + dNfa2trakvAOKZk4DuaTCKtbe31wTYwcFBvgkAAhmSgIXQYBTo4uJiZWWlCbC0nW8FgEAmJGBRNBhFub29XVtba + prY2Mj3woAsYxHwAJpMMrx7t27pr5WVlY + f / 6cbwWAWGYjYLE0GCV4 / A9 / HR8f51sBIJzBCFi4NPLmLejD4 / ra2dnJtwJAH0xFQIQ0 + OYtiPW4vjY3N116HoB + GYmAIGn8zVsQ5ePHj016Jf7ZZQBKYB4C4jRzcN6BBXt80Xn1BUAhTEJANA1GgMcXnffJQwDKYQwCeqDBWDQXnQegTGYgoB8ajMVx0XkAimUAAnrTzMd5B7pwe3v79dxX4qLzAJTG6AP0LE3JeQte5 + Li4uvvfSUbGxt + 9QuA0ph7gP6lWTlvwbyOjo6 + XvMw2dnZUV8AFMjQAxShGZrzDswihdbe3l7zI5SkDPN7XwAUy7gDFCRNz3kLpnNzc7O5udmkV7K + vu6ahwCUzKwDlCXN0HkLXnJxcTEajZr0Sra3t33sEIDCGXSA4qRJOm / BZGO / 9PXx48d8BwAUzJQDlKgZqfMOPLG / v9 / 8kCSrq6vn5 + f5DgAom / kGKFearfMW / Oj29nZra6tJr2Rzc / Pm5ibfBwDFM9wARUsTdt6CH364vr5eX19v0ivxS18AVMdkA5Quzdl5i2E7Pz9fXV1t0it5//59vgMA6mGsASrQDNx5h0F6fMmNtHFycpLvAICqGGiAamiwwXp8yY3RaHRxcZHvAIDamGaAmqT5O28xDGOX3NjY2Li + vs73AUCFjDJAZdIUnrdYdmOX3Hj37p1LbgBQO3MMUJ9mHM87LKmxS24cHBzkOwCgZiYYoFZpKM9bLB2X3ABgWRlfgIppsKXkkhsALDGzC1C3NKPnLernkhsALD2DC1C9ZljPO1TLJTcAGAIjC7Ak0siet6iQS24AMBDmFWB5pME9b1GVw8NDl9wAYCAMK8BS0WB1 + fz58 + bmZpNeiUtuALD0TCrAsklzfN6ibI9PfCUuuQHAEBhTgCWUpvm8RZHGTnwlBwcHLrkBwBCYUYDllGb6vEVhxk58ra + v + 9ghAMNhQAGWU5rs8xbFcOILAAwowNJK833eogBPf + PLiS8ABsh0AiyzNOjnLfozduIrZViKsXwfAAyM0QRYcmniz1v0YezEVyqx1GP5PgAYHnMJsPzS3J + 3COTEFwA8ZSgBBiEFQN4ihBNfANDKRAIMRVMCeYeFceILAJ5hFgGGJSVB3mIBnPgCgOcZRIDBSWGQt + jUly9fdnZ2mvRy4gsAWplCgCFqIiHv8Dqpu / 7617 / mnR9++PDhgxNfADCJ + QMYLg32et9 /      //3ag8vLy3wTADCZ4QMYNA02t8+fP3/33XcPpxLvraysXFxc5PsAgAlMHsDQpXjIW0zn9vZ2f3+/      6a7GaDQ6OjrKdwMAkxk7APxK2AyOj49XV1ebP7FkZWXl4OAgJVm + GwB4loEDIEs5kbdqdXV1evppd / ftvaaPvrq / aXd399Pp6dVVfvSsbm5utra28vM9ePfu3fX1db4bAJiCAAP6d7o7ngvx3n6675K00bylJ64W + yabl59T6q4UXfmZppNq7NPpdC9Zzt8OACwBAQb07epT / wP + vd3Th7eTth7++43T3eYxizNfYlydfnpdHr3YYVef / ik / tF / 5bwcAaifAgL4tPm6m89OI3 + znnXsRjThzYVy9sr0emVx / hzv /      kh / TMwEGwJIQYEDfyguwRropb4W8xZlOgXX / gcgnL//58+fNzc18b/8EGABLQoABfSvsI4iPpVsf      / hsRYDMUxkLezjcFdnh4uLKyku8pggADYEkIMKB3ZZwCm3AGqrlz8e9w6hNg0 / fqw8UQH + QbnpXfwNiJr5WVnxXxGcSp / 3gAoHACDOjf / W8zzS6P5i3yA2by7BmW0 / zET + WvfrVpT / C8mF8PV9Voj5Wrq9NPk39v7O1//d/d2ImvVGKpx4r/2wGAmggwoFaTTpwt4GRJ4Es96/n8ejvtheUffoVs/Jn+8W//fi1vPvzzyinG8qPnUsofGQAURoABtRpegE16G8k872TSNeybE1 / 5QfMSYADQSoABtRpcgE3ur / nex + 3t7f7 + Tn6KB6PR6OjoKN / 9OgIMAFoJMKBWQwuwif01 + 7u4u7v7 + PHj6upqfoaHzxweHBykJMuPeDUBBgCtBBhQqwIC7E1gTUz8 / a + Zvt8mvUajUf7iB +/ evbu + vs6P6IgAA4BWAgyoVQEBdp9E + SEL99pvtzW9NjY2zs / P8yM6JcAAoJUAA2pVQoCll2q28gMXZ + IJsJev0N6aXmtra8fHx / kRCyDAAKCVAANqVUiANdJO3lqQuQKsl / RqCDAAaCXAgFoVFWBJc1Pe6dwsvwJ2c3NzcnKys7PTS3o1BBgAtBJgQK1KC7BGujVvdWvyv8H89RTY2dnZwcHBxsZGvuORyPRqCDAAaCXAgFqVGWBJuiNvdWnSe3jz5l93t7a2VlZW8u634tOrIcAAoJUAA2pVbIAlzd15pxuTT4G12djYODg4ODs7y18dToABQCsBBtSq5ABrpEfkrS5MehNfjUajnZ2dk5OTm5ub / DX9EWAA0EqAAbUqP8CS5nF5Z16Xl5f7 +/ trf / c3zbO1 + 7f /zo8ugwADgFYCDKhVFQHWSA / NW7M4Ozu77661tebFXrZ7Wk7cCDAAaCXAgFpVFGBJ8wV5Z7Kbm5vj4 + Pt7e3Wi2r84uc / y1uTvN0tJHAEGAC0EmBAreoKsEb6mrz1rcvLy / fv37deQT4ZjUZ7e3vn5 + dTXorj7e7pVd + ZI8AAoJUAA2pVY4AlzVc226m7UlmN / XPJX62vrx8cHFxcXDQPzqa + GmK / GSbAAKCVAANqVUCApciZU / ra3d1Pc / 7O1sS30 + Lt3K / yOgIMAFoJMKBWJQTY6833Xmf7R8H6yDABBgCtBBhQq6UIsPsnzi8yo6vT3dkiLLn / VGL + 8kUTYADQSoABtVqOADv99rfCZnP1afYIe / M2JMMEGAC0EmBArZYmwBppJ2 / NZp4zYcmiL9EhwACglQADarVkAZY0N + WdmVydznMubJEfSRRgANBKgAG1WoIAa32r6fa8Nav7k2Gl / F6YAAOAVgIMqFUBAfaz / N9v / elPf8pfN6 / mefLOrOY4Gxb4RybAABg4AQbUqoAA+ 8loNNrZ2Tk5Obm7u8tf9GrNM + edWc2aYV3 / qQkwAGglwIBaxYz45 + fnHz / u / kN + 7nE//8Vob28vPSY / egHSq + SteczyqcRvfh / ttQQYALQSYECtFjTi393d / eUvf / ntb3 / 7q1 / 9Kj / jRL + MqYnmxfLOPKa9UmKHCSbAAKCVAANqFTDiX15eHh4ebm7 + Y37qcaE10bxk3pnDNBXW3TckwACglQADahU44p / +e37uMT3URHrVvDWPlyOsq5NgAgwAWgkwoFaRAVZUTTSvnXdmd / Xp2Qbr6HsSYADQSoABtXp + xL + 5ufn1r3 + db3pkfX19a2vrw4cPJycnFxcXzVO9pMSaSC + ft2Y36ft50M03JcAAoJUAA2o1acT / 5//8352dnbzz5s3a2tp3P0q3 / +53v / vDH / 5wfn6eCu3Lly / 5uV5QaE2kd5C3ZvbcabBOPoUowACglQADajVpxP / lf / zP9fV1flA3yq2J5n3knZlM + p46 + q4EGAC0EmBArQJH / NJrIr2VvDWDyQXWxSkwAQYArQQYUCsB9lh6N3lrWpM / hSjAAGBhBBhQKwE2Jr2hvDUVAQYAPRBgQK0E2FPN28o7LxBgANADAQYU6vLy8vxHR0dHH370m9 / 85o9//OOXL18E2CTpneWt50wMsE6+KwEGAK0EGBAkJdPt7e319fWf//zn3//+9zmnPnzIV4h/sLKykuf0b21ubu7t7aUMe/wvdwmwZ6Q3l7cmmvRNuQw9ACyQAAMW5fz8/Pj4+ ODgIJXV6upqHsCn01pcYwTY89L7y1utJvZXN9 + UAAOAVgIMeJXr6 + vmU4LvH + zs7KTcWl9fz + P2Sx7Oe323v7 + fvjblVnqeZ4prjAB7UfMu8843Jv8CWEffkwADgFYCDHjZ3d1dSqOzs7OUSc0ZrSQP1FMbjUbpq9IznJycXF5e5qd + hQEFWJNLb3c / nc7zeulL89ZXk76h7r4lAQYArQQY8JOnobW5uZkH59ltbGxsb2 + np0rPeXt7m1 + jO8MJsMenq + arsOZr887ks18dfkcCDABaCTAYtNRFJycnOzs7c4dWqqzUae / evUuhlaRnm + ljhK8xnAB78vJv3 + 7Ocy4sfeWz9dXR9TceCDAAaCXAYIguLy8PDw + nj67V1dWvlfXx48eUWJ18hvCVBhxg2Yxnw65On42vLvNLgAHABAIMll9znYwUTs2nCkejUZ6FnygztCYpIMDSi3XlufiZ / PKNFGKpxK4mfdtXqbx2n0 + ve13mlwADgAkEGCyb5ve4Dg8Pt7e3NzY28tg72fr6+ v7 +/ tnZWf76ehQRYB2aHEDPf2xwXC66e / mWKXT + hybAAKCVAANqtWwB9sz7nq3A5pFfqDsCDABaCTCgVgMKsEUmWPOyzXbzWp0QYADQSoABtQoc8Rd / Bip58X1fnU7xm1yzeLs79pLNzXnndQQYALQSYFCo9 +/ f54l1Cpubm3t7e0dHRzHXfy9E5IgfcQpsuotgTHdBjRfdX8d + 0h9T84i8My8BBgCtBBgU5 / b2dn9 / P4 + rT4xGo9Ra5 + fn + dFDdn9G6NsLTaTd8ZM6Xen89NOYGcOkubLh7O / o / g9oukvXpwfnrfmE / u0AQDUEGJTl7OxsdXU1D6xv3mxtbd3d3eX7oE1qsRRju / e58zTI7m98m + 78dDr5jNdEzVPkHQCgC1ZWKMjNzc3Kykoz9SbqixKkH8W8BQC8mmUVSnF2dra2ttak12g0Oj4 + zndA35ofy7wDALyCBRVKkaKrGXOT77//Pt8KxUg/mXkLAJiX1RSK8Piah1tbW/lWKEzzI5p3AIDZWUehf4/ra3t7O98KpUo / qHkLAJiRRRR6Nnbuy1U3qELzE5t3AICpWT6hN4 + vupGoL6rT / OjmHQBgChZO6MFYeiXqi3o1P8N5BwB4liUTevD4gofJ9va2 + qJ2zQ9z3gEAJrBYQg8e / 2vLe3t7 + VaoX / qRzlsAQBsrJfTg8PBQerGsHv6PBYsLALSzRgLQPRkGAK2sjgAsigwDgDHWRQAWS4MBwFcWRQAWzqkwAGhYDgEIosEAwFoIQBynwgAYOKsgANE0GACDZQkEoAdOhQEwTBY / AHqjwQAYGisfAH1yKgyAQbHmAdA / DQbAQFjwACiCU2EADIGlDoCCaDAAlpt1DoCyOBUGwBKzwgFQIhkGwFKytgFQLhkGwJKxqgFQOhkGwNKwngFQBxkGwBKwkgFQExkGQNWsYQDUR4YBUCmrFwC1kmEAVMe6BUDdZBgAFbFiAbAMNBgAVbBcAbAknAoDoHwWKgCWigYDoGRWKQCWjQYDoFiWKACW0MOnEa1xABTH4gTA0tJgAJTGygTAMnMqDICiWJMAWH4yDIBCWI0AGAoZBkDvrEMADIsMA6BHViAAhkiGAdALaw8AwyXDAAhm1QFg6GQYAGGsNwBwT4MBEMBiAwCZU2EALJplBgC + ocEAWBxrDACM02AALIgFBgBaPHwa0SoJQMcsLQAwkQYDoFvWFQB4jlNhAHTIigIAL5NhAHTCWgIA05JhALySVQQAZqPBAJibJQQAZuZUGADzsXgAwJxkGACzsmwAwKvIMACmZ8EAgA5oMACmYbUAgG44FQbAi6wTANAlDQbAMywSANAxp8IAmMTyAAALocEAeMraAACL4lQYAGOsCgCwWBoMgK8sCQCwcE6FAdCwGABAEA0GgJUAAOJoMICBswwAQKiHTyNafwEGygIAAD3QYADD5OgPAP3QYAAD5NAPAL15 + DSitRhgQBz0AaBnMgxgOBzuAaAIMgxgCBzoAaAgMgxguTnEA0BxZBjAsnJwB4BCyTCA5eOwDgBFazIsyfsA1MzRHADqkDtMiQHUzEEcACqjwQDq5QgOAPVxKgygUo7dAFCrJsOSvA9A8RyyAaB6MgygFg7WALAkNBhA + RypAWB5aDCAwjlMA8BSefg04r28D0BJHJ0BYDlpMIACOTQDwNLSYAClcVwGgGX28GlEyz1AKRyRAWD5yTCAQjgWA8BQaDCA3jkQA8CAaDCAfjkKA8CwPHwa0QAA0A / HXwAYIg0G0AsHXwAYKA0GEM + RFwCG6 + HTiPfyPgAL5oALAPjFMIAgDrUAQCbDABbNQRYA + IYGA1gcR1gAYJxTYQAL4tgKALTTYACdc2AFACZyKgygWw6pAMALNBhAVxxPAYCX9dtgChBYGg5nAMBU7j + M2FMINS + d5H2AajmQAQAz6LGCBBiwBBzIAIDZPJyL6meE0GBA7RzFAIB59JJhAgyonaMYADC / +AzTYEDVHMIAgNdqMizJ + wsW9kIAnXP8AgA6E5NhAgyol + MXANCxRWeYAAPq5fgFACyEBgN4ysELAFiU + xNhCyslDQbUyJELAFisBWWYAANq5MgFAERYRIZpMKA6DlsAQJxuM0yAAdVx2AIAonWYYR0 + FUAABywAoB9dtZMAAyrigAUA9KmTDNNgQC0crQCA / r2yoAQYUAtHKwCgCPcnwl7RURoMqIJDFQBQkNdkmAYDyuc4BQAUp8mwJO9PZ9bHA8RznAIAytVkWJL3nzXlwwB65DgFAFRgygzTYEDhHKQAgGpoMKB2jlAAQE3uT4Q9m1gCDCiZIxQAUJ / nM0yDAcVyeAIAatVkWJL3f / T0FoBCODwBANVrMix5vNtsAxTFsQkAWB5Nen2VbwUohgMTALC0NBhQGkclAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAIIsAAAACCCDAAAIAgAgwAACCIAAMAAAgiwAAAAIIIMAAAgCACDAAAIIgAAwAACCLAAAAAgggwAACAIAIMAAAgiAADAAAI8cMP / w9QpwdXx4ClDQAAAABJRU5ErkJggg == ";
                //Convert.ToBase64String(test);
                //{ byte[7824]}
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    var stringtoimage = ContentTools.StringtoImage(request_data.stringimage);
                    string folder = DataFactory.FOLDER_TOUCHPOINT_CONTENT_UPLOAD + "\\profile\\" + cookieData.ID + "\\";
                    //string folder = DataFactory.FOLDER_TOUCHPOINT_CONTENT_UPLOAD + "\\profile\\" + "121212" + "\\";
                    new FileInfo(folder).Directory.Create();
                    int maxWidth = 128, maxHeight = 128;
                    ContentTools.saveContentUploadImageCrop(stringtoimage, maxWidth, maxHeight, folder, "profile");
                    //stringtoimage.Save(DataFactory.FOLDER_TOUCHPOINT_CONTENT_UPLOAD+"\\profile\\" + cookieData.ID + "\\profile.jpg");
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "User",
                        this.GetType().Name,
                        "",
                        request_data.device);
                    string clntnum = cookieData.ID.ToString();
                    var updatehasimage = (from _updatehasimage in tmlth.tidpassoccc
                                          where clntnum.Equals( (_updatehasimage.clntnum + "").Trim())
                                          select _updatehasimage).FirstOrDefault();
                    if (updatehasimage == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    updatehasimage.has_image_url = 1;
                    tmlth.SaveChanges();

                    return DataGenerator.generateTextResult(ResultData.CODE_SUCCESS);
                }
                catch (Exception e)
                {
                    ContentTools.ErrorControllerLog("ERROR:" + e.Message);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                if (ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().Exception.Message == "Maximum request length exceeded.")
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_MAX_FILE_LENGTH + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                else
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
            }
        }
    }
}
