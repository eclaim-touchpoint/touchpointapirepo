﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Mobile.User
{
    public class WebRequestCheckOldPasswordController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if(ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                //string decryptData = data;
                WebRequestCheckOldPassword request_data = JsonConvert.DeserializeObject<WebRequestCheckOldPassword>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {


                #region start query(fetch) data from db
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "User",
                        this.GetType().Name,
                        (
                        "CLNTNUM" + "=" + request_data.clntnum + "," +
                        "USERID" + "=" + request_data.userid
                        ),
                        request_data.device);

                    var enc_password = GenerateSHA256.hashsha256(request_data.password).ToString();
                    var checkoldpass = (from _chk in tmlth.tidpassoccc
                                        where (_chk.clntnum == request_data.clntnum) &&
                                        (_chk.userid == request_data.userid) &&
                                        (_chk.enc_password == enc_password)
                                        select _chk);
                    if(checkoldpass.Count() == 0)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_RETRY + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
