﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.User
{
    public class WebSubmitChangeEmailController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                //string  decryptData = data;
               string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                             ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebSubmitChangeEmail request_data = JsonConvert.DeserializeObject<WebSubmitChangeEmail>(decryptData);
                StringBuilder sb = new StringBuilder();
                DateTime now = DateTime.Now;
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "User",
                        this.GetType().Name,
                        (
                            "USERID" + "=" + request_data.userid + "," +
                            "CLNTNUM" + "=" + request_data.clntnum + "," +
                            "EMAIL" + "=" + request_data.email

                        ),
                        request_data.device);

                    var account = (from ac in tmlth.tidpassoccc
                                   where ac.clntnum == request_data.clntnum && ac.userid == request_data.userid
                                   select ac).FirstOrDefault();

                    if (account == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_NOTFOUND);
                    }

                    var reponse = tmlth.SP_TP_SET_CHANGE_EMAIL(request_data.userid, request_data.email,now).FirstOrDefault();
                    if (reponse == "SUCCESS")
                    {
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS , DataFactory.ENC_TOUCHPOINT_KEY));
                    }
                    else
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_SQL_NORESULT);
                    }

                }
                catch (Exception e)
                {
                    ContentTools.ErrorControllerLog("ERROR:" + e.Message);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
