﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.User
{
    public class WebRequestUserDetailIDCardController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                //string decryptData = data;
                WebRequestUserDetailIDCard request_data = JsonConvert.DeserializeObject<WebRequestUserDetailIDCard>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));

                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "User",
                        this.GetType().Name,
                        (
                        ""
                        ),
                        request_data.device);

                    #region start query(fetch) data from db
                    //var getdetailidcard = tmlth.SP_TP_GET_USER_DETAIL_ID_CARD(request_data.Card_ID).FirstOrDefault();
                    var getdetailidcard = tmlth.SP_TP_GET_USER_DETAIL_ID_CARD(request_data.Card_ID).Select(x => new WebResponeUserDetail
                    {
                        idcard = x.idcard,
                        clntnum = x.clntnum,
                        chdrnum = x.chdrnum,
                        userid = x.userid,
                        name_ins = x.lastname_ins,
                        mobile_no = x.mobile_no,
                        email = x.email
                    }).FirstOrDefault();
                    //WebResponeUserDetail getdata = JsonConvert.DeserializeObject<WebResponeUserDetail>(JsonConvert.SerializeObject(getdetailidcard));
                    if (getdetailidcard == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    sb.Append("{");
                    sb.Append("\"detailidcard\":");
                    sb.Append(JsonConvert.SerializeObject(getdetailidcard));
                    sb.Append("}");
                    //return DataGenerator.generateTextResult(sb.ToString());
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    ContentTools.ErrorControllerLog("ERROR:" + e.Message);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
