﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_API_SERVER.TmlthRequestOTP;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.OTP
{
    public class WebAuthOTPRequestController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebAuthOTPRequest";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                             ResultData.CODE_ERROR_FORMAT_MISSING);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                             ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestUpdateInfoOTP request_data = JsonConvert.DeserializeObject<WebRequestUpdateInfoOTP>(decryptData);
                StringBuilder sb = new StringBuilder();

                DateTime now = DateTime.Now;
                try
                {
                    // *** this Feature not require Login
                    //CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    //if (cookieData.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_SEEM_TO_HACK, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    SubmitLog.ACTION_LOG(tmlth,
                            request_data.clntnum,
                            null,
                            "OTP",
                            this.GetType().Name,
                            (
                                "USERID" + "=" + request_data.userid + "," +
                                "CLNTNUM" + "=" + request_data.clntnum + "," +
                                "FORGOT_TYPE" + "=" + request_data.forgot_type + "," +
                                "MOBILE" + "=" + request_data.mobile

                            ),
                            request_data.device);

                    string otp_key = "";
                    switch (request_data.forgot_type)
                    {
                        case 1: otp_key = DataFactory.OTP_KEY_FORGOT_PASSWORD; break;
                        case 2: otp_key = DataFactory.OTP_KEY_FORGOT_PIN; break;
                        case 3: otp_key = DataFactory.OTP_KEY_REGISTRATION; break;
                        case 10: otp_key = DataFactory.OTP_KEY_FORGOT_PASSWORD; break;
                        case 11: otp_key = DataFactory.OTP_KEY_REGISTRATION; break;
                        case 12: otp_key = DataFactory.OTP_KEY_FORGOT_PIN; break;
                    }

                    // in Forgot Menu Where by [idcard]
                    var user = (from u in tmlth.tidpassoccc
                                where u.idcard == request_data.idcard.Replace("-","")
                                select new
                                    {
                                        idcard = u.idcard,
                                        username = u.userid,
                                        mobile_no = u.mobile_no,
                                    }
                                ).FirstOrDefault();

                    // in Setting Menu Where by [clntnum],[User]
                    if(request_data.idcard == "")
                    {
                        user = (from u in tmlth.tidpassoccc
                                where u.clntnum == request_data.clntnum && u.userid == request_data.userid
                                select new
                                    {
                                        idcard = u.idcard,
                                        username = u.userid,
                                        mobile_no = u.mobile_no,
                                    }
                                ).FirstOrDefault();
                    }

                    string mobile_no = "";
                    string idcard = "";
                    string username = "";

                    if (request_data.forgot_type != 3)
                    {
                        mobile_no = user.mobile_no;
                        idcard = user.idcard;
                        username = user.username;
                        //CASE REGISTER: ไม่ต้องมี user ก็ได้
                        if (user == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_NOTFOUND);
                        }
                    } else
                    {
                        //CASE REGISTER: ส่งเบอร์มา
                        mobile_no = request_data.mobile;
                        idcard = request_data.idcard;
                        username = request_data.userid;
                    }


                    wsOTP OTPWebService = new wsOTP();
                    string otpkey = RandomString(6);
                    string response = OTPWebService.Request_OTP(mobile_no, idcard, otp_key, username, otpkey);

                    sb.Append("{");
                    sb.Append("\"otp\":" + response);
                    sb.Append(",\"mobile_no\":\"" + mobile_no + "\"");
                    sb.Append(",\"otpkey\":\"" + otpkey + "\"");
                    sb.Append("}");

                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||"  + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
