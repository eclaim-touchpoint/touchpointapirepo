﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Version
{
    public class WebRequestVersionController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestVersion";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                WebRequestPolicyUser request_data = JsonConvert.DeserializeObject<WebRequestPolicyUser>(decryptData);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken
                        //var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_TOKEN_DENIED + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_COOKIE_RESULT_TOKEN_DENIED);
                        //return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        #endregion
                    }
                    #endregion

                    #region start query(fetch) data from db           
                    DateTime now = DateTime.Now;
                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,  // for mobile 
                        cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,  // for cms and if both is blank = 0 it's mean mobile only
                        "Version", 
                        this.GetType().Name, 
                        "ID = " + request_data.appid, 
                        request_data.device);
                    WebResponseAppVersion app_version = tmlth.sp_cp_GetAppVersion(request_data.appid).Select(x => new WebResponseAppVersion
                    {
                       appid = x.AppID.ToString(),
                       appnm = x.AppNM,
                       appversion = x.AppVersion,
                       appflag = x.AppFlagForceUpdate,
                       appplatform  = x.AppPlatform,
                       applatest = x.AppLatestDataDate,
                       strappTH = x.strAppLatestDataDate,
                       strappEN = x.strAppLatestDataDateEN,
                    }).FirstOrDefault();

                    WebResponseDataDate datadate = tmlth.SP_TP_GET_DATA_DATE(request_data.appid).Select(x => new WebResponseDataDate
                    {
                        appnm = x.AppNM,
                        applatest = x.AppLatestDataDate,
                        strappTH = x.strAppLatestDataDate,
                        strappEN = x.strAppLatestDataDateEN,
                    }).FirstOrDefault();

                    var new_version = (from _ver in tmlth.TP_SETTING_MOBILE_SPECIFICATION
                                       where _ver.ACTIVE == 1
                                       orderby _ver.ID descending
                                       select new
                                       {
                                           android_version_name = _ver.ANDROID_VERSION_NAME,
                                           android_version_code = _ver.ANDROID_VERSION_CODE,
                                           ios_version_name = _ver.IOS_VERSION_NAME,
                                           ios_version_code = _ver.IOS_VERSION_CODE,
                                           start_date = _ver.START_DATE,
                                           end_date = _ver.END_DATE,
                                       }).FirstOrDefault();

                    string clntnum = cookieData.ID.ToString();

                    var agreement = (from _agreement in tmlth.TP_AGREEMENT
                                     where _agreement.ACTIVE == 1 && _agreement.END_DATE == null &&
                                     (!_agreement.TP_AGEEMENT_LOG.Where(x => x.CLNTNUM == clntnum).Any())
                                     orderby _agreement.START_DATE descending 
                                     select new
                                     {
                                         id = _agreement.ID,
                                         version = _agreement.VERSION_NO,
                                         detail = _agreement.DETAIL,
                                         start_date = _agreement.START_DATE,
                                         end_date = _agreement.END_DATE,
                                         active = _agreement.ACTIVE,
                                     }).FirstOrDefault();

                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));                   
                    sb.Append(",\"appversion\":");
                    sb.Append(JsonConvert.SerializeObject(app_version));
                    sb.Append(",\"agreement\":");
                    sb.Append(JsonConvert.SerializeObject(agreement));
                    sb.Append(",\"data_date\":");
                    sb.Append(JsonConvert.SerializeObject(datadate));
                    sb.Append(",\"version\":");
                    sb.Append(JsonConvert.SerializeObject(new_version));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
