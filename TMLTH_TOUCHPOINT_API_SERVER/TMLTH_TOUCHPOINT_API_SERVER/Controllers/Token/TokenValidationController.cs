﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Token
{

    public class TokenValidationController : ApiController
    {
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            webapp_mobileEntities tmlth = new webapp_mobileEntities();
            if (ModelState.IsValid)
            {
                string decryptdata = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptdata == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestTokenValidation request_data = JsonConvert.DeserializeObject<WebRequestTokenValidation>(decryptdata);
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        #region validateToken                       
                        if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_SESSION_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_SESSION_TIMEOUT + cookieData.NEW_TOKEN);
                        }
                        else if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_TOKEN_TIMEOUT_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_TOKEN_EXPIRE + cookieData.NEW_TOKEN);
                        }
                        else if (cookieData.RESULT == ResultData.CODE_COOKIE_RESULT_IP_NOTSAME_ERROR)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_IP_CHANGE + cookieData.NEW_TOKEN);
                        }
                        else
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_SEEM_TO_HACK);
                        }
                        #endregion
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "Token",
                        this.GetType().Name,
                        "",
                        request_data.device);
                    var checkuser = from _user in tmlth.tidpassoccc
                                    where _user.clntnum == cookieData.ID.ToString() &&
                                    _user.userid == cookieData.USERNAME
                                    select _user;
                    if (checkuser == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    sb.Append("{");
                    sb.Append("\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e )
                {
                    ContentTools.ErrorControllerLog("ERROR:" + e.Message);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_GET_METHOD);
                }
                

            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_GET_METHOD);
            }
        }
      
    }

}
