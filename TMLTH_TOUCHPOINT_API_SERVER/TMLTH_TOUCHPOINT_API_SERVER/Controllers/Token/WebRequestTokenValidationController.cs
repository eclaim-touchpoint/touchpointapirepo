﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Token
{

    public class WebRequestTokenValidationController : ApiController
    {
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
        }
        
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {

                string decryptdata = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptdata == null)
                {
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                try
                {
                    #region readtoken
                    WebRequestTokenValidation readtoken = JsonConvert.DeserializeObject<WebRequestTokenValidation>(decryptdata);
                    var handler = new JwtSecurityTokenHandler();
                    var jsonToken = handler.ReadToken(readtoken.admin);
                    var tokenS = handler.ReadToken(readtoken.admin) as JwtSecurityToken;
                    var userid = tokenS.Claims.First(Claim => Claim.Type == "AccountID").Value;
                    var username = tokenS.Claims.First(Claim => Claim.Type == "AccountUsername").Value;
                    var name = tokenS.Claims.First(Claim => Claim.Type == "Accountname").Value;
                    var isapp = tokenS.Claims.First(claim => claim.Type == "Isapp").Value;
                    var t_ip_clint = tokenS.Claims.First(Claim => Claim.Type == "IP Client").Value;
                    var Dayofyear = tokenS.Claims.First(Claim => Claim.Type == "DayofYear").Value;
                    var RoleID = tokenS.Claims.First(Claim => Claim.Type == "RoleId").Value;
                    var Session = tokenS.Claims.First(Claim => Claim.Type == "Session").Value;
                    var exp = tokenS.Claims.First(Claim => Claim.Type == "exp").Value;
                    #endregion
                    #region Check Session
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.UTF8.GetBytes(DataFactory.SECURITY_KEY);
                    DateTime now = DateTime.Now;
                    StringBuilder sb = new StringBuilder();
                    var Checksession = Convert.ToDateTime(Session);
                    if (Checksession > now)
                    {
                        #region UPDATE SESSTION             
                        var utc = Convert.ToDouble(exp);
                        var newexp = DataFactory.UnixTimeStampToDateTime(utc);
                        var newSession = DateTime.Now.AddMinutes(15).ToString();
                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(new Claim[]
                            {
                                new Claim("AccountID", userid),
                                new Claim("AccountUsername", username),
                                new Claim("Accountname", name),
                                new Claim("IP Client",   t_ip_clint),
                                new Claim("Isapp",  isapp),
                                new Claim("DayofYear", Dayofyear),
                                new Claim("RoleId",RoleID),
                                new Claim("Session",newSession)
                            }),
                            Expires = newexp,
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        };

                        var createtoken = tokenHandler.CreateToken(tokenDescriptor);
                        var token = tokenHandler.WriteToken(createtoken);
              
                        sb.Append(token);
                   
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));

                        #endregion
                    }
                    else                          
                    {
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SESSION_TIMEOUT, DataFactory.ENC_TOUCHPOINT_KEY));
                    }
                    #endregion
                }
                catch(Exception e )
                {
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
               ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
                }
            }
            else
            {
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
               ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
            }
        }
      
    }

}
