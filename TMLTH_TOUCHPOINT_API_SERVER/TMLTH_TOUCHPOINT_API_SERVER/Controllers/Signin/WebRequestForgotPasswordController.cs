﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Signin
{
    public class WebRequestForgotPasswordController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestForgotPassword";
            if (ModelState.IsValid)

            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                           ResultData.CODE_ERROR_FORMAT_MISSING);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                           ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestforgetpassword request_data = JsonConvert.DeserializeObject<WebRequestforgetpassword[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();


                try
                {
                    // *** This feature Login not require ***
                    //CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);
                    //if (cookieData.RESULT != 1)
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //        ResultData.CODE_ERROR_SEEM_TO_HACK, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}

                    SubmitLog.ACTION_LOG(tmlth,
                    null,
                    null,
                    "Signin",
                    this.GetType().Name,
                    (
                        "EMAIL" + "=" + request_data.email + "," +
                        "MOBILENO" + "=" + request_data.mobileno
                    ),
                    request_data.device);
                    #region start query(fetch) data from db                  
                    WebResponseforgetpassword forgotpass = tmlth.SP_TP_GET_USER_FORGOT_PASSWORD(request_data.idcard.Replace("-","")/*, request_data.email, request_data.mobileno*/).Select(x=> new WebResponseforgetpassword
                    {
                        idcard = x.idcard,
                        clntnum = x.clntnum,
                        chdrnum = x.chdrnum,
                        userid = x.userid,
                        nameins = x.name_ins,
                        lastnameins = x.lastname_ins,
                        mobileno = x.mobile_no,
                        email = x.email,
                        agreement = x.agreement,
                        upddate = x.upddate,
                        registerdt = x.register_dt,
                        phone = x.phone,
                        birthDate = x.cltdob_ins
                    }).FirstOrDefault();

                    if (forgotpass == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    if(forgotpass.birthDate != request_data.birthdate)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_RETRY + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_WRONG);
                    }
                    #endregion
                    sb.Append("{");
                    sb.Append("\"mobile_no\":" + "\"" + forgotpass.mobileno + "\"");
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
