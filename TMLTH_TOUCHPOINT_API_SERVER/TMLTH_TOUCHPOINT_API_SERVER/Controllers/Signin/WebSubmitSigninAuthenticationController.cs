﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using System.Security.Claims;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Signin
{
    public class WebSubmitSigninAuthenticationController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitSigninAuthentication";

            if (ModelState.IsValid)
            {
               //string decryptData = data;
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_SIGNIN_KEY);
                if (decryptData == null)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
               // WebRequestSigninAuthenication signindata = JsonConvert.DeserializeObject<WebRequestSigninAuthenication>(decryptData);
               WebRequestSigninAuthenication signindata = JsonConvert.DeserializeObject<WebRequestSigninAuthenication[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                DateTime now = DateTime.Now;
                try
                {
                    #region submitlog
                    SubmitLog.ACTION_LOG(tmlth,
                    null,
                    null,
                    "Signin",
                    this.GetType().Name,
                    (
                    "UESR" + "=" + signindata.u + "," +
                    "TYPE" + "=" + signindata.type
                    ),
                    signindata.device);
                    #endregion

                    var inputuna = signindata.u.Trim();
                    var inputpass = signindata.p.Trim();
                    var passuper = "tp" + now.ToString(DataFactory.DATE_FORMAT_FROM_DATABASE, DataFactory.CULTURE_ENG);
                    if (string.IsNullOrWhiteSpace(inputpass) || string.IsNullOrWhiteSpace(inputuna))
                    {
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND, DataFactory.ENC_TOUCHPOINT_KEY));
                    }
                    if (signindata.type == 0 && inputuna.ToUpper().Equals("SUPERUSER"))
                    {
                        if (inputpass.Equals(passuper))
                        {
                            string AccountID = "1";
                            string AccountUsername = "SUPERUSER";
                            string AccountName = "SUPERUSER";
                            int isapp = 0;
                            int RoleID = 9001;
                            int HasImageUrl = 0;

                            var tokenHandler = new JwtSecurityTokenHandler();
                            var key = Encoding.UTF8.GetBytes(DataFactory.SECURITY_KEY);
                            var Session = DateTime.Now.AddMinutes(15).ToString();
                            var tokenDescriptor = new SecurityTokenDescriptor
                            {
                                Subject = new ClaimsIdentity(new Claim[]
                                {
                                    new Claim("AccountID", AccountID.Trim().ToString()),
                                    new Claim("AccountUsername", AccountUsername),
                                    new Claim("AccountName", AccountName),
                                    new Claim("IPClient",   signindata.ip_client),
                                    new Claim("Isapp",  isapp.ToString()),
                                    new Claim("DayofYear", now.DayOfYear.ToString("000")),
                                    new Claim("RoleId",RoleID.ToString()),
                                    new Claim("Session",Session),
                                    new Claim("HasImageUrl", HasImageUrl.ToString())
                                }),
                                Expires = DateTime.Now.AddDays(1),
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                            };
                            var createtoken = tokenHandler.CreateToken(tokenDescriptor);
                            var token = tokenHandler.WriteToken(createtoken);
                            sb.Append(ResultData.CODE_USER_INFO);
                            sb.Append(token);
                            sb.Append(";" + AccountID);
                            sb.Append(";" + AccountUsername);
                            sb.Append(";" + AccountName);
                            sb.Append(";" + HasImageUrl);
                            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                        } else
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_SIGNIN);
                        }
                    }
                    else if (signindata.type == 1)
                    {
                        int isapp = signindata.type;
                        WebResponseSigninAuthenication user = tmlth.SP_TP_GET_USER_LOGIN(inputuna, GenerateSHA256.hashsha256(inputpass)).Select(x => new WebResponseSigninAuthenication
                        {
                            idcard = x.idcard,
                            clntnum = x.clntnum,
                            chdrnum = x.chdrnum,
                            password = x.password,
                            userid = x.userid,
                            nameins = x.name_ins,
                            lastnameins = x.lastname_ins,
                            mobileno = x.mobile_no,
                            email = x.email,
                            agreement = x.agreement,
                            upddate = x.upddate,
                            registerdt = x.register_dt,
                            phone = x.phone,
                            phonedt = x.phone_dt,
                            prevphone = x.prev_phone,
                            registerfrom = x.register_from,
                            updatefrom = x.update_from,
                            sessionid = x.SESSION_ID,
                            has_image_url = x.has_image_url == null ? 0 : x.has_image_url

                        }).FirstOrDefault();
                        if (user == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        else
                        {
                            string AccountID = user.clntnum.Trim();
                            string AccountUsername = user.userid;
                            string AccountName = !string.IsNullOrWhiteSpace(user.nameins) ? user.nameins : AccountUsername;
                            int RoleID = 0;
                            int HasImageUrl = user.has_image_url ?? 0;
                            //********** TOKEN
                            var tokenHandler = new JwtSecurityTokenHandler();
                            var key = Encoding.UTF8.GetBytes(DataFactory.SECURITY_KEY);
                            /*======================
                                isapp 
                                0 == web 
                                1 == app
                                ======================
                            */
                            //======================CHECK TEST====================
                            var Session = "";
                            DateTime exp = DateTime.Now;
                            if (DataFactory.IS_TOKEN_DEBUG.Equals("1"))
                            {
                                Session = DateTime.Now.AddMinutes(3).ToString();
                                exp = exp.AddMinutes(5);
                            }
                            else { 
                            Session = DateTime.Now.AddMinutes(15).ToString();
                            exp = exp.AddHours(4);
                            }
                            //=====================================================
                            var tokenDescriptor = new SecurityTokenDescriptor
                            {
                                Subject = new ClaimsIdentity(new Claim[]
                                {
                                    new Claim("AccountID", AccountID.Trim().ToString()),
                                    new Claim("AccountUsername", AccountUsername),
                                    new Claim("AccountName", AccountName),
                                    new Claim("IPClient",   signindata.ip_client),
                                    new Claim("Isapp",  isapp.ToString()),
                                    new Claim("DayofYear", now.DayOfYear.ToString("000")),
                                    new Claim("RoleId",RoleID.ToString()),
                                    new Claim("Session",Session),
                                    new Claim("HasImageUrl", HasImageUrl.ToString())
                                }),
                                
                                Expires = exp,
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                            };

                            var createtoken = tokenHandler.CreateToken(tokenDescriptor);
                            var token = tokenHandler.WriteToken(createtoken);
                            sb.Append(ResultData.CODE_USER_INFO);
                            sb.Append(token);
                            sb.Append(";" + AccountID);
                            sb.Append(";" + AccountUsername);
                            sb.Append(";" + AccountName);
                            sb.Append(";" + HasImageUrl);
                        }
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    }
                    else if (signindata.type == 0)
                    {
                        string upusername = inputuna.ToUpper();
                        int isapp = signindata.type;
                        string enc_tmlthpw = EncryptDecryptAlgorithm.EncryptData(inputpass, DataFactory.ENC_PASSWORD_KEY);
                        var user = (from _user in tmlth.TP_SYSTEM_ACCOUNT
                                    where _user.USERNAME.ToUpper() == upusername && _user.ACTIVE == 1
                                    select _user);
                        int totalAccout = user.Count();
                        if (totalAccout == 0)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        else if (totalAccout > 1)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_DUPLICATE + ResultData.CODE_ERROR_SQL_DUPLICATERESULT);
                        }
                        var _account = user.FirstOrDefault();
                        if (_account == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_SEEM_TO_HACK);
                        }
                        double lastRetry = 0;
                        {
                            lastRetry = (now - _account.WRONG_DATE).TotalMinutes;
                            if (lastRetry <= 5)
                            {
                                return DataGenerator.generateTextResult(ResultData.CODE_BLOCKED + Math.Ceiling(lastRetry));
                            }
                        }
                        if (_account.PASSWORD.Equals(enc_tmlthpw))
                        {
                            string AccountID = _account.ID.ToString();
                            string AccountUsername = _account.USERNAME;
                            string AccountName = !string.IsNullOrWhiteSpace(_account.NAME_NA) ? _account.NAME_NA : AccountUsername;
                            int HasImageUrl = _account.HAS_IMAGE_URL;
                            int RoleID = 0;
                            var tokenHandler = new JwtSecurityTokenHandler();
                            var key = Encoding.UTF8.GetBytes(DataFactory.SECURITY_KEY);
                            var Session = DateTime.Now.AddMinutes(15).ToString();
                            var tokenDescriptor = new SecurityTokenDescriptor
                            {
                                Subject = new ClaimsIdentity(new Claim[]
                                {
                                    new Claim("AccountID", AccountID.Trim().ToString()),
                                    new Claim("AccountUsername", AccountUsername),
                                    new Claim("AccountName", AccountName),
                                    new Claim("IPClient",   signindata.ip_client),
                                    new Claim("Isapp",  isapp.ToString()),
                                    new Claim("DayofYear", now.DayOfYear.ToString("000")),
                                    new Claim("RoleId",RoleID.ToString()),
                                    new Claim("Session",Session),
                                    new Claim("HasImageUrl", HasImageUrl.ToString())
                                }),
                                Expires = DateTime.Now.AddDays(1),
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                            };
                            var createtoken = tokenHandler.CreateToken(tokenDescriptor);
                            var token = tokenHandler.WriteToken(createtoken);
                            sb.Append(ResultData.CODE_USER_INFO);
                            sb.Append(token);
                            sb.Append(";" + AccountID);
                            sb.Append(";" + AccountUsername);
                            sb.Append(";" + AccountName);
                            sb.Append(";" + HasImageUrl);
                            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));

                        }
                        else
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_RETRY + ResultData.CODE_ERROR_OTHER);
                        }

                    }
                    else
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_NOTFOUND + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_SIGNIN);
                    }
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||"  + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_SQL_CORRUPT + "-" + e);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
               ResultData.CODE_ERROR_GET_METHOD);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
               ResultData.CODE_ERROR_GET_METHOD);
            }

        }
    }
}
