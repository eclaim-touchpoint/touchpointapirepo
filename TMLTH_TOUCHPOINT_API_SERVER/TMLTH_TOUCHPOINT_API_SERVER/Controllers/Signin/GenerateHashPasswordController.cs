﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;
using static TMLTH_TOUCHPOINT_API_SERVER.Class.SubmitLog;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Signin
{
    public class GenerateHashPasswordController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            var page = "GenerateHashPasswordController";
            try
            {
                DeviceInfo device = new DeviceInfo();
                SubmitDevice devices =  new SubmitDevice();
                devices.application_id = Convert.ToInt32(device.application_id);
                devices.application_code = device.application_code;
                devices.mobile_manufacturer = device.mobile_manufacturer;
                devices.mobile_model = device.mobile_model;
                devices.mobile_os_name = device.mobile_os_name;
                devices.mobile_os_version = device.mobile_os_version;

                SubmitLog.ACTION_LOG(tmlth,
                    null,
                    null,
                    "Signin",
                    this.GetType().Name,
                    "",
                    devices
                    );
                var getpassword = (from getpass in tmlth.tidpassoccc
                                   where getpass.enc_password == null || getpass.enc_password.Equals("") || getpass.enc_password == ""
                                   select getpass);
                if (getpassword.Any())
                {
                    foreach (var hashpass in getpassword)
                    {
                        var hashpassword = GenerateSHA256.hashsha256(hashpass.password.Trim());
                        hashpass.enc_password = hashpassword;
                    }
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(ResultData.CODE_SUCCESS);
                }
                return DataGenerator.generateTextResult(ResultData.CODE_NOTFOUND);
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_HASHPASSWORD + "-" + e);
            }
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
              ResultData.CODE_ERROR_GET_METHOD);
        }
    }
}
