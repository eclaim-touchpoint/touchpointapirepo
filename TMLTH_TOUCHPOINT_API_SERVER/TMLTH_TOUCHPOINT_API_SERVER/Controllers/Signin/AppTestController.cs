﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Signin
{
    public class AppTestController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TMLTH +
                ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                var test = tmlth.SP_TP_GET_DOCUMENT_FORM();
                var test1 = tmlth.SP_TP_GET_HOSPITAL();

                //var province = tmlth.SP_TP_GET_DOCUMENT_FORM().ToList();

                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(data);
                var tokenS = handler.ReadToken(data) as JwtSecurityToken;
                var jti = tokenS.Claims.First(claim => claim.Type == "iat").Value;
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                SubmitRegister register = JsonConvert.DeserializeObject<SubmitRegister[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                DateTime now = DateTime.Now;

                try
                {

                }
                catch (Exception e)
                {
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_SQL_CORRUPT, DataFactory.ENC_TOUCHPOINT_KEY));
                }
            }
            else
            {
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TMLTH +
               ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));
            }
            return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TMLTH +
              ResultData.CODE_ERROR_GET_METHOD, DataFactory.ENC_TOUCHPOINT_KEY));

        }
    }
}
