﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Signin
{
    public class WebSubmitRegisterController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {

            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
            ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitRegister";
            if (ModelState.IsValid)
            {
              
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                try
                {
                    WebSubmitRegister submit_data = JsonConvert.DeserializeObject<WebSubmitRegister>(decryptData);
                    //WebSubmitRegister submit_data = JsonConvert.DeserializeObject<WebSubmitRegister>(decryptData);
                    StringBuilder sb = new StringBuilder();
                    DateTime now = DateTime.Now;
                    string birthdate = submit_data.birthdate.ToString();
                    string lasercode = submit_data.lasercode;
                    /*if (!string.IsNullOrWhiteSpace(lasercode))
                    {
                        TmlthRegisterService.wsAuthorization_AuthenSoapClient CheckDopa = new TmlthRegisterService.wsAuthorization_AuthenSoapClient();
                        var responsedopa = CheckDopa.VerifyDOPA(submit_data.idcard, submit_data.lgivname_ins, submit_data.lsurname_ins, birthdate, submit_data.lasercode);
                       
                        if (responsedopa == null ||
                            responsedopa.CODE == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_RETRY + "DOPA RETURN EMPTY RESULT");
                        }
                        else if (!responsedopa.CODE.Equals("0"))
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_RETRY + responsedopa.DESCRIPTION);
                        }
                    }*/

                    SubmitLog.ACTION_LOG(tmlth,
                        null,
                        null,
                        "Signin",
                        this.GetType().Name,
                        (
                        "NAME" + "=" + submit_data.lgivname_ins + "," +
                        "SURNAME" + "=" + submit_data.lsurname_ins + "," +
                        "USER_ID" + "=" + submit_data.userid + "," +
                        "EMAIL" + "=" + submit_data.email + "," +
                        "AGREEMENT" + "=" + submit_data.agreement + "," +
                        "MOBILENO" + "=" + submit_data.mobileno + "," +
                        "TYPE" + "=" + submit_data.type
                        ),
                        submit_data.device);
                    WebResponseRegister getregister = tmlth.SP_TP_GET_USER_REGISTER(submit_data.idcard, submit_data.lgivname_ins, submit_data.lsurname_ins, submit_data.userid,submit_data.mobileno ,(submit_data.birthdateDecimal) ).Select(x=> new WebResponseRegister
                    {
                        insured_secuityno = x.insured_secuityno,
                        clntnum_ins = x.clntnum_ins,
                        lgivname_ins = x.lgivname_ins,
                        lsurname_ins = x.lsurname_ins,
                        cltphone01_ins = x.cltphone01_ins,
                        cltphone02_ins = x.cltphone02_ins,
                        secuityno_own = x.secuityno_own,
                        secuityno_ins = x.secuityno_ins,
                        clntnum_own = x.clntnum_own,
                        lgivname_own = x.lgivname_own,
                        lsurname_own = x.lsurname_own,
                        chdrnum = x.chdrnum,
                        pstatcode = x.pstatcode,
                        id_type = x.id_type,
                        is_math_id = x.is_math_id,
                        is_math_birth = x.is_math_birth,
                        is_math_mobile = x.is_math_mobile,
                        chdrnum_system_existing = x.chdrnum_system_existing,
                        chdrnum_system_isvalid = x.chdrnum_system_isvalid,
                        chdrnum_input_existing = x.chdrnum_input_existing,
                        chdrnum_input_isvalid = x.chdrnum_input_isvalid,
                        massage = x.massage
                    }).FirstOrDefault();

                    if(getregister.massage != "OK")
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_RETRY + getregister.massage);
                    }
                    else
                    {
                        if (submit_data.type == 2)
                        {
                            var saveregister = tmlth.SP_TP_SAVE_USER(getregister.insured_secuityno, getregister.clntnum_ins, getregister.chdrnum, GenerateSHA256.hashsha256(submit_data.password), submit_data.userid, getregister.lgivname_ins,
                                getregister.lsurname_ins, getregister.cltphone02_ins, now, getregister.cltphone01_ins, submit_data.email, now, submit_data.email, submit_data.agreement, now, now, "", now, "");
                        }
                        return DataGenerator.generateTextResult(ResultData.CODE_SUCCESS);
                    }

                }
                catch(Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||"  + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
            //return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT,
            //DataFactory.ENC_TOUCHPOINT_KEY));

        }
    }
}
