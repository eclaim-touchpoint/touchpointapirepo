﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.ContentArticle
{
    public class WebRequestContentCategoryController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestContentCategory";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestContent request_data = JsonConvert.DeserializeObject<WebRequestContent[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                                        "ContentArticle",
                                        this.GetType().Name,
                                        (
                                            "ID" + "=" + request_data.id
                                        ),
                                        request_data.device);

                    #region Object Referance
                    //*** content type
                    sb.Append("{");
                    var contentType = (from _content_type in tmlth.TP_REF_CONTENT_TYPE
                                       select new { id = _content_type.ID, name = _content_type.NAME });
                    sb.Append("\"content_type\":");
                    sb.Append(JsonConvert.SerializeObject(contentType));

                    if (request_data.id > 0)
                    {

                        var content = (from _content in tmlth.TP_CONTENT
                                       where _content.CONTENT_CATEGORY_ID == request_data.id &&
                                        _content.ACTIVE == 1
                                       orderby _content.NO
                                       select new WebResponseContent { 
                                           id = _content.ID,
                                           no = _content.NO,
                                           title = _content.TITLE,
                                           content_type_id = _content.CONTENT_TYPE_ID,
                                           image_url = _content.IMAGE_URL,
                                           start_dateRaw = _content.START_DATE,
                                           end_dateRaw = _content.END_DATE,
                                           has_url_link = _content.HAS_URL_LINK,
                                           is_application_popup = _content.IS_APPLICATION_POPUP,
                                           url_link = _content.URL_LINK,
                                           is_premium_user = _content.IS_PREMIUM_USER,
                                           is_external_notification = _content.IS_EXTERNAL_NOTIFICATION,
                                       });
                        
                        sb.Append(",\"content\":");
                        sb.Append(JsonConvert.SerializeObject(content));

                        var content_category = (from _content in tmlth.TP_CONTENT_CATEGORY
                                       where _content.ID == request_data.id &&
                                        _content.ACTIVE == 1
                                       select new WebResponseContentCategory
                                       {
                                           id = _content.ID,
                                           content_type_id = _content.CONTENT_TYPE_ID,
                                           name = _content.NAME,
                                           description = _content.DESCRIPTION
                                       }).FirstOrDefault();

                        sb.Append(",\"content_category\":");
                        sb.Append(JsonConvert.SerializeObject(content_category));

                    }
                    #endregion
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }

    }
}
