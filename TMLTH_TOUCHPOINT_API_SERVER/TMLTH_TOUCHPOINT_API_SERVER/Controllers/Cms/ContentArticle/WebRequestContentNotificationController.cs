﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.ContentArticle
{
    public class WebRequestContentNotificationController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestContentNotification";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestContentNotification request_data = JsonConvert.DeserializeObject<WebRequestContentNotification[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();

                CookieResult cookieData = new CookieResult();
                if (request_data.is_check_cookie == 1)
                {
                    cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);
                }
                try
                {
                   if (request_data.is_check_cookie == 1)
                   {
                        if (cookieData.IS_APP == 0)
                        {
                            if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                            {
                                return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                            }
                        }
                    }

                    #region Object Referance   
                    DateTime now = DateTime.Now;
                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 && cookieData.ID > 0 ? cookieData.ID : (int?)null,
                    "ContentArticle",
                    this.GetType().Name,
                    (
                        "IS_PREMIUM" + "=" + request_data.is_premium + "," +
                        "IS_CHECK_COOKIE" + "=" + request_data.is_check_cookie
                    ),
                    request_data.device);
                    var contentnotification = (from _content in tmlth.TP_CONTENT
                                               where _content.IS_PREMIUM_USER == request_data.is_premium &&
                                        _content.ACTIVE == 1 && now >= _content.START_DATE  && now <= _content.END_DATE &&
                                        _content.IS_APPLICATION_POPUP == 1
                                        orderby _content.START_DATE descending
                                       select new WebResponseContent
                                       {
                                           id = _content.ID,
                                           title = _content.TITLE,
                                           content_type_id = _content.CONTENT_TYPE_ID,
                                           image_url = _content.IMAGE_URL,
                                           start_dateRaw = _content.START_DATE,
                                           end_dateRaw = _content.END_DATE,
                                           has_url_link = _content.HAS_URL_LINK,
                                           url_link = _content.URL_LINK,
                                           is_premium_user = _content.IS_PREMIUM_USER,
                                           is_external_notification = _content.IS_EXTERNAL_NOTIFICATION,
                                       });

                    /*if (!contentnotification.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }*/
                    sb.Append("{");
                    sb.Append("\"contentnotification\":");
                    sb.Append(JsonConvert.SerializeObject(contentnotification));
                    sb.Append("}");
                    #endregion
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}

