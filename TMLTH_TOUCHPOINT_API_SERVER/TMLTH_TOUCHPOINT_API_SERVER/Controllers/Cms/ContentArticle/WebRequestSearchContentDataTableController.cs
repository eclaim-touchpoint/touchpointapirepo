﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.ContentArticle
{
    public class WebRequestSearchContentDataTableController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestSearchContentDataTable";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                EncryptTableData request_data = JsonConvert.DeserializeObject<EncryptTableData[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                //*** workspace
                try
                {
                    #region validateCookie
                    if (request_data.mode == 0)
                    {
                        if (cookieData.IS_APP == 0)
                        {
                            if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                            {
                                return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                            }
                        }
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "ContentArticle",
                    this.GetType().Name,
                    (
                    ""
                    ),
                    request_data.device);

                    //*** set account detail
                    int system_account_id = cookieData.ID;
                    int system_account_role_id = cookieData.ROLE_ID;
                    #region start query(fetch) data from db
                    //*** start query(fetch) as list
                    int iorder = request_data.sort;
                    int istart = request_data.start;
                    int ilen = request_data.len;
                    string searchString = HttpUtility.UrlDecode(request_data.search.Trim());
                    int sEcho = request_data.sEcho;
                    int mode = request_data.mode;
                    //*** start query(fetch all as list) from sqlserver
                    var QCDTR = ((IEnumerable<TP_CONTENT>)tmlth.TP_CONTENT)
                        .Where(x => x.ACTIVE > 0 &&
                        (request_data.id > 0 ? (x.CONTENT_CATEGORY_ID == null && x.NO == null) : true)
                        )
                        .Select((r, i) => new { no = i + 1, sys = r });

                    string[] searchStrings = searchString.Split(new[] { "&amp;", "search=1" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var search_data in searchStrings)
                    {
                        try
                        {
                            string[] search = search_data.Split(new[] { '=' }, 2);
                            if (search.Length % 2 != 0) { continue; }
                            string field = search[0];
                            string valueRaw = search[1].Trim();

                            //*** name,mobile,email
                            if (field.Equals("txt") && mode == 0)
                            {

                                //valueRaw = valueRaw.ToUpper();
                                QCDTR = QCDTR.Where(x => x.sys.TITLE.Contains(valueRaw) ||
                                                         x.sys.TP_REF_CONTENT_TYPE.NAME.Contains(valueRaw.ToUpper()) ||
                                                         x.sys.START_DATE.ToString().Equals(valueRaw) ||
                                                         x.sys.END_DATE.ToString().Equals(valueRaw)
                                                         );
                              

                                //                             x.sys.URL_LINK.ToUpper().Contains(valueRaw.ToUpper()) ||
                                //                             x.sys.IS_PREMIUM_USER == int.Parse(valueRaw) ||
                                //                             x.sys.IS_EXTERNAL_NOTIFICATION == int.Parse(valueRaw)); 
                            }
                            else if (field.Equals("title") && mode == 0)
                            {
                                QCDTR = QCDTR.Where(x => x.sys.TITLE.ToUpper().Contains(valueRaw.ToUpper()));

                            }
                            else if (field.Equals("type"))
                            {
                                QCDTR = QCDTR.Where(x => x.sys.CONTENT_TYPE_ID == int.Parse(valueRaw));
                            }
                            else if(field.Equals("is_premium_user"))
                            {
                                int value = int.Parse(valueRaw);
                                if (cookieData.IS_APP == 1)
                                {
                                    QCDTR = QCDTR.Where(x => x.sys.IS_PREMIUM_USER == 0 || x.sys.IS_PREMIUM_USER == value);
                                }
                                else
                                {
                                    QCDTR = QCDTR.Where(x => x.sys.IS_PREMIUM_USER == value);
                                }
                            }
                            else if (field.Equals("is_external_notification"))
                            {
                                int value = int.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.sys.IS_EXTERNAL_NOTIFICATION == value);
                            }
                            else if (field.Equals("is_application_popup"))
                            {
                                int value = int.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.sys.IS_APPLICATION_POPUP == value);
                            }
                            else if (field.Equals("st_from") && mode == 0)
                            {
                               DateTime date = DateTime.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.sys.START_DATE >= date);
                            }
                            else if (field.Equals("st_to") && mode == 0)
                            {
                                DateTime date = DateTime.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.sys.START_DATE <= date);
                            }
                            else if (field.Equals("en_from") && mode == 0)
                            {
                                DateTime date = DateTime.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.sys.END_DATE >= date);
                            }
                            else if (field.Equals("en_to") && mode == 0)
                            {
                                DateTime date = DateTime.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.sys.END_DATE <= date);
                            }
                            else if (field.Equals("category"))
                            {
                                int value = int.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.sys.CONTENT_CATEGORY_ID == value);
                            }
                        }
                        catch { continue; }
                    }

                    switch (iorder)
                    {
                        case 0: QCDTR = QCDTR.OrderBy(x => x.no); break;
                        case 1: QCDTR = QCDTR.OrderBy(x => x.sys.CONTENT_TYPE_ID); break;
                        case 2: QCDTR = QCDTR.OrderBy(x => x.sys.TITLE); break;
                        case 3: QCDTR = QCDTR.OrderBy(x => x.sys.IMAGE_URL); break;
                        case 4: QCDTR = QCDTR.OrderBy(x => x.sys.START_DATE); break;
                        case 5: QCDTR = QCDTR.OrderBy(x => x.sys.END_DATE); break;
                        case 6: QCDTR = QCDTR.OrderBy(x => x.sys.URL_LINK); break;
                        case 7: QCDTR = QCDTR.OrderBy(x => x.sys.IS_PREMIUM_USER); break;
                        case 8: QCDTR = QCDTR.OrderBy(x => x.sys.IS_EXTERNAL_NOTIFICATION); break;

                        case 100: QCDTR = QCDTR.OrderByDescending(x => x.no); break;
                        case 101: QCDTR = QCDTR.OrderByDescending(x => x.sys.CONTENT_TYPE_ID); break;
                        case 102: QCDTR = QCDTR.OrderByDescending(x => x.sys.TITLE); break;
                        case 103: QCDTR = QCDTR.OrderByDescending(x => x.sys.IMAGE_URL); break;
                        case 104: QCDTR = QCDTR.OrderByDescending(x => x.sys.START_DATE); break;
                        case 105: QCDTR = QCDTR.OrderByDescending(x => x.sys.END_DATE); break;
                        case 106: QCDTR = QCDTR.OrderByDescending(x => x.sys.URL_LINK); break;
                        case 107: QCDTR = QCDTR.OrderByDescending(x => x.sys.IS_PREMIUM_USER); break;
                        case 108: QCDTR = QCDTR.OrderByDescending(x => x.sys.IS_EXTERNAL_NOTIFICATION); break;

                        default: QCDTR = QCDTR.OrderByDescending(x => x.sys.ID); break;
                    }

                    int totalDisplayRecord = QCDTR.Count();
                    QCDTR = QCDTR.Skip(istart).Take(ilen);
                    var QCDR = (from _search in QCDTR
                                select new WebResponseContentSearchDataTable
                                {
                                    no = _search.no,
                                    id = _search.sys.ID,
                                    content_type_name = _search.sys.TP_REF_CONTENT_TYPE.NAME,
                                    title= _search.sys.TITLE,
                                    image_url = _search.sys.IMAGE_URL,
                                    start_date_raw = _search.sys.START_DATE,                                    
                                    end_date_raw = _search.sys.END_DATE,
                                    has_url_link = _search.sys.HAS_URL_LINK,
                                    url_link = _search.sys.URL_LINK ?? "-",
                                    is_application_popup = _search.sys.IS_APPLICATION_POPUP,
                                    is_premium_user = _search.sys.IS_PREMIUM_USER,
                                    is_external_notification = _search.sys.IS_EXTERNAL_NOTIFICATION
                                });

                    sb.Append("{");
                    sb.Append("\"sEcho\":");
                    sb.Append(sEcho);

                    sb.Append(",\"role\":");
                    sb.Append(system_account_role_id);

                    sb.Append(",\"iTotalRecords\":");
                    sb.Append(QCDR.Count());

                    sb.Append(",\"iTotalDisplayRecords\":");
                    sb.Append(totalDisplayRecord);

                    sb.Append(",\"data\":");
                    sb.Append(JsonConvert.SerializeObject(QCDR));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}

