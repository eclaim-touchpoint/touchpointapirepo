﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.ContentArticle
{
    public class WebSubmitContentCategoryController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitContentCategory";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            if (decryptData == null)
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
            WebSubmitContentCategory submit_data = JsonConvert.DeserializeObject<WebSubmitContentCategory[]>(decryptData)[0];
            StringBuilder sb = new StringBuilder();
            DateTime now = DateTime.Now;
            CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

            try
            {
                #region validateCookie
                if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                {
                    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                }
                #endregion
                #region submitlog
                SubmitLog.ACTION_LOG(tmlth,
                cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                "ContentArticle",
                this.GetType().Name,
                (
                    "ID" + "=" + submit_data.id + "," +
                    "CONTENT_TYPE_ID" + "=" + submit_data.content_type_id + "," +
                    "TITLE" + "=" + submit_data.name + "," +
                    "DESCRIPTION" + "=" + submit_data.description + "," +
                    "ACTIVE" + "=" + submit_data.active
                ),
                submit_data.device);

                #endregion
                int systemaccount_id = cookieData.ID;
                #region start query(insert) data to db
                
                if (submit_data.id == 0)
                {

                    int content_no = 0;


                    var content = (from _content in tmlth.TP_CONTENT_CATEGORY
                                   where _content.CONTENT_TYPE_ID == submit_data.content_type_id &&
                                   _content.ACTIVE == 1
                                   select new { id = _content.ID, no = _content.NO, name = _content.NAME });

                    if (content.Where(x => x.name == submit_data.name).Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_DUPLICATE);
                    }

                    content_no = content.Count() + 1;
                    TP_CONTENT_CATEGORY newcontent = new TP_CONTENT_CATEGORY()
                    {
                        CONTENT_TYPE_ID = submit_data.content_type_id,
                        NO = content_no,
                        NAME = submit_data.name,
                        DESCRIPTION = submit_data.description,
                        ACTIVE = 1,
                        CREATE_BY = systemaccount_id,
                        CREATE_DATE = now
                    };
                    tmlth.TP_CONTENT_CATEGORY.AddObject(newcontent);
                    tmlth.SaveChanges();


                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + newcontent.ID, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                else
                {
                    var updateContent = (from _content in tmlth.TP_CONTENT_CATEGORY
                                         where _content.ID == submit_data.id && _content.ACTIVE == 1
                                         select _content).FirstOrDefault();
                    if (updateContent == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    updateContent.MODIFY_BY = systemaccount_id;
                    updateContent.MODIFY_DATE = now;
                    if (submit_data.active == 1)
                    {
                        updateContent.NAME = submit_data.name;
                        updateContent.DESCRIPTION = submit_data.description;
                        updateContent.ACTIVE = submit_data.active;
                    }
                    else
                    {
                        updateContent.ACTIVE = submit_data.active;
                    }

                    int reorder = 1;
                    foreach (var x in submit_data.list_content)
                    {
                        var updatesorting = (from _content in tmlth.TP_CONTENT
                                             where _content.ID == x.id
                                             select _content).FirstOrDefault();
                        if (updatesorting == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        updatesorting.CONTENT_CATEGORY_ID = submit_data.id;
                        updatesorting.MODIFY_BY = systemaccount_id;
                        updatesorting.MODIFY_DATE = now;
                        updatesorting.NO = reorder++;

                    }
                    tmlth.SaveChanges();

                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }

                

                #endregion
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }

    }
}
