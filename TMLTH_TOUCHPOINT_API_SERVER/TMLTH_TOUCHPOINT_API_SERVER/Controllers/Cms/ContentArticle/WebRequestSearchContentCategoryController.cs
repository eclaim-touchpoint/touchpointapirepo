﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.ContentArticle
{
    public class WebRequestSearchContentCategoryController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {

            if (ModelState.IsValid)
            {
                string page = "WebRequestSearchContentCategory";
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestSearchFAQCategory request_data = JsonConvert.DeserializeObject<WebRequestSearchFAQCategory[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    #endregion
                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "ContentArticle",
                    this.GetType().Name,
                    (
                        "ID" + "=" + request_data.id
                    ),
                    request_data.device);

                    sb.Append("{");

                    var contentType = (from _content_type in tmlth.TP_REF_CONTENT_TYPE
                                       where _content_type.ACTIVE == 1
                                       select new { id = _content_type.ID, name = _content_type.NAME });
                    sb.Append("\"content_type\":");
                    sb.Append(JsonConvert.SerializeObject(contentType));

                    /*var content_category = (from _content_category in tmlth.TP_CONTENT_CATEGORY
                                       where _content_category.ACTIVE > 0 && _content_category.CONTENT_TYPE_ID == request_data.id
                                       orderby _content_category.NO
                                       select new { id = _content_category.ID, name = _content_category.NAME, no = _content_category.NO });

                    sb.Append("\"content_category\":");
                    sb.Append(JsonConvert.SerializeObject(content_category));*/
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }

                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }

        }

    }
}
