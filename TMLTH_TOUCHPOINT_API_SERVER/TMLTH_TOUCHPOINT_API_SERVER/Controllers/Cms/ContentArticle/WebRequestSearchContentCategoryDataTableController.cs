﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.ContentArticle
{
    public class WebRequestSearchContentCategoryDataTableController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestSearchContentCategoryDataTable";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                EncryptTableData request_data = JsonConvert.DeserializeObject<EncryptTableData[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                //*** workspace
                try
                {
                    #region validateCookie
                    if (request_data.mode == 0)
                    {
                        if (cookieData.IS_APP == 0)
                        {
                            if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                            {
                                return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                            }
                        }
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "ContentArticle",
                    this.GetType().Name,
                    (
                    ""
                    ),
                    request_data.device);

                    //*** set account detail
                    int system_account_id = cookieData.ID;
                    int system_account_role_id = cookieData.ROLE_ID;
                    #region start query(fetch) data from db
                    //*** start query(fetch) as list
                    int iorder = request_data.sort;
                    int istart = request_data.start;
                    int ilen = request_data.len;
                    string searchString = HttpUtility.UrlDecode(request_data.search.Trim());
                    int sEcho = request_data.sEcho;
                    int mode = request_data.mode;
                    //*** start query(fetch all as list) from sqlserver
                    var QCDTR = ((IEnumerable<TP_CONTENT_CATEGORY>)tmlth.TP_CONTENT_CATEGORY)
                        .Where(x => x.ACTIVE > 0)
                        .Select((r, i) => new { no = i + 1, content_category = r });

                    string[] searchStrings = searchString.Split(new[] { "&amp;", "search=1" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var search_data in searchStrings)
                    {
                        try
                        {
                            string[] search = search_data.Split(new[] { '=' }, 2);
                            if (search.Length % 2 != 0) { continue; }
                            string field = search[0];
                            string valueRaw = search[1].Trim();

                            if (field.Equals("type"))
                            {
                                QCDTR = QCDTR.Where(x => x.content_category.CONTENT_TYPE_ID == int.Parse(valueRaw));
                            }
                            else if (field.Equals("name"))
                            {
                                QCDTR = QCDTR.Where(x => x.content_category.NAME.Contains(valueRaw));
                            }
                        }
                        catch { continue; }
                    }

                    switch (iorder)
                    {
                        case 0: QCDTR = QCDTR.OrderBy(x => x.no); break;
                        case 1: QCDTR = QCDTR.OrderBy(x => x.content_category.CONTENT_TYPE_ID); break;

                        case 100: QCDTR = QCDTR.OrderByDescending(x => x.no); break;
                        case 101: QCDTR = QCDTR.OrderByDescending(x => x.content_category.CONTENT_TYPE_ID); break;

                        default: QCDTR = QCDTR.OrderByDescending(x => x.content_category.ID); break;
                    }

                    int totalDisplayRecord = QCDTR.Count();
                    QCDTR = QCDTR.Skip(istart).Take(ilen);
                    var QCDR = (from _search in QCDTR
                                select new WebResponseContentCategorySearchDataTable
                                {
                                    no = _search.no,
                                    id = _search.content_category.ID,
                                    content_type_name = _search.content_category.TP_REF_CONTENT_TYPE.NAME,
                                    name = _search.content_category.NAME,
                                });

                    sb.Append("{");
                    sb.Append("\"sEcho\":");
                    sb.Append(sEcho);

                    sb.Append(",\"role\":");
                    sb.Append(system_account_role_id);

                    sb.Append(",\"iTotalRecords\":");
                    sb.Append(QCDR.Count());

                    sb.Append(",\"iTotalDisplayRecords\":");
                    sb.Append(totalDisplayRecord);

                    sb.Append(",\"data\":");
                    sb.Append(JsonConvert.SerializeObject(QCDR));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }

    }
}
