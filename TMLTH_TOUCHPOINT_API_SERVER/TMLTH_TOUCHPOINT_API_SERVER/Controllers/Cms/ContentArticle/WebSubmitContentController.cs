﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.ContentArticle
{
    public class WebSubmitContentController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitContent";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            if (decryptData == null)
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + 
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
            WebSubmitContent submit_data = JsonConvert.DeserializeObject<WebSubmitContent[]>(decryptData)[0];
            StringBuilder sb = new StringBuilder();
            DateTime now = DateTime.Now;
            CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

            try
            {
                #region validateCookie
                if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                {
                    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                }
                #endregion
                #region submitlog
                SubmitLog.ACTION_LOG(tmlth,
                cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                "ContentArticle",
                this.GetType().Name,
                (
                    "ID" + "=" + submit_data.id + "," +
                    "CONTENT_TYPE_ID" + "=" + submit_data.content_type_id + "," +
                    "TITLE" + "=" + submit_data.title + "," +
                    "IMAGE_URL" + "=" + submit_data.image_url + "," +
                    "START_DATE" + "=" + submit_data.start_date + "," +
                    "END_DATE" + "=" + submit_data.end_date + "," +
                    "HAS_URL_LINK" + "=" + submit_data.has_url_link + "," +
                    "URL_LINK" + "=" + submit_data.url_link + "," +
                    "IS_APPLICATION_POPUP" + "=" + submit_data.is_application_popup + "," +
                    "IS_PREMIUM_USER" + "=" + submit_data.is_premium_user + "," +
                    "IS_EXTERNAL_NOTIFICATION" + "=" + submit_data.is_external_notification + "," +
                    "ACTIVE" + "=" + submit_data.active
                ),
                submit_data.device);

        #endregion
        int systemaccount_id = cookieData.ID;
                #region start query(insert) data to db
                if (submit_data.content_type_id == DataFactory.REF_CONTENT_TYPE_DOCUMENT)
                {
                    submit_data.end_date = null;
                    submit_data.start_date = now;
                    //submit_data.image_url = "";
                    submit_data.is_external_notification = 0;
                    submit_data.is_premium_user = 0;
                }
                if (submit_data.id == 0)
                {

                    var content = (from _content in tmlth.TP_CONTENT
                                   where _content.TITLE == submit_data.title &&
                                   _content.ACTIVE == 1
                                   select new { id = _content.ID});

                    if(content.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_DUPLICATE);
                    }

                    var checkContent = (from _content in tmlth.TP_CONTENT
                                        where _content.CONTENT_CATEGORY_ID == submit_data.category && _content.ACTIVE == 1
                                        orderby _content.NO descending
                                        select new { id = _content.ID, no = _content.NO }).Count();

                    TP_CONTENT newcontent = new TP_CONTENT()
                    {
                        CONTENT_TYPE_ID = submit_data.content_type_id,
                        CONTENT_CATEGORY_ID = submit_data.category > 0 ? submit_data.category : (int?)null,
                        NO = submit_data.content_type_id == 3 ? checkContent + 1 : (int?)null,
                        TITLE = submit_data.title,
                        IMAGE_URL = submit_data.image_url,
                        START_DATE = submit_data.start_date,
                        END_DATE = submit_data.end_date,
                        HAS_URL_LINK = submit_data.has_url_link,
                        URL_LINK = submit_data.url_link,
                        IS_PREMIUM_USER = submit_data.is_premium_user,
                        IS_EXTERNAL_NOTIFICATION = submit_data.is_external_notification,
                        IS_APPLICATION_POPUP = submit_data.is_application_popup,
                        ACTIVE = 1,
                        CREATE_BY = systemaccount_id,
                        CREATE_DATE = now,
                    };
                    tmlth.TP_CONTENT.AddObject(newcontent);
                    tmlth.SaveChanges();

                    if (submit_data.is_external_notification == 1)
                    {
                        try
                        {
                            //================= add Data in Firebase ================
                            var meassage = new
                            {
                                to = "/topics/news",
                                notification = new
                                {
                                    title = "ข่าวใหม่ " + "(" + now.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO) + ")",
                                    body = submit_data.title.ToString(),
                                    sound = "default"
                                    //data = new
                                    //{
                                    //    title = "ข่าวใหม่ " + "(" + now.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO) + ")",
                                    //    body = submit_data.title.ToString(),
                                    //    sound = "default"
                                    //}
                                },
                                data = new
                                {
                                    title = "ข่าวใหม่ " + "(" + now.ToString(DataFactory.DATE_FORMAT, DataFactory.CULTURE_INFO) + ")",
                                    body = submit_data.title.ToString(),
                                    sound = "default"
                                }

                            };
                            var json = JsonConvert.SerializeObject(meassage);
                            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                            string message_key = DataFactory.FIREBASE_MESSAGES_KEY;
                            string sender = DataFactory.FIREBASE_SENDER;

                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                            tRequest.Method = "post";
                            tRequest.ContentType = "application/json";
                            tRequest.Headers.Add($"Authorization: key={message_key}");
                            tRequest.Headers.Add($"Sender: id={sender}");

                            tRequest.ContentLength = byteArray.Length;
                            Stream dataStream = tRequest.GetRequestStream();
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            dataStream.Close();

                            WebResponse tresponse = tRequest.GetResponse();
                            dataStream = tresponse.GetResponseStream();
                            StreamReader tReander = new StreamReader(dataStream);

                            string sResponseFromServer = tReander.ReadToEnd();

                            tReander.Close();
                            dataStream.Close();
                            tresponse.Close();
                        }
                        catch (Exception e)
                        {
                            var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_FIREBASE_PUSH_MESSAGE);
                        }

                    }
                    
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + newcontent.ID, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                else
                {                   
                    var updateContent = (from _content in tmlth.TP_CONTENT
                                         where _content.ID == submit_data.id && _content.ACTIVE == 1
                                         select _content).FirstOrDefault();
                    if (updateContent == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }                   
                    updateContent.MODIFY_BY = systemaccount_id;
                    updateContent.MODIFY_DATE = now;
                    if (submit_data.active == 1)
                    {
                        updateContent.CONTENT_TYPE_ID = submit_data.content_type_id;
                        updateContent.TITLE = submit_data.title;
                        updateContent.IMAGE_URL = submit_data.image_url;
                        updateContent.HAS_URL_LINK = submit_data.has_url_link;
                        updateContent.URL_LINK = submit_data.url_link;
                        updateContent.START_DATE = submit_data.start_date;
                        updateContent.END_DATE = submit_data.end_date;
                        updateContent.IS_APPLICATION_POPUP = submit_data.is_application_popup;
                        updateContent.IS_PREMIUM_USER = submit_data.is_premium_user;
                        updateContent.IS_EXTERNAL_NOTIFICATION = submit_data.is_external_notification;
                    }
                    else
                    {
                        updateContent.ACTIVE = submit_data.active;
                    }
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                #endregion
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }

}
