﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class WebRequestAppVersionController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                string page = "WebRequestAppVersion";
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                StringBuilder sb = new StringBuilder();
                WebRequestAppVersion request_data = JsonConvert.DeserializeObject<WebRequestAppVersion[]>(decryptData)[0];
                try
                {

                    sb.Append("{");
                    var new_version = (from _ver in tmlth.TP_SETTING_MOBILE_SPECIFICATION
                                       where _ver.ACTIVE == 1
                                       orderby _ver.ID descending
                                       select new
                                       {
                                           android_name = _ver.ANDROID_VERSION_NAME,
                                           android_code = _ver.ANDROID_VERSION_CODE,
                                           ios_name = _ver.IOS_VERSION_NAME,
                                           ios_code = _ver.IOS_VERSION_CODE,
                                           start_date = _ver.START_DATE,
                                           end_date = _ver.END_DATE,
                                       }).FirstOrDefault();

                    sb.Append("\"version\":");
                    sb.Append(JsonConvert.SerializeObject(new_version));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT,
                    DataFactory.ENC_TOUCHPOINT_KEY));
                }
            }
            else
            {
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT,
                DataFactory.ENC_TOUCHPOINT_KEY));
            }
        }
    }
}
