﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.Version
{
    public class WebSubmitAppVersionController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitAppVersion";
            if (ModelState.IsValid)
            {
                    string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                    if (decryptData == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                            ResultData.CODE_ERROR_FORMAT_MISSING);
                    }

                    WebSubmitAppVersion submit_data = JsonConvert.DeserializeObject<WebSubmitAppVersion[]>(decryptData)[0];
                    StringBuilder sb = new StringBuilder();
                    CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                                cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                                cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                                "Version_CMS",
                                this.GetType().Name,
                                (
                                    "ID" + "=" + submit_data.id + "," +
                                    "ANDROID_NAME" + "=" + submit_data.android_name + "," +
                                    "IS_EDIT" + "=" + submit_data.is_edit + "," +
                                    "ANDROID_CODE" + "=" + submit_data.android_code + "," +
                                    "IOS_NAME" + "=" + submit_data.ios_name + "," +
                                    "IOS_CODE" + "=" + submit_data.ios_code

                                ),
                                submit_data.device);
                    DateTime now = DateTime.Now;
                    int system_account_id = cookieData.ID;
                  
                    var old_version = (from _ver in tmlth.TP_SETTING_MOBILE_SPECIFICATION
                                        orderby _ver.ID descending
                                       select _ver).FirstOrDefault();

                    if (old_version != null)
                    {
                        if ((submit_data.android_name == old_version.ANDROID_VERSION_NAME &&
                                            submit_data.android_code == old_version.ANDROID_VERSION_CODE &&
                                            submit_data.ios_name == old_version.IOS_VERSION_NAME &&
                                            submit_data.ios_code == old_version.IOS_VERSION_CODE
                                            ))
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_DUPLICATE + ResultData.CODE_ERROR_DATA_WRONG);
                        }
                    }

                    TP_SETTING_MOBILE_SPECIFICATION new_ver = new TP_SETTING_MOBILE_SPECIFICATION()
                    {
                        ANDROID_VERSION_NAME = submit_data.android_name,
                        ANDROID_VERSION_CODE = submit_data.android_code,
                        IOS_VERSION_NAME = submit_data.ios_name,
                        IOS_VERSION_CODE = submit_data.ios_code,
                        START_DATE = now,
                        END_DATE = (DateTime?)null,
                        ACTIVE = 1,
                        CREATE_BY = cookieData.ID,
                        CREATE_DATE = now,
                        MODIFY_BY = cookieData.ID,
                        MODIFY_DATE = now,
                    };

                    if (old_version != null)
                    {
                        old_version.ACTIVE = 0;
                        old_version.END_DATE = now;
                    }

                    tmlth.TP_SETTING_MOBILE_SPECIFICATION.AddObject(new_ver);
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + new_ver.ID, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
