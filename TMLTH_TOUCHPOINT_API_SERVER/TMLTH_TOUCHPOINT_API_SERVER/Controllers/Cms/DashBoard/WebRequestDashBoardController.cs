﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.ContentArticle
{
    public class WebRequestDashBoardController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                string page = "WebRequestDashBoard";
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestDashBoard request_data = JsonConvert.DeserializeObject<WebRequestDashBoard[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    #endregion
                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "DashBoard",
                    this.GetType().Name,
                    (
                        "ID" + "=" + request_data.id
                    ),
                    request_data.device);

                    #region Object Referance
                    //*** content type
                    sb.Append("{");
                    #region menu
                    var main = (from _menu in tmlth.TP_TEMPLATE_APPLICATION
                               where _menu.ACTIVE > 0
                               select new
                               {
                                   id = _menu.ID,
                                   refid = _menu.TEMPLATE_APPLICATION_PAGE_ID,
                                   des = _menu.DESCRIPTION,
                                   update = _menu.MODIFY_DATE ?? _menu.CREATE_DATE,
                                   active = _menu.ACTIVE
                               });
                    main = main.OrderByDescending(x => x.update);
                    var menu = (from __menu in main
                                   select new WebResponseUpdateApptemMenu
                                   {
                                       id = __menu.id,
                                       refid = __menu.refid,                                     
                                       des = __menu.des,
                                       updateraw = __menu.update,
                                       active = __menu.active
                                   });
                    sb.Append("\"menu\":");
                    sb.Append(JsonConvert.SerializeObject(menu));
                    #endregion
                    #region image
                    var img = (from _img in tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE
                               where _img.ACTIVE > 0
                               select new
                               {
                                   id = _img.ID,
                                   refid = _img.TEMPLATE_APPLICATION_PAGE_ID,
                                   Url = _img.IMAGE_URL,
                                   update = _img.MODIFY_DATE ?? _img.CREATE_DATE
                                   });
                    img = img.OrderByDescending(x => x.update);
                    var image = (from _imge in img
                                   select new WebResponseUpdateApptemimage
                                   {
                                       id = _imge.id,
                                       refid = _imge.refid,
                                       url = _imge.Url,
                                       updateraw = _imge.update
                                   });
                    sb.Append(",\"image\":");
                    sb.Append(JsonConvert.SerializeObject(image));
                    #endregion
                    #region content
                    var cont = (from _content in tmlth.TP_CONTENT
                                where _content.ACTIVE > 0 
                                   select new
                                   {
                                       id = _content.ID,
                                       refid = _content.CONTENT_TYPE_ID,
                                       title = _content.TITLE,                                       
                                       update = _content.MODIFY_DATE ?? _content.CREATE_DATE                                     
                                   });
                    cont = cont.OrderByDescending(x => x.update);
                    var content = (from _cont in cont
                                   select new WebResponseUpdateContent {
                                       id = _cont.id,
                                       refid = _cont.refid,
                                       titleraw = _cont.title,                                   
                                       updateraw = _cont.update
                                   }).Take(10);
                    sb.Append(",\"content\":");
                    sb.Append(JsonConvert.SerializeObject(content));
                    #endregion
                    #region faq
                    var fa = (from _faq in tmlth.TP_FAQ
                              where _faq.ACTIVE > 0
                               select new
                               {
                                   id = _faq.ID,
                                   question = _faq.QUESTION,
                                   createdate = _faq.CREATE_DATE,
                                   modifydate = _faq.MODIFY_DATE,
                                   update = _faq.MODIFY_DATE ?? _faq.CREATE_DATE
                               });
                    fa = fa.OrderByDescending(x => x.update);
                    var faq = (from _faq in fa
                                   select new WebResponseUpdateFAQ
                                   {
                                       id = _faq.id,
                                       questionraw = _faq.question,
                                       updateraw = _faq.update
                                   }).Take(10);
                    sb.Append(",\"faq\":");
                    sb.Append(JsonConvert.SerializeObject(faq));
                    #endregion
                    sb.Append(",\"token\":");
                    sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));
                    
                    #endregion

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            } else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
