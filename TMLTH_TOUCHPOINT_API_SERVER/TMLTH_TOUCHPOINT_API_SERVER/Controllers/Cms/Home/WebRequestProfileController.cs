﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.SystemAccount
{
    public class WebRequestProfileController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestProfile";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestProfile request_data = JsonConvert.DeserializeObject<WebRequestProfile[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                //*** workspace
                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    //*** set account detail

                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                                        cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                                        cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                                        "Home",
                                        this.GetType().Name,
                                        "",
                                        request_data.device);
                    #region start query(fetch) data from db
                    if (cookieData.IS_APP == 0)
                    {
                        var profile = (from _sys in tmlth.TP_SYSTEM_ACCOUNT
                                        where _sys.ID == cookieData.ID &&
                                                _sys.ACTIVE > 0
                                        select new
                                        {
                                            id = _sys.ID,
                                            role_id = _sys.SYSTEM_ACCOUNT_ROLE_ID,
                                            username = _sys.USERNAME,
                                            name_en = _sys.NAME_EN,
                                            name_na = _sys.NAME_NA,
                                            employee_no = _sys.EMPLOYEE_NO,
                                            mobile = _sys.MOBILE,
                                            email = _sys.EMAIL,
                                            has_img_url = _sys.HAS_IMAGE_URL,
                                            active = _sys.ACTIVE,
                                            modify_date = _sys.MODIFY_DATE
                                        }).FirstOrDefault();
                        if (profile == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        sb.Append("{");
                        sb.Append("\"Profilecms\":");
                        sb.Append(JsonConvert.SerializeObject(profile));

                        #region Object Referance
                        //*** role
                        var roleList = (from _role in tmlth.TP_REF_SYSTEM_ACCOUNT_ROLE
                                        select new { id = _role.ID, name = _role.NAME });
                        sb.Append(",\"role_list\":");
                        sb.Append(JsonConvert.SerializeObject(roleList));
                        #endregion

                    }
                    else
                    {
                        var profile = (from _sys in tmlth.tidpassoccc
                                        where _sys.clntnum.Equals(cookieData.ID.ToString())
                                        select new
                                        {
                                            id = _sys.clntnum,
                                            username = _sys.userid,
                                            name = _sys.name_ins,
                                            last = _sys.lastname_ins,
                                            mobile = _sys.mobile_no,
                                            email = _sys.email,

                                        }).FirstOrDefault();
                        if (profile == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        sb.Append("{");
                        sb.Append("\"Profilemobile\":");
                        sb.Append(JsonConvert.SerializeObject(profile));
                    }

                   
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }

    }
}

