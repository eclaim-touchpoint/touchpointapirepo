﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class WebRequestSearchFAQCategoryDataTableController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestSearchFAQDataTable";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                EncryptTableData request_data = JsonConvert.DeserializeObject<EncryptTableData[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                //*** workspace
                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }

                    //*** set account detail
                    int system_account_id = cookieData.ID;
                    int system_account_role_id = cookieData.ROLE_ID;
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "FAQ",
                    this.GetType().Name,
                    "",
                    request_data.device);

                    #region start query(fetch) data from db
                    //*** start query(fetch) as list
                    int iorder = request_data.sort;
                    int istart = request_data.start;
                    int ilen = request_data.len;
                    string searchString = HttpUtility.UrlDecode(request_data.search.Trim());
                    int sEcho = request_data.sEcho;

                    //*** start query(fetch all as list) from sqlserver
                    var QCDTR = ((IEnumerable<TP_FAQ_CATEGORY>)tmlth.TP_FAQ_CATEGORY)
                        .Where(x => x.ACTIVE > 0)
                        .Select((r, i) => new { no = i + 1, sys = r });

                    string[] searchStrings = searchString.Split(new[] { "&amp;", "search=1" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var search_data in searchStrings)
                    {
                        try
                        {
                            string[] search = search_data.Split(new[] { '=' }, 2);
                            if (search.Length % 2 != 0) { continue; }
                            string field = search[0];
                            string valueRaw = search[1].Trim();

                            //*** simple search
                            if (field.Equals("txt"))
                            {
                                //valueRaw = valueRaw.ToUpper();
                                QCDTR = QCDTR.Where(x => x.sys.NAME.Contains(valueRaw) ||
                                                         x.sys.DESCRIPTION.Contains(valueRaw));
                            }
                            //*** FAQName
                            else if (field.Equals("id"))
                            {
                                QCDTR = QCDTR.Where(x => x.sys.ID == int.Parse(valueRaw));
                            }
                            //*** No
                            else if (field.Equals("no"))
                            {
                                QCDTR = QCDTR.Where(x => x.sys.NO == int.Parse(valueRaw));
                            }
                            //*** Question
                            else if (field.Equals("name"))
                            {
                                QCDTR = QCDTR.Where(x => x.sys.NAME.Contains(valueRaw));
                            }
                            else if (field.Equals("description"))
                            {
                                QCDTR = QCDTR.Where(x => x.sys.DESCRIPTION.Contains(valueRaw));
                            }



                        }
                        catch { continue; }
                    }

                    switch (iorder)
                    {
                        case 0: QCDTR = QCDTR.OrderBy(x => x.sys.ID); break;
                        case 1: QCDTR = QCDTR.OrderBy(x => x.sys.NO); break;
                        case 2: QCDTR = QCDTR.OrderBy(x => x.sys.NAME); break;
                        case 3: QCDTR = QCDTR.OrderBy(x => x.sys.DESCRIPTION); break;

                        case 100: QCDTR = QCDTR.OrderByDescending(x => x.sys.ID); break;
                        case 101: QCDTR = QCDTR.OrderByDescending(x => x.sys.NO); break;
                        case 102: QCDTR = QCDTR.OrderByDescending(x => x.sys.NAME); break;
                        case 103: QCDTR = QCDTR.OrderByDescending(x => x.sys.DESCRIPTION); break;
                        default: QCDTR = QCDTR.OrderByDescending(x => x.sys.NO); break;
                    }

                    int totalDisplayRecord = QCDTR.Count();
                    QCDTR = QCDTR.Skip(istart).Take(ilen);
                    var QCDR = (from _search in QCDTR
                                orderby _search.sys.NO
                                select new WebResponseFAQSearchDataTable
                                {
                                    no = _search.no,
                                    id = _search.sys.ID,
                                    category_no = _search.sys.NO,                                                         
                                    name = _search.sys.NAME,     
                                    description = _search.sys.DESCRIPTION
                                   
                                });
                    //var dataQCDR = QCDR.OrderBy(x => x.no);

                    sb.Append("{");
                    sb.Append("\"sEcho\":");
                    sb.Append(sEcho);

                    sb.Append(",\"role\":");
                    sb.Append(system_account_role_id);

                    sb.Append(",\"iTotalRecords\":");
                    sb.Append(QCDR.Count());

                    sb.Append(",\"iTotalDisplayRecords\":");
                    sb.Append(totalDisplayRecord);

                    sb.Append(",\"data\":");
                    sb.Append(JsonConvert.SerializeObject(QCDR));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}

