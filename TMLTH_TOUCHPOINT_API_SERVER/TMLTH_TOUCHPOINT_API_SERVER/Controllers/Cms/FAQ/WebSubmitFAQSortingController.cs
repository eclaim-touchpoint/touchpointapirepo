﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.ContentArticle
{
    public class WebSubmitFAQSortingController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitFAQSorting";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            if (decryptData == null)
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + 
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
            WebSubmitFAQSorting submit_data = JsonConvert.DeserializeObject<WebSubmitFAQSorting[]>(decryptData)[0];
            StringBuilder sb = new StringBuilder();
            DateTime now = DateTime.Now;
            CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

            try
            {
                int newcatid = 0; 
                #region validateCookie
                if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                {
                    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                }
                int systemaccount_id = cookieData.ID;
                #endregion

                SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "FAQ",
                    this.GetType().Name,
                    (
                    "LIST_QUESTION" + "=" + submit_data.list_question + "," +
                    "ID" + "=" + submit_data.id + "," +
                    "NAME" + "=" + submit_data.name + "," +
                    "NAMETH" + "=" + submit_data.nameth + "," +
                    "ACTIVE" + "=" + submit_data.active
                    ),
                    submit_data.device);

                #region start query(insert) data to db
                if (submit_data.name != null && submit_data.nameth != null && submit_data.id == 0)
                {
                    var faqcatno = (from _faqcat in tmlth.TP_FAQ_CATEGORY
                                    where _faqcat.ACTIVE > 0
                                    select new
                                    {
                                        nocat = _faqcat.NO
                                    });
                    int count = faqcatno.Count();
                    TP_FAQ_CATEGORY newfaqcat = new TP_FAQ_CATEGORY()
                    {
                        NO = count + 1,
                        NAME = submit_data.name,
                        DESCRIPTION = submit_data.nameth,
                        ACTIVE = 1,
                        CREATE_BY = systemaccount_id,
                        CREATE_DATE = now,
                    };
                    tmlth.TP_FAQ_CATEGORY.AddObject(newfaqcat);
                    tmlth.SaveChanges();
                    newcatid = newfaqcat.ID;
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + newcatid, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                else if (submit_data.id > 0)
                {
                    var updatecat = (from _cat in tmlth.TP_FAQ_CATEGORY
                                     where _cat.ID == submit_data.id && _cat.ACTIVE == 1
                                     select _cat).FirstOrDefault();
                    if (updatecat == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    updatecat.ACTIVE = submit_data.active;
                    updatecat.MODIFY_BY = systemaccount_id;
                    updatecat.MODIFY_DATE = now;
                    if (submit_data.active == 1)
                    {
                        //updatecat.FAQ_CATEGORY_ID = 1;
                        ////updatefaq.NO = submit_data.faq_no;
                        updatecat.NAME = submit_data.name;
                        updatecat.DESCRIPTION = submit_data.nameth;
                    }

                    if (submit_data.active == 0)
                    {
                        var updsortingcat = (from _up in tmlth.TP_FAQ_CATEGORY
                                          where _up.ACTIVE > 0 && _up.ID != submit_data.id
                                          select _up);
                        updatecat.NO = 0;
                        int newno = 1;
                        foreach (var upsort in updsortingcat)
                        {
                            upsort.NO = newno;
                            newno++;
                        }
                        var delectfaq = (from _delect in tmlth.TP_FAQ
                                         where _delect.ACTIVE > 0 && _delect.FAQ_CATEGORY_ID == submit_data.id
                                         select _delect);
                        foreach (var defaq in delectfaq)
                        {
                            defaq.ACTIVE = 0;
                            defaq.MODIFY_BY = cookieData.ID;
                            defaq.MODIFY_DATE = now;
                        }

                    }
                  
                }
                
                    foreach (var x in submit_data.list_question)
                    {
                        var updatesorting = (from _faq in tmlth.TP_FAQ
                                             where _faq.ID == x.id
                                             select _faq).FirstOrDefault();
                        if (updatesorting == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        updatesorting.MODIFY_BY = systemaccount_id;
                        updatesorting.MODIFY_DATE = now;
                        updatesorting.NO = x.faq_no;
                    }
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                
                #endregion
                
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }

}
