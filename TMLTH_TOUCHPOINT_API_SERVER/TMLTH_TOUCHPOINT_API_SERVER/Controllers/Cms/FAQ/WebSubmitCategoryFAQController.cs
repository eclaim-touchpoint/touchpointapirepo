﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class WebSubmitCategoryFAQController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitCatagoryFAQ";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            if (decryptData == null)
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + 
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
            WebSubmitCatagoryFAQ submit_data = JsonConvert.DeserializeObject<WebSubmitCatagoryFAQ[]>(decryptData)[0];
            StringBuilder sb = new StringBuilder();
            DateTime now = DateTime.Now;
            CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

            try
            {
                #region validateCookie
                if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                {
                    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                }
                int systemaccount_id = cookieData.ID;
                #endregion

                SubmitLog.ACTION_LOG(tmlth,
                                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                                    "FAQ",
                                    this.GetType().Name,
                                    (
                                        "ID" + "=" + submit_data.id + "," +
                                        "NAME" + "=" + submit_data.name + "," +
                                        "DESCRIPTION" + "=" + submit_data.description + "," +
                                        "NO" + "=" + submit_data.no + "," +
                                        "ACTIVE" + "=" + submit_data.active
                                    )
                                    ,
                                    submit_data.device);
                #region start query(insert) data to db

                if (submit_data.id == 0)
                {                   
                    var faq = (from _faq in tmlth.TP_FAQ_CATEGORY
                               where _faq.ID == submit_data.id && _faq.ACTIVE == 1
                               select _faq).FirstOrDefault();
                    if (faq != null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_DUPLICATE);
                    }
                    TP_FAQ newfaq = new TP_FAQ()
                    {
                        ID = submit_data.id,                    
                        NO = submit_data.no,
                        QUESTION = submit_data.name,
                        ANSWER = submit_data.description,
                        ACTIVE = 1,
                        CREATE_BY = systemaccount_id,
                        CREATE_DATE = now,
                    };
                    tmlth.TP_FAQ.AddObject(newfaq);
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS , DataFactory.ENC_TOUCHPOINT_KEY));
                    //return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + newfaq.ID, DataFactory.ENC_TOUCHPOINT_KEY));
                }                
                else
                {                   
                    var updatefaq = (from _faq in tmlth.TP_FAQ_CATEGORY
                                        where _faq.ID == submit_data.id && _faq.ACTIVE == 1
                                        select _faq).FirstOrDefault();
                    if (updatefaq == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    updatefaq.ACTIVE = submit_data.active;
                    updatefaq.MODIFY_BY = systemaccount_id;
                    updatefaq.MODIFY_DATE = now;
                    if (submit_data.active == 1)
                    {
                       
                        //updatefaq.NO = submit_data.faq_no;
                        updatefaq.NAME = submit_data.name;
                        updatefaq.DESCRIPTION = submit_data.description;
                    }

                    if (submit_data.active == 0)
                    {
                        var updsorting = (from _up in tmlth.TP_FAQ_CATEGORY
                                          where _up.ACTIVE > 0 && _up.ID != submit_data.id
                                          select _up);
                        updatefaq.NO = 0;
                        int newno = 1;
                        foreach (var a in updsorting)
                        {                            
                            a.NO = newno;
                            newno++;
                        }
                    }
                                  
                    tmlth.SaveChanges();
                }
                #endregion
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }

}
