﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.FAQ
{
    public class WebRequestFAQController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                string page = "WebRequestFAQ";
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestFAQ request_data = JsonConvert.DeserializeObject<WebRequestFAQ[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "FAQ",
                    this.GetType().Name,
                    (
                        "ID" + "=" + request_data.id
                    ),
                    request_data.device);

                    #region Object Referance
                    //*** content type
                    sb.Append("{");

                    if (request_data.id > 0)
                    {

                        var faq = (from _faq in tmlth.TP_FAQ
                                      where _faq.ID == request_data.id &&
                                       _faq.ACTIVE == 1
                                      orderby _faq.NO
                                      select new WebResponseFAQ
                                      {
                                          id = _faq.ID,
                                          faq_category_id = _faq.FAQ_CATEGORY_ID,
                                          faq_no = _faq.NO,
                                          question = _faq.QUESTION,
                                          answer = _faq.ANSWER
                                      }).FirstOrDefault();

                        if(faq == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }

                        sb.Append("\"faq\":");
                        sb.Append(JsonConvert.SerializeObject(faq));
                        sb.Append(",\"token\":");
                        sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));

                    }
                    #endregion

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }

    }
}
