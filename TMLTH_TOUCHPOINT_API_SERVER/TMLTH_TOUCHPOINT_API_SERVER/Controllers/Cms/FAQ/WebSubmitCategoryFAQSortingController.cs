﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class WebSubmitCategoryFAQSortingController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitCatagoryFAQSorting";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            if (decryptData == null)
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + 
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
            WebSubmitCatagoryFAQSorting submit_data = JsonConvert.DeserializeObject<WebSubmitCatagoryFAQSorting[]>(decryptData)[0];
            StringBuilder sb = new StringBuilder();
            DateTime now = DateTime.Now;
            CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

            try
            {
                #region validateCookie
                if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                {
                    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                }
                int systemaccount_id = cookieData.ID;
                #endregion

                SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "FAQ",
                    this.GetType().Name,
                    (
                       "UPDATE FAQ CATEGORY"
                    ),
                    submit_data.device);

                #region start query(insert) data to db
                //var anyDuplicate = submit_data.list_FAQ.GroupBy(x => x.question).Any(g => g.Count() > 1);
                //if (anyDuplicate)
                //{
                //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                //          ResultData.CODE_DUPLICATE);
                //}   
                foreach (var x in submit_data.list_category)
                    {
                        var updatesorting = (from _faq in tmlth.TP_FAQ_CATEGORY
                                             where _faq.ID == x.id
                                         select _faq).FirstOrDefault();
                        if (updatesorting == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                         updatesorting.MODIFY_BY = systemaccount_id;
                         updatesorting.MODIFY_DATE = now;
                         updatesorting.NO = x.no;
                          
                        
                    }
                    tmlth.SaveChanges();
                
                #endregion
                return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }

}
