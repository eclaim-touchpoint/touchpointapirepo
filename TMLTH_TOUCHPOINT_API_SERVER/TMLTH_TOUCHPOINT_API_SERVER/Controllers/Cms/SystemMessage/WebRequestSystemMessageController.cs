﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers
{
    public class WebRequestSystemMessageController : ApiController
    {
        string page = "WebRequestSystemMessage";
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestSystemMessage request_data = JsonConvert.DeserializeObject<WebRequestSystemMessage[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(request_data.admin, request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    //*** set account detail
                    int system_account_id = cookieData.ID;

                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "SystemMessage",
                    this.GetType().Name,
                    "",
                    request_data.device);

                    #region start query(fetch) data from db
                    //*** get drop down or multiple choice
                    sb.Append("{");

                    #region Object Referance
                    //*** message type
                    var message_type = (from _mess in tmlth.TP_REF_SYSTEM_MESSAGE_TYPE
                                        select new
                                        {
                                            id = _mess.ID,
                                            name = _mess.NAME
                                        });
                    sb.Append("\"message_type\":");
                    sb.Append(JsonConvert.SerializeObject(message_type));
                    #endregion
                    DateTime month = DateTime.Now.AddMonths(-3);
                    //if (request_data.id > 0)
                    //{
                    string clntnum = cookieData.ID.ToString();
                    var systemMessage = (from _sys in tmlth.TP_SYSTEM_MESSAGE
                                         where _sys.REF_CLNTNUM == clntnum &&
                                               _sys.IS_FINISH == 1 &&
                                               _sys.ACTIVE > 0 && _sys.CREATE_DATE >= month
                                         select new WebRensponSystemMessage
                                         {
                                             id = _sys.ID,
                                             clntnum = _sys.REF_CLNTNUM,
                                             chdrnum = _sys.REF_CHDRNUM,
                                             type_id = _sys.SYSTEM_MESSAGE_TYPE_ID,
                                             header = _sys.HEADER,
                                             message = _sys.MESSAGE,
                                             has_img_url = _sys.HAS_IMAGE_URL,
                                             active = _sys.ACTIVE,
                                             read_firstdate_raw = _sys.CLIENT_READ_FIRST_DATE,
                                             read_lastdate_raw = _sys.CLIENT_READ_LAST_DATE,
                                             createdateraw = _sys.CREATE_DATE,
                                             modify_dateraw = _sys.MODIFY_DATE,
                                             is_read = _sys.CLIENT_READ_FIRST_DATE == null ? 0 : 1
                                         }) ;
                     
                    /*var systemMessageResult = systemMessage.GroupBy(x => x.createdate).Select(group => new{
                            createdate = group.OrderByDescending(x => x.createdate)
                        }).OrderByDescending(x => x.createdate.First().createdate);*/
                         
                    sb.Append(",\"systemMessageResult\":");
                    sb.Append(JsonConvert.SerializeObject(systemMessage));
                    //}
                    sb.Append("}");

                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                    #endregion
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
