﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.SystemMessage
{
    public class WebSubmitSystemMessageController : ApiController
    {
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            webapp_mobileEntities tmlth = new webapp_mobileEntities();
            string page = "WebSubmitSystemMessage";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebSubmitSystemMessage submit_data = JsonConvert.DeserializeObject<WebSubmitSystemMessage[]>(decryptData)[0];
                DateTime now = DateTime.Now;
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(submit_data.admin, submit_data.ip_client);

                //*** workspace
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "SystemMessage",
                    this.GetType().Name,
                    (
                    "ID" + "=" + submit_data.id + "," +
                    "active" + "=" + submit_data.active
                    ),
                    submit_data.device);
                    #region validate data

                    var updateSystemMessage = (from _sys in tmlth.TP_SYSTEM_MESSAGE
                                                       where _sys.ID == submit_data.id
                                                       && _sys.ACTIVE > 0 
                                                       && _sys.IS_FINISH == 1 
                                                       select _sys).FirstOrDefault();
                    if (updateSystemMessage == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    updateSystemMessage.ACTIVE = submit_data.active;
                    updateSystemMessage.MODIFY_BY = cookieData.ID.ToString().Trim();
                    updateSystemMessage.MODIFY_DATE = now;
                    if (submit_data.active == 1)
                    { 
                        if (updateSystemMessage.CLIENT_READ_FIRST_DATE == null)
                        {
                            updateSystemMessage.CLIENT_READ_FIRST_DATE = now;
                            updateSystemMessage.CLIENT_READ_LAST_DATE = now;
                        }
                        else
                        {
                            updateSystemMessage.CLIENT_READ_LAST_DATE = now;
                        }
                    }

                    #endregion
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_MESSAGE);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
