﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.SystemAccount
{
    public class WebSubmitSystemAccountController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitSystemAccount";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            if (decryptData == null)
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_FORMAT_MISSING);
            }
            WebSubmitSystemAccount submit_data = JsonConvert.DeserializeObject<WebSubmitSystemAccount[]>(decryptData)[0];
            StringBuilder sb = new StringBuilder();
            DateTime now = DateTime.Now;
            CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

            try
            {
                #region validateCookie
                if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                {
                    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                }

                string username = submit_data.username.Trim().ToUpper();
                int systemaccount_id = cookieData.ID;
                #endregion

                SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "SystemAccount",
                    this.GetType().Name,
                    (
                    "ID" + "=" + submit_data.id + "," +
                    "USERNAME" + "=" + submit_data.username + "," +
                    "EMPLOYEE_NO" + "=" + submit_data.employee_no + "," +
                    "NAME_EN" + "=" + submit_data.name_en + "," +
                    "NAME_NA" + "=" + submit_data.name_na + "," +
                    "MOBILE" + "=" + submit_data.mobile + "," +
                    "EMAIL" + "=" + submit_data.email + "," +
                    "HAS_IMAGE_URL" + "=" + submit_data.has_image_url + "," +
                    "ACTIVE" + "=" + submit_data.active
                    ),
                    submit_data.device);

                #region start query(insert) data to db
                var systemaccount = (from _acc in tmlth.TP_SYSTEM_ACCOUNT
                                     where _acc.ID != submit_data.id && _acc.ACTIVE == 1
                                     select _acc);
               
                //JArray jarr = new JArray();
                //if (systemaccount.Where(_sys => _sys.USERNAME == username).Any())
                //{
                //    jarr.Add(new JObject {
                //            { "field","Username"},
                //            { "error_detail","Username \"" + submit_data.username + "\" Is Duplicate."}
                //        });
                //}
                //if (systemaccount.Where(_sys => _sys.NAME_EN == submit_data.name_en).Any())
                //{
                //    jarr.Add(new JObject {
                //            { "field","Name EN"},
                //            { "error_detail","Name (English) \"" + submit_data.name_en + "\" Is Duplicate."}
                //        });
                //}
                //if (systemaccount.Where(_sys => _sys.NAME_NA == submit_data.name_na).Any())
                //{
                //    jarr.Add(new JObject {
                //            { "field","Name TH"},
                //            { "error_detail","Name (TH) \"" + submit_data.name_na + "\" Is Duplicate."}
                //    });
                //}
                //if (systemaccount.Where(_sys => _sys.EMAIL == submit_data.email).Any())
                //{
                //    jarr.Add(new JObject {
                //            { "field","Email"},
                //            { "error_detail","Email \"" + submit_data.email + "\" Is Duplicate."}
                //        });
                //}
                //if (systemaccount.Where(_sys => _sys.MOBILE == submit_data.mobile).Any())
                //{
                //    jarr.Add(new JObject {
                //            { "field","mobile"},
                //            { "error_detail","mobile \"" + submit_data.mobile + "\" Is Duplicate."}
                //        });
                //}
                //if (string.IsNullOrWhiteSpace(submit_data.employee_no))
                //{
                //    jarr.Add(new JObject {
                //            { "field","Employee No."},
                //            { "error_detail","Employee No Can't Empty."}
                //        });
                //}
                //if (systemaccount.Where(_sys => _sys.EMPLOYEE_NO == submit_data.employee_no).Any())
                //{
                //    jarr.Add(new JObject {
                //            { "field","Employee No."},
                //            { "error_detail","Employee No \"" + submit_data.employee_no + "\" Is Duplicate."}
                //        });
                //}
                //if (jarr.Any())
                //{
                //    return DataGenerator.generateTextResult(ResultData.CODE_DUPLICATE + jarr);
                //}                

                if (submit_data.id == 0)
                {

                    string duptext = "";
                    if (systemaccount.Where(_sys => _sys.USERNAME == username).Any())
                    {
                        duptext = " "+submit_data.username.ToString();      
                        return DataGenerator.generateTextResult(ResultData.CODE_DUPLICATE + duptext);
                    }

                   

                    string enc_tmlthpw = EncryptDecryptAlgorithm.EncryptData(submit_data.password, DataFactory.ENC_PASSWORD_KEY);
                    TP_SYSTEM_ACCOUNT newaccount = new TP_SYSTEM_ACCOUNT()
                    {
                        SYSTEM_ACCOUNT_ROLE_ID = submit_data.role_id,
                        USERNAME = submit_data.username.ToUpper().Trim(),
                        PASSWORD = enc_tmlthpw,
                        EMPLOYEE_NO = submit_data.employee_no,
                        NAME_EN = submit_data.name_en,
                        NAME_NA = submit_data.name_na,
                        MOBILE = submit_data.mobile,
                        EMAIL = submit_data.email,
                        HAS_IMAGE_URL= submit_data.has_image_url,
                        WRONG_COUNT = 0,
                        WRONG_DATE = now,
                        OTP = "",
                        OTP_COUNT = 0,
                        OTP_DATE = now,
                        ACTIVE = 1,
                        CREATE_BY = cookieData.ID,
                        CREATE_DATE = now,
                    };
                    tmlth.TP_SYSTEM_ACCOUNT.AddObject(newaccount);
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + newaccount.ID, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                else
                {                  
                    var updateContent = (from _content in tmlth.TP_SYSTEM_ACCOUNT
                                         where _content.ID == submit_data.id && _content.ACTIVE == 1
                                         select _content).FirstOrDefault();
                    if (updateContent == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }

                    updateContent.MODIFY_BY = systemaccount_id;
                    updateContent.MODIFY_DATE = now;
                    if (submit_data.active == 1)
                    {
                        updateContent.SYSTEM_ACCOUNT_ROLE_ID = submit_data.role_id;
                        // updateEMPLOYEE.USERNAME = submit_data.una;
                        //updateEMPLOYEE.PASSWORD = submit_data.pass; //No up pass 01 31 2019
                        updateContent.EMPLOYEE_NO = submit_data.employee_no;
                        updateContent.NAME_EN = submit_data.name_en;
                        updateContent.NAME_NA = submit_data.name_na;
                        updateContent.MOBILE = submit_data.mobile;
                        updateContent.EMAIL = submit_data.email;
                        updateContent.HAS_IMAGE_URL = submit_data.has_image_url;
                    }
                    else
                    {
                        updateContent.ACTIVE = submit_data.active;
                    }
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                #endregion
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }
}
