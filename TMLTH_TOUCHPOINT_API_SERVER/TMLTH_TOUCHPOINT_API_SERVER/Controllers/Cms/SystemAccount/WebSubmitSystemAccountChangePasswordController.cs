﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.SystemAccount
{
    public class WebSubmitSystemAccountChangePasswordController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitSystemAccountChangePassword";
            if (ModelState.IsValid)
            {
                //*** validate data
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                SubmitSystemAccountChangePasswordData submit_data = JsonConvert.DeserializeObject<SubmitSystemAccountChangePasswordData[]>(decryptData)[0];
                DateTime now = DateTime.Now;
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

                //*** workspace
                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                    "SystemAccount",
                    this.GetType().Name,
                    (
                        "ID" + "=" + submit_data.id
                    ),
                    submit_data.device);

                    //*** set account detail
                    int system_account_id = cookieData.ID;
                    int system_account_role_id = cookieData.ROLE_ID;
                   
                    //*** validate permission
                    //if (system_account_role_id == DataFactory.ROLE_MKT_STAFF && system_account_id != submit_data.id)
                    //{
                    //    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    //            ResultData.CODE_ERROR_NO_PERMISSION, DataFactory.ENC_TOUCHPOINT_KEY));
                    //}
                    //#endregion

                    #region validate data
                    //*** Validate Username & Password
                    if (string.IsNullOrWhiteSpace(submit_data.new_ppw))
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + "New Password Is Empty.");
                    }

                    string enc_new_ppw = EncryptDecryptAlgorithm.EncryptData(submit_data.new_ppw, DataFactory.ENC_PASSWORD_KEY);
                    var updateSystemAccountPassword = (from _sys in tmlth.TP_SYSTEM_ACCOUNT
                                                       where _sys.ID == submit_data.id &&
                                                             _sys.ACTIVE > 0
                                                       select _sys).FirstOrDefault();
                    if (updateSystemAccountPassword == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }

                    #endregion

                    updateSystemAccountPassword.PASSWORD = enc_new_ppw;
                    updateSystemAccountPassword.MODIFY_BY = system_account_id;
                    updateSystemAccountPassword.MODIFY_DATE = now;
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else 
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }

    }
}
