﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Template
{
    public class WebRequestAppTemplateDataTableController : ApiController
    {

        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestAppTemplateDataTable";
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                EncryptTableData request_data = JsonConvert.DeserializeObject<EncryptTableData[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }

                    #endregion


                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                        "AppTemplate",
                        this.GetType().Name,
                        (
                        ""
                        ), 
                        request_data.device);
                    #region start query(fetch) data from db
                    //*** start query(fetch) as list
                    int iorder = request_data.sort;
                    int istart = request_data.start;
                    int ilen = request_data.len;
                    string searchString = HttpUtility.UrlDecode(request_data.search.Trim());
                    int sEcho = request_data.sEcho;

                    //*** start query(fetch all as list) from sqlserver
                    var QCDTR = (from _page in tmlth.TP_REF_TEMPLATE_APPLICATION_PAGE
                                 join _img in tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE on _page.ID equals _img.TEMPLATE_APPLICATION_PAGE_ID
                                 let _cbn = (from _cb in tmlth.TP_SYSTEM_ACCOUNT
                                             where _cb.ID == _page.CREATE_BY
                                             select _cb.NAME_NA).FirstOrDefault()
                                 let _mbn = (from _mb in tmlth.TP_SYSTEM_ACCOUNT
                                             where _mb.ID == _page.MODIFY_BY
                                             select _mb.NAME_NA).FirstOrDefault()
                                 where _page.ACTIVE > 0 && _img.ACTIVE > 0
                                 select new
                                 {
                                     _page.ID,
                                     _page.NAME,
                                     _page.DESCRIPTION,
                                     _img.IMAGE_URL,
                                     CREATENAME = _cbn,
                                     _page.CREATE_DATE,
                                     MODIFYNAME = _mbn,
                                     _page.MODIFY_DATE,
                                 });

                    string[] searchStrings = searchString.Split(new[] { "&amp;", "search=1" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var search_data in searchStrings)
                    {
                        try
                        {
                            string[] search = search_data.Split(new[] { '=' }, 2);
                            if (search.Length % 2 != 0) { continue; }
                            string field = search[0];
                            string valueRaw = search[1].Trim();

                            //*** name,mobile,email
                            if (field.Equals("txt"))
                            {
                                QCDTR = QCDTR.Where(x => x.NAME.Contains(valueRaw));
                            }
                        }
                        catch { continue; }
                    }

                    switch (iorder)
                    {
                        case 0: QCDTR = QCDTR.OrderBy(x => x.ID); break;
                        case 1: QCDTR = QCDTR.OrderBy(x => x.NAME); break;
                        case 2: QCDTR = QCDTR.OrderBy(x => x.DESCRIPTION); break;
                        case 3: QCDTR = QCDTR.OrderBy(x => x.MODIFYNAME); break;
                        case 4: QCDTR = QCDTR.OrderBy(x => x.MODIFY_DATE); break;

                        case 100: QCDTR = QCDTR.OrderByDescending(x => x.ID); break;
                        case 101: QCDTR = QCDTR.OrderByDescending(x => x.NAME); break;
                        case 102: QCDTR = QCDTR.OrderByDescending(x => x.DESCRIPTION); break;
                        case 103: QCDTR = QCDTR.OrderByDescending(x => x.MODIFYNAME); break;
                        case 104: QCDTR = QCDTR.OrderByDescending(x => x.MODIFY_DATE); break;

                        default: QCDTR = QCDTR.OrderBy(x => x.ID); break;
                    }
                    int totalDisplayRecord = QCDTR.Count();
                    QCDTR = QCDTR.Skip(istart).Take(ilen);
                    var QCDR = (from _search in QCDTR
                                select new WebResponseAppTemplateDataTable
                                {
                                    id = _search.ID,
                                    name = _search.NAME,
                                    description = _search.DESCRIPTION,
                                    create_by_name = _search.CREATENAME,
                                    create_date_raw = _search.CREATE_DATE,
                                    modify_by_name = _search.MODIFYNAME == null ? "-" : _search.MODIFYNAME,
                                    modify_date_raw = _search.MODIFY_DATE,
                                    image_url = _search.IMAGE_URL
                                });
                    #endregion
                    sb.Append("{");
                    sb.Append("\"sEcho\":");
                    sb.Append(sEcho);
                    

                    sb.Append(",\"iTotalRecords\":");
                    sb.Append(QCDR.Count());

                    sb.Append(",\"iTotalDisplayRecords\":");
                    sb.Append(totalDisplayRecord);

                    //sb.Append("\"token\":");
                    //sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));

                    sb.Append(",\"data\":");
                    sb.Append(JsonConvert.SerializeObject(QCDR));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
