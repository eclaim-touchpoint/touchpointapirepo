﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.AppTemplate
{
    public class WebSubmitAppTemplateImageController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitAppTemplateImage";
            string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
            if (decryptData == null)
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT + ResultData.CODE_ERROR_FORMAT_MISSING);
            }
            WebSubmitTemplateAppImage submit_data = JsonConvert.DeserializeObject<WebSubmitTemplateAppImage[]>(decryptData)[0];
            StringBuilder sb = new StringBuilder();
            DateTime now = DateTime.Now;
            CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

            try
            {
                #region validateCookie
                if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                {
                    return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                }
                #endregion
                SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                    "AppTemplate",
                    this.GetType().Name,
                    (
                    "ID" + "=" + submit_data.id + "," +
                    "TEMPLATEAPPPAGEID" + "=" + submit_data.templateapppageid + "," +
                    "IMAGEURL" + "=" + submit_data.imageurl + "," +
                    "ACTIVE" + "=" + submit_data.active
                    )
                    ,
                    submit_data.device);

                #region start query(insert) data to db
                if (submit_data.active == 0)
                {
                    var updateimage = (from _upimg in tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE
                                       where _upimg.ID == submit_data.id && _upimg.ACTIVE > 1
                                       select _upimg).FirstOrDefault();
                    if (updateimage == null)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    updateimage.ACTIVE = submit_data.active;
                 
                    updateimage.MODIFY_BY = cookieData.ID;
                    updateimage.MODIFY_DATE = now;
                    updateimage.END_DATE = now;
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
               
                    var checkrefpageid = (from _check in tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE
                                          where _check.TEMPLATE_APPLICATION_PAGE_ID == submit_data.templateapppageid && _check.ACTIVE >0
                                          select _check);
                    if (checkrefpageid.Any())
                    {
                        foreach (var check in checkrefpageid)
                        {
                            check.ACTIVE = 0;
                            check.END_DATE = now;
                            check.MODIFY_DATE = now;
                            check.MODIFY_BY = cookieData.ID;
                        }
                    }
                    var appimg = (from _img in tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE
                                  where _img.ID != submit_data.id && _img.ACTIVE >0
                                  select _img);
                    TP_REF_TEMPLATE_APPLICATION_IMAGE newimage = new TP_REF_TEMPLATE_APPLICATION_IMAGE()
                    {
                      TEMPLATE_APPLICATION_PAGE_ID = submit_data.templateapppageid,
                      IMAGE_URL = submit_data.imageurl,
                      START_DATE = now,
                      ACTIVE = submit_data.active,
                     
                      CREATE_BY = cookieData.ID,
                      CREATE_DATE =now
                    };
                    tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE.AddObject(newimage);
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + newimage.ID, DataFactory.ENC_TOUCHPOINT_KEY));
                
                
                #endregion
            }
            catch (Exception e)
            {
                var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
            }
        }
    }
}
