﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Template
{
    public class WebSubmitAppTemplateController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitAppTemplate";
            if (ModelState.IsValid)
            {
                //var decryptData = data;
               string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebSubmitAppTemplate submit_data = JsonConvert.DeserializeObject<WebSubmitAppTemplate[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                    }
                    #endregion

                    DateTime now = DateTime.Now;
                    int system_account_id = cookieData.ID;
                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? system_account_id : (int?)null,
                        "AppTemplate",
                        this.GetType().Name,
                        "ID = " + submit_data.id + "," + 
                        "LIST_APP_TEMPLATE =" + submit_data.list_app_template 
                        ,
                        submit_data.device);
                    var anyDuplicate = submit_data.list_app_template.GroupBy(x => x.position_no).Any(g => g.Count() > 1);
                    if (anyDuplicate)
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                              ResultData.CODE_DUPLICATE);
                    }
                    foreach (var x in submit_data.list_app_template)
                    { 
                        var updateTemplate = (from _uptemp in tmlth.TP_TEMPLATE_APPLICATION
                                              where _uptemp.ID == x.id && 
                                                         _uptemp.TEMPLATE_APPLICATION_PAGE_ID == submit_data.id &&
                                                         _uptemp.ACTIVE >0
                                                   select _uptemp).FirstOrDefault();
                        if (updateTemplate == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }
                        if (updateTemplate.ACTIVE > 0)
                        {
                            // *****************Edit name menu**********************
                            if (x.name != null && x.description != null ) {
                                updateTemplate.DESCRIPTION = x.description;
                                updateTemplate.NAME = x.name;
                                updateTemplate.MODIFY_BY = system_account_id;
                                updateTemplate.MODIFY_DATE = now;
                            }
                            //*****************************************************
                            if (updateTemplate.POSITION_NO != x.position_no)
                            {
                                updateTemplate.POSITION_NO = x.position_no;
                                updateTemplate.SIZE_TOTAL = x.size_total;
                                updateTemplate.MODIFY_BY = system_account_id;
                                updateTemplate.MODIFY_DATE = now;
                            }
                           
                            updateTemplate.ACTIVE = x.active;
                         
                        }                     
                    }
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }

        }
    }

}
