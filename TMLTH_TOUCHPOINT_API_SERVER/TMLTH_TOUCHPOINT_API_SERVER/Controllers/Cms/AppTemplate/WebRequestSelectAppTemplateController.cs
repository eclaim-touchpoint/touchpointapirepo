﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.AppTemplate
{
    public class WebRequestSelectAppTemplateController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestSelectAppTemplate";
            if(ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestSelectTemplateApp request_data = JsonConvert.DeserializeObject<WebRequestSelectTemplateApp[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);

                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                        "AppTemplate",
                        this.GetType().Name,
                        (
                        ""
                        ),
                        request_data.device);


                    var selecttem = (from _tem in tmlth.TP_REF_TEMPLATE_APPLICATION_PAGE
                                     where _tem.ACTIVE > 0
                                     select new WebResponseSelectAppTemplate
                                     {
                                         id = _tem.ID,
                                         name = _tem.NAME,
                                         description = _tem.DESCRIPTION
                                     });

                    var selecimg = (from _img in tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE
                                             where _img.ACTIVE > 0
                                             select new WebResponseTemplateAppImage
                                             {
                                                 id = _img.ID,
                                                 image_url = _img.IMAGE_URL
                                             });
                   
                    sb.Append("{");
                    sb.Append("\"selecttemplate\":");
                    sb.Append(JsonConvert.SerializeObject(selecttem));
                    sb.Append(",\"selecttimage\":");
                    sb.Append(JsonConvert.SerializeObject(selecimg));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));

                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" +  cookieData.ID  + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
