﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Template
{
    public class WebRequestAppTemplateController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestAppTemplate";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                WebRequestTemplateApp request_data = JsonConvert.DeserializeObject<WebRequestTemplateApp[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = new CookieResult();

                if (request_data.ischeckcookie == 1)
                {
                    cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);
                }
                try
                {

                    #region validateCookie
                    if (request_data.ischeckcookie == 1)
                    {
                        if (cookieData.IS_APP == 0)
                        {
                            if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                            {
                                return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                            }
                        }
                    }
                    #endregion
                    SubmitLog.ACTION_LOG(tmlth,
                        cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                        cookieData.IS_APP == 0 ? request_data.ischeckcookie == 1 ? cookieData.ID : (int?)null : (int?)null, 
                        "AppTemplate",
                        this.GetType().Name,
                        (
                            "ID" + "=" + request_data.id + "," +
                            "TEMPLATEAPPPAGEID" + "=" + request_data.templateapppageid + "," +
                            "ISCHECKCOOKIE" + "=" + request_data.ischeckcookie 
                        ), 
                        request_data.device);
                    var templateapppage = (from _apppage in tmlth.TP_TEMPLATE_APPLICATION
                                            let _cbn = (from _cb in tmlth.TP_SYSTEM_ACCOUNT
                                                        where _cb.ID == _apppage.CREATE_BY
                                                        select _cb.NAME_NA).FirstOrDefault()
                                            let _mbn = (from _mb in tmlth.TP_SYSTEM_ACCOUNT
                                                        where _mb.ID == _apppage.MODIFY_BY
                                                        select _mb.NAME_NA).FirstOrDefault()
                                            where _apppage.ACTIVE > 0 &&
                                            _apppage.TEMPLATE_APPLICATION_PAGE_ID == request_data.id

                                            orderby _apppage.POSITION_NO
                                            select new WebResponseTemplateApp
                                            {
                                                id = _apppage.ID,
                                                template_app_page_id = _apppage.TEMPLATE_APPLICATION_PAGE_ID,
                                                name = _apppage.NAME,
                                                position_no = _apppage.POSITION_NO,
                                                size_total = _apppage.SIZE_TOTAL,
                                                description = _apppage.DESCRIPTION,
                                                create_by_name = _cbn,
                                                create_date_raw = _apppage.CREATE_DATE,
                                                modify_by_name = _mbn == null ? "-" : _mbn,
                                                modify_date_raw = _apppage.MODIFY_DATE,
                                                active = _apppage.ACTIVE
                                            });

                    if (request_data.ischeckcookie == 0 || request_data.ischeckcookie == 1)
                    {
                        templateapppage = templateapppage.Where(x => x.active == 1);
                    }

                    if (!templateapppage.Any())
                    {
                        return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                            ResultData.CODE_ERROR_DATA_NOT_FOUND);
                    }
                    sb.Append("{");
                    sb.Append("\"app_template\":");
                    sb.Append(JsonConvert.SerializeObject(templateapppage));

                    var templateapppageimg = (from _img in tmlth.TP_REF_TEMPLATE_APPLICATION_IMAGE
                                                where _img.ACTIVE == 1 && _img.TEMPLATE_APPLICATION_PAGE_ID == request_data.id
                                                select new WebResponseTemplateAppImage
                                                {
                                                    id = _img.ID,
                                                    image_url = _img.IMAGE_URL
                                                }).FirstOrDefault();

                    sb.Append(",\"app_template_img_url\":");
                    sb.Append(JsonConvert.SerializeObject(templateapppageimg));
                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));

                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}