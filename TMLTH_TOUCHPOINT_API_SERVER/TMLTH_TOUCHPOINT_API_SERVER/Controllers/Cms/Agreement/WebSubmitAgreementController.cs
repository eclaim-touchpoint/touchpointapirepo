﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.Agreement
{
    public class WebSubmitAgreementController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebSubmitAgreement";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebSubmitAgreement submit_data = JsonConvert.DeserializeObject<WebSubmitAgreement[]>(decryptData)[0];
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(submit_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), submit_data.ip_client);
                StringBuilder sb = new StringBuilder();
                DateTime now = DateTime.Now;

                try
                {
                    #region validateCookie
                    if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                    {
                        return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                    }

                    int systemaccount_id = cookieData.ID;
                    #endregion
                     SubmitLog.ACTION_LOG(tmlth,
                     cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                     cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                     "Agreement",
                     this.GetType().Name,
                     (
                     "ID" + "=" + submit_data.id + "," +
                     "DETAIL" + "=" + submit_data.detail + "," +
                     "START_DATE" + "=" + submit_data.start_date + "," +
                     "END_DATE" + "=" + submit_data.end_date
                     ),
                     submit_data.device);


                    if(submit_data.id == 0)
                    {

                        decimal version = (decimal)0.00;
                        var agreement = (from _ver in tmlth.TP_AGREEMENT
                                         orderby _ver.VERSION_NO descending
                                         select _ver).FirstOrDefault();

                        if(agreement != null)
                        {
                            version = agreement.VERSION_NO + 1;
                        }
                      
                        TP_AGREEMENT newagreement = new TP_AGREEMENT()
                        {
                            VERSION_NO = version,
                            DETAIL = submit_data.detail,
                            ACTIVE = submit_data.active,
                            CREATE_BY = cookieData.ID,
                            CREATE_DATE = now,
                        };
                        tmlth.TP_AGREEMENT.AddObject(newagreement);
                        tmlth.SaveChanges();
                        return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_RESULTID + newagreement.ID, DataFactory.ENC_TOUCHPOINT_KEY));
                    }
                    else 
                    {
                        var updateagreementdraft = (from _ver in tmlth.TP_AGREEMENT
                                                    where _ver.ID == submit_data.id
                                                    select _ver).FirstOrDefault();
                        if (updateagreementdraft == null)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                                ResultData.CODE_ERROR_DATA_NOT_FOUND);
                        }

                        if(updateagreementdraft.ACTIVE == 0 || updateagreementdraft.ACTIVE == 1)
                        {
                            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR +
                                ResultData.CODE_ERROR_DATA_INCOMPATIBLE);
                        }

                        if (submit_data.active == 1)
                        {
                            var agreementpublic = (from _ver in tmlth.TP_AGREEMENT
                                                   where _ver.ACTIVE == 1
                                                   select _ver).FirstOrDefault();
                            updateagreementdraft.START_DATE = now;
                            if (agreementpublic != null)
                            {
                                agreementpublic.ACTIVE = 0;
                                agreementpublic.END_DATE = now;
                                agreementpublic.MODIFY_BY = cookieData.ID;
                                agreementpublic.MODIFY_DATE = now;
                                updateagreementdraft.VERSION_NO = agreementpublic.VERSION_NO + (decimal)1.00;
                            } else
                            {
                                updateagreementdraft.VERSION_NO = (decimal)1.00;
                            }
                        }
                        updateagreementdraft.ACTIVE = submit_data.active;
                       
                        updateagreementdraft.DETAIL = submit_data.detail;
                        updateagreementdraft.MODIFY_BY = cookieData.ID;
                        updateagreementdraft.MODIFY_DATE = now;
                    }
                    tmlth.SaveChanges();
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(ResultData.CODE_SUCCESS, DataFactory.ENC_TOUCHPOINT_KEY));

                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }

            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
