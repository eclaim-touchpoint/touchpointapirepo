﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.Agreement
{
    public class WebRequestAgreementController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestAgreement";
            if (ModelState.IsValid)
            {
                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }
                WebRequestAgreement request_data = JsonConvert.DeserializeObject<WebRequestAgreement[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);
                try
                {
                    #region validate
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }
                    #endregion
                    SubmitLog.ACTION_LOG(tmlth,
                                            cookieData.IS_APP == 1 && cookieData.IS_BLANK == 0 ? cookieData.ID.ToString() : null,
                                            cookieData.IS_APP == 0 && cookieData.IS_BLANK == 0 ? cookieData.ID : (int?)null,
                                            "Agreement",
                                            this.GetType().Name,
                                            (
                                               ""
                                            ),
                                            request_data.device);
                    int system_account_id = cookieData.ID;
                    sb.Append("{");
                    var agreementpublic = (from _agree in tmlth.TP_AGREEMENT
                                            where cookieData.IS_APP == 1 ? (_agree.ACTIVE == 1) : (request_data.id == 0 ? _agree.ACTIVE > 0 :
                                                _agree.ID == request_data.id)
                                            orderby _agree.ID descending
                                            select new { 
                                                id = _agree.ID,
                                                version_no = _agree.VERSION_NO,
                                                detail = _agree.DETAIL,
                                                start_date = _agree.START_DATE,
                                                active = _agree.ACTIVE
                                            }).FirstOrDefault();
                       
                    sb.Append("\"agreement\":");
                    sb.Append(JsonConvert.SerializeObject(agreementpublic));

                    var has_draft = (from _agree in tmlth.TP_AGREEMENT
                                    where _agree.ACTIVE == 2 && _agree.ID != request_data.id
                                    select _agree);

                    sb.Append(",\"has_draft\":");
                    sb.Append(JsonConvert.SerializeObject(has_draft.Any() ? 1 : 0));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }
            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }
    }
}
