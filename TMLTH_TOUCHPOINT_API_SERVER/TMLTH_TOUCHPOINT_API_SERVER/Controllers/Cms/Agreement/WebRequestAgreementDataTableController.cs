﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using TMLTH_TOUCHPOINT_API_SERVER.Class;
using TMLTH_TOUCHPOINT_API_SERVER.Class.Object;
using TMLTH_TOUCHPOINT_API_SERVER.Models;
using TMLTH_TOUCHPOINT_CMS.Class;

namespace TMLTH_TOUCHPOINT_API_SERVER.Controllers.Cms.Agreement
{
    public class WebRequestAgreementDataTableController : ApiController
    {
        webapp_mobileEntities tmlth = new webapp_mobileEntities();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                ResultData.CODE_ERROR_GET_METHOD);
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string data)
        {
            string page = "WebRequestAgreementDataTable";
            if (ModelState.IsValid)
            {

                string decryptData = EncryptDecryptAlgorithm.DecryptData(data, DataFactory.ENC_TOUCHPOINT_KEY);
                if (decryptData == null)
                {
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_SIGNIN +
                        ResultData.CODE_ERROR_FORMAT_MISSING);
                }

                EncryptTableData request_data = JsonConvert.DeserializeObject<EncryptTableData[]>(decryptData)[0];
                StringBuilder sb = new StringBuilder();
                CookieResult cookieData = DataValidation.validateSystemAccountCookie(EncryptDecryptAlgorithm.DecryptData(request_data.admin, DataFactory.ENC_SECRET_SIGNIN_KEY), request_data.ip_client);
                try
                {
                    #region validateCookie
                    if (cookieData.IS_APP == 0)
                    {
                        if (cookieData.RESULT != ResultData.CODE_COOKIE_RESULT_ISVALID)
                        {
                            return DataGenerator.generateTextResult(DataValidation.GetCookieErrorMesseage(cookieData.RESULT));
                        }
                    }

                    #endregion

                    SubmitLog.ACTION_LOG(tmlth,
                    cookieData.IS_APP == 1 ? cookieData.ID.ToString() : null,
                    cookieData.IS_APP == 0 ? cookieData.ID : (int?)null,
                    "Agreement",
                    this.GetType().Name,
                    (
                    ""
                    ),
                    request_data.device);

                    int iorder = request_data.sort;
                    int istart = request_data.start;
                    int ilen = request_data.len;
                    string searchString = HttpUtility.UrlDecode(request_data.search.Trim());
                    int sEcho = request_data.sEcho;
                    var QCDTR = ((IEnumerable<TP_AGREEMENT>)tmlth.TP_AGREEMENT)
                                 .Where(x => x.ACTIVE <= 1)
                                 .Select((r, i) => new
                                 {
                                     no = i + 1,
                                     id = r.ID,
                                     version = r.VERSION_NO,
                                     detail = r.DETAIL,
                                     active = r.ACTIVE,
                                     start_date = r.START_DATE,
                                     end_date = r.END_DATE,
                                 });
                                 
                    string[] searchStrings = searchString.Split(new[] { "&amp;", "search=1" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var search_data in searchStrings)
                    {
                        try
                        {
                            string[] search = search_data.Split(new[] { '=' }, 2);
                            if (search.Length % 2 != 0) { continue; }
                            string field = search[0];
                            string valueRaw = search[1].Trim();

                            //*** name,mobile,email
                            if (field.Equals("txt"))
                            {
                                int value = int.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.version == value);
                            } else if (field.Equals("version"))
                            {
                                int value = int.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.version == value);
                            } else if(field.Equals("status"))
                            {
                                int value = int.Parse(valueRaw);
                                QCDTR = QCDTR.Where(x => x.active == value);
                            } else if(field.Equals("st_from"))
                            {
                                DateTime date = DateTime.ParseExact(valueRaw, "dd MMM yyyy", CultureInfo.InvariantCulture);
                                QCDTR = QCDTR.Where(x => x.start_date >= date);
                            } else if (field.Equals("st_to"))
                            {
                                DateTime date = DateTime.ParseExact(valueRaw, "dd MMM yyyy", CultureInfo.InvariantCulture).AddDays(1);
                                QCDTR = QCDTR.Where(x => x.start_date <= date);
                            } else if (field.Equals("en_from"))
                            {
                                DateTime date = DateTime.ParseExact(valueRaw, "dd MMM yyyy", CultureInfo.InvariantCulture);
                                QCDTR = QCDTR.Where(x => x.end_date >= date);
                            } else if (field.Equals("en_to"))
                            {
                                DateTime date = DateTime.ParseExact(valueRaw, "dd MMM yyyy", CultureInfo.InvariantCulture).AddDays(1);
                                QCDTR = QCDTR.Where(x => x.end_date <= date);
                            }
                        }
                        catch { continue; }
                    }
                    switch (iorder)
                    {
                        case 0: QCDTR = QCDTR.OrderBy(x => x.id); break;
                        case 1: QCDTR = QCDTR.OrderBy(x => x.version); break;
                        case 2: QCDTR = QCDTR.OrderBy(x => x.start_date); break;
                        case 3: QCDTR = QCDTR.OrderBy(x => x.end_date); break;


                        case 100: QCDTR = QCDTR.OrderByDescending(x => x.id); break;
                        case 101: QCDTR = QCDTR.OrderByDescending(x => x.version); break;
                        case 102: QCDTR = QCDTR.OrderByDescending(x => x.start_date); break;
                        case 103: QCDTR = QCDTR.OrderByDescending(x => x.end_date); break;

                    }
                    int totalDisplayRecord = QCDTR.Count();
                    QCDTR = QCDTR.Skip(istart).Take(ilen);
                    var QCDR = (from _search in QCDTR
                                select new WebRequestAgreementDataTable
                                {
                                    no = _search.no,
                                    id = _search.id,
                                    version = _search.version,
                                    start_date_raw = _search.start_date,
                                    end_date_raw = _search.end_date,
                                    active = _search.active
                                });

                    sb.Append("{");
                    sb.Append("\"sEcho\":");
                    sb.Append(sEcho);


                    sb.Append(",\"iTotalRecords\":");
                    sb.Append(QCDR.Count());

                    sb.Append(",\"iTotalDisplayRecords\":");
                    sb.Append(totalDisplayRecord);

                    //sb.Append("\"token\":");
                    //sb.Append(JsonConvert.SerializeObject(cookieData.NEW_TOKEN));

                    sb.Append(",\"data\":");
                    sb.Append(JsonConvert.SerializeObject(QCDR));

                    sb.Append("}");
                    return DataGenerator.generateTextResult(EncryptDecryptAlgorithm.EncryptData(sb.ToString(), DataFactory.ENC_TOUCHPOINT_KEY));
                }
                catch (Exception e)
                {
                    var errorlog = ContentTools.ErrorControllerLog(page + "||" + cookieData.ID + "||" + e);
                    return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_ACCOUNT);
                }
            }

            else
            {
                return DataGenerator.generateTextResult(ResultData.CODE_ERROR + ResultData.CODE_ERROR_TOUCHPOINT +
                    ResultData.CODE_ERROR_FORMAT_MISSING);
            }
        }

    }
}
